﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Employee.aspx.vb" Inherits="Admin_A_HTML_HTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Employee Info</h1>
    <div class="btn-toolbar">

        <a href="<%= domainName %>AllEmployee" data-toggle="modal" class="btn btn-primary"><i class="icon-back"></i>Back</a>
        <div class="btn-group">
        </div>
    </div>

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Employee"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>


    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <div class="left_div">
                <label>
                    Employee Name  :</label>
            </div>
            <div class="right_div">
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="form"
                    ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>
            <asp:Panel ID="Panel2" runat="server">
                <div class="left_div">
                    <label>
                        Employee  ID :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtCode" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtCode" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
            <div class="left_div">
                <label>
                    Department  :</label>
            </div>
            <div class="right_div">
                <asp:DropDownList ID="ddlDept" runat="server" DataSourceID="sdsDept" CssClass="input-xlarge" AppendDataBoundItems="True"
                    DataTextField="Department" DataValueField="DeptID" AutoPostBack="True">
                    <asp:ListItem Value="">Please Select Department</asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsDept" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT DISTINCT [DeptID], [Department] FROM [Department] WHERE ([Status] = @Status)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ValidationGroup="form"
                    ControlToValidate="ddlDept" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>
            
            <div class="left_div">
                <label>
                    Designation  :</label>
            </div>
            <div class="right_div">
                <asp:DropDownList ID="ddlDesig" runat="server" DataSourceID="sdsDesig" CssClass="input-xlarge" 
                    DataTextField="Designation" DataValueField="DesigID">
                    
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsDesig" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT DISTINCT DesigID, Designation FROM Designations WHERE (Status = @Status) AND (DeptID = @DeptID)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        <asp:ControlParameter ControlID="ddlDept" Name="DeptID" Type="Int32" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" ValidationGroup="form"
                    ControlToValidate="ddlDesig" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            <div>
                <div class="left_div">
                </div>
                <div class="right_div">
                    <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />
                    <label class="red">
                    </label>
                </div>
            </div>

        </div>


        <div class="btn-toolbar">
            <div class="right_div">
                <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            </div>

            <div class="btn-group">
            </div>
        </div>




    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [Employees] ([EmployeeName], [DepartmentID], [EmployeeCode], [Designation], [Status]) VALUES (@EmployeeName,@DepartmentID, @EmployeeCode, @Designation, @Status)"
        UpdateCommand="UPDATE [Employees] SET [EmployeeName] = @EmployeeName, [EmployeeCode]=@EmployeeCode,  [DepartmentID]=@DepartmentID, [Designation]=@Designation, [Status] = @Status WHERE [UID] = @UID">
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="EmployeeName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" DefaultValue="True" Name="Status" PropertyName="Checked" Type="Boolean" />
            <asp:ControlParameter ControlID="ddlDept" Name="DepartmentID" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="txtCode" Name="EmployeeCode" PropertyName="Text" />
            <asp:ControlParameter ControlID="ddlDesig" Name="Designation" PropertyName="SelectedValue" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="EmployeeName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
            <asp:ControlParameter ControlID="ddlDept" Name="DepartmentID" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="txtCode" Name="EmployeeCode" PropertyName="Text" />
            <asp:ControlParameter ControlID="ddlDesig" Name="Designation" PropertyName="SelectedValue" />
            <asp:RouteParameter Name="UID" RouteKey="uid" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

