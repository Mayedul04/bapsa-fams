﻿
Imports System.Data.SqlClient

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page
    Public permissions As List(Of Integer) = New List(Of Integer)

    Protected Sub btnSubmit_ServerClick(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If (hdnID.Value <> "") Then
            If (permissions.Contains(2)) Then

                If sdsUser.Update() > 0 Then
                    divSuccess.Visible = True
                    ' GridView1.DataBind()
                    ClearField()
                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Alter"
                divError.Visible = True
            End If
        Else
            If (permissions.Contains(1)) Then

                If sdsUser.Insert() > 0 Then
                    divSuccess.Visible = True
                    ' GridView1.DataBind()
                    ClearField()
                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Add Item"
                divError.Visible = True
        End If
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (divDelete.Visible = True) Then
                divDelete.Visible = False
            End If
        End If
        getPermission()
    End Sub
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 5 ' 5 is the Section ID for Config
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT   UID, EmployeeName, Status, Designation, DepartmentID, EmployeeCode  FROM   Employees where UID=@UID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("UID", Data.SqlDbType.Int)
        cmd.Parameters("UID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("UID").ToString()
            txtTitle.Text = reader("EmployeeName").ToString()
            txtCode.Text = reader("EmployeeCode").ToString()

            ddlDept.SelectedValue = reader("DepartmentID").ToString()
            ddlDesig.DataBind()
            ddlDesig.SelectedValue = reader("Designation").ToString()

            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub
    Private Sub DeleteRecordByID(id As Integer)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim usercount As Integer = 0
        Dim assetcount As Integer = 0

        Dim selectString1 = "SELECT  Count(ItemID)  FROM   Assets where AssetUser=@AssetUser "
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmd1.Parameters.Add("AssetUser", Data.SqlDbType.Int)
        cmd1.Parameters("AssetUser").Value = id
        assetcount = cmd1.ExecuteScalar()

        If (assetcount > 0) Then
            divDelete.Visible = True
        Else
            If (usercount > 0) Then
                sdsUser.Update()
            End If
            If sdsUser.Delete() > 0 Then
                divSuccess.Visible = True
                hdnID.Value = ""
                '  GridView1.DataBind()
            Else
                divError.Visible = True
            End If

        End If
        conn.Close()
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (permissions.Contains(3)) Then

            If (hdnID.Value <> 0) Then
                DeleteRecordByID(hdnID.Value)
                ClearField()
            Else
                lblErrMessage.Text = "Please select a item to Delete"
                divError.Visible = True
            End If
        Else
            lblErrMessage.Text = "You do not have Permission to Deletes"
            divError.Visible = True
        End If
    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        ' ddlDept.SelectedIndex = 0
        txtCode.Text = ""
        'ddlDesig.SelectedIndex = 0
        hdnID.Value = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub
    Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
        If (e.CommandName = "Edit") Then

            Dim categoryID As Integer = Integer.Parse(e.CommandArgument)
            LoadContent(categoryID)

        End If
    End Sub
    Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
        If (txtAutoSearch.Text.Contains(":")) Then
            Dim split = txtAutoSearch.Text.Split(":")
            Dim name1 As String = split(0)
            Dim id As Integer = split(1)
            hdnID.Value = id
            LoadContent(hdnID.Value)
        Else
            lblErrMessage.Text = "Please select a Valid item from Suggested List"
            divError.Visible = True
        End If

    End Sub
End Class
