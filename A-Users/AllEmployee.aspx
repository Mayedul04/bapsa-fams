﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllEmployee.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="span5" style="margin-left:0px; margin-top:5px;">
        <div class="btn-group">
             <script type="text/javascript" language="javascript">
                                    function OnContactSelected(source, eventArgs) {
                                        var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                                        document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();
                                        
                                    } 
                                </script>
         <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divDelete" visible="false" runat="server">
            <asp:Label ID="Label1" runat="server" Text="Asset(s) has been occupied by this user. Please Delete the Depedencies first"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        </div>
       
        <div class="span12 form-top-strip">
           <p>Employee Form</p> 
        </div>
        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        Search Employee
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" Width="100%" onfocus="if(this.value=='')this.value='';"
                            onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                            MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetEmployeeList"
                            ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                            CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                            OnClientItemSelected="OnContactSelected">
                        </cc1:AutoCompleteExtender>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Employee Name:</label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">


                    <div class="left_div">
                        <label>
                            Employee  ID :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCode" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Department  :</label>
                            </div>
                            <div class="right_div">
                                <asp:DropDownList ID="ddlDept" runat="server" DataSourceID="sdsDept" CssClass="input-large" Width="100%" AppendDataBoundItems="True"
                                    DataTextField="Department" DataValueField="DeptID" AutoPostBack="True">
                                    <asp:ListItem Value="">Please Select Department</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsDept" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [DeptID], [Department] FROM [Department] WHERE ([Status] = @Status and DeptID>1)">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="ddlDept" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Designation  :</label>
                            </div>
                            <div class="right_div">
                                <asp:DropDownList ID="ddlDesig" runat="server" DataSourceID="sdsDesig" CssClass="input-large" Width="100%"
                                    DataTextField="Designation" DataValueField="DesigID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsDesig" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT DesigID, Designation FROM Designations WHERE (Status = @Status) AND (DeptID = @DeptID)">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                        <asp:ControlParameter ControlID="ddlDept" Name="DeptID" Type="Int32" PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="ddlDesig" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="span12">
                    <div class="left_div">
                    </div>
                    <div class="right_div">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                    </div>
                </div>
                <div class="span12">
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="span7">
        <div class="span12 form-top-strip" style="margin-top:3px;width:100%;">
           <p>All Employees</p> 
        </div>
        <div class="well" style="height:245px; overflow-y:scroll; padding-top:0px; display:block">

            <table class="table">
                <thead>
                    <tr>
                        
                        <th style="width: 140px;">Name
                        </th>
                        <th style="width: 190px;">Department
                        </th >
                        <th style="width: 150px;">Designation
                        </th>
                        <%--<th>Status
                        </th>--%>
                        <th style="width: 40px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="UID" DataSourceID="sdsUser">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                              <%--  <td>
                                    <asp:Label ID="Code" runat="server" Text='<%# Eval("EmployeeCode") %>' />
                                </td>--%>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("EmployeeName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Department") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Designation") %>' />
                                </td>
                               <%-- <td>
                                    <asp:CheckBox ID="Stat" runat="server" Checked='<%# Eval("Status") %>' />

                                </td>--%>
                                <td>
                                    <asp:LinkButton ID="cmdEdit" CssClass="btn btn-success" CommandName="Edit" runat="server"
                                        CommandArgument='<%# Eval("UID") %>' Text="Edit" />
                                </td>
                            </tr>
                        </ItemTemplate>

                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>

            <asp:SqlDataSource ID="sdsUser" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT Employees.UID, Employees.EmployeeName, Employees.Status, Designations.Designation AS Designation, Department.Department, Employees.EmployeeCode FROM Department INNER JOIN Employees ON Department.DeptID = Employees.DepartmentID INNER JOIN Designations ON Employees.Designation = Designations.DesigID"
                DeleteCommand="DELETE FROM [Employees] WHERE [UID] = @UID" InsertCommand="INSERT INTO [Employees] ([EmployeeName], [DepartmentID], [EmployeeCode], [Designation], [Status]) VALUES (@EmployeeName,@DepartmentID, @EmployeeCode, @Designation, @Status)"
                UpdateCommand="UPDATE [Employees] SET [EmployeeName] = @EmployeeName, [EmployeeCode]=@EmployeeCode,  [DepartmentID]=@DepartmentID, [Designation]=@Designation, [Status] = @Status WHERE [UID] = @UID">
                <DeleteParameters>
                    <asp:ControlParameter ControlID="hdnID" Name="UID" PropertyName="Value" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="EmployeeName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" DefaultValue="True" Name="Status" PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="ddlDept" Name="DepartmentID" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="txtCode" Name="EmployeeCode" PropertyName="Text" />
                    <asp:ControlParameter ControlID="ddlDesig" Name="Designation" PropertyName="SelectedValue" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="EmployeeName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" DefaultValue="True" Name="Status" PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="ddlDept" Name="DepartmentID" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="txtCode" Name="EmployeeCode" PropertyName="Text" />
                    <asp:ControlParameter ControlID="ddlDesig" Name="Designation" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="hdnID" DefaultValue="" Name="UID" PropertyName="Value" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>

    </div>
    <!-- Eof content -->
</asp:Content>
