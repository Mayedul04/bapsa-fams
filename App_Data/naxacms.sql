USE [master]
GO
/****** Object:  Database [nexacms]    Script Date: 5/17/2015 10:01:54 AM ******/
CREATE DATABASE [nexacms] ON  PRIMARY 
( NAME = N'nexacms', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\nexacms.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'nexacms_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\nexacms_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [nexacms] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [nexacms].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [nexacms] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [nexacms] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [nexacms] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [nexacms] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [nexacms] SET ARITHABORT OFF 
GO
ALTER DATABASE [nexacms] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [nexacms] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [nexacms] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [nexacms] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [nexacms] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [nexacms] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [nexacms] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [nexacms] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [nexacms] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [nexacms] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [nexacms] SET  DISABLE_BROKER 
GO
ALTER DATABASE [nexacms] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [nexacms] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [nexacms] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [nexacms] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [nexacms] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [nexacms] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [nexacms] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [nexacms] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [nexacms] SET  MULTI_USER 
GO
ALTER DATABASE [nexacms] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [nexacms] SET DB_CHAINING OFF 
GO
USE [nexacms]
GO
/****** Object:  StoredProcedure [dbo].[sprClearSqlInjectionByTable]    Script Date: 5/17/2015 10:01:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Debashis Chowdhury>
-- Create date: <23 dec 2014>
-- Description:	<Clearing SQL injection by table name.
--  Here I use  “sys.columns” table to get my table fields and apply it to cursor. 
--	I create a dynamic sql statement to clear each column of the table name that user is supplying. 
--	I clear only text columns like verchar, nverchar.>
--	Parameter @injectionScript ='</title><style>.a7'
--	parameter @tableName= 'nameOfTheTable'
-- =============================================
CREATE PROCEDURE [dbo].[sprClearSqlInjectionByTable] 
	-- Add the parameters for the stored procedure here
	@injectionScript nvarchar(100),
	@tableName as varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	--SET NOCOUNT ON prevents the sending of DONE_IN_PROC messages to the client for each statement in a stored procedure. For stored procedures that contain several statements that do not return much actual data, or for procedures that contain Transact-SQL loops, setting SET NOCOUNT to ON can provide a significant performance boost, because network traffic is greatly reduced.
	DECLARE @Name varchar(80),@max_length int,@precision tinyint,  @myString nvarchar(500), @replace nvarchar(100);

	set @replace=@injectionScript
	DECLARE vendor_cursor CURSOR FOR 
	SELECT name,max_length,precision FROM sys.columns WHERE object_id = OBJECT_ID('dbo.'+@tableName) 

	OPEN vendor_cursor

	FETCH NEXT FROM vendor_cursor 
	INTO @Name ,@max_length,@precision

	WHILE @@FETCH_STATUS = 0
	BEGIN

		--if cast( @max_length as int)>15 or cast(@max_length as int)=-1
		if @precision=0
		begin
		
			set @myString='UPDATE [dbo].'+@tableName+'
			set ['+@Name+']=  SUBSTRING(['+@Name+'], 0, CHARINDEX('''+@replace +''', ['+@Name+']))
			where ['+@Name+'] like ''%'+@replace +'%'' '
			exec (@myString) 
			print @myString
		end;
		FETCH NEXT FROM vendor_cursor 
	INTO @Name ,@max_length,@precision
	    
	END 
	CLOSE vendor_cursor;
	DEALLOCATE vendor_cursor;
	
END

GO
/****** Object:  Table [dbo].[AdminPanelLogin]    Script Date: 5/17/2015 10:01:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AdminPanelLogin](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[PS_1] [nvarchar](100) NULL,
	[UN_1] [nvarchar](100) NULL,
	[Title] [nvarchar](300) NULL,
	[Email] [nvarchar](200) NULL,
	[Role] [varchar](50) NULL,
	[CreationDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_AdminPanelLogin_1] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AlertMessage]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AlertMessage](
	[AlertMessageID] [int] IDENTITY(1,1) NOT NULL,
	[MessageFrom] [nvarchar](200) NULL,
	[FromEmail] [nvarchar](50) NULL,
	[Message] [nvarchar](max) NULL,
	[LastUpdated] [datetime] NULL,
	[HasRead] [bit] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_AlertMessage] PRIMARY KEY CLUSTERED 
(
	[AlertMessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Banner]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banner](
	[BannerID] [int] IDENTITY(1,1) NOT NULL,
	[SectionName] [varchar](50) NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](500) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[BigDetails] [nvarchar](max) NULL,
	[Lang] [nvarchar](10) NULL,
	[MasterID] [int] NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[BannerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BlogArticles]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BlogArticles](
	[ArticleID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](400) NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_BlogArticles] PRIMARY KEY CLUSTERED 
(
	[ArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Career]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Career](
	[CareerID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
	[PublishedDate] [datetime] NULL,
	[Deadline] [datetime] NULL,
	[PublishedBy] [nvarchar](50) NULL,
	[JobCode] [nvarchar](100) NULL,
 CONSTRAINT [PK_Career] PRIMARY KEY CLUSTERED 
(
	[CareerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CareerApplicant]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerApplicant](
	[CareerApplicantID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Address] [nvarchar](max) NULL,
	[Email] [nvarchar](100) NULL,
	[Phone] [nvarchar](50) NULL,
	[CV] [nvarchar](500) NULL,
	[LastUpdated] [date] NULL,
	[CareerMasterID] [int] NULL,
	[CoverLetter] [nvarchar](500) NULL,
	[DesiredPosition] [nvarchar](500) NULL,
	[Subject] [nvarchar](500) NULL,
	[Photo] [nvarchar](500) NULL,
	[AttachOthers] [nvarchar](500) NULL,
	[Message] [nvarchar](max) NULL,
	[JobCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_CareerApplicant] PRIMARY KEY CLUSTERED 
(
	[CareerApplicantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CommonGallery]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CommonGallery](
	[CommonGalleryID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[TableName] [varchar](25) NULL,
	[TableMasterID] [int] NULL,
 CONSTRAINT [PK_CommonGallery] PRIMARY KEY CLUSTERED 
(
	[CommonGalleryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[ContactID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](400) NULL,
	[Designation] [nvarchar](300) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](30) NULL,
	[Mobile] [nvarchar](30) NULL,
	[Address] [nvarchar](300) NULL,
	[ContactFor] [nvarchar](100) NULL,
	[Subject] [nvarchar](500) NULL,
	[Message] [nvarchar](max) NULL,
	[ContactDate] [datetime] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[ContactID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contents]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contents](
	[ContentID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[DetailText] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](250) NULL,
	[BigImage] [nvarchar](250) NULL,
	[ImageAltText] [nvarchar](150) NULL,
	[Link] [nvarchar](250) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[TableName] [nvarchar](150) NULL,
	[TableID] [int] NULL,
	[Category] [nvarchar](200) NULL,
 CONSTRAINT [PK_Contents] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CountryList]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CountryList](
	[CountryCode] [nvarchar](20) NOT NULL,
	[CountryName] [nvarchar](50) NULL,
	[SordIndex] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_CountryList] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Gallery]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gallery](
	[GalleryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_Gallery] PRIMARY KEY CLUSTERED 
(
	[GalleryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GalleryItem]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GalleryItem](
	[GalleryItemID] [int] IDENTITY(1,1) NOT NULL,
	[GalleryID] [int] NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_GalleryItem] PRIMARY KEY CLUSTERED 
(
	[GalleryItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HTML]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HTML](
	[HtmlID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[LastUpdated] [datetime] NULL,
	[LinkTitle] [nvarchar](300) NULL,
	[VideoLink] [nvarchar](400) NULL,
	[VideoCode] [nvarchar](max) NULL,
	[SmallImageWidth] [smallint] NULL,
	[SmallImageHeight] [smallint] NULL,
	[BigImageWidth] [smallint] NULL,
	[BigImageHeight] [smallint] NULL,
	[VideoWidth] [smallint] NULL,
	[VideoHeight] [smallint] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_HTML] PRIMARY KEY CLUSTERED 
(
	[HtmlID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Instagram]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Instagram](
	[ImportID] [int] IDENTITY(1,1) NOT NULL,
	[ImageID] [int] NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[OriginalLink] [varchar](100) NULL,
 CONSTRAINT [PK_Instagram] PRIMARY KEY CLUSTERED 
(
	[ImportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InstagramImage]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InstagramImage](
	[ImageID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[OriginalLink] [varchar](100) NULL,
 CONSTRAINT [PK_InstagramImage] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Languages](
	[Lang] [varchar](10) NOT NULL,
	[LangFullName] [nvarchar](50) NULL,
	[SortIndex] [smallint] NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Lang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List](
	[ListID] [int] IDENTITY(1,1) NOT NULL,
	[Category] [nvarchar](400) NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List] PRIMARY KEY CLUSTERED 
(
	[ListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Event]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Event](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Event] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Event_Details]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[List_Event_Details](
	[EventDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[EventID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Time] [nvarchar](200) NULL,
 CONSTRAINT [PK_List_Event_Details] PRIMARY KEY CLUSTERED 
(
	[EventDetailsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[List_News]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_News](
	[NewsID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[Category] [nvarchar](150) NULL,
	[NewsType] [nvarchar](150) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[FileUploaded] [nvarchar](250) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
	[ReportDate] [datetime] NULL,
	[Publisher] [nvarchar](250) NULL,
 CONSTRAINT [PK_List_News] PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Partners]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Partners](
	[PartnerID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](30) NULL,
	[Mobile] [nvarchar](30) NULL,
	[Fax] [nvarchar](30) NULL,
	[Address] [nvarchar](500) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Partners] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Profile]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Profile](
	[ProfileID] [int] IDENTITY(1,1) NOT NULL,
	[PersonName] [nvarchar](300) NULL,
	[Designation] [nvarchar](300) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[LinkedIn] [nvarchar](500) NULL,
	[Facebook] [nvarchar](500) NULL,
	[Twitter] [nvarchar](500) NULL,
	[Mobile] [varchar](20) NULL,
	[Phone] [varchar](20) NULL,
	[Email] [varchar](50) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_List_Profile] PRIMARY KEY CLUSTERED 
(
	[ProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Service]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Service](
	[ServiceID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](max) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [nvarchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Service] PRIMARY KEY CLUSTERED 
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List_Testimonial]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List_Testimonial](
	[TestimonialID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](max) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[WrittenBy] [nvarchar](400) NULL,
	[Designation] [nvarchar](300) NULL,
	[Country] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Featured] [bit] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_List_Testimonial] PRIMARY KEY CLUSTERED 
(
	[TestimonialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List1]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List1](
	[ListID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[Link] [nvarchar](500) NULL,
	[Featured] [bit] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
	[Category] [nvarchar](200) NULL,
	[AreaName] [nvarchar](200) NULL,
	[Guide] [nvarchar](max) NULL,
 CONSTRAINT [PK_List1] PRIMARY KEY CLUSTERED 
(
	[ListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[List1_Child]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[List1_Child](
	[ChildID] [int] IDENTITY(1,1) NOT NULL,
	[ListID] [int] NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[Link] [nvarchar](500) NULL,
	[Featured] [bit] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
 CONSTRAINT [PK_List1_Child] PRIMARY KEY CLUSTERED 
(
	[ChildID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsletterSubscription]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsletterSubscription](
	[NewSubID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](400) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](20) NULL,
	[Mobile] [nvarchar](20) NULL,
	[Comments] [nvarchar](max) NULL,
	[SubscriptionDate] [datetime] NULL,
 CONSTRAINT [PK_NewsletterSubscription] PRIMARY KEY CLUSTERED 
(
	[NewSubID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Reg_Member_]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reg_Member_](
	[RegID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](400) NULL,
	[Email] [nvarchar](200) NULL,
	[Phone] [nvarchar](20) NULL,
	[Mobile] [nvarchar](20) NULL,
	[Organization] [nvarchar](400) NULL,
	[Designation] [nvarchar](300) NULL,
	[Address] [nvarchar](500) NULL,
	[Comments] [nvarchar](max) NULL,
	[RegistrationDate] [datetime] NULL,
	[UN_1] [nvarchar](100) NULL,
	[PS_1] [nvarchar](100) NULL,
 CONSTRAINT [PK_Reg_Member_] PRIMARY KEY CLUSTERED 
(
	[RegID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SEO]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SEO](
	[SEOID] [bigint] IDENTITY(1,1) NOT NULL,
	[PageType] [nvarchar](100) NULL,
	[PageID] [varchar](20) NULL,
	[SEOTitle] [nvarchar](200) NULL,
	[SEODescription] [nvarchar](max) NULL,
	[SEOKeyWord] [nvarchar](max) NULL,
	[SEORobot] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[Lang] [nvarchar](10) NULL,
	[FocusKeyword] [nvarchar](100) NULL,
 CONSTRAINT [PK_SEO] PRIMARY KEY CLUSTERED 
(
	[SEOID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Settings_Dimention]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Settings_Dimention](
	[DimentionID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [nvarchar](100) NULL,
	[Category] [nvarchar](400) NULL,
	[SmallImageWidth] [smallint] NULL,
	[SmallImageHeight] [smallint] NULL,
	[BigImageWidth] [smallint] NULL,
	[BigImageHeight] [smallint] NULL,
	[VideoWidth] [smallint] NULL,
	[VideoHeight] [smallint] NULL,
 CONSTRAINT [PK_Settings_Dimention] PRIMARY KEY CLUSTERED 
(
	[DimentionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Settings_Module]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Settings_Module](
	[SModID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleName] [varchar](100) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Settings_Module] PRIMARY KEY CLUSTERED 
(
	[SModID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SpecialOffer]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SpecialOffer](
	[SpecialOfferID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](400) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[BigDetails] [nvarchar](max) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[BigImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[Link] [varchar](400) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_SpecialOffer] PRIMARY KEY CLUSTERED 
(
	[SpecialOfferID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoGallery]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoGallery](
	[VideoGalleryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryID] [int] NULL,
	[Title] [nvarchar](400) NULL,
	[SmallImage] [nvarchar](400) NULL,
	[ImageAltText] [nvarchar](400) NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[MasterID] [int] NULL,
	[Lang] [varchar](10) NULL,
 CONSTRAINT [PK_VideoGallery] PRIMARY KEY CLUSTERED 
(
	[VideoGalleryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VideoGalleryItem]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VideoGalleryItem](
	[VideoGalleryItemID] [int] IDENTITY(1,1) NOT NULL,
	[VideoGalleryID] [int] NULL,
	[CategoryID] [int] NULL,
	[Title] [varchar](150) NULL,
	[SmallDetails] [nvarchar](1000) NULL,
	[VideoOriginalURL] [varchar](150) NULL,
	[VideoCoreURL] [varchar](150) NULL,
	[VideoImageURL] [varchar](150) NULL,
	[VideoEmbedCode] [varchar](1000) NULL,
	[Featured] [bit] NULL,
	[SortIndex] [int] NULL,
	[Status] [bit] NULL,
	[LastUpdated] [datetime] NULL,
 CONSTRAINT [PK_VideoGalleryItem] PRIMARY KEY CLUSTERED 
(
	[VideoGalleryItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ContactAndNewsletterCombineResult]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ContactAndNewsletterCombineResult]
AS
SELECT        COUNT(dbo.Contact.ContactID) AS ContactsInADay, COUNT(dbo.NewsletterSubscription.NewSubID) AS SubscriptionInADay, 
                         ISNULL(dbo.NewsletterSubscription.SubscriptionDate, dbo.Contact.ContactDate) AS theDate
FROM            dbo.Contact FULL OUTER JOIN
                         dbo.NewsletterSubscription ON dbo.Contact.ContactDate = dbo.NewsletterSubscription.SubscriptionDate
GROUP BY dbo.Contact.ContactID, dbo.Contact.ContactDate, dbo.NewsletterSubscription.NewSubID, dbo.NewsletterSubscription.SubscriptionDate

GO
/****** Object:  View [dbo].[View1]    Script Date: 5/17/2015 10:01:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View1]
AS
SELECT        GroupContacts.totalContact, GroupSubs.totalSubs, ISNULL(GroupContacts.ContactDate, GroupSubs.SubscriptionDate) AS Expr1
FROM            (SELECT        COUNT(ContactID) AS totalContact, ContactDate
                          FROM            dbo.Contact
                          GROUP BY ContactDate) AS GroupContacts FULL OUTER JOIN
                             (SELECT        COUNT(NewSubID) AS totalSubs, SubscriptionDate
                               FROM            dbo.NewsletterSubscription
                               GROUP BY SubscriptionDate) AS GroupSubs ON GroupContacts.ContactDate = GroupSubs.SubscriptionDate

GO
SET IDENTITY_INSERT [dbo].[AdminPanelLogin] ON 

INSERT [dbo].[AdminPanelLogin] ([UserID], [PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (1, N'12345', N'admin', N'Administrator01', N'admin@admin.com', N'Admin', CAST(0x0000A2FE01369FEC AS DateTime), N'1', 1)
INSERT [dbo].[AdminPanelLogin] ([UserID], [PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (2, N'admin', N'sa', N'SA WVSS', N'admin@wvss.net', N'admin', CAST(0x0000A08F0117D260 AS DateTime), N'admin', 1)
SET IDENTITY_INSERT [dbo].[AdminPanelLogin] OFF
SET IDENTITY_INSERT [dbo].[BlogArticles] ON 

INSERT [dbo].[BlogArticles] ([ArticleID], [Category], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (1, N'All', N'Lorem Ipsum', N'Lorem Ipsum Lorem Ipsum', N'<p>Lorem IpsumLorem IpsumLorem Ipsum</p>

<p>Lorem IpsumLorem IpsumLorem IpsumLorem Ipsum</p>
', N'Content/Lorem-Ipsum-Small782014131950.jpg', N'Content/Lorem-Ipsum-Big782014131950.jpg', N'Lorem Ipsum', N'http://lipsum.lipsum.com/', 1, 1, 1, CAST(0x0000A3800101D384 AS DateTime), 1, N'en')
SET IDENTITY_INSERT [dbo].[BlogArticles] OFF
SET IDENTITY_INSERT [dbo].[Career] ON 

INSERT [dbo].[Career] ([CareerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [PublishedDate], [Deadline], [PublishedBy], [JobCode]) VALUES (1, N'Career Opportunity 1', N'Career with us for the post of "Lorem imsum". There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.', N'<p>Responsibilities:</p>
<ul>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
    <li>Nam molestie consectetur magna, a facilisis augue luctus eget.</li>
    <li>Sed ullamcorper tellus sed eros hendrerit ut posuere mauris hendrerit.</li>
    <li>Maecenas sodales scelerisque urna, et euismod diam tincidunt a.</li>
    <li>Maecenas aliquet felis vitae dui laoreet ac pellentesque lectus eleifend.</li>
    <li>Fusce eleifend tellus at mauris auctor fermentum.</li>
</ul>
<p>Benefits:</p>
<ul>
    <li>Etiam ornare ante eu tortor aliquam in bibendum tellus hendrerit.</li>
    <li>Praesent mattis mattis arcu, dapibus iaculis urna dictum sed.</li>
    <li>Vestibulum lobortis aliquet lacus, convallis lobortis nulla placerat eu.</li>
    <li>Donec iaculis faucibus dolor, vel facilisis erat rhoncus id.</li>
    <li>Donec eget enim id turpis consequat vestibulum.</li>
</ul>
<p>&nbsp;</p>', N'Content/CareerSmall2372012103630.jpg', N'Content/CareerBig2372012103630.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A2FD00E5EF48 AS DateTime), 1, N'en', CAST(0x0000A2E300000000 AS DateTime), CAST(0x0000A2FA00000000 AS DateTime), N'test', N'12345')
INSERT [dbo].[Career] ([CareerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [PublishedDate], [Deadline], [PublishedBy], [JobCode]) VALUES (2, N'Career Opportunity 1', N'Career with us for the post of "Lorem imsum". There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.', N'<p>Responsibilities:</p>
<ul>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
    <li>Nam molestie consectetur magna, a facilisis augue luctus eget.</li>
    <li>Sed ullamcorper tellus sed eros hendrerit ut posuere mauris hendrerit.</li>
    <li>Maecenas sodales scelerisque urna, et euismod diam tincidunt a.</li>
    <li>Maecenas aliquet felis vitae dui laoreet ac pellentesque lectus eleifend.</li>
    <li>Fusce eleifend tellus at mauris auctor fermentum.</li>
</ul>
<p>Benefits:</p>
<ul>
    <li>Etiam ornare ante eu tortor aliquam in bibendum tellus hendrerit.</li>
    <li>Praesent mattis mattis arcu, dapibus iaculis urna dictum sed.</li>
    <li>Vestibulum lobortis aliquet lacus, convallis lobortis nulla placerat eu.</li>
    <li>Donec iaculis faucibus dolor, vel facilisis erat rhoncus id.</li>
    <li>Donec eget enim id turpis consequat vestibulum.</li>
</ul>
<p>&nbsp;</p>', N'Content/CareerSmall2372012103630.jpg', N'Content/CareerBig2372012103630.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A2FD00E61144 AS DateTime), 1, N'ar', CAST(0x0000A2E300000000 AS DateTime), CAST(0x0000A2FA00000000 AS DateTime), N'test', N'12345')
INSERT [dbo].[Career] ([CareerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang], [PublishedDate], [Deadline], [PublishedBy], [JobCode]) VALUES (3, N'Career Opportunity 2', N'this is a test career', N'<p>&nbsp;this is a test career</p>
', NULL, NULL, NULL, NULL, 1, 2, 1, CAST(0x0000A31400D60254 AS DateTime), 2, N'en', CAST(0x0000A2E400000000 AS DateTime), CAST(0x0000A2FA00000000 AS DateTime), N'Admin', N'23456')
SET IDENTITY_INSERT [dbo].[Career] OFF
SET IDENTITY_INSERT [dbo].[CareerApplicant] ON 

INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (2, N'Mazhar Abbas', NULL, N'mazhar.abbas376@gmail.com', N'00971566321591', N'Content/Mazhar-Abbas-CV163201464427.doc', CAST(0x4B380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Mazhar-Abbas-Photo163201464426.jpg', NULL, N'I''m an experienced web developer having plus 2 years professional experience. I have great expertise in PHP, Codeigniter, Wordpress, JQUERY, Javascript, Ajax and Mysql. I am very interested to work in your organisation. ', N'web developer')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (3, N'Manimaran', NULL, N'manima198326@gmail.com', N'971502872971', N'Content/Manimaran-CV163201491850.doc', CAST(0x4B380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Manimaran-Photo163201491850.jpg', NULL, N'I have 7 Years Experience in Software Development . Currently Looking for a Job', N'Software Engineer')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (4, N'Sadir Chandroth', NULL, N'sadirmuhammed@gmail.com', N'00971561952579', N'Content/Sadir-Chandroth-CV193201464333.doc', CAST(0x4E380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Sadir-Chandroth-Photo193201464333.jpg', N'Content/Sadir-Chandroth-OthersFile193201464333.doc', N'To succeed in an environment of growth and excellence and earn a job which provides me job   Satisfaction and self development and help me achieve personal as well as organization goals', N'Sales , Accountant , Cashier')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (5, N'Zuhaib Bader', NULL, N'zuhaibbader@hotmail.com', N'+971566169799', N'Content/Zuhaib-Bader-CV193201413158.pdf', CAST(0x4E380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Zuhaib-Bader-Photo193201413158.jpg', NULL, N'Dear Sir / Madam,

I am Looking for Job as an assistant accountant or in administration Department.

Currently i am working as an accountant in a cruising company in Abu Dhabi and wish to in you esteem Organisation.

Regards,

Zuhaib Bader', N'Finance & Accounts')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (6, N'Trevor Kuniski', NULL, N'tkuniski@gmail.com', N'+971567335763', N'Content/Trevor-Kuniski-CV233201493620.doc', CAST(0x52380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Trevor-Kuniski-Photo233201493620.jpg', NULL, NULL, N'1')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (7, N'Manimaran', NULL, N'manima198326@gmail.com', N'971502872971', N'Content/Manimaran-CV24320147330.doc', CAST(0x53380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Manimaran-Photo24320147330.jpg', NULL, N'I have 7 years experience in Software Development', N'Software Engineer')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (8, N'NAVEED', NULL, N'ksiraji@gmail.com', N'00971569128463', N'Content/NAVEED-CV243201419340.docx', CAST(0x53380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/NAVEED-Photo243201419340.jpg', NULL, N'Hello Dear Hiring Manager, 
 
Accountant, with 5 years experience, looking for a suitable opening in finance / accounts. Can join immediately.

Accountant / Admin , B.COM, DICA, CPA (Computerized Professional Accountant) With Tally ERP.9, Peachtree, QuickBooks and Finalization of financial statements and analysis. Advanced expertise of Microsoft Office.
', N'Accountant/ Assistant Accountant')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (9, N'MR JANAK ANANDBHAI KIDEEYA', NULL, N'janak_kideeya@yahoo.com', N'261331938042', N'Content/MR-JANAK-ANANDBHAI-KIDEEYA-CV2532014124924.doc', CAST(0x54380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/MR-JANAK-ANANDBHAI-KIDEEYA-Photo2532014124924.jpg', NULL, N'i am willing to change my job ', N'any')
INSERT [dbo].[CareerApplicant] ([CareerApplicantID], [Name], [Address], [Email], [Phone], [CV], [LastUpdated], [CareerMasterID], [CoverLetter], [DesiredPosition], [Subject], [Photo], [AttachOthers], [Message], [JobCode]) VALUES (10, N'Shilpa Rathi', NULL, N'shilpa305@gmail.com', N'00971504246430', N'Content/Shilpa-Rathi-CV273201414412.pdf', CAST(0x56380B00 AS Date), NULL, NULL, NULL, NULL, N'Content/Shilpa-Rathi-Photo273201414412.jpg', NULL, N' Kind Attention : HR Dept. / General Manager

Dear Sir / Madam ,

Greetings of the day !

I am interested in working with this reputed company & enclosing my CV (attached file) herewith for your perusal.  
Kindly go through it and consider me for the post of  Sales/ Business Coordinator or Commercial Assistant in your esteemed organization.        

I look forward to hearing from you soon.
 

Yours Sincerely, 
 

Shilpa Rathi

Mobile no. 050-4246430
                    
shilpa305@gmail.com

 



', N'0000')
SET IDENTITY_INSERT [dbo].[CareerApplicant] OFF
SET IDENTITY_INSERT [dbo].[CommonGallery] ON 

INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (12, N'remittances', N'Content/remittances.png', N'Content/remittances.png', N'remittances', NULL, 1, CAST(0x0000A3130101A828 AS DateTime), N'List', 1)
INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (13, N'service-sample-1', N'Content/service-sample-1.jpg', N'Content/service-sample-1.jpg', N'service-sample-1', NULL, 1, CAST(0x0000A3130101A828 AS DateTime), N'List', 1)
INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (14, N'Accessories-Test-Product-1-Blue-Small51201416616', N'Content/Accessories-Test-Product-1-Blue-Small51201416616.jpg', N'Content/Accessories-Test-Product-1-Blue-Small51201416616.jpg', N'Accessories-Test-Product-1-Blue-Small51201416616', NULL, 1, CAST(0x0000A32901002C78 AS DateTime), N'HTML', 1)
INSERT [dbo].[CommonGallery] ([CommonGalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (15, N'Accessories-Test-Product-1-Pink-Small51201416757', N'Content/Accessories-Test-Product-1-Pink-Small51201416757.jpg', N'Content/Accessories-Test-Product-1-Pink-Small51201416757.jpg', N'Accessories-Test-Product-1-Pink-Small51201416757', NULL, 1, CAST(0x0000A32900F4AB8C AS DateTime), N'HTML', 1)
SET IDENTITY_INSERT [dbo].[CommonGallery] OFF
SET IDENTITY_INSERT [dbo].[Contact] ON 

INSERT [dbo].[Contact] ([ContactID], [FullName], [Designation], [Email], [Phone], [Mobile], [Address], [ContactFor], [Subject], [Message], [ContactDate]) VALUES (118, N'Debashis', NULL, N'debashis.chowdhury@wvss.net', N'2334343', NULL, NULL, NULL, NULL, N'Test message', CAST(0x0000A337010EED6C AS DateTime))
SET IDENTITY_INSERT [dbo].[Contact] OFF
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AD', N'ANDORRA', 23, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AE', N'UNITED ARAB EMIRATES', 244, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AF', N'AFGHANISTAN', 19, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AG', N'ANTIGUA AND BARBUDA', 27, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AI', N'ANGUILLA', 25, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AL', N'ALBANIA', 20, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AM', N'ARMENIA', 29, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AN', N'NETHERLANDS ANTILLES', 172, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AO', N'ANGOLA', 24, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AQ', N'ANTARCTICA', 26, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AR', N'ARGENTINA', 28, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AS', N'AMERICAN SAMOA', 22, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AT', N'AUSTRIA', 32, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AU', N'AUSTRALIA', 31, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AW', N'ARUBA', 30, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'AZ', N'AZERBAIJAN', 33, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BA', N'BOSNIA AND HERZEGOWINA', 45, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BB', N'BARBADOS', 37, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BD', N'BANGLADESH', 36, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BE', N'BELGIUM', 39, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BF', N'BURKINA FASO', 52, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BG', N'BULGARIA', 51, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BH', N'BAHRAIN', 35, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BI', N'BURUNDI', 53, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BJ', N'BENIN', 41, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BM', N'BERMUDA', 42, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BN', N'BRUNEI DARUSSALAM', 50, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BO', N'BOLIVIA', 44, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BR', N'BRAZIL', 48, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BS', N'BAHAMAS', 34, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BT', N'BHUTAN', 43, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BV', N'BOUVET ISLAND', 47, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BW', N'BOTSWANA', 46, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BY', N'BELARUS', 38, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'BZ', N'BELIZE', 40, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CA', N'CANADA', 56, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CC', N'COCOS (KEELING); ISLANDS', 64, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CD', N'CONGO, THE DRC', 68, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CF', N'CENTRAL AFRICAN REPUBLIC', 59, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CG', N'CONGO', 67, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CH', N'SWITZERLAND', 227, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CI', N'COTE D''IVOIRE', 71, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CK', N'COOK ISLANDS', 69, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CL', N'CHILE', 61, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CM', N'CAMEROON', 55, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CN', N'CHINA', 62, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CO', N'COLOMBIA', 65, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CR', N'COSTA RICA', 70, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CU', N'CUBA', 73, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CV', N'CAPE VERDE', 57, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CX', N'CHRISTMAS ISLAND', 63, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CY', N'CYPRUS', 74, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'CZ', N'CZECH REPUBLIC', 75, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DE', N'GERMANY', 100, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DJ', N'DJIBOUTI', 77, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DK', N'DENMARK', 76, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DM', N'DOMINICA', 78, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DO', N'DOMINICAN REPUBLIC', 79, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'DZ', N'ALGERIA', 21, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EC', N'ECUADOR', 81, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EE', N'ESTONIA', 86, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EG', N'EGYPT', 82, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'EH', N'WESTERN SAHARA', 256, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ER', N'ERITREA', 85, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ES', N'SPAIN', 218, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ET', N'ETHIOPIA', 87, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FI', N'FINLAND', 91, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FJ', N'FIJI', 90, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FK', N'FALKLAND ISLANDS (MALVINAS);', 88, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FM', N'MICRONESIA, FEDERATED STATES OF', 159, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FO', N'FAROE ISLANDS', 89, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FR', N'FRANCE', 92, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'FX', N'FRANCE, METROPOLITAN', 93, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GA', N'GABON', 97, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GB', N'UNITED KINGDOM', 245, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GD', N'GRENADA', 105, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GE', N'GEORGIA', 99, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GF', N'FRENCH GUIANA', 94, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GH', N'GHANA', 101, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GI', N'GIBRALTAR', 102, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GL', N'GREENLAND', 104, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GM', N'GAMBIA', 98, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GN', N'GUINEA', 109, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GP', N'GUADELOUPE', 106, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GQ', N'EQUATORIAL GUINEA', 84, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GR', N'GREECE', 103, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GS', N'SOUTH GEORGIA AND SOUTH S.S.', 217, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GT', N'GUATEMALA', 108, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GU', N'GUAM', 107, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GW', N'GUINEA-BISSAU', 110, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'GY', N'GUYANA', 111, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HK', N'HONG KONG', 116, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HM', N'HEARD AND MC DONALD ISLANDS', 113, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HN', N'HONDURAS', 115, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HR', N'CROATIA (local name: Hrvatska);', 72, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HT', N'HAITI', 112, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'HU', N'HUNGARY', 117, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ID', N'INDONESIA', 120, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IE', N'IRELAND', 123, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IL', N'ISRAEL', 124, 0)
GO
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IN', N'INDIA', 119, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IO', N'BRITISH INDIAN OCEAN TERRITORY', 49, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IQ', N'IRAQ', 122, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IR', N'IRAN (ISLAMIC REPUBLIC OF);', 121, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IS', N'ICELAND', 118, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'IT', N'ITALY', 125, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'JM', N'JAMAICA', 126, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'JO', N'JORDAN', 128, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'JP', N'JAPAN', 127, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KE', N'KENYA', 130, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KG', N'KYRGYZSTAN', 135, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KH', N'CAMBODIA', 54, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KI', N'KIRIBATI', 131, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KM', N'COMOROS', 66, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KN', N'SAINT KITTS AND NEVIS', 199, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KP', N'KOREA, D.P.R.O.', 132, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KR', N'KOREA, REPUBLIC OF', 133, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KW', N'KUWAIT', 134, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KY', N'CAYMAN ISLANDS', 58, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'KZ', N'KAZAKHSTAN', 129, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LA', N'LAOS', 136, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LB', N'LEBANON', 138, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LC', N'SAINT LUCIA', 200, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LI', N'LIECHTENSTEIN', 142, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LK', N'SRI LANKA', 219, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LR', N'LIBERIA', 140, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LS', N'LESOTHO', 139, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LT', N'LITHUANIA', 143, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LU', N'LUXEMBOURG', 144, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LV', N'LATVIA', 137, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'LY', N'LIBYAN ARAB JAMAHIRIYA', 141, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MA', N'MOROCCO', 165, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MC', N'MONACO', 161, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MD', N'MOLDOVA, REPUBLIC OF', 160, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ME', N'MONTENEGRO', 163, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MG', N'MADAGASCAR', 147, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MH', N'MARSHALL ISLANDS', 153, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MK', N'MACEDONIA', 146, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ML', N'MALI', 151, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MM', N'MYANMAR (Burma);', 167, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MN', N'MONGOLIA', 162, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MO', N'MACAU', 145, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MP', N'NORTHERN MARIANA ISLANDS', 180, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MQ', N'MARTINIQUE', 154, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MR', N'MAURITANIA', 155, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MS', N'MONTSERRAT', 164, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MT', N'MALTA', 152, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MU', N'MAURITIUS', 156, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MV', N'MALDIVES', 150, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MW', N'MALAWI', 148, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MX', N'MEXICO', 158, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MY', N'MALAYSIA', 149, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'MZ', N'MOZAMBIQUE', 166, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NA', N'NAMIBIA', 168, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NC', N'NEW CALEDONIA', 173, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NE', N'NIGER', 176, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NF', N'NORFOLK ISLAND', 179, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NG', N'NIGERIA', 177, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NI', N'NICARAGUA', 175, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NL', N'NETHERLANDS', 171, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NO', N'NORWAY', 181, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NP', N'NEPAL', 170, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NR', N'NAURU', 169, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NU', N'NIUE', 178, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'NZ', N'NEW ZEALAND', 174, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'OM', N'OMAN', 182, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PA', N'PANAMA', 185, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PE', N'PERU', 188, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PF', N'FRENCH POLYNESIA', 95, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PG', N'PAPUA NEW GUINEA', 186, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PH', N'PHILIPPINES', 189, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PK', N'PAKISTAN', 183, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PL', N'POLAND', 191, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PM', N'ST. PIERRE AND MIQUELON', 221, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PN', N'PITCAIRN', 190, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PR', N'PUERTO RICO', 193, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PT', N'PORTUGAL', 192, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PW', N'PALAU', 184, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'PY', N'PARAGUAY', 187, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'QA', N'QATAR', 194, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RE', N'REUNION', 195, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RO', N'ROMANIA', 196, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RS', N'SERBIA', 207, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RU', N'RUSSIAN FEDERATION', 197, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'RW', N'RWANDA', 198, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SA', N'SAUDI ARABIA', 205, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SB', N'SOLOMON ISLANDS', 213, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SC', N'SEYCHELLES', 208, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SD', N'SUDAN', 222, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SE', N'SWEDEN', 226, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SG', N'SINGAPORE', 210, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SH', N'ST. HELENA', 220, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SI', N'SLOVENIA', 212, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SJ', N'SVALBARD AND JAN MAYEN ISLANDS', 224, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SK', N'SLOVAKIA (Slovak Republic);', 211, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SL', N'SIERRA LEONE', 209, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SM', N'SAN MARINO', 203, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SN', N'SENEGAL', 206, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SO', N'SOMALIA', 214, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SR', N'SURINAME', 223, 1)
GO
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SS', N'SOUTH SUDAN', 216, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ST', N'SAO TOME AND PRINCIPE', 204, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SV', N'EL SALVADOR', 83, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SY', N'SYRIAN ARAB REPUBLIC', 228, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'SZ', N'SWAZILAND', 225, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TC', N'TURKS AND CAICOS ISLANDS', 240, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TD', N'CHAD', 60, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TF', N'FRENCH SOUTHERN TERRITORIES', 96, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TG', N'TOGO', 233, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TH', N'THAILAND', 232, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TJ', N'TAJIKISTAN', 230, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TK', N'TOKELAU', 234, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TM', N'TURKMENISTAN', 239, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TN', N'TUNISIA', 237, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TO', N'TONGA', 235, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TP', N'EAST TIMOR', 80, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TR', N'TURKEY', 238, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TT', N'TRINIDAD AND TOBAGO', 236, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TV', N'TUVALU', 241, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TW', N'TAIWAN, PROVINCE OF CHINA', 229, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'TZ', N'TANZANIA, UNITED REPUBLIC OF', 231, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UA', N'UKRAINE', 243, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UG', N'UGANDA', 242, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UM', N'U.S. MINOR ISLANDS', 247, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'US', N'UNITED STATES', 246, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UY', N'URUGUAY', 248, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'UZ', N'UZBEKISTAN', 249, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VA', N'HOLY SEE (VATICAN CITY STATE);', 114, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VC', N'SAINT VINCENT AND THE GRENADINES', 201, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VE', N'VENEZUELA', 251, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VG', N'VIRGIN ISLANDS (BRITISH);', 253, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VI', N'VIRGIN ISLANDS (U.S.);', 254, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VN', N'VIET NAM', 252, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'VU', N'VANUATU', 250, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'WF', N'WALLIS AND FUTUNA ISLANDS', 255, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'WS', N'SAMOA', 202, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'YE', N'YEMEN', 257, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'YT', N'MAYOTTE', 157, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ZA', N'SOUTH AFRICA', 215, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ZM', N'ZAMBIA', 258, 1)
INSERT [dbo].[CountryList] ([CountryCode], [CountryName], [SordIndex], [Status]) VALUES (N'ZW', N'ZIMBABWE', 259, 1)
SET IDENTITY_INSERT [dbo].[Instagram] ON 

INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (41, 1, N'Finally!! :) #DNP#diaryngpanget', N'Content/InstaThumb2142014122547168.jpg', N'Content/InstaBig2142014122547895.jpg', 1, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage8.ak.instagram.com/d7a088aac92d11e38ea224be059598b0_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (42, 2, N'Shout out for shout out!ðŸ“¢ðŸ“¢ðŸ“¢ Follow @susieee_xo #sfs #s4s #shoutout #diaryngpanget #dnp #fab4 #cinegang #jadine #yandre', N'Content/InstaThumb2142014122548637.jpg', N'Content/InstaBig2142014122549391.jpg', 2, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage2.ak.instagram.com/89f88cb6c92c11e39ee80002c9c8e732_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (43, 3, N'Will watch Diary ng Panget with Tita @florachua3 Ate @steph_hanie and @marcderrick_8!!!! :)))))) #DNP #Movie #DiaryNgPanget #happykiddo', N'Content/InstaThumb2142014122550208.jpg', N'Content/InstaBig2142014122550963.jpg', 3, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage6.ak.instagram.com/69463aaec92c11e39a720002c9c705fa_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (44, 4, N'#DNP # ST2 #ROCKETEER ðŸ’‹ðŸŽµðŸŽ¶ðŸ’¿ðŸ˜—', N'Content/InstaThumb2142014122551613.jpg', N'Content/InstaBig2142014122552550.jpg', 4, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage10.ak.instagram.com/72a4cbcec92711e389340002c954cdde_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (45, 5, N'Tapos na manuod ng #dnp ganda! Gwapo ni @mjcayabyab6', N'Content/InstaThumb21420141225530.jpg', N'Content/InstaBig2142014122553674.jpg', 5, 1, CAST(0x0000A31400000000 AS DateTime), N'http://photos-b.ak.instagram.com/hphotos-ak-ash/914349_507726032667273_270245080_n.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (46, 6, N'Shout out for Shout out? #sfs #dnp #diaryngpanget #shoutout #s4s #bored', N'Content/InstaThumb2142014122554285.jpg', N'Content/InstaBig2142014122554439.jpg', 6, 1, CAST(0x0000A31400000000 AS DateTime), N'http://photos-b.ak.instagram.com/hphotos-ak-ash/10175205_689253741130921_69870160_n.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (48, 8, N'It all started with ate Denny! An amazing writer! Love you!ðŸ‘ You didn''t just made the cast proud but also to all of the supporters of Diary Ng Panget! We love you ate Denny! Good luck in everything you do!â¤â¤â¤ #denny #diaryngpangetthemovie #diaryngpanget #dnp #dnpcast #jadine #yandre #teameyoss #teamlorhad #nadinelustre #jamesreid #yassipressman #andreparas #proud #fangirl #fab4 #cinegang', N'Content/InstaThumb2142014122555376.jpg', N'Content/InstaBig2142014122555534.jpg', 8, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage6.ak.instagram.com/800ca780c92911e3a14f0002c955b9d6_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (49, 9, N'Ako si Maxine :) isa akong royalty from england ðŸ’Ž direct descendant ni Queen Elizabeth.

#DNP is â¤', N'Content/InstaThumb2142014122555863.jpg', N'Content/InstaBig2142014122556480.jpg', 9, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage4.ak.instagram.com/655c594ec92911e399990002c9443240_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (50, 10, N'busy ung mag-nanay  hahaha
#luvu 
#DNP', N'Content/InstaThumb214201412255741.jpg', N'Content/InstaBig2142014122557660.jpg', 10, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/10e49f02c92911e3b6690002c9121700_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (51, 11, N'Now watching :) Movie Marathon as always ! #DNP', N'Content/InstaThumb2142014122558230.jpg', N'Content/InstaBig2142014122558498.jpg', 11, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/6444b2c6c92511e3a50d0002c954e1a8_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (52, 12, N'Mwahahahaha atlast kita na jud ko. #DNP #JAMESREID ðŸ˜ðŸ˜˜', N'Content/InstaThumb214201412255952.jpg', N'Content/InstaBig2142014122559326.jpg', 12, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/2011b584c92711e394e90002c9cf0568_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (53, 13, N'Waaaaaaaaaaaaaahhhh â™¡
I like youuuuuuu na James Reid â™¥â™¥â™¥
#crossSandford #sokilig #whysoHot #DnP', N'Content/InstaThumb2142014122559741.jpg', N'Content/InstaBig21420141226044.jpg', 13, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage10.ak.instagram.com/2bc057d8c92611e3aa640002c9dbdc7e_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (54, 14, N'Diary Ng Panget Castâœ¨ #diaryngpanget #dnp #andreparas #yassipressman #nadinelustre #jamesreid #teameyoss #teamlorhad #jadine #yandre #fab4 #cinegang', N'Content/InstaThumb214201412260343.jpg', N'Content/InstaBig214201412260639.jpg', 14, 1, CAST(0x0000A31400000000 AS DateTime), N'http://photos-e.ak.instagram.com/hphotos-ak-prn/10249304_778537588825964_955313542_n.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (56, 16, N'Heller
#DNP#afternoonðŸ˜‰', N'Content/InstaThumb214201412261442.jpg', N'Content/InstaBig214201412261757.jpg', 16, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/46feaf1ac92411e38ee224be059cdf80_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (57, 17, N'So what happened to #100happydaysofkea? I don''t know either. I guess I got too caught up with my recent #DnP fever. ðŸ’” the important thing is I''m fairly happy and very thankful of all God''s blessings to me. Everyday will be a happy day, regardless if I''m able to post it out here or not. Loving life because it''s worth living â™¥', N'Content/InstaThumb214201412262133.jpg', N'Content/InstaBig214201412262419.jpg', 17, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage8.ak.instagram.com/5dee9100c92311e3a3b00002c9e04a24_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (58, 18, N'Girl''s ideal guy: handsome and funnyðŸ˜»ðŸ˜½ #bromance #diaryngpanget #dnp #jamesreid #andreparas', N'Content/InstaThumb214201412262952.jpg', N'Content/InstaBig214201412263108.jpg', 18, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage6.ak.instagram.com/8b86df82c92311e3a2190002c9e185c6_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (59, 19, N'Watching Diary Ng Panget together with @zairabotz Melodz & Rae :) #DiaryNgPanget #movie #DNP #marathon', N'Content/InstaThumb214201412263308.jpg', N'Content/InstaBig214201412263468.jpg', 19, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage10.ak.instagram.com/cb7e1412c92211e3bbf70002c9e26c1a_8.jpg')
INSERT [dbo].[Instagram] ([ImportID], [ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (60, 20, N'Andre Paras is the awesomest actor ever!ðŸ‘Š #andreparas #basketball #feastofthebeast #jamesreid #dnp', N'Content/InstaThumb214201412263965.jpg', N'Content/InstaBig214201412264256.jpg', 20, 1, CAST(0x0000A31400000000 AS DateTime), N'http://distilleryimage0.ak.instagram.com/2a05d03ec92211e394dc0002c950d154_8.jpg')
SET IDENTITY_INSERT [dbo].[Instagram] OFF
SET IDENTITY_INSERT [dbo].[InstagramImage] ON 

INSERT [dbo].[InstagramImage] ([ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (1, N'Lory! @yassipressman #YassiPressman #DNP', N'Content/InstaThumb2142014122554681.jpg', N'Content/InstaBig2142014122554933.jpg', 7, 1, CAST(0x0000A31400000000 AS DateTime), NULL)
INSERT [dbo].[InstagramImage] ([ImageID], [Title], [SmallImage], [BigImage], [SortIndex], [Status], [LastUpdated], [OriginalLink]) VALUES (2, N'Look How sweet they are â™¥
-Admin Aviary
 #JaDinenatics#reidxjames#nadzlustre#dnp#diaryngpanget#jadineloveteam#Jadine#love#follow4follow#followback#f4f#alwaysfollowback#followme#followsback#couples', N'Content/InstaThumb214201412260954.jpg', N'Content/InstaBig214201412261109.jpg', 15, 1, CAST(0x0000A31400000000 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[InstagramImage] OFF
INSERT [dbo].[Languages] ([Lang], [LangFullName], [SortIndex]) VALUES (N'ar', N'Arabic', 2)
INSERT [dbo].[Languages] ([Lang], [LangFullName], [SortIndex]) VALUES (N'en', N'English', 1)
SET IDENTITY_INSERT [dbo].[List_Event] ON 

INSERT [dbo].[List_Event] ([EventID], [CategoryID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (1, NULL, N'Private Label Middle East', N'www.privatelabelmiddleeast.com
01 - 03 Oct', NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A27C0101817C AS DateTime), 2, N'en')
INSERT [dbo].[List_Event] ([EventID], [CategoryID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (6, NULL, N'Private Label Middle East', N'www.privatelabelmiddleeast.com
01 - 03 Oct', NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A27C0100C4D0 AS DateTime), 2, N'ar')
SET IDENTITY_INSERT [dbo].[List_Event] OFF
SET IDENTITY_INSERT [dbo].[List_Event_Details] ON 

INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (1, 9, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (2, 2, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A26B00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (3, 3, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (4, 4, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (5, 5, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (6, 6, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24C00000000 AS DateTime), NULL)
INSERT [dbo].[List_Event_Details] ([EventDetailsID], [EventID], [StartDate], [EndDate], [Time]) VALUES (7, 1, CAST(0x0000A24A00000000 AS DateTime), CAST(0x0000A24D00000000 AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[List_Event_Details] OFF
SET IDENTITY_INSERT [dbo].[List_Partners] ON 

INSERT [dbo].[List_Partners] ([PartnerID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Email], [Phone], [Mobile], [Fax], [Address], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (1, N'Rhythmlink', N'With a large portfolio of partners and brands, we have exclusive relationships with American and European suppliers and manufacturers and are thus able to source reliable products at great value. Our business model is based on earning the exclusive right to represent and develop our manufacturers’ brands in our active markets and creating room for their success.', NULL, N'Content/Rhythmlink-Small2952014152230.jpg', NULL, N'Rhythmlink', N'sasa@ss.com', NULL, NULL, NULL, NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A33A00FD68A8 AS DateTime), 1, N'en')
SET IDENTITY_INSERT [dbo].[List_Partners] OFF
SET IDENTITY_INSERT [dbo].[List_Service] ON 

INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (2, N'Test Service 1', N'Contrary to popular belief', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.<img alt="" src="http://localhost:14431/ApolloCMSNew/userfiles/Service2412012161639.jpg" /></p>
', N'Content/Test-Service-1-Small1542014151811.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00FC2FC4 AS DateTime), 2, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (3, N'Test Service 1', N'Contrary to popular belief, Lorem Ipsum is not simply random text. ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C3819C AS DateTime), 3, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (4, N'Test Service 1', N'Contrary to popular belief,', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C36DB0 AS DateTime), 4, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (5, N'Test Service 1', N'Contrary to popular belief,  ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C33B4C AS DateTime), 5, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (6, N'Test Service 1', N'Contrary to popular belief,', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 0, 1, 1, CAST(0x0000A30E00C32D3C AS DateTime), 6, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (7, N'Test Service 1', N'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A09E0109B0CC AS DateTime), 7, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (8, N'Test Service 1', N'Contrary to popular belief, Lorem Ipsum is not simply random text. ', N'<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A30E00C35190 AS DateTime), 8, N'en')
INSERT [dbo].[List_Service] ([ServiceID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (10, N'Test Service 1.', N'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in ', N'<p><img alt="" src="http://localhost:21946/NexaCms/Admin/Content/UserFiles/Accessories-Test-Product-1-Big5120141598.jpg" style="height:538px; width:552px" />&nbsp; Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.<img alt="" src="http://localhost:14431/ApolloCMSNew/userfiles/Service2412012161639.jpg" /></p>
', N'Content/ServiceSmall1972012163424.jpg', N'Content/ServiceBig1972012163424.jpg', NULL, N'http://www.wvss.net', 1, 1, 1, CAST(0x0000A30E00C35D48 AS DateTime), 10, N'en')
SET IDENTITY_INSERT [dbo].[List_Service] OFF
SET IDENTITY_INSERT [dbo].[List_Testimonial] ON 

INSERT [dbo].[List_Testimonial] ([TestimonialID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [WrittenBy], [Designation], [Country], [SortIndex], [Featured], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (2, NULL, N'1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt urna neque, volutpat ultrices leo molestie nec.', NULL, NULL, NULL, NULL, N'Adreano', N'Tester', N'UAE', 1, 0, 1, CAST(0x0000A31700DA604C AS DateTime), 2, N'en')
INSERT [dbo].[List_Testimonial] ([TestimonialID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [WrittenBy], [Designation], [Country], [SortIndex], [Featured], [Status], [LastUpdated], [MasterID], [Lang]) VALUES (5, NULL, N'List_TestimonialList_TestimonialList_TestimonialList_Testimonial', NULL, NULL, NULL, NULL, N'WVSS', N'Tester', N'UAE', 2, 0, 1, CAST(0x0000A31700DB0DBC AS DateTime), 3, N'en')
SET IDENTITY_INSERT [dbo].[List_Testimonial] OFF
SET IDENTITY_INSERT [dbo].[NewsletterSubscription] ON 

INSERT [dbo].[NewsletterSubscription] ([NewSubID], [FullName], [Email], [Phone], [Mobile], [Comments], [SubscriptionDate]) VALUES (1, N'Debashis', N'debashis.chowdhury@wvss.net', NULL, NULL, NULL, CAST(0x0000A19F01064C70 AS DateTime))
INSERT [dbo].[NewsletterSubscription] ([NewSubID], [FullName], [Email], [Phone], [Mobile], [Comments], [SubscriptionDate]) VALUES (2, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A30E00000000 AS DateTime))
INSERT [dbo].[NewsletterSubscription] ([NewSubID], [FullName], [Email], [Phone], [Mobile], [Comments], [SubscriptionDate]) VALUES (3, NULL, N'debashis.chowdhury1@wvss.net', NULL, NULL, NULL, CAST(0x0000A30E00000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[NewsletterSubscription] OFF
SET IDENTITY_INSERT [dbo].[Reg_Member_] ON 

INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (1, N'saiful', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09C00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (2, N'debasis', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09C00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (3, N'mayedul', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09D00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (4, N'rajib roy', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09D00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (6, N'ferry', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09E00000000 AS DateTime), NULL, NULL)
INSERT [dbo].[Reg_Member_] ([RegID], [FullName], [Email], [Phone], [Mobile], [Organization], [Designation], [Address], [Comments], [RegistrationDate], [UN_1], [PS_1]) VALUES (7, N'francis', NULL, NULL, NULL, NULL, NULL, NULL, N'Text Message', CAST(0x0000A09E00000000 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Reg_Member_] OFF
SET IDENTITY_INSERT [dbo].[Settings_Dimention] ON 

INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (1, N'portfolio', N'nexa', 352, 199, 618, 349, 600, 442)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (2, N'News', N'nexa', 184, 118, 184, 118, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (3, N'Banner', N'Home', 415, 200, 730, 351, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (4, N'Blog', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (5, N'Career', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (6, N'Event', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (7, N'Gallery', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (8, N'GalleryItem', N'nexa', 108, 67, 532, 399, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (9, N'Hotel', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (10, N'Hotel', N'Image', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (11, N'List', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (12, N'List1', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (13, N'List1Child', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (14, N'List_Outlet', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (15, N'List_OutletBanner', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (16, N'List_OutletGallery', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (17, N'List_OutletGalleryItem', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (18, N'Profile', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (19, N'Service', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (20, N'SpecialOffer', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (21, N'Testimonial', N'', 150, 200, 400, 600, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (22, N'Video', N'', NULL, NULL, NULL, NULL, 400, 600)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (23, N'client', N'nexa', 148, 101, 440, 300, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (24, N'casestudy', N'nexa', 245, 100, 1152, 469, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (25, N'casestudydetails', N'nexa', 130, 100, 452, 350, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (26, N'Webservice', N'nexa', 334, 176, 570, 300, NULL, NULL)
INSERT [dbo].[Settings_Dimention] ([DimentionID], [ModuleName], [Category], [SmallImageWidth], [SmallImageHeight], [BigImageWidth], [BigImageHeight], [VideoWidth], [VideoHeight]) VALUES (27, N'SocialMedia', N'nexa', 334, 176, 570, 300, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Settings_Dimention] OFF
SET IDENTITY_INSERT [dbo].[Settings_Module] ON 

INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (1, N'HTML', 1, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (2, N'Banner', 2, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (3, N'Gallery', 3, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (4, N'News', 4, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (5, N'Profile', 5, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (6, N'Event', 6, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (7, N'Service', 7, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (8, N'Testimonial', 8, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (9, N'List', 9, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (10, N'List1', 10, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (11, N'SEO', 11, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (12, N'Contact', 12, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (13, N'NewsSub', 13, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (14, N'RegistrationForm', 14, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (16, N'SpecialOffer', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (17, N'Blog', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (18, N'FAQ', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (19, N'Career', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (20, N'NewsletterSubscription', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (21, N'DBBackup', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (23, N'Login', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (24, N'VideoGallery', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (25, N'GoogleAnalytics', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (26, N'SEOReport', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (27, N'AlertMessage', 0, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (29, N'Hotel', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (30, N'Outlet', 0, 0)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (31, N'Portfolio', 15, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (32, N'WebServices', 16, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (33, N'SocialMedia', 17, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (34, N'CompanyProduct', 18, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (35, N'Facebook', 19, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (36, N'CaseStudy', 20, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (37, N'Client', 21, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (38, N'Management', 22, 1)
INSERT [dbo].[Settings_Module] ([SModID], [ModuleName], [SortIndex], [Status]) VALUES (39, N'InstagramGallery', 23, 1)
SET IDENTITY_INSERT [dbo].[Settings_Module] OFF
SET IDENTITY_INSERT [dbo].[SpecialOffer] ON 

INSERT [dbo].[SpecialOffer] ([SpecialOfferID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [StartDate], [EndDate], [MasterID], [Lang]) VALUES (1, N'test special offer', N'test special offer test special offer test special offer', N'<p>&nbsp;test special offertest special offertest special offertest special offertest special offertest special offertest special offertest special offertest special offertest special offer</p>', N'Content/sp-offer-Small1092012104244.jpg', N'Content/sp-offer-Big1092012104322.jpg', N'sp offer', NULL, 1, 1, 1, CAST(0x0000A0C800B0B4B8 AS DateTime), CAST(0x0000A0C200000000 AS DateTime), CAST(0x0000A0D200000000 AS DateTime), NULL, NULL)
INSERT [dbo].[SpecialOffer] ([SpecialOfferID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [StartDate], [EndDate], [MasterID], [Lang]) VALUES (4, N'Oceanic Khorfakkan Resort & Spa Factsheet (English)', N'Oceanic Khorfakkan Resort & Spa Factsheet (English)Oceanic Khorfakkan Resort & Spa Factsheet (English)', N'<p>ss</p>
', NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A37F0113B02C AS DateTime), NULL, NULL, 1, N'en')
INSERT [dbo].[SpecialOffer] ([SpecialOfferID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [ImageAltText], [Link], [Featured], [SortIndex], [Status], [LastUpdated], [StartDate], [EndDate], [MasterID], [Lang]) VALUES (5, N'Oceanic Khorfakkan Resort & Spa Factsheet (arabic)', N'Oceanic Khorfakkan Resort & Spa Factsheet (English)Oceanic Khorfakkan Resort & Spa Factsheet (English)', NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, CAST(0x0000A27C010BEB80 AS DateTime), NULL, NULL, 1, N'ar')
SET IDENTITY_INSERT [dbo].[SpecialOffer] OFF
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[10] 2[21] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Contact"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 7
         End
         Begin Table = "NewsletterSubscription"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 135
               Right = 425
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 2580
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ContactAndNewsletterCombineResult'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ContactAndNewsletterCombineResult'
GO
USE [master]
GO
ALTER DATABASE [nexacms] SET  READ_WRITE 
GO
