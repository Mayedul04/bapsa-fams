﻿
Partial Class Admin_MasterPage_Main
    Inherits System.Web.UI.MasterPage
    Public domainName As String
    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            If Not Utility.isLoggedIn(Request) Then
                Response.Cookies("userName").Value = ""
                Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userFullName").Value = ""
                Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userpass").Value = ""
                Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)

                If Request.Url.AbsolutePath.ToLower().Contains("default") Then
                    Response.Redirect("login")
                End If
                Response.Redirect("login")
            End If
            ltUser.Text = Request.Cookies("userFullName").Value
        End If
    End Sub



    Protected Function GetCurrentClass(ParamArray pageNames As String()) As String
        Dim virtualPath As String = Page.AppRelativeVirtualPath.Replace("~/", "").ToLower()
        For Each elem As String In pageNames
            If virtualPath.Contains(elem.ToLower()) Then
                Return "active"
            End If
        Next
        Return ""

    End Function


End Class

