﻿
Imports System.Data.SqlClient

Partial Class Admin_MasterPage_Main
    Inherits System.Web.UI.MasterPage
    Public domainName As String
    Public permissions As List(Of Integer) = New List(Of Integer)
    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        Context.Items.Add("FinancialYearStart", "2016/07/01")
        If (Not Request.Cookies("UserID") Is Nothing) Then
            Context.Items.Add("UserID", Request.Cookies("UserID").Value)
            'Response.Cookies("UserID").Value = ""
            'Response.Cookies("UserID").Expires = DateTime.Now.AddDays(-2)
        End If

    End Sub
    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            If Not Utility.isLoggedIn(Request) Then
                Response.Cookies("userName").Value = ""
                Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userFullName").Value = ""
                Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userpass").Value = ""
                Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userID").Value = ""
                Response.Cookies("userID").Expires = Date.Now.AddDays(-2)

                If Request.Url.AbsolutePath.ToLower().Contains("default") Then
                    Response.Redirect("login")
                End If
                Response.Redirect("login")
            End If
            ltUser.Text = Request.Cookies("userFullName").Value

        End If
        getPermission()
        If (permissions.Contains(5) = False) Then
            pnlConfig.Visible = False
        End If
    End Sub
    Public Function getNavigations() As String
        Dim retstr As String = ""
        If (permissions.Contains(6)) Then
            retstr += "<li  class=" & GetCurrentClass("Category") & "><a href=""" & domainName & "AllCategory"">Assets Category</a></li>"
        End If
        If (permissions.Contains(1)) Then
            retstr += "<li  class=" & GetCurrentClass("FixedAssets") & "><a href=""" & domainName & "NewAssets"">New Assets Entry</a></li>"
            retstr += "<li  class=" & GetCurrentClass("StartingItems") & "><a href=""" & domainName & "StartingAsset"">Starting Assets Entry</a></li>"
        End If
        If (permissions.Contains(2)) Then
            retstr += "<li  class=" & GetCurrentClass("Transfer") & "><a href=""" & domainName & "TransferAsset"">Assets Transfer</a></li>"
        End If
        If (permissions.Contains(3)) Then
            retstr += "<li  class=" & GetCurrentClass("Revaluate") & "><a href=""" & domainName & "RevaluateAsset"">Assets Revaluate</a></li>"
            retstr += "<li  class=" & GetCurrentClass("Dispose") & "><a href=""" & domainName & "DisposeAsset"">Assets Dispose/Resell</a></li>"
        End If

        Return retstr
    End Function
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select SectID from UserPermissions where UserID=@userID and SectPermission=@SectPermission"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("userID", Data.SqlDbType.Int, 32)
        cmd.Parameters("userID").Value = Request.Cookies("userID").Value
        cmd.Parameters.Add("SectPermission", Data.SqlDbType.Bit).Value = True
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read

            permissions.Add(reader("SectID").ToString())
        End While
        conn.Close()

    End Sub
    Protected Function SetPermission(ByVal sectid As Integer) As String
        For Each item In permissions
            If (item = sectid) Then
                Return "visible"
            End If
        Next
        Return "hidden"
    End Function
    Protected Function GetCurrentClass(ParamArray pageNames As String()) As String
        Dim virtualPath As String = Page.AppRelativeVirtualPath.Replace("~/", "").ToLower()
        For Each elem As String In pageNames
            If virtualPath.Contains(elem.ToLower()) Then
                Return "active"
            End If
        Next
        Return ""

    End Function


End Class

