﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="CategoryEdit.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<h1 class="page-title">Category</h1>--%>
    <div class="span12" style="margin-left: 0px; margin-top: 5px;">
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divDelete" visible="false" runat="server">
        <asp:Label ID="Label1" runat="server" Text="This Category may have Assets. Please Delete the Depedencies first"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="span12 form-top-strip">
            <p>Category Form</p>
     </div>
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <div class="span12">
                <div class="left_div">
                    <label>
                        Category Name :</label>
                    <asp:HiddenField ID="hdnID" runat="server" Value="" />
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="span12">
                <div class="left_div">
                    <label>
                        Parent :</label>
                </div>
                <div class="right_div">

                    <asp:DropDownList ID="ddlCat" runat="server" DataSourceID="sdsParent" CssClass="input-large"
                        DataTextField="CategoryName" DataValueField="CatID">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsParent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status)">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
            <div class="span12">
                <div class="left_div">
                    <label>
                        Code :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtCode" runat="server" CssClass="input-large" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>

            </div>

            <div class="span12">
                <div class="left_div">
                    <label>
                        Descriptions :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine" CssClass="input-large"
                        Rows="4"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                        runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                        ErrorMessage="* limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                </div>
            </div>


            <div class="span12">
                <div class="left_div">
                </div>
                <div class="right_div">
                    <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                </div>
            </div>

            <div class="span12">
                <div class="span4">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                </div>
                <div class="span4">
                    <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                </div>
                <div class="span4">
                    <button runat="server" id="btnClear" validationgroup="form" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                </div>
            </div>

        </div>

        <asp:SqlDataSource ID="sdsObject" runat="server"
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Category] WHERE [CatID] = @CatID"
            InsertCommand="INSERT INTO [Category] ([ParentID], [CategoryName], [Code], [Descriptions], [Status]) VALUES (@ParentID, @CategoryName, @Code, @Descriptions, @Status)"
            SelectCommand="SELECT * FROM [Category]"
            UpdateCommand="UPDATE [Category] SET [ParentID] = @ParentID, [CategoryName] = @CategoryName, [Code] = @Code, [Descriptions] = @Descriptions, [Status] = @Status WHERE [CatID] = @CatID">
            <DeleteParameters>
                <asp:ControlParameter ControlID="hdnID" Name="CatID" PropertyName="Value" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="ddlCat" Name="ParentID" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="txtTitle" Name="CategoryName" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="Descriptions" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="ddlCat" Name="ParentID" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="txtTitle" Name="CategoryName" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="Descriptions" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="hdnID" Name="CatID" PropertyName="Value" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>


    </div>

   </div>
</asp:Content>
