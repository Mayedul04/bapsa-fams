﻿
Imports System.Windows.Forms

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (divDelete.Visible = True) Then
                divDelete.Visible = False
            End If

        End If

    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        CatID, CategoryName, Code, Descriptions, ParentID, Status  FROM   Category where CatID=@CatID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("CatID", Data.SqlDbType.Int)
        cmd.Parameters("CatID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("CatID").ToString()
            txtTitle.Text = reader("CategoryName").ToString()
            txtCode.Text = reader("Code").ToString()

            txtDetails.Text = reader("Descriptions").ToString()

            ddlCat.SelectedValue = reader("ParentID").ToString()

            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub


    Private Sub DeleteRecordByID(id As Integer)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim categorycount As Integer = 0
        Dim assetcount As Integer = 0
        Dim selectString = "SELECT  Count(CatID)  FROM   Category where ParentID=@ParentID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ParentID", Data.SqlDbType.Int)
        cmd.Parameters("ParentID").Value = id
        categorycount = cmd.ExecuteScalar()
        Dim selectString1 = "SELECT  Count(ItemID)  FROM   Assets where Category=@Category "
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmd1.Parameters.Add("Category", Data.SqlDbType.Int)
        cmd1.Parameters("Category").Value = id
        assetcount = cmd1.ExecuteScalar()

        If (categorycount > 0 Or assetcount > 0) Then
            divDelete.Visible = True
        Else
            If sdsObject.Delete() > 0 Then
                divSuccess.Visible = True
                hdnID.Value = ""
                'GridView1.DataBind()
            Else
                divError.Visible = True
            End If

        End If
        conn.Close()
    End Sub
    'Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
    '    If (e.CommandName = "Edit") Then


    '        Dim categoryID As Integer = Integer.Parse(e.CommandArgument)
    '        LoadContent(categoryID)

    '    End If
    'End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick

        If (hdnID.Value <> "") Then
            If sdsObject.Update() > 0 Then
                divSuccess.Visible = True
                ' GridView1.DataBind()
                ClearField()
                ddlCat.DataBind()
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
            Else
                divError.Visible = True
            End If

        Else
            If sdsObject.Insert() > 0 Then
                divSuccess.Visible = True
                '  GridView1.DataBind()
                ClearField()
                ddlCat.DataBind()
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
            Else
                divError.Visible = True
            End If

        End If

    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (hdnID.Value <> 0) Then
            DeleteRecordByID(hdnID.Value)
            ClearField()
        End If

    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        txtDetails.Text = ""
        txtCode.Text = ""
        ddlCat.SelectedIndex = 0
        hdnID.Value = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub
End Class
