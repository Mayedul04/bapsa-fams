﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page
    Public permissions As List(Of Integer) = New List(Of Integer)
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (divDelete.Visible = True) Then
                divDelete.Visible = False
            End If

        End If
        GridView1.DataBind()
        getPermission()
    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        CatID, CategoryName, Code, Descriptions, ParentID, Status  FROM   Category where CatID=@CatID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("CatID", Data.SqlDbType.Int)
        cmd.Parameters("CatID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("CatID").ToString()
            txtTitle.Text = reader("CategoryName").ToString()
            txtCode.Text = reader("Code").ToString()

            txtDetails.Text = reader("Descriptions").ToString()

            ddlCat.SelectedValue = reader("ParentID").ToString()

            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub

    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 6 ' 6 is the Section ID for Category
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    'Private Sub LoadTreeView()
    '    TreeView1.Nodes.Clear()
    '    Dim SuperParent As TreeNode = New TreeNode()
    '    SuperParent.Text = "Categories"
    '    SuperParent.Tag = 0
    '    'TreeView1.DataBind()
    '    Dim PrSet As DataSet = PDataset("Select CatID,CategoryName from Category where ParentID=1 and Status=1")
    '    For Each dr As DataRow In PrSet.Tables(0).Rows
    '        Dim maincat As TreeNode = New TreeNode()
    '        maincat.Text = dr("CategoryName").ToString()
    '        maincat.Tag = dr("CatID").ToString()

    '        SuperParent.Nodes.Add(maincat)
    '        fill_Tree(Integer.Parse(maincat.Tag.ToString()))
    '    Next
    '    TreeView1.ExpandAll()
    'End Sub
    'Private Sub fill_Tree(parentid As Integer)
    '    Dim PrSet As DataSet = PDataset("Select CatID,CategoryName from Category where  ParentID=" & parentid & " and Status=1")
    '    For Each dr As DataRow In PrSet.Tables(0).Rows
    '        Dim tnParent As TreeNode = New TreeNode()
    '        tnParent.Text = dr("CategoryName").ToString()
    '        tnParent.Tag = dr("CatID").ToString()
    '        tnParent.ImageIndex = 0
    '        'TreeView1.Nodes.Add(tnParent)
    '        FillChild(tnParent, tnParent.Tag.ToString())
    '    Next
    'End Sub

    'Public Sub FillChild(parent As TreeNode, ParentId As String)
    '    Dim ds As DataSet = PDataset("Select  CatID,CategoryName from Category where  ParentID=" & ParentId & " and Status=1")
    '    For Each dr As DataRow In ds.Tables(0).Rows
    '        Dim child As TreeNode = New TreeNode()
    '        child.Text = dr("CategoryName").ToString().Trim()
    '        child.Tag = dr("CatID").ToString().Trim()
    '        child.ImageIndex = 0
    '        parent.Nodes.Add(child)
    '        FillChild(child, child.Tag.ToString())
    '    Next
    'End Sub

    Protected Function PDataset(Select_Statement As String) As DataSet
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        Dim ad As SqlDataAdapter = New SqlDataAdapter(Select_Statement, conn)
        Dim ds As DataSet = New DataSet()
        ad.Fill(ds)
        Return ds
    End Function
    Private Sub DeleteRecordByID(id As Integer)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim categorycount As Integer = 0
        Dim assetcount As Integer = 0
        Dim selectString = "SELECT  Count(CatID)  FROM   Category where ParentID=@ParentID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ParentID", Data.SqlDbType.Int)
        cmd.Parameters("ParentID").Value = id
        categorycount = cmd.ExecuteScalar()
        Dim selectString1 = "SELECT  Count(ItemID)  FROM   Assets where Category=@Category "
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmd1.Parameters.Add("Category", Data.SqlDbType.Int)
        cmd1.Parameters("Category").Value = id
        assetcount = cmd1.ExecuteScalar()

        If (categorycount > 0 Or assetcount > 0) Then
            divDelete.Visible = True
        Else
            If sdsObject.Delete() > 0 Then
                divSuccess.Visible = True
                hdnID.Value = ""
                ' GridView1.DataBind()
            Else
                divError.Visible = True
            End If

        End If
        conn.Close()
    End Sub
    Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
        If (e.CommandName = "Edit") Then


            Dim categoryID As Integer = Integer.Parse(e.CommandArgument)
            LoadContent(categoryID)

        End If
    End Sub
    Private Function IsDuplicate(ByVal parentid As Integer) As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  CatID  FROM   Category where ParentID=@ParentID and (CategoryName=@Name or Code=@Code)"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ParentID", Data.SqlDbType.Int).Value = parentid
        cmd.Parameters.Add("Name", Data.SqlDbType.NVarChar, 50).Value = txtTitle.Text.Trim
        cmd.Parameters.Add("Code", Data.SqlDbType.NVarChar, 50).Value = txtCode.Text.Trim

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            conn.Close()
            Return True
        Else
            conn.Close()
            Return False
        End If
    End Function
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick

        If (hdnID.Value <> "") Then
            If (permissions.Contains(2)) Then
                If sdsObject.Update() > 0 Then
                    divSuccess.Visible = True
                    'GridView1.DataBind()
                    ClearField()
                    ddlCat.DataBind()
                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Alter"
                divError.Visible = True
            End If


        Else
            If (permissions.Contains(1)) Then
                If (IsDuplicate(ddlCat.SelectedValue) = False) Then
                    If sdsObject.Insert() > 0 Then
                        divSuccess.Visible = True
                        ' GridView1.DataBind()
                        ClearField()
                        ddlCat.DataBind()
                        btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                    Else
                        divError.Visible = True
                    End If
                Else
                    lblErrMessage.Text = "This Category already Exist."
                    divError.Visible = True
                End If

            Else
                lblErrMessage.Text = "You do not have Permission to Add new Item"
                divError.Visible = True
            End If


        End If

    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (permissions.Contains(3)) Then
            If (hdnID.Value <> 0) Then
                DeleteRecordByID(hdnID.Value)
                ClearField()
            Else
                lblErrMessage.Text = "Please select a item to Delete"
                divError.Visible = True
            End If
        Else
            lblErrMessage.Text = "You do not have Permission to Delete"
            divError.Visible = True
        End If
    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        txtDetails.Text = ""
        txtCode.Text = ""
        ddlCat.SelectedIndex = 0
        hdnID.Value = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub

    'Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
    '    If (txtAutoSearch.Text.Contains(":")) Then
    '        Dim split = txtAutoSearch.Text.Split(":")
    '        Dim name1 As String = split(0)
    '        Dim id As Integer = split(1)
    '        hdnID.Value = id
    '        LoadContent(hdnID.Value)
    '    Else
    '        lblErrMessage.Text = "Please select a Valid item from Suggested List"
    '        divError.Visible = True
    '    End If

    'End Sub
    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged
        If (ddlCategory.SelectedIndex <> 0) Then
            hdnID.Value = ddlCategory.SelectedValue
            LoadContent(hdnID.Value)
        Else
            ClearField()
        End If

    End Sub
    Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
        hdnID.Value = Request.Form(hdnCatId.UniqueID)
        LoadContent(hdnID.Value)
    End Sub
End Class
