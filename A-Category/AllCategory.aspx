﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllCategory.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        /* popup_box DIV-Styles*/
        .divSuccess {
            display: none; /* Hide the DIV */
            position: fixed;
            _position: absolute; /* hack for internet explorer 6 */
            height: 300px;
            width: 600px;
            background: #FFFFFF;
            left: 300px;
            top: 150px;
            z-index: 100; /* Layering ( on-top of others), if you have lots of layers: I just maximized, you can change it yourself */
            margin-left: 15px;
            /* additional features, can be omitted */
            border: 2px solid #ff0000;
            padding: 15px;
            font-size: 15px;
            -moz-box-shadow: 0 0 5px #ff0000;
            -webkit-box-shadow: 0 0 5px #ff0000;
            box-shadow: 0 0 5px #ff0000;
        }

        #container {
            background: #d2d2d2; /*Sample*/
            width: 100%;
            height: 100%;
        }

        a {
            cursor: pointer;
            text-decoration: none;
        }

        /* This is for the positioning of the Close Link */
        #popupBoxClose {
            font-size: 20px;
            line-height: 15px;
            right: 5px;
            top: 5px;
            position: absolute;
            color: #6fa5e2;
            font-weight: 500;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="span5" style="margin-left: 0px; margin-top: 5px;">
        <div class="btn-group">
            <script type="text/javascript" language="javascript">
                function OnContactSelected(source, eventArgs) {
                    var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                    document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();

                }
            </script>

        </div>
        <div class="success-details" id="divSuccess" visible="false" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
            <a id="popupBoxClose">Close</a>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divDelete" visible="false" runat="server">
            <asp:Label ID="Label1" runat="server" Text="This Category may have Assets. Please Delete the Depedencies first"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="span12 form-top-strip">
            <p>Category Form</p>
        </div>
        <div class="well">

            <div id="myTabContent" class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        Search Category
                    </div>
                    <%--<label>Search by Category Name</label>--%>
                    <asp:HiddenField ID="hdnCatId" runat="server" />
                    <div class="right_div">
                        <asp:TextBox ID="txtAutoSearch" runat="server"></asp:TextBox>
                        <%--<asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                            onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
                        <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                            MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetCategoryList"
                            ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                            CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                            OnClientItemSelected="OnContactSelected">
                        </cc1:AutoCompleteExtender>--%>
                        <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true" Width="100%"
                            AppendDataBoundItems="true" DataSourceID="sdscat"
                            DataTextField="CategoryName" DataValueField="CatID">
                            <asp:ListItem Value="">Select Category</asp:ListItem>

                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdscat" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [CatID], [CategoryName] FROM [Category] where CatID>1"></asp:SqlDataSource>
                    </div>

                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Parent Category:</label>
                    </div>
                    <div class="right_div">

                        <asp:DropDownList ID="ddlCat" runat="server" DataSourceID="sdsParent" CssClass="input-large" Width="100%"
                            DataTextField="CategoryName" DataValueField="CatID">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsParent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </div>
                
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Category Name :</label>
                                <asp:HiddenField ID="hdnID" runat="server" Value="" />
                            </div>
                            <div class="right_div">
                                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" MaxLength="400" Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Code :</label>
                            </div>
                            <div class="right_div">

                                <asp:TextBox ID="txtCode" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>

                        </div>

                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Descriptions :</label>
                            </div>
                            <div class="right_div">

                                <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine" CssClass="input-large" Width="100%"
                                    Rows="4"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                                    runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                                    ErrorMessage="*" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                            </div>
                        </div>


                        <div class="span12">
                            <div class="left_div">
                            </div>
                            <div class="right_div">
                                <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                            </div>
                        </div>
                    
                <div class="span12">
                    <div class="span4" style="width: 33%">
                        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                    </div>
                    <div class="span4" style="width: 33%">
                        <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                    </div>
                    <div class="span4" style="width: 33%">
                        <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                    </div>
                </div>

            </div>



            <asp:SqlDataSource ID="sdsObject" runat="server"
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [Category] WHERE [CatID] = @CatID"
                InsertCommand="INSERT INTO [Category] ([ParentID], [CategoryName], [Code], [Descriptions], [Status]) VALUES (@ParentID, @CategoryName, @Code, @Descriptions, @Status)"
                SelectCommand="SELECT * FROM [Category]"
                UpdateCommand="UPDATE [Category] SET [ParentID] = @ParentID, [CategoryName] = @CategoryName, [Code] = @Code, [Descriptions] = @Descriptions, [Status] = @Status WHERE [CatID] = @CatID">
                <DeleteParameters>
                    <asp:ControlParameter ControlID="hdnID" Name="CatID" PropertyName="Value" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="ddlCat" Name="ParentID" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="txtTitle" Name="CategoryName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="Descriptions" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="ddlCat" Name="ParentID" PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="txtTitle" Name="CategoryName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="Descriptions" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnID" Name="CatID" PropertyName="Value" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>


        </div>
    </div>
    <div class="span7">
        <div class="span12 form-top-strip" style="margin-top: 3px; width: 100%;">
            <p>Categories</p>
        </div>
        <div class="well" style="height: 269px; overflow-y: scroll; padding-top: 0px; display: block;">
            <table class="table">
                <thead>
                    <tr>

                        <th style="width: 200px">Category
                        </th>
                        <th style="width: 200px">Parent Category
                        </th>

                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="CatID" DataSourceID="sdsCatList">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <%-- <td>
                                    <asp:Label ID="ID" runat="server" Text='<%# Eval("CatID") %>' />
                                </td>--%>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("CategoryName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Parent" runat="server" Text='<%# Eval("Parent") %>' />
                                </td>
                                <%--<td>
                                    <asp:Label ID="Code" runat="server" Text='<%# Eval("Code") %>' />
                                </td>

                                <td>
                                    <asp:Label ID="Desc" runat="server" Text='<%# Eval("Descriptions") %>' />
                                </td>--%>
                                <td>
                                    <asp:LinkButton ID="cmdEdit" CssClass="btn btn-success" CommandName="Edit" runat="server"
                                        CommandArgument='<%# Eval("CatID") %>' Text="Edit" />

                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <asp:SqlDataSource ID="sdsCatList" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT Category_1.CategoryName AS Parent, Category.CatID, Category.ParentID, Category.Code, Category.CategoryName, Category.Descriptions, Category.Status FROM Category INNER JOIN Category AS Category_1 ON Category.ParentID = Category_1.CatID">
            <DeleteParameters>
                <asp:Parameter Name="CatID" />
            </DeleteParameters>

        </asp:SqlDataSource>


        <%--<div id="container"> <!-- Main Page -->
    <h1>sample</h1>
</div>--%>
    </div>
    <%--<asp:TreeView ID="TreeView1" runat="server"></asp:TreeView>--%>
    <script src="../lib/jquery-1.8.1.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"
        type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
        type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
        rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#popupBoxClose').click(function () {
                unloadPopupBox();
            });

            $('#container').click(function () {
                loadPopupBox();
            });

        });


        function success() {

            $('.success-details').popover();
        }

        function unloadPopupBox() {    // TO Unload the Popupbox
            $('#divSuccess').fadeOut("slow");
            $("#container").css({ // this is just for style        
                "opacity": "1"
            });
        }

        function loadPopupBox() {    // To Load the Popupbox
            $('#divSuccess').fadeIn("slow");
            $("#container").css({ // this is just for style
                "opacity": "0.3"
            });
        }

        $("#<%=txtAutoSearch.ClientID %>").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<%=ResolveUrl("AutoComplete.asmx/GetCategoryList") %>',
                    data: "{ 'prefixText': '" + request.term + "'}",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        response($.map(data.d, function (item) {
                            return {
                                label: item.split('-')[0],
                                val: item.split('-')[1]
                            }
                        }))
                    },
                    error: function (response) {
                        alert(response.responseText);
                    },
                    failure: function (response) {
                        alert(response.responseText);
                    }
                });
            },
            select: function (e, i) {
                $("#<%=hdnCatId.ClientID %>").val(i.item.val);
                alert(i.item.val);
            },
            minLength: 1
        });
    </script>

</asp:Content>
