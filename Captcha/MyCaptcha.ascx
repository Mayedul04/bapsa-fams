﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyCaptcha.ascx.cs" Inherits="MyCaptcha"  EnableViewState="true"%>

<asp:Image ID="ImgCaptcha" BackColor="White"  Width="100px" Height ="30px" runat="server" />

<asp:ImageButton ID="btnTryNewWords" runat="server"  ImageUrl="tryNew.png" 
    onclick="btnTryNewWords_Click" ValidationGroup="tryNewWord" />
<asp:TextBox ID="TxtCpatcha" runat="server"  placeholder="Enter the code shown on Left" CssClass ="form-control" Text=""></asp:TextBox>
<asp:RequiredFieldValidator Display="Dynamic" ID="reqCaptchaValidator"   runat="server"
ErrorMessage="* Please type the word" ControlToValidate="TxtCpatcha" ForeColor="Red"></asp:RequiredFieldValidator>
                                        
