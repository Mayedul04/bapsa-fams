﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Transfer.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Assets Transfer</h1>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="Two Group can not be same"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->


    <div class="span3" style="margin-left:0px;">
        <asp:DropDownList ID="ddlPrimary" runat="server" AutoPostBack="true" CssClass="input-large modelchange">

            <asp:ListItem Value="1">Branch to Branch</asp:ListItem>
            <asp:ListItem Value="2">Employee to Employee</asp:ListItem>
        </asp:DropDownList>
        <asp:HiddenField ID="hdnTID" runat="server" />
        <asp:HiddenField ID="hdnSourceID" runat="server" />
        <asp:HiddenField ID="hdnDestID" runat="server" />
        <asp:HiddenField ID="hdnDeptS" runat="server" />
        <asp:HiddenField ID="hdnDeptDest" runat="server" />
        <asp:HiddenField ID="hdnSourceUserID" runat="server" />
        <asp:HiddenField ID="hdnDestUserID" runat="server" />
    </div>
    <div class="span3">
        <button runat="server" id="btnReport" validationgroup="form" class="btn btn-primary"><i class="icon-search"></i>&nbsp;Transfer History</button>
    </div>

    <div class="span12">
        <asp:UpdatePanel ID="upg" runat="server">
            <ContentTemplate>
                <div class="span5">
                    <div class="span12 form-top-strip" style="margin-left: 0px;">
                        <p>Asset Source</p>
                    </div>
                    

                        <div class="well">
                            <div class="left_div">
                                <label>Select Source</label>
                            </div>
                            <div class="right_div">

                                <asp:DropDownList ID="ddlSource" runat="server" CssClass="input-large" AutoPostBack="true"
                                    DataSourceID="sdsSource" DataTextField="SourceName" DataValueField="SourceID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [LocID] As SourceID, [LocationName] as SourceName FROM [Location] WHERE ([Status] = @Status)">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                            <div class="left_div">
                                <label>Select Category</label>
                            </div>
                            <div class="right_div">

                                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="input-large" AutoPostBack="true"
                                    DataSourceID="sdsCategory" DataTextField="CategoryName" DataValueField="CatID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsCategory" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and CatID>1 and ParentID<>1">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width:120px">Asset
                                        </th>
                                        
                                        <th  style="width:10px;">Select
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="ItemID" DataSourceID="sdsSourceAssets">
                                        <EmptyDataTemplate>
                                            <table runat="server" style="">
                                                <tr>
                                                    <td>No data was returned.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <ItemTemplate>
                                            <tr style="">
                                                <%--<td>
                                                    <asp:Label ID="ID" runat="server" Text='<%# Eval("ItemName") %>' />
                                                    
                                                </td>--%>
                                                <td>
                                                    <asp:Label ID="Parent" runat="server" Text='<%# Eval("ItemCode") %>' />
                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("ItemID") %>' />
                                                </td>

                                                <td>
                                                    <asp:CheckBox ID="transfer" runat="server" Checked="false" />
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>

                                        </LayoutTemplate>
                                    </asp:ListView>
                                </tbody>
                            </table>
                        </div>
                        <asp:SqlDataSource ID="sdsSourceAssets" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [ItemID], [ItemName], [ItemCode], [Location], [Supplier], [Department], [AssetUser] FROM [Assets] WHERE ([Location] = @Location and Category=@Category and Status='Active')">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hdnSourceID" Name="Location" PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="ddlCategory" Name="Category" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>

                        </asp:SqlDataSource>
                    </div>
                



                <div class="span2" style="margin-top:42px;">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">Transfer&nbsp;<i class="icon-fast-forward"></i></button>
                </div>
                <div class="span5">
                    <div class="span12 form-top-strip" style="margin-left: 0px;">
                        <p>Asset Destination</p>
                    </div>
                  

                        <div class="well">
                            <div class="left_div">
                                <label>Select Destination</label>
                            </div>
                            <div class="right_div">

                                <asp:DropDownList ID="ddlDestination" runat="server" CssClass="input-large" DataSourceID="sdsDest" AutoPostBack="true"
                                    DataTextField="DestinationName" DataValueField="DestID">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsDest" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [LocID] as DestID, [LocationName] as DestinationName FROM [Location] WHERE (([Status] = @Status) AND ([LocID] &lt;&gt; @SourceID))">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                        <asp:ControlParameter ControlID="hdnSourceID" Name="SourceID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </div>


                            <table class="table">
                                <thead>
                                    <tr>
                                       
                                        <th style="width:400px">Assets
                                        </th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:ListView ID="ListView1" runat="server" DataKeyNames="ItemID" DataSourceID="sdsAssetDest">
                                        <EmptyDataTemplate>
                                            <table runat="server" style="">
                                                <tr>
                                                    <td>No data was returned.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <ItemTemplate>
                                            <tr style="">
                                                <%--<td>
                                                    <asp:Label ID="ID" runat="server" Text='<%# Eval("ItemName") %>' />
                                                </td>--%>
                                                <td>
                                                    <asp:Label ID="Parent" runat="server" Text='<%# Eval("ItemCode") %>' />
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                        <LayoutTemplate>
                                            <tr id="itemPlaceholder" runat="server">
                                            </tr>

                                        </LayoutTemplate>
                                    </asp:ListView>
                                </tbody>
                            </table>
                        </div>
                        <asp:SqlDataSource ID="sdsAssetDest" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [ItemID], [ItemName], [ItemCode], [Location], [Supplier], [Department], [AssetUser] FROM [Assets] WHERE ([Location] = @Location and Category=@Category and Status='Active')">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="hdnDestID" Name="Location" PropertyName="Value" Type="Int32" />
                                <asp:ControlParameter ControlID="ddlCategory" Name="Category" PropertyName="SelectedValue" Type="Int32" />
                            </SelectParameters>

                        </asp:SqlDataSource>
                   
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>



    <asp:HiddenField ID="hdndate" runat="server" />
    <asp:SqlDataSource ID="sdstransferTracking" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [TransferTracking] ([TransferType], [SourceID], [DestinationID], [TransferedAsset], [TransDate]) VALUES (@TransferType, @SourceID, @DestinationID, @TransferedAsset, @TransDate)">

        <InsertParameters>
            <asp:Parameter DefaultValue="Location" Name="TransferType" Type="String" />
            <asp:ControlParameter ControlID="hdnSourceID" DefaultValue="" Name="SourceID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnDestID" Name="DestinationID" PropertyName="Value" Type="Int32" />
            <asp:Parameter Name="TransferedAsset" Type="String" DefaultValue="" />
            <asp:ControlParameter ControlID="hdndate" DbType="Date" Name="TransDate" PropertyName="Value" />
        </InsertParameters>

    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="Script" runat="server">

    <%--<script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_ContentPlaceHolder1_pnlLocationTransfer").show();
            $("#ctl00_ContentPlaceHolder1_pnlUserTransfer").hide();
        });
            $(".modelchange").change(function () {
                var end = this.value;
                if (end == "1")
                {
                    
                    $("#ctl00_ContentPlaceHolder1_hdnTID").val(end);
                    $("#ctl00_ContentPlaceHolder1_pnlLocationTransfer").show();
                    $("#ctl00_ContentPlaceHolder1_pnlUserTransfer").hide();
                }
                else if (end == "2") {
                   
                    $("#ctl00_ContentPlaceHolder1_hdnTID").val(end);
                    $("#ctl00_ContentPlaceHolder1_pnlLocationTransfer").hide();
                    $("#ctl00_ContentPlaceHolder1_pnlUserTransfer").show();
                }
                
            });
        </script>--%>
</asp:Content>
