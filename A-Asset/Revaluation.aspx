﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Revaluation.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="span5" style="margin-left: 0px; margin-top: 5px;">
        <div class="well-border">
            <div class="tab-content">
                <div class="success-details" visible="false" id="divSuccess" runat="server">
                    <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                    <div class="corners">
                        <span class="success-left-top"></span><span class="success-right-top"></span><span
                            class="success-left-bot"></span><span class="success-right-bot"></span>
                    </div>
                </div>
                <div class="error-details" id="divError" visible="false" runat="server">
                    <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                    <div class="corners">
                        <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
                    </div>
                </div>
                <%--<div class="btn-group">
            <script type="text/javascript" language="javascript">
                function OnContactSelected(source, eventArgs) {
                    var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                    document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();

                }
            </script>
            <label>Search by Asset Name or Code</label>
            <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
            <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetFullList"
                ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                OnClientItemSelected="OnContactSelected">
            </cc1:AutoCompleteExtender>
        </div>--%>
                <div class="span12">
                    <asp:DropDownList ID="ddlBranch" runat="server" Width="100%" DataSourceID="sdsLoc" CssClass="input-large"
                        DataTextField="LocationName" DataValueField="LocID" AppendDataBoundItems="true" TabIndex="0">
                        <asp:ListItem Value="">Search in Branch</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsLoc" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [LocID], [LocationName] FROM [Location] WHERE ([Status] = @Status)">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span12">
                    <asp:DropDownList ID="ddlCategory" runat="server" Width="100%" DataSourceID="sdsParent" CssClass="input-large" AutoPostBack="true"
                        DataTextField="CategoryName" DataValueField="CatID" TabIndex="0" AppendDataBoundItems="true">
                        <asp:ListItem Value="">Select Category</asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnSearchCat" runat="server" />
                    <asp:SqlDataSource ID="sdsParent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and CatID>1 and ParentID<>1">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span12">
                    <asp:DropDownList ID="ddlAssets" runat="server" Width="100%" DataSourceID="sdsSearchAsset" CssClass="input-large" AutoPostBack="true"
                        DataTextField="ItemCode" DataValueField="ItemID" TabIndex="0" AppendDataBoundItems="false">
                        <asp:ListItem Value="">Select Asset</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsSearchAsset" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT Distinct Assets.[ItemCode], Assets.[ItemID] FROM YearlyJournal inner join Assets on Assets.ItemID=YearlyJournal.ItemID where  (([Status] = @Status) AND (Assets.[Location] = @Location) AND (Assets.[Category] = @Category))">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="Active" Name="Status" Type="String" />
                            <asp:ControlParameter ControlID="ddlBranch" Name="Location" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlCategory" Name="Category" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
        </div>
        <div class="span12 form-top-strip">
            <p>Revaluation Form</p>
        </div>
        <div class="well">
            <div class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Asset Name :</label>
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:HiddenField ID="hdnCatID" runat="server" Value="" />
                        <asp:HiddenField ID="hdnRevalSurplus" runat="server" Value="" />
                        <asp:HiddenField ID="hdnIsRevaluate" runat="server" Value="" />
                        <asp:HiddenField ID="hdnDate" runat="server" Value="" />
                        <asp:HiddenField ID="hdnCurrentClosingDepre" runat="server" Value="" />
                        <asp:HiddenField ID="hdnCurentClosing" runat="server" Value="" />
                        <asp:HiddenField ID="hdnNewWDV" runat="server" Value="" />
                        <asp:HiddenField ID="hdnNewOpening" runat="server" Value="" />
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" ReadOnly="true" MaxLength="400" Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Code :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCode" runat="server" ReadOnly="true" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Closing Cost :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCClosingCost" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Depreciation :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCDepreciation" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12" runat="server" id="pnlRevSurplus" visible="false">
                    <div class="left_div">
                        <label>
                            Revaluation Surplus:</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtRevSurplus" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>
                        <asp:HiddenField ID="hdnCurrentRevSurplus" runat="server" />
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Dep Rate :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCRate" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current WDV :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCWDF" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12" runat="server" id="pnlRevValue">
                    <div class="left_div">
                        <label>
                            Set New WDV :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtRevValue" runat="server" CssClass="input-large" Width="100%" TabIndex="3"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtRevValue" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtRevValue" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12" runat="server" id="pnlRevRate" visible="false">
                    <div class="left_div">
                        <label>
                            New Dep Rate :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtNDepreRate" runat="server" CssClass="input-large" Width="100%" TabIndex="4"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtNDepreRate" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>

                    </div>
                </div>
<div class="span12">
                <div class="span5" style="width:50%">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary" tabindex="5"><i class="icon-save"></i>&nbsp;Save</button>
                </div>

                <div class="span5" style="width:50%">
                    <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh" tabindex="7"></i>&nbsp;Clear</button>
                </div>
</div>

            </div>
        </div>
    </div>
    <div class="span5" style="display: none;">
        <div class="well">
            <div class="tab-content">
                <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>--%>


                <div class="span12" style="display: none;">

                    <div class="left_div">
                        <label>
                            New Opening Cost :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtRevOpening" runat="server" CssClass="input-large" Width="100%" TabIndex="1"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtRevOpening" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Enabled="false" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtRevOpening" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12" style="display: none;">
                    <div class="left_div">
                        <label>
                            New Opening Deprecaition :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtNewOpeningDepre" runat="server" CssClass="input-large" Width="100%" TabIndex="2"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtNewOpeningDepre" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Enabled="false" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtNewOpeningDepre" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>


                <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
            </div>
        </div>
    </div>


    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [CatID], [ItemName], [ClosingDepreciation], [ClosingCost], [CalculatedDate], [IsRevaluated]) VALUES (@ItemID, @CatID, @ItemName, @ClosingDepreciation, @ClosingCost, @CalculatedDate, @IsRevaluated)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnCatID" Name="CatID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnCurrentClosingDepre" Name="ClosingDepreciation" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="txtCWDF" Name="ClosingCost" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="CalculatedDate" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnIsRevaluate" Name="IsRevaluated" PropertyName="Value" DefaultValue="False" Type="Boolean" />
        </InsertParameters>

    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sdsObjectRevaluation" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [CatID], [ItemName], [ClosingDepreciation], [ClosingCost], [CalculatedDate], [IsRevaluated]) VALUES (@ItemID, @CatID, @ItemName, @ClosingDepreciation, @ClosingCost, @CalculatedDate, @IsRevaluated)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnCatID" Name="CatID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnCurrentClosingDepre" Name="ClosingDepreciation" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnNewOpening" Name="ClosingCost" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="CalculatedDate" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnIsRevaluate" Name="IsRevaluated" PropertyName="Value" DefaultValue="True" Type="Boolean" />
        </InsertParameters>

    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsAssets" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        UpdateCommand="UPDATE [Assets] SET [DepreciationRate] = @DepreciationRate WHERE [ItemID] = @ItemID">

        <UpdateParameters>
            <asp:ControlParameter ControlID="txtNDepreRate" Name="DepreciationRate" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
        </UpdateParameters>

    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsRevaluation" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [RevaluationTracking] ([ItemID], [RevaluationSarplus], [RavaluationDate]) VALUES (@ItemID, @RevaluationSarplus, @RavaluationDate)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnRevalSurplus" Name="RevaluationSarplus" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="RavaluationDate" PropertyName="Value" />
        </InsertParameters>

    </asp:SqlDataSource>
</asp:Content>
