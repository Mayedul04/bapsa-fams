﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="FixedAssets.aspx.vb" Inherits="Admin_A_HTML_HTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--<h1 class="page-title">Assets</h1>--%>
    <div class="span10" style="margin-left:0px;">
        <%--<div class="btn-group">
            <script type="text/javascript" language="javascript">
                function OnContactSelected(source, eventArgs) {
                    var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                    document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();

                }
            </script>
            <label>Search by Assset Name or Code</label>
            <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
            <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetCompletionList"
                ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                OnClientItemSelected="OnContactSelected">
            </cc1:AutoCompleteExtender>
        </div>--%>
        
                <div class="span4">
                    <asp:DropDownList ID="ddlBranch" runat="server" DataSourceID="sdsLoc" CssClass="input-large"
                        DataTextField="LocationName" DataValueField="LocID" AppendDataBoundItems="true" TabIndex="0">
                        <asp:ListItem Value="">Search in Branch</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="span4">
                    <asp:DropDownList ID="ddlCategory" runat="server" DataSourceID="sdsParent" CssClass="input-large" AutoPostBack="true"
                        DataTextField="CategoryName" DataValueField="CatID" TabIndex="0" AppendDataBoundItems="true">
                        <asp:ListItem Value="">Select Category</asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnSearchCat" runat="server" />
                    <asp:SqlDataSource ID="sds" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and CatID>1 and ParentID<>1">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span4">
                    <asp:DropDownList ID="ddlAssets" runat="server" DataSourceID="sdsSearchAsset" CssClass="input-large" AutoPostBack="true"
                        DataTextField="ItemCode" DataValueField="ItemID" TabIndex="0" AppendDataBoundItems="false">
                        <asp:ListItem Value="">Select Asset</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsSearchAsset" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT [ItemCode], [ItemID] FROM [Assets] WHERE (([Status] = @Status) AND ([Location] = @Location) AND ([Category] = @Category))">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="Active" Name="Status" Type="String" />
                            <asp:ControlParameter ControlID="ddlBranch" Name="Location" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlCategory" Name="Category" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
           

        <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
    </div>
    <div class="span10 form-top-strip" style="margin-left:0px;">
            <p>Asset Entry Form</p>
    </div>
    <!-- asset code generation -->
    <div class="span10" style="margin-left:0px;">
        <div class="well">
    <div class="span6">
        <div class="well-border">
            <div class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Asset Name :</label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" Width="100%" MaxLength="400" TabIndex="1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                  Main Category :</label>
                            </div>
                            <div class="right_div">

                                <asp:DropDownList ID="ddlParent" runat="server" DataSourceID="sdsmaincat" CssClass="input-large" AutoPostBack="true"
                                    DataTextField="CategoryName" DataValueField="CatID" TabIndex="2">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsmaincat" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and  ParentID=1">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="ddlParent" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Category :</label>
                            </div>
                            <div class="right_div">

                                <asp:DropDownList ID="ddlCat" runat="server" DataSourceID="sdsParent" CssClass="input-large" AutoPostBack="false"
                                    DataTextField="CategoryName" DataValueField="CatID" TabIndex="2">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsParent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and CatID>1 and ParentID=@ParentID">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />

                                        <asp:ControlParameter ControlID="ddlParent" DefaultValue="" Name="ParentID" PropertyName="SelectedValue" />

                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="ddlCat" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <a href="<%= domainName & "CategoryEdit" %>" class="cms fancybox.iframe" tabindex="126">
                                    <button class="btn-primary" tabindex="116"><i class="icon-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Project :</label>
                            </div>
                            <div class="right_div">

                                <asp:DropDownList ID="ddlProject" runat="server" DataSourceID="sdsPro" CssClass="input-large" AutoPostBack="false"
                                    DataTextField="ProjectName" DataValueField="ProjID" TabIndex="3">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsPro" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [ProjID], [ProjectName] FROM [Projects] WHERE ([Status] = @Status)">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="ddlLocation" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <a href="<%= domainName & "ProjectEdit" %>" class="cms fancybox.iframe" tabindex="127">
                                    <button class="btn-primary" tabindex="117"><i class="icon-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Location :</label>
                            </div>
                            <div class="right_div">

                                <asp:DropDownList ID="ddlLocation" runat="server" DataSourceID="sdsLoc" CssClass="input-large" AutoPostBack="false"
                                    DataTextField="LocationName" DataValueField="LocID" TabIndex="4">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsLoc" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [LocID], [LocationName] FROM [Location] WHERE ([Status] = @Status)">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="ddlLocation" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <a href="<%= domainName & "LocationEdit" %>" class="cms fancybox.iframe" tabindex="127">
                                    <button class="btn-primary" tabindex="117"><i class="icon-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Code :
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" Display="Dynamic"
                            runat="server" ControlToValidate="txtCode" ValidationExpression="^[\s\S\w\W\d\D]{0,25}$"
                            ErrorMessage=" too long" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                </label>
                            </div>
                            <div class="right_div">

                                <asp:TextBox ID="txtCode" runat="server" CssClass="input-large" MaxLength="400" TabIndex="150"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="txtCode" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <!-- asset allocation -->
    <div class="span6 second-child adjust-height" <%--style="margin-bottom:10px; height:210px; margin-left:27px; width:48%;"--%>>
        <div class="well-border">
            <div class="tab-content">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Department  :</label>
                            </div>
                            <div class="right_div">
                                <asp:DropDownList ID="ddlDept" runat="server" DataSourceID="sdsDept" CssClass="input-large" AppendDataBoundItems="True"
                                    DataTextField="Department" DataValueField="DeptID" AutoPostBack="True" TabIndex="5">
                                    <asp:ListItem Value="">Please Select</asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsDept" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT [DeptID], [Department] FROM [Department] WHERE ([Status] = @Status) and DeptID>1">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="ddlDept" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <a href="<%= domainName & "DepartmentEdit" %>" class="cms fancybox.iframe" tabindex="129">
                                    <button class="btn-primary" tabindex="119"><i class="icon-plus"></i></button>
                                </a>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    User  :</label>
                            </div>
                            <div class="right_div">
                                <asp:DropDownList ID="ddlUser" runat="server" DataSourceID="sdsUser" CssClass="input-large"
                                    DataTextField="EmployeeName" DataValueField="UID" TabIndex="6">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="sdsUser" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    SelectCommand="SELECT DISTINCT UID, EmployeeName FROM Employees WHERE (Status = @Status) AND (DepartmentID = @DepartmentID)">
                                    <SelectParameters>
                                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                        <asp:ControlParameter ControlID="ddlDept" Name="DepartmentID" Type="Int32" PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>

                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="span12">

                    <div class="left_div">
                        <label>
                            Supplier :</label>
                    </div>
                    <div class="right_div">

                        <asp:DropDownList ID="ddlSupplier" runat="server" DataSourceID="sdsSupplier" CssClass="input-large" AppendDataBoundItems="true"
                            DataTextField="Supplier" DataValueField="SupID" TabIndex="7">
                            <asp:ListItem Value="">Please Select</asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsSupplier" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [SupID], [Supplier] FROM [Supplier] WHERE ([Status] = @Status)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="ddlSupplier" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <a href="<%= domainName & "SupplierEdit" %>" class="cms fancybox.iframe" tabindex="128">
                            <button class="btn-primary" tabindex="118"><i class="icon-plus"></i></button>
                        </a>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Service  Type :</label>
                    </div>
                    <div class="right_div">

                        <asp:DropDownList ID="ddlServiceType" runat="server" CssClass="input-large" TabIndex="8">
                            <asp:ListItem Value="">Select Type</asp:ListItem>
                            <asp:ListItem Value="Warrenty">Warrenty</asp:ListItem>
                            <asp:ListItem Value="Garrenty">Garrenty</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="ddlServiceType" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Service Period(Months) :
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtServicePeriod" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtServicePeriod" runat="server" CssClass="input-large" TabIndex="9"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtServicePeriod" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="well-border">
            <div class="tab-content">


                <div class="span12">
                    <div class="left_div">
                        <label>
                            Transaction No :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtTrans" runat="server" CssClass="input-large" TabIndex="10"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTrans" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Depreciation Model:</label>
                    </div>
                    <div class="right_div">

                        <asp:DropDownList ID="ddlDepreModel" runat="server" CssClass="input-large modelchange" TabIndex="11">
                            <asp:ListItem Value="">Select Model</asp:ListItem>
                            <asp:ListItem Value="Declining Balance">Declining Balance</asp:ListItem>
                            <asp:ListItem Value="Straight Line">Straight Line</asp:ListItem>
                            <asp:ListItem Value="Sum of Years">Sum of Year's Digits</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="ddlDepreModel" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12 depre-rate" id="depre-rate">
                    <div class="left_div">
                        <label>
                            Depreciation Rate(%) :
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtDepreRate" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtDepreRate" runat="server" CssClass="input-large" TabIndex="12"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtDepreRate" Enabled="false" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12 scrap-value" id="scrap-value" style="display:none;">
                    <div class="left_div">
                        <label>
                            Estimated Scrap Value:
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtScrapValue" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtScrapValue" runat="server" CssClass="input-large" Text="0" TabIndex="13"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtScrapValue" Enabled="false" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12 life" id="life">
                    <div class="left_div">
                        <label>
                            Estimated Useful Life:
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtLifeLength" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnSumofYear" Value="0" runat="server" />
                        <asp:TextBox ID="txtLifeLength" runat="server" CssClass="input-large" TabIndex="14"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtLifeLength" Enabled="false" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Status :</label>
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="input-large" TabIndex="15">
                            <asp:ListItem Value="Active">Active</asp:ListItem>
                            <asp:ListItem Value="Disposed">Disposed</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Descriptions :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine" CssClass="input-large"
                            Rows="4" TabIndex="16"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                            runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                            ErrorMessage=" *" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span6 second-child" <%--style="width:48%; margin-left:27px;"--%>>
        <div class="well-border">
            <div class="tab-content">
                <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>--%>
                <div class="span12">
                    <asp:HiddenField ID="hdnIncTax" Value="0" runat="server" />
                    <div class="left_div">
                        <asp:CheckBox ID="chkIncTax" CssClass="input-small chkIncTax" runat="server" />
                    </div>
                    <div class="right_div">
                        Include TAX/VAT
                     </div>
                </div>
                <div class="span12" id="basicvalue">
                    <div class="left_div">
                        <asp:Literal ID="ltrValuelabel" Visible="false" runat="server"></asp:Literal>
                        <label>Basic Value :</label>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtBaseValue" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtBaseValue" runat="server" CssClass="input-large" TabIndex="18"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtBaseValue" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>


                <div class="span12" style="display: none">
                    <div class="left_div">
                        <label>
                            Quantity :
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="form" SetFocusOnError="True"
                                    runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtqty" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">
                        <div class="span4">
                            <asp:TextBox ID="txtqty" runat="server" Text="1" CssClass="input-mini" TabIndex="114"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtqty" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                        <div class="span5">
                            <asp:DropDownList ID="ddlQUnit" runat="server" CssClass="input-mini" TabIndex="115">
                                <asp:ListItem Value="pcs">Pcs</asp:ListItem>
                                <asp:ListItem Value="nos">Nos</asp:ListItem>
                                <asp:ListItem Value="set">Set</asp:ListItem>
                                <asp:ListItem Value="box">Box</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Tax(in %) :</label>
                    </div>
                    <div class="right_div">
                        <div class="span4">
                            <asp:TextBox ID="txtTax" runat="server" CssClass="input-mini taxrate" TabIndex="19"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="watertaxrate"  runat="server" TargetControlID="txtTax" WatermarkText="Rate(ex:7)"></cc1:TextBoxWatermarkExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="form" SetFocusOnError="True"
                                runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtTax" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        </div>
                        <div class="span5">
                            <asp:TextBox ID="txtTotalTax" runat="server" ReadOnly="true" CssClass="input-small taxtotal" TabIndex="119"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="wt" runat="server" TargetControlID="txtTotalTax" WatermarkText="Total Tax"></cc1:TextBoxWatermarkExtender>
                        </div>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            VAT(in %) :</label>
                    </div>
                    <div class="right_div">
                        <div class="span4">
                            <asp:TextBox ID="txtVat" runat="server" CssClass="input-mini vaterate" TabIndex="20"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="watervatrate"  runat="server" TargetControlID="txtVat" WatermarkText="Rate(ex: 5)"></cc1:TextBoxWatermarkExtender>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="form" SetFocusOnError="True"
                                runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtVat" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        </div>
                        <div class="span5">
                            <asp:TextBox ID="txtTotalVat" ReadOnly="true" runat="server" CssClass="input-small vatetotal" TabIndex="220"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtTotalVat" WatermarkText="Total Vat"></cc1:TextBoxWatermarkExtender>
                        </div>
                    </div>
                </div>
                <div class="span12" id="totalvalue">
                    <div class="left_div">
                        <asp:Literal ID="ltrBottomHeading" Visible="false" runat="server"></asp:Literal>
                         <label id="totaltext">Total Value :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtTotal" runat="server" ReadOnly="true" CssClass="input-large totalvalue" TabIndex="221"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Purchase Date :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtPurchaseDate" runat="server" CssClass="input-large" TabIndex="21"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtPurchaseDate" runat="server" Format="">
                                </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtPurchaseDate" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <asp:HiddenField ID="hdnBase" runat="server" />
                <asp:HiddenField ID="hdnVat" runat="server" />
                <asp:HiddenField ID="hdnTax" runat="server" />
                <asp:HiddenField ID="hdnTotal" runat="server" />
                <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
            </div>
        </div>

    </div>
    <div class="span6 second-child" <%--style="margin-left:28px; width: 48%;margin-top:10px;"--%>>
        <%--<div class="span2">
                    <a href="<%= domainName  %>AllAssets" data-toggle="modal" class="btn btn-primary"><i class="icon-back"></i>Back</a>
                </div>--%>
        <div class="span4" style="width:33%">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary" tabindex="22"><i class="icon-save"></i>&nbsp;Add New</button>
        </div>
        <div class="span4" style="width:33%">
            <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary" tabindex="23"><i class="icon-trash"></i>&nbsp;Delete</button>
        </div>
        <div class="span4" style="width:33%">
            <button runat="server" id="btnClear" class="btn btn-primary" tabindex="24"><i class="icon-refresh"></i>&nbsp;Clear</button>
        </div>

        <asp:SqlDataSource ID="sdsObject" runat="server"
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Assets] WHERE [ItemID] = @ItemID"
            SelectCommand="SELECT * FROM [Assets]"
            UpdateCommand="UPDATE [Assets] SET [ItemName] = @ItemName, [ItemCode] = @ItemCode, [Category] = @Category, [Location]=@Location,[Project]=@Project, [Details] = @Details, [Quantity] = @Quantity,  [ServiceCategory] = @ServiceCategory, [ServicePeriod] = @ServicePeriod, [OpeningDepreciation]=@OpeningDepreciation,  [Department]=@Department, [Supplier]=@Supplier, [TransactionID]=@TransactionID,[AssetUser]=@AssetUser,[QUnit]=@QUnit WHERE [ItemID] = @ItemID">
            <DeleteParameters>
                <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            </DeleteParameters>

            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtCode" Name="ItemCode" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="ddlCat" Name="Category" PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="txtDetails" Name="Details" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtqty" Name="Quantity" PropertyName="Text" Type="Int32" />

                <asp:ControlParameter ControlID="ddlServiceType" Name="ServiceCategory" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="txtServicePeriod" Name="ServicePeriod" PropertyName="Text" Type="Int32" />
                <asp:Parameter DefaultValue="0" Name="OpeningDepreciation" Type="Single" />

                <asp:ControlParameter ControlID="ddlStatus" Name="Status" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="ddlDept" Name="Department" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlLocation" Name="Location" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlProject" Name="Project" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlSupplier" Name="Supplier" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlUser" Name="AssetUser" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ddlQUnit" Name="QUnit" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="txtTrans" Name="TransactionID" PropertyName="Text" DefaultValue="" />

            </UpdateParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="sdsYearJournal" runat="server"
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [ItemName], [CatID], [ClosingDepreciation], [ClosingCost], [CalculatedDate],[RemainingYear]) VALUES (@ItemID, @ItemName,@CatID, @ClosingDepreciation, @ClosingCost, @CalculatedDate,@RemainingYear)">
            <InsertParameters>
                <asp:Parameter Name="ItemID" DefaultValue="" />
                <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" />
                <asp:ControlParameter ControlID="ddlCat" Name="CatID" PropertyName="SelectedValue" />
                <asp:Parameter Name="ClosingDepreciation" DefaultValue="0"></asp:Parameter>
                <asp:ControlParameter ControlID="hdnTotal" DefaultValue="0" Name="ClosingCost" PropertyName="Value" />
                <asp:Parameter Name="CalculatedDate" DefaultValue="" />
                <asp:ControlParameter ControlID="txtLifeLength" Name="RemainingYear" PropertyName="Text" DefaultValue="0" />
            </InsertParameters>

        </asp:SqlDataSource>
    </div>
        </div>
    </div>
   



    







</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="Script" runat="server">

    <script type="text/javascript">

        $(".modelchange").change(function () {
            var end = this.value;
            if (end == "Declining Balance") {
                $("#scrap-value").hide();
                $("#depre-rate").show();
                $("#life").hide();
            }
            else if (end == "Straight Line") {
                $("#scrap-value").hide();
                $("#depre-rate").show();
                $("#life").hide();
            }
            else if (end == "Sum of Years") {
                $("#scrap-value").hide();
                $("#depre-rate").hide();
                $("#life").show();
            }
        });
        $(".chkIncTax").click(function () {

            var inc = $('#<%= chkIncTax.ClientID%>').is(':checked');
            
            if (inc == 1)
            {
               $('#basicvalue').find('label').text('Total Value :');
               $('#totalvalue').find('label').text('Basic Value :');
            }
            else
            {
                $('#basicvalue').find('label').text('Basic Value :');
                $('#totalvalue').find('label').text('Total Value :');
            }
        });
        $(".vaterate").focusout(function () {
            var inc = $('#<%= chkIncTax.ClientID%>').is(':checked');
            
            if (inc == 1)
            {
                var total = Number($('#ctl00_ContentPlaceHolder1_txtBaseValue').val());
                var tax = Number($('#ctl00_ContentPlaceHolder1_txtTax').val());
                var vat = Number($('#ctl00_ContentPlaceHolder1_txtVat').val());
                
                
                var totalvat = Number((total * vat) / (100 + vat)).toFixed(2);
               var totalaftervat = Number($('#ctl00_ContentPlaceHolder1_txtBaseValue').val())-Number(totalvat);
               var totaltax = Number((totalaftervat * tax) / 100).toFixed(2);
                //var basic = Number(total-totaltax - totalvat).toFixed(2);
               var basic = totalaftervat;
                $("#ctl00_ContentPlaceHolder1_hdnTotal").val(total);
                $("#ctl00_ContentPlaceHolder1_hdnBase").val(basic);
                $("#ctl00_ContentPlaceHolder1_txtTotal").val(basic);
                $("#ctl00_ContentPlaceHolder1_txtBaseValue").val(total);
                $("#ctl00_ContentPlaceHolder1_txtTotalTax").val(totaltax);
                $("#ctl00_ContentPlaceHolder1_txtTotalVat").val(totalvat);
                $("#ctl00_ContentPlaceHolder1_hdnTax").val(tax);
                $("#ctl00_ContentPlaceHolder1_hdnVat").val(vat);
                $("#txtTotal").focusin();
            }
            else
            {
                var basic = Number($('#ctl00_ContentPlaceHolder1_txtBaseValue').val());
                var tax = Number($('#ctl00_ContentPlaceHolder1_txtTax').val()) / 100;
                var vat = Number($('#ctl00_ContentPlaceHolder1_txtVat').val()) / 100;
                var taxtotal = Number(basic * tax);
                var vattotal = Number(basic * vat);

               // var total = (basic) + (taxtotal) + (vattotal);
                var total = (basic)  + (vattotal);
                $("#ctl00_ContentPlaceHolder1_txtTotalTax").val(taxtotal);
                $("#ctl00_ContentPlaceHolder1_txtTotalVat").val(vattotal);
                $("#ctl00_ContentPlaceHolder1_hdnTax").val(Number($('#ctl00_ContentPlaceHolder1_txtTax').val()));
                $("#ctl00_ContentPlaceHolder1_hdnVat").val(Number($('#ctl00_ContentPlaceHolder1_txtVat').val()));
                $("#ctl00_ContentPlaceHolder1_txtTotal").val(total);
                $("#ctl00_ContentPlaceHolder1_hdnTotal").val(total);
                $("#ctl00_ContentPlaceHolder1_hdnBase").val(basic);
                $("#txtTotal").focusin();
            }
            
        });


        //$(".vatetotal").focusout(function () {
        //    var total = Number($('#ctl00_ContentPlaceHolder1_txtBaseValue').val());
        //    var tax = Number($('#ctl00_ContentPlaceHolder1_txtTotalTax').val());
        //    var vat = Number($('#ctl00_ContentPlaceHolder1_txtTotalVat').val());
        //    var basic = (total) - (tax + vat);
        //    var taxrate = Number((tax / basic) * 100).toFixed(2);
        //    var vatrate = Number((vat / basic) * 100).toFixed(2);
        //    $("#ctl00_ContentPlaceHolder1_hdnTotal").val(total);
        //    $("#ctl00_ContentPlaceHolder1_hdnBase").val(basic);
        //    $("#ctl00_ContentPlaceHolder1_txtTotal").val(basic);
        //    $("#ctl00_ContentPlaceHolder1_txtBaseValue").val(total);
        //    $('#ctl00_ContentPlaceHolder1_txtTax').val(taxrate);
        //    $('#ctl00_ContentPlaceHolder1_txtVat').val(vatrate);
        //    $("#ctl00_ContentPlaceHolder1_hdnTax").val(tax);
        //    $("#ctl00_ContentPlaceHolder1_hdnVat").val(vat);
        //    $("#txtTotal").focusin();
        //});

    </script>
</asp:Content>

