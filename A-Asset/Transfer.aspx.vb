﻿
Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web.UI


Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page
    Public permissions As List(Of Integer) = New List(Of Integer)
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 2 ' 2 is the Section ID for Asset Transfer
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Protected Sub btnReport_ServerClick(sender As Object, e As System.EventArgs) Handles btnReport.ServerClick
        Response.Redirect("LocationTransferReport")
    End Sub
    Protected Sub btnSubmit_ServerClick(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        ' If (permissions.Contains(1)) Then

        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "Update Assets Set Location=@Location where ItemID=@ItemID "
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
        cmd.Parameters.Add("Location", Data.SqlDbType.Int)
        hdndate.Value = Date.Today

        Dim insertCount = 0
        For Each item In GridView1.Items

            Dim chkBox = DirectCast(item.FindControl("transfer"), CheckBox)
            If chkBox.Checked Then

                Dim ItemID As String = DirectCast(item.FindControl("hdnID"), HiddenField).Value
                cmd.Parameters("Location").Value = ddlDestination.SelectedValue
                cmd.Parameters("ItemID").Value = ItemID
                divSuccess.Visible = True
                lblSuccMessage.Text = ItemID & ddlDestination.SelectedValue
                If cmd.ExecuteNonQuery() > 0 Then
                    insertCount += 1
                    sdstransferTracking.InsertParameters.Item(3).DefaultValue = ItemID
                    sdstransferTracking.Insert()
                    ListView1.DataBind()

                End If

            End If

        Next
        If insertCount > 0 Then
            divSuccess.Visible = True
            lblSuccMessage.Text = insertCount & " Assets have been Transfered."
            GridView1.DataBind()
        End If
        'Else
        '    lblErrMessage.Text = "You do not have Permission to Delete"
        'divError.Visible = True
        'End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then

        Else
            ddlSource.SelectedIndex = 0
            hdnSourceID.Value = 1
            ddlDestination.DataBind()
            hdnDestID.Value = ddlDestination.SelectedValue
            GridView1.DataBind()
            ListView1.DataBind()
        End If

    End Sub
    Protected Function GetControlThatCausedPostBack(ByVal page As Page) As Control
        Dim control As Control = Nothing

        Dim ctrlname As String = page.Request.Params.Get("__EVENTTARGET")
        If ctrlname IsNot Nothing AndAlso ctrlname <> String.Empty Then
            control = page.FindControl(ctrlname)
        Else
            For Each ctl As String In page.Request.Form
                Dim c As Control = page.FindControl(ctl)
                If TypeOf c Is System.Web.UI.WebControls.Button OrElse TypeOf c Is System.Web.UI.WebControls.ImageButton Then
                    control = c
                    Exit For
                End If
            Next ctl
        End If
        Return control

    End Function
    Protected Sub ddlSource_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSource.SelectedIndexChanged
        hdnSourceID.Value = ddlSource.SelectedValue
        ddlDestination.DataBind()
        hdnDestID.Value = ddlDestination.SelectedValue
        GridView1.DataBind()
    End Sub
    Protected Sub ddlDestination_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDestination.SelectedIndexChanged
        hdnDestID.Value = ddlDestination.SelectedValue
    End Sub

    Protected Sub ddlPrimary_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrimary.SelectedIndexChanged
        If (ddlPrimary.SelectedValue = 2) Then
            Response.Redirect("TransferAssetUser")
        End If
    End Sub
End Class
