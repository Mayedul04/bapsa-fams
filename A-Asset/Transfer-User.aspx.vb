﻿
Imports System.Data.SqlClient

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page
    Public permissions As List(Of Integer) = New List(Of Integer)
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 2 ' 2 is the Section ID for Asset Transfer
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Protected Function GetControlThatCausedPostBack(ByVal page As Page) As Control
        Dim control As Control = Nothing

        Dim ctrlname As String = page.Request.Params.Get("__EVENTTARGET")
        If ctrlname IsNot Nothing AndAlso ctrlname <> String.Empty Then
            control = page.FindControl(ctrlname)
        Else
            For Each ctl As String In page.Request.Form
                Dim c As Control = page.FindControl(ctl)
                If TypeOf c Is System.Web.UI.WebControls.Button OrElse TypeOf c Is System.Web.UI.WebControls.ImageButton Then
                    control = c
                    Exit For
                End If
            Next ctl
        End If
        Return control

    End Function
    Protected Sub btnReport_ServerClick(sender As Object, e As System.EventArgs) Handles btnReport.ServerClick
        Response.Redirect("UserTransferReport")
    End Sub
    Protected Sub btnUserTransfer_ServerClick(sender As Object, e As System.EventArgs) Handles btnUserTransfer.ServerClick

        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()
        Dim selectString As String = "Update Assets Set AssetUser=@AssetUser where ItemID=@ItemID "
        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
        cmd.Parameters.Add("AssetUser", Data.SqlDbType.Int)

        hdndate.Value = Date.Today
        Dim insertCount = 0
        For Each item In grdSourceUserList.Items

            Dim chkBox = DirectCast(item.FindControl("utransfer"), CheckBox)
            If chkBox.Checked Then

                Dim ItemID As String = DirectCast(item.FindControl("hdnID"), HiddenField).Value
                cmd.Parameters("AssetUser").Value = ddlUserDest.SelectedValue
                cmd.Parameters("ItemID").Value = ItemID
                divSuccess.Visible = True

                If cmd.ExecuteNonQuery() > 0 Then
                    insertCount += 1

                    grdDestUserList.DataBind()

                End If

                sdstransferTracking.InsertParameters.Item(3).DefaultValue = ItemID
                sdstransferTracking.Insert()
            End If

        Next
        If insertCount > 0 Then
            divSuccess.Visible = True
            lblSuccMessage.Text = insertCount & " Assets have been Transfered."
            grdSourceUserList.DataBind()
        End If

    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            Try
                Dim wcICausedPostBack As WebControl = CType(GetControlThatCausedPostBack(TryCast(sender, Page)), WebControl)
                If wcICausedPostBack.ID = ddlDestDept.ID Or wcICausedPostBack.ID = ddlUserDest.ID Then
                    grdDestUserList.DataBind()
                End If
                If wcICausedPostBack.ID = ddlUserSource.ID Or wcICausedPostBack.ID = ddlSDept.ID Then
                    grdSourceUserList.DataBind()
                End If
            Catch ex As Exception
                ddlPrimary.Focus()
            End Try
            'If (hdnSourceUserID.Value <> ddlUserSource.SelectedValue) Then
            '    grdSourceUserList.DataBind()
            'End If
            'If (hdnDestUserID.Value <> ddlUserDest.SelectedValue) Then
            '    grdDestUserList.DataBind()
            'End If
        Else
            'ddlSource.SelectedIndex = 0
            'hdnSourceID.Value = 1
            'ddlDestination.DataBind()
            'hdnDestID.Value = ddlDestination.SelectedValue
            'GridView1.DataBind()
            'ListView1.DataBind()
        End If

    End Sub


    Protected Sub ddlSDept_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSDept.SelectedIndexChanged
        hdnDeptS.Value = ddlSDept.SelectedValue
        ddlUserSource.DataBind()
        hdnSourceUserID.Value = ddlUserSource.SelectedValue
        'grdSourceUserList.DataBind()
    End Sub
    Protected Sub ddlUserSource_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserSource.SelectedIndexChanged
        hdnSourceUserID.Value = ddlUserSource.SelectedValue
        'grdSourceUserList.DataBind()
    End Sub
    Protected Sub ddlDestDept_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDestDept.SelectedIndexChanged
        hdnDeptDest.Value = ddlDestDept.SelectedValue
        ddlUserDest.DataBind()
        'hdnDestUserID.Value = ddlUserDest.SelectedValue
        'grdDestUserList.DataBind()
    End Sub
    Protected Sub ddlUserDest_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserDest.SelectedIndexChanged
        hdnDestUserID.Value = ddlUserDest.SelectedValue
        'grdDestUserList.DataBind()
    End Sub
    Protected Sub ddlPrimary_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrimary.SelectedIndexChanged
        If (ddlPrimary.SelectedValue = 1) Then
            Response.Redirect("transferasset")
        End If

    End Sub
End Class
