﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default2.aspx.vb" Inherits="A_Asset_Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
        <p>
            <asp:Label ID="Label2" runat="server" Text="Email"></asp:Label>
            <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource2" DataTextField="Name" DataValueField="DIT">
            </asp:DropDownList>
        </p>
        <p>
            <asp:Button ID="Button1" runat="server" Text="Button" />
        </p>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionStringTest %>" DeleteCommand="DELETE FROM [Persons] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Persons] ([Name], [District], [Thana], [Active]) VALUES (@Name, @District, @Thana, @Active)" SelectCommand="SELECT * FROM [Persons]" UpdateCommand="UPDATE [Persons] SET [Name] = @Name, [District] = @District, [Thana] = @Thana, [Active] = @Active WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtName" Name="Name" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="DropDownList1" Name="District" PropertyName="SelectedValue" Type="Int32" />
                <asp:Parameter DefaultValue="1" Name="Thana" Type="Int32" />
                <asp:Parameter DefaultValue="True" Name="Active" Type="Boolean" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Name" Type="String" />
                <asp:Parameter Name="District" Type="Int32" />
                <asp:Parameter Name="Thana" Type="Int32" />
                <asp:Parameter Name="Active" Type="Boolean" />
                <asp:Parameter Name="ID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionStringTest %>" SelectCommand="SELECT [DIT], [Name] FROM [District]"></asp:SqlDataSource>
    </form>
</body>
</html>
