﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Resell.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Asset Reselling</h1>
    <div class="span11">
        <div class="btn-group">
            <script type="text/javascript" language="javascript">
                function OnContactSelected(source, eventArgs) {
                    var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                    document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();

                }
            </script>
            <label>Search by Asset Name or Code</label>
            <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
            <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetCompletionList"
                ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                OnClientItemSelected="OnContactSelected">
            </cc1:AutoCompleteExtender>
        </div>
        <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
    </div>
    <div class="span5">

        <div class="well">
            <div class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Asset Name :</label>
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:HiddenField ID="hdnCatID" runat="server" Value="" />
                        <asp:HiddenField ID="hdnRevalSurplus" runat="server" Value="" />
                        <asp:HiddenField ID="hdnIsRevaluate" runat="server" Value="" />
                        <asp:HiddenField ID="hdnDate" runat="server" Value="" /> 
                        <asp:HiddenField ID="hdnClosingDepre" runat="server" Value="" />
                        
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" ReadOnly="true" MaxLength="400" Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Code :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCode" runat="server" ReadOnly="true" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Closing Cost :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCClosingCost" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Depreciation :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCDepreciation" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current WDV :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCWDF" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Dep Rate :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCRate" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="span5">
        <div class="well">
            <div class="tab-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="span12">
                            <asp:HiddenField ID="hdnNewWDV" runat="server" Value="" />
                            <div class="left_div">
                                <label>
                                    New Opening Cost :</label>
                            </div>
                            <div class="right_div">

                                <asp:TextBox ID="txtRevOpening" AutoPostBack="true"  runat="server" CssClass="input-large" Width="100%" TabIndex="1"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="form" SetFocusOnError="True"
                                    runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtRevOpening" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="txtRevOpening" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    New Opening Deprecaition :</label>
                            </div>
                            <div class="right_div">

                                <asp:TextBox ID="txtNewOpeningDepre" AutoPostBack="true"  runat="server" CssClass="input-large" Width="100%" TabIndex="2"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="form" SetFocusOnError="True"
                                    runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtNewOpeningDepre" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="txtNewOpeningDepre" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    Revaluated Value :</label>
                            </div>
                            <div class="right_div">

                                <asp:TextBox ID="txtRevValue" runat="server" ReadOnly="true" CssClass="input-large" Width="100%" TabIndex="3"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="form" SetFocusOnError="True"
                                    runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtRevValue" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="txtRevValue" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="span12">
                            <div class="left_div">
                                <label>
                                    New Dep Rate :</label>
                            </div>
                            <div class="right_div">

                                <asp:TextBox ID="txtNDepreRate" runat="server" CssClass="input-large" Width="100%" TabIndex="4"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ValidationGroup="form" SetFocusOnError="True"
                                    runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtNDepreRate" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ValidationGroup="form"
                                    ControlToValidate="txtNDepreRate" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="span5" style="float: left; margin-left: 45px;">

        <div class="span4">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary" TabIndex="5"><i class="icon-save"></i>&nbsp;Save</button>
        </div>
        
        <div class="span4">
            <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh" TabIndex="7"></i>&nbsp;Clear</button>
        </div>

    </div>

    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [CatID], [ItemName], [ClosingDepreciation], [ClosingCost], [CalculatedDate], [IsRevaluated]) VALUES (@ItemID, @CatID, @ItemName, @ClosingDepreciation, @ClosingCost, @CalculatedDate, @IsRevaluated)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnCatID" Name="CatID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnClosingDepre" Name="ClosingDepreciation" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="txtCWDF" Name="ClosingCost" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="CalculatedDate" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnIsRevaluate" Name="IsRevaluated" PropertyName="Value" DefaultValue="False" Type="Boolean" />
        </InsertParameters>

    </asp:SqlDataSource>

     <asp:SqlDataSource ID="sdsObjectRevaluation" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [CatID], [ItemName], [ClosingDepreciation], [ClosingCost], [CalculatedDate], [IsRevaluated]) VALUES (@ItemID, @CatID, @ItemName, @ClosingDepreciation, @ClosingCost, @CalculatedDate, @IsRevaluated)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnCatID" Name="CatID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtNewOpeningDepre" Name="ClosingDepreciation" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="txtRevOpening" Name="ClosingCost" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="CalculatedDate" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnIsRevaluate" Name="IsRevaluated" PropertyName="Value" DefaultValue="True" Type="Boolean" />
        </InsertParameters>

    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsAssets" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        UpdateCommand="UPDATE [Assets] SET [DepreciationRate] = @DepreciationRate WHERE [ItemID] = @ItemID">

        <UpdateParameters>
            <asp:ControlParameter ControlID="txtNDepreRate" Name="DepreciationRate" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
        </UpdateParameters>

    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsRevaluation" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [RevaluationTracking] ([ItemID], [RevaluationSarplus], [RavaluationDate]) VALUES (@ItemID, @RevaluationSarplus, @RavaluationDate)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnRevalSurplus" Name="RevaluationSarplus" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="RavaluationDate" PropertyName="Value" />
        </InsertParameters>

    </asp:SqlDataSource>
</asp:Content>
