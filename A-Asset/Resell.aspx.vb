﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If

        End If

    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 1 YearlyJournal.*, Assets.ItemCode, Assets.DepreciationRate FROM YearlyJournal inner join Assets on Assets.ItemID=YearlyJournal.ItemID where YearlyJournal.ItemID=@ItemID Order by YearlyJournal.ID Desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
        cmd.Parameters("ItemID").Value = id
        Dim closingvalue As Double = 0
        Dim closingdepre As Double = 0
        Dim rate As Double = 0
        Dim lastcalculatedate As Date
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("ItemID").ToString()
            hdnCatID.Value = reader("CatID").ToString()
            txtTitle.Text = reader("ItemName").ToString()
            txtCode.Text = reader("ItemCode").ToString()
            closingvalue = reader("ClosingCost").ToString()
            closingdepre = reader("ClosingDepreciation").ToString()
            rate = reader("DepreciationRate").ToString()
            lastcalculatedate = Date.Parse(reader("CalculatedDate").ToString())
            ' schkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        Dim currentvalue = closingvalue - closingdepre
        txtCWDF.Text = (currentvalue - currentvalue * ((rate * Date.Today.Subtract(lastcalculatedate).Days) / (365 * 100))).ToString("#.###")
        txtCRate.Text = rate
        hdnClosingDepre.Value = closingdepre + (currentvalue * ((rate * Date.Today.Subtract(lastcalculatedate).Days) / (365 * 100)))
        txtCDepreciation.Text = (closingdepre + currentvalue * ((rate * Date.Today.Subtract(lastcalculatedate).Days) / (365 * 100))).ToString("#.###")
        txtCClosingCost.Text = closingvalue
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub



    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnDate.Value = Date.Today
        If (hdnID.Value <> "") Then
            'Last Calculation before revaluation
            If (sdsObject.Insert() > 0) Then
                If (sdsObjectRevaluation.Insert() > 0) Then
                    hdnRevalSurplus.Value = Double.Parse(hdnNewWDV.Value.ToString()) - Double.Parse(txtCWDF.Text)
                    If (txtNDepreRate.Text <> "") Then
                        sdsAssets.Update()
                    Else
                        divError.Visible = True
                    End If
                    If (sdsRevaluation.Insert()) Then
                        divSuccess.Visible = True
                        ClearField()
                    Else
                        divError.Visible = True
                    End If
                    divSuccess.Visible = True
                Else
                    divError.Visible = True
                End If
            Else
                divError.Visible = True
            End If

        End If

    End Sub


    Private Function ClearField()
        txtTitle.Text = ""
        txtCWDF.Text = ""
        txtCode.Text = ""
        txtCRate.Text = ""
        hdnID.Value = ""
        txtCClosingCost.Text = ""
        txtCDepreciation.Text = ""
        txtNDepreRate.Text = ""
        txtNewOpeningDepre.Text = ""
        txtRevOpening.Text = ""
        txtRevValue.Text = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub

    Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
        Dim split = txtAutoSearch.Text.Split(":")
        Dim name1 As String = split(0)
        Dim id As Integer = split(1)
        hdnID.Value = id
        LoadContent(hdnID.Value)
    End Sub
    Protected Sub txtRevOpening_TextChanged(sender As Object, e As EventArgs) Handles txtRevOpening.TextChanged
        Dim openingNewdepri As Double = 0
        If (txtNewOpeningDepre.Text <> "") Then
            openingNewdepri = Double.Parse(txtNewOpeningDepre.Text)
        End If
        hdnNewWDV.Value = Double.Parse(txtRevOpening.Text)
        txtRevValue.Text = hdnNewWDV.Value
        txtNewOpeningDepre.Focus()
    End Sub
    Protected Sub txtNewOpeningDepre_TextChanged(sender As Object, e As EventArgs) Handles txtNewOpeningDepre.TextChanged
        Dim openingNewdepri As Double = 0.0
        If (txtNewOpeningDepre.Text <> "") Then
            openingNewdepri = Double.Parse(txtNewOpeningDepre.Text)
        End If
        hdnNewWDV.Value = Double.Parse(txtRevOpening.Text) - openingNewdepri
        txtRevValue.Text = hdnNewWDV.Value
        txtNDepreRate.Focus()
    End Sub
End Class
