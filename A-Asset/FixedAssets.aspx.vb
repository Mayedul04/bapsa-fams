﻿
Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Web.Services

Partial Class Admin_A_HTML_HTML
    Inherits System.Web.UI.Page
    Public domainName As String
    Public permissions As List(Of Integer) = New List(Of Integer)
    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        getPermission()
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If (hdnID.Value <> "") Then
                LoadContent(hdnID.Value)
            End If
            txtPurchaseDate.Text = Date.Today
            'watertaxrate.Enabled = True
            'watervatrate.Enabled = True
            ltrValuelabel.Text = "<label>Basic Value :</label>"
            ltrBottomHeading.Text = "<label>Total Value :</label>"
        Else
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (hdnID.Value <> "") Then
                LoadContent(ddlAssets.SelectedValue)
            End If

            'ddlAssets.AppendDataBoundItems = False
            'ddlAssets.Items.Clear()

            'If (rbtInclude.SelectedValue = 1) Then
            '    txtTotalVat.ReadOnly = False
            '    txtTotalVat.TabIndex = 20
            '    txtVat.ReadOnly = True
            '    txtVat.TabIndex = 119
            '    txtTotalTax.ReadOnly = False
            '    txtTotalTax.TabIndex = 19
            '    txtTax.ReadOnly = True
            '    txtTax.TabIndex = 220
            '    watertaxrate.Enabled = True
            '    watervatrate.Enabled = True
            '    ltrValuelabel.Text = "<label>Total Value :</label>"
            '    ltrBottomHeading.Text = "<label>Basic Value :</label>"
            'Else
            '    txtTotalVat.ReadOnly = True
            '    txtTotalVat.TabIndex = 220
            '    txtVat.ReadOnly = False
            '    txtVat.TabIndex = 20
            '    txtTotalTax.ReadOnly = True
            '    txtTotalTax.TabIndex = 119
            '    txtTax.ReadOnly = False
            '    txtTax.TabIndex = 19

            '    watertaxrate.Enabled = True
            '    watervatrate.Enabled = True
            '    ltrValuelabel.Text = "<label>Basic Value :</label>"
            '    ltrBottomHeading.Text = "<label>Total Value :</label>"
            'End If
            Try
                Dim wcICausedPostBack As WebControl = CType(GetControlThatCausedPostBack(TryCast(sender, Page)), WebControl)
                Dim indx As Integer = wcICausedPostBack.TabIndex
                Dim ctrl =
                 From control In wcICausedPostBack.Parent.Controls.OfType(Of WebControl)()
                 Where control.TabIndex > indx
                 Select control
                ctrl.DefaultIfEmpty(wcICausedPostBack).First().Focus()
            Catch ex As Exception
                txtTitle.Focus()
            End Try


        End If

        If (ddlDept.SelectedValue <> "") Then
            ddlUser.Items.Clear()
            sdsUser.SelectParameters.Item(1).DefaultValue = ddlDept.SelectedValue
            sdsUser.DataBind()

            ddlUser.DataBind()
        End If

        'If ddlCat.SelectedValue <> "" Then
        '    LoadItemNumber()
        'End If


    End Sub

    Protected Function GetControlThatCausedPostBack(ByVal page As Page) As Control
        Dim control As Control = Nothing

        Dim ctrlname As String = page.Request.Params.Get("__EVENTTARGET")
        If ctrlname IsNot Nothing AndAlso ctrlname <> String.Empty Then
            control = page.FindControl(ctrlname)
        Else
            For Each ctl As String In page.Request.Form
                Dim c As Control = page.FindControl(ctl)
                If TypeOf c Is System.Web.UI.WebControls.Button OrElse TypeOf c Is System.Web.UI.WebControls.ImageButton Then
                    control = c
                    Exit For
                End If
            Next ctl
        End If
        Return control

    End Function
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 1 ' 1 is the Section ID for New Asset
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        Try
            If (hdnID.Value <> "") Then
                If (permissions.Contains(2)) Then
                    If sdsObject.Update() > 0 Then
                        divSuccess.Visible = True

                        ClearField()
                        btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                    Else
                        divError.Visible = True
                    End If
                Else
                    lblErrMessage.Text = "You do not have Permission to Alter"
                    divError.Visible = True
                End If


            Else
                If (permissions.Contains(1)) Then
                    If (ddlDepreModel.SelectedValue = "Sum of Years") Then
                        Dim lifelenth = Integer.Parse(txtLifeLength.Text)
                        Dim i As Integer
                        Dim sum = 0
                        For i = 1 To lifelenth
                            sum += i
                        Next
                        hdnSumofYear.Value = sum
                    Else
                        txtLifeLength.Text = "0"
                    End If

                    If (txtDepreRate.Text = "") Then
                        txtDepreRate.Text = "0"
                    End If
                    'If (txtTotalTax.Text = "") Then
                    '    hdnTax.Value = "0"
                    'End If
                    'If (txtTotalVat.Text = "") Then
                    '    hdnVat.Value = "0"
                    'End If
                    'If (hdnIncTax.Value = "0") Then
                    '    hdnBase.Value = txtBaseValue.Text
                    '    hdnTotal.Value = txtTotal.Text
                    'Else
                    '    hdnBase.Value = txtTotal.Text
                    '    hdnTotal.Value = txtBaseValue.Text
                    'End If
                    Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
                    Dim insertstring = "INSERT INTO [Assets] ([ItemName], [ItemCode], [Category], [Location],[Project], [Details], [Quantity], [BaseValue], [ItemTax], [ItemVAT], [TotalValue], [PurchaseDate], [TransactionID],[ServiceCategory], [ServicePeriod], [OpeningDepreciation], [DepreciationRate], [Department], [Supplier], [AssetUser],[QUnit],[DepreciationModel],[EstScrapValue],[EstLife], [SumofYear], [Status]) " &
                                                     " VALUES (@ItemName, @ItemCode, @Category,@Location, @Project, @Details, @Quantity, @BaseValue, @ItemTax, @ItemVAT, @TotalValue, @PurchaseDate,@TransactionID, @ServiceCategory,@ServicePeriod,@OpeningDepreciation, @DepreciationRate, @Department, @Supplier, @AssetUser,@QUnit,@DepreciationModel,@EstScrapValue,@EstLife,@SumofYear, @Status); SELECT SCOPE_IDENTITY();"
                    conn.Open()
                    Dim cmd As SqlCommand = New SqlCommand(insertstring, conn)
                    cmd.Parameters.Add("@ItemName", SqlDbType.NVarChar, 50).Value = txtTitle.Text
                    cmd.Parameters.Add("@ItemCode", SqlDbType.NVarChar, 55).Value = txtCode.Text
                    cmd.Parameters.Add("@Category", SqlDbType.Int, 32).Value = ddlCat.SelectedValue
                    cmd.Parameters.Add("@Location", SqlDbType.Int, 32).Value = ddlLocation.SelectedValue
                    cmd.Parameters.Add("@Project", SqlDbType.Int, 32).Value = ddlProject.SelectedValue
                    cmd.Parameters.Add("@Details", SqlDbType.NVarChar, 150).Value = txtDetails.Text
                    cmd.Parameters.Add("@Quantity", SqlDbType.Int, 32).Value = txtqty.Text
                    cmd.Parameters.Add("@BaseValue", SqlDbType.Real).Value = hdnBase.Value
                    cmd.Parameters.Add("@ItemTax", SqlDbType.Float).Value = hdnTax.Value
                    cmd.Parameters.Add("@ItemVAT", SqlDbType.Float).Value = hdnVat.Value
                    cmd.Parameters.Add("@TotalValue", SqlDbType.Real).Value = hdnTotal.Value
                    cmd.Parameters.Add("@PurchaseDate", SqlDbType.Date).Value = txtPurchaseDate.Text
                    cmd.Parameters.Add("@TransactionID", SqlDbType.NVarChar, 50).Value = txtTrans.Text
                    cmd.Parameters.Add("@ServiceCategory", SqlDbType.NVarChar, 50).Value = ddlServiceType.SelectedValue
                    cmd.Parameters.Add("@ServicePeriod", SqlDbType.Int, 32).Value = txtServicePeriod.Text
                    cmd.Parameters.Add("@OpeningDepreciation", SqlDbType.Real).Value = 0
                    cmd.Parameters.Add("@DepreciationRate", SqlDbType.Float).Value = txtDepreRate.Text
                    cmd.Parameters.Add("@Department", SqlDbType.Int, 32).Value = ddlDept.SelectedValue
                    cmd.Parameters.Add("@Supplier", SqlDbType.Int, 32).Value = ddlSupplier.SelectedValue
                    cmd.Parameters.Add("@AssetUser", SqlDbType.Int, 32).Value = ddlUser.SelectedValue
                    cmd.Parameters.Add("@QUnit", SqlDbType.NVarChar, 20).Value = ddlQUnit.SelectedValue
                    cmd.Parameters.Add("@DepreciationModel", SqlDbType.NVarChar, 30).Value = ddlDepreModel.SelectedValue
                    cmd.Parameters.Add("@EstScrapValue", SqlDbType.Real).Value = txtScrapValue.Text
                    cmd.Parameters.Add("@EstLife", SqlDbType.Int, 32).Value = txtLifeLength.Text
                    cmd.Parameters.Add("@SumofYear", SqlDbType.Int, 32).Value = hdnSumofYear.Value
                    cmd.Parameters.Add("@Status", SqlDbType.NVarChar, 20).Value = ddlStatus.SelectedValue

                    Dim last = cmd.ExecuteScalar()
                    Dim lastentered As Int32 = Int32.Parse(last.ToString())
                    Dim bookstartdate = Date.Parse(txtPurchaseDate.Text)

                    sdsYearJournal.InsertParameters.Item(0).DefaultValue = lastentered
                    sdsYearJournal.InsertParameters.Item(5).DefaultValue = bookstartdate
                    If sdsYearJournal.Insert() > 0 Then
                        divSuccess.Visible = True
                        ClearField()
                        btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                    Else
                        divError.Visible = True
                    End If
                Else
                    lblErrMessage.Text = "You do not have Permission to Add new Item"
                    divError.Visible = True
                End If




            End If
        Catch ex As Exception
            lblErrMessage.Text = ex.Message
            divError.Visible = True
        End Try


    End Sub

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        ItemID, [ItemName], [ItemCode], [Category],[Department],[Location], [Supplier], [AssetUser],[QUnit], [Details], [Quantity], [BaseValue], [ItemTax], [ItemVAT], [TotalValue], [PurchaseDate], [ServiceCategory], [ServicePeriod],[OpeningDepreciation], [DepreciationRate], [Status],[TransactionID],[DepreciationModel],[EstScrapValue],[EstLife], [SumofYear]  FROM   Assets where ItemID=@ItemID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
        cmd.Parameters("ItemID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("ItemID").ToString()
            txtTitle.Text = reader("ItemName").ToString()
            txtCode.Text = reader("ItemCode").ToString()
            txtqty.Text = reader("Quantity").ToString()
            txtDetails.Text = reader("Details").ToString()
            txtTrans.Text = reader("TransactionID").ToString()
            ddlDept.SelectedValue = reader("Department").ToString()
            ddlLocation.SelectedValue = reader("Location").ToString()
            ddlSupplier.SelectedValue = reader("Supplier").ToString()
            txtBaseValue.Text = reader("BaseValue").ToString()
            txtTax.Text = reader("ItemTax").ToString()
            txtVat.Text = reader("ItemVat").ToString()
            txtTotal.Text = reader("TotalValue").ToString()
            hdnBase.Value = reader("BaseValue").ToString()
            hdnTax.Value = reader("ItemTax").ToString()
            hdnVat.Value = reader("ItemVat").ToString()
            hdnTotal.Value = reader("TotalValue").ToString()

            ddlQUnit.SelectedValue = reader("QUnit").ToString()
            txtPurchaseDate.Text = reader("PurchaseDate").ToString()
            ddlCat.SelectedValue = reader("Category").ToString()
            If (ddlDept.SelectedValue <> "") Then
                'sdsUser.SelectParameters.Item(1).DefaultValue = ddlDept.SelectedValue
                sdsUser.DataBind()
                ddlUser.DataBind()
                ddlUser.SelectedValue = reader("AssetUser").ToString()
            End If

            txtServicePeriod.Text = reader("ServicePeriod").ToString()
            'txtOpeningDepre.Text = reader("OpeningDepreciation").ToString()
            txtDepreRate.Text = reader("DepreciationRate").ToString()
            ddlServiceType.SelectedValue = reader("ServiceCategory").ToString()
            ddlStatus.SelectedValue = reader("Status").ToString()
            ddlDepreModel.SelectedValue = reader("DepreciationModel").ToString()
            txtScrapValue.Text = reader("EstScrapValue").ToString()
            txtLifeLength.Text = reader("EstLife").ToString()
            hdnSumofYear.Value = reader("SumofYear").ToString()
            Dim nos As Integer = Integer.Parse(txtqty.Text)
            Dim base As Double = Double.Parse(hdnBase.Value)
            Dim totaltax As Decimal = 0
            Dim totalvat As Decimal = 0

            If (txtTax.Text <> "") Then
                totaltax = base * (Decimal.Parse(hdnTax.Value) / 100)
            End If
            If (txtVat.Text <> "") Then
                totalvat = base * (Decimal.Parse(hdnVat.Value) / 100)
            End If
            txtTotalTax.Text = totaltax
            txtTotalVat.Text = totalvat

        Else
            conn.Close()

        End If
        conn.Close()
        chkIncTax.Enabled = False
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub

    'Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
    '    If (e.CommandName = "Edit") Then


    '        Dim categoryID As Integer = Integer.Parse(e.CommandArgument)
    '        LoadContent(categoryID)

    '    End If
    'End Sub
    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (permissions.Contains(3)) Then
            If (hdnID.Value <> 0) Then
                If sdsObject.Delete() > 0 Then
                    divSuccess.Visible = True

                    ClearField()
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "Please select a item to delete"
                divError.Visible = True
            End If
        Else
            lblErrMessage.Text = "You do not have Permission to Delete"
            divError.Visible = True
        End If


    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        txtDetails.Text = ""
        txtCode.Text = ""

        hdnID.Value = ""
        txtBaseValue.Text = ""
        txtDepreRate.Text = ""
        txtPurchaseDate.Text = DateTime.Today
        txtqty.Text = 1
        txtServicePeriod.Text = ""
        txtTax.Text = ""
        txtVat.Text = ""
        txtTotal.Text = ""
        txtTotalTax.Text = ""
        txtTotalVat.Text = ""
        txtTrans.Text = ""
        ddlDepreModel.SelectedIndex = 0
        'ddlCat.SelectedIndex = 0
        'ddlDept.SelectedIndex = 0
        'ddlLocation.SelectedIndex = 0
        'ddlQUnit.SelectedIndex = 0
        'ddlServiceType.SelectedIndex = 0
        'ddlStatus.SelectedIndex = 0
        'ddlSupplier.SelectedIndex = 0
        'ddlUser.SelectedIndex = 0

    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub


    Private Sub LoadItemNumber()
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        Dim cmd As SqlCommand = New SqlCommand("Select Code from Category where CatID=@CatID", conn)
        conn.Open()
        cmd.Parameters.Add("CatID", SqlDbType.Int, 32).Value = ddlCat.SelectedValue
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim prefix As String = ""
        Dim length As Integer = 4
        Dim formatedvalue As String = ""
        Dim needed0 As Integer = 0

        While (reader.Read())


            prefix = reader("Code").ToString()
            'length = Integer.Parse(reader("Length").ToString())
        End While
        reader.Close()
        Dim cmd1 = New SqlCommand("Select Code from Location where LocID=@LocID", conn)

        cmd1.Parameters.Add("LocID", SqlDbType.Int, 32).Value = ddlLocation.SelectedValue
        reader = cmd1.ExecuteReader()
        Dim loccode As String
        If reader.Read() Then
            loccode = reader("Code").ToString()
        End If
        reader.Close()

        Dim cmd2 = New SqlCommand("Select ProjectCode from Projects where ProjID=@ProjID", conn)

        cmd2.Parameters.Add("ProjID", SqlDbType.Int, 32).Value = ddlProject.SelectedValue
        reader = cmd2.ExecuteReader()
        Dim procode As String
        If reader.Read() Then
            procode = reader("ProjectCode").ToString()
        End If
        reader.Close()

        conn.Close()
        Dim lastvoucherid As Integer = getLastAID() + 1
        needed0 = length - lastvoucherid.ToString().Length
        '//if (lastvoucherid == 1)
        '//    needed0 = length - 1;
        '//else
        '//needed0=length - int.Parse(Math.Ceiling(Math.Log10(lastvoucherid)).ToString());

        For index As Integer = 1 To needed0
            formatedvalue += "0"
        Next
        formatedvalue += lastvoucherid.ToString()
        txtCode.Text = "BAPSA/" + procode + "/" + loccode + "/" + prefix + "/" + formatedvalue

    End Sub


    Private Function getLastAID() As Integer
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        Dim retid As Int32 = 0
        Dim retstr As String = ""
        Dim cmd As SqlCommand = New SqlCommand("Select top 1 ItemID from Assets where Category=@CatID order by ItemID Desc", conn)
        conn.Open()
        cmd.Parameters.Add("CatID", SqlDbType.Int, 32).Value = ddlCat.SelectedValue
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While (reader.Read())

            retstr = reader("ItemID").ToString()

        End While
        conn.Close()
        If (retstr <> "") Then
            'retid = Int32.Parse(digits)
            'Dim digits As String = retstr.Substring(2)
            retid = Int32.Parse(retstr)
        Else retid = 0
        End If

        Return retid
    End Function
    'Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
    '    If (txtAutoSearch.Text.Contains(":")) Then
    '        Dim split = txtAutoSearch.Text.Split(":")
    '        Dim name1 As String = split(0)
    '        Dim id As Integer = split(1)
    '        hdnID.Value = id
    '        LoadContent(hdnID.Value)
    '    Else
    '        lblErrMessage.Text = "Please select a Valid item from Suggested List"
    '        divError.Visible = True
    '    End If


    'End Sub



    'Protected Sub rbtInclude_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbtInclude.SelectedIndexChanged
    '    hdnIncTax.Value = rbtInclude.SelectedValue
    'End Sub
    Protected Sub txtPurchaseDate_TextChanged(sender As Object, e As EventArgs) Handles txtPurchaseDate.TextChanged
        txtPurchaseDate.Text = Date.Parse(txtPurchaseDate.Text).ToShortDateString()
    End Sub

    Protected Sub ddlAssets_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssets.SelectedIndexChanged
        If (ddlAssets.SelectedIndex <> 0) Then
            hdnID.Value = ddlAssets.SelectedValue
            ' LoadContent(hdnID.Value)
        End If
    End Sub
    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged
        If (ddlCategory.SelectedIndex <> 0) Then
            ' hdnSearchCat.Value = ddlCategory.SelectedValue

            ddlAssets.DataBind()
            If ddlAssets.Items.Count > 0 Then
                ddlAssets.SelectedIndex = 0
                hdnID.Value = ddlAssets.SelectedValue
                LoadContent(hdnID.Value)
            Else
                ClearField()
            End If

        End If
    End Sub
End Class
