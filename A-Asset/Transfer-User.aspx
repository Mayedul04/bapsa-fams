﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Transfer-User.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Assets Transfer</h1>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="Two Group can not be same"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->


    <div class="span3" style="margin-left: 0px">
        <asp:DropDownList ID="ddlPrimary" runat="server" AutoPostBack="true" CssClass="input-large modelchange">

            <asp:ListItem Value="1">Branch to Branch</asp:ListItem>
            <asp:ListItem Selected="True" Value="2">Employee to Employee</asp:ListItem>
        </asp:DropDownList>
        <asp:HiddenField ID="hdnTID" runat="server" />

        <asp:HiddenField ID="hdnDeptS" runat="server" />
        <asp:HiddenField ID="hdnDeptDest" runat="server" />
        <asp:HiddenField ID="hdnSourceUserID" runat="server" />
        <asp:HiddenField ID="hdnDestUserID" runat="server" />
    </div>
    <div class="span3">
        <button runat="server" id="btnReport" validationgroup="form" class="btn btn-primary"><i class="icon-search"></i>&nbsp;Transfer History</button>
    </div>


    <div class="span12">
        <div class="span5" style="min-height: 400px">
            <div class="span12 form-top-strip" style="margin-left: 0px;">
                <p>Asset Source</p>
            </div>
            <div class="span12">
                <div class="well">
                    <div class="left_div">
                        <label>Department</label>
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlSDept" runat="server" CssClass="input-large" AutoPostBack="true"
                            DataSourceID="sdsDeptSource" DataTextField="Department" DataValueField="DeptID">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsDeptSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [DeptID], [Department]  FROM [Department] WHERE ([Status] = @Status)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                    <div class="left_div">
                        <label>Source</label>
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlUserSource" runat="server" CssClass="input-large" AutoPostBack="true"
                            DataSourceID="sdsUserSource" DataTextField="EmployeeName" DataValueField="UID">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsUserSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [UID], [EmployeeName] FROM [Employees] WHERE ([Status] = @Status and DepartmentID=@DeptID)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                <asp:ControlParameter ControlID="hdnDeptS" Name="DeptID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>

                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 120px">Asset
                                </th>

                                <th style="width: 10px;">Select
                                </th>

                            </tr>
                        </thead>
                        <tbody>
                            <asp:ListView ID="grdSourceUserList" runat="server" DataKeyNames="ItemID" DataSourceID="sdsUserSourceGrid">
                                <EmptyDataTemplate>
                                    <table runat="server" style="">
                                        <tr>
                                            <td>No data was returned.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr style="">
                                        <%--<td>
                                            <asp:Label ID="ID" runat="server" Text='<%# Eval("ItemName") %>' />
                                            
                                        </td>--%>
                                        <td>
                                            <asp:Label ID="Parent" runat="server" Text='<%# Eval("ItemCode") %>' />
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("ItemID") %>' />
                                        </td>

                                        <td>
                                            <asp:CheckBox ID="utransfer" runat="server" Checked="false" />
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <LayoutTemplate>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>

                                </LayoutTemplate>
                            </asp:ListView>
                        </tbody>
                    </table>
                </div>
                <asp:SqlDataSource ID="sdsUserSourceGrid" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [ItemID], [ItemName], [ItemCode], [Location], [Supplier], [Department], [AssetUser] FROM [Assets] WHERE ( [AssetUser]=@AssetUser and Status='Active')">
                    <SelectParameters>
                        <%--<asp:ControlParameter ControlID="hdnDeptS" Name="Department" PropertyName="Value" Type="Int32" />--%>
                        <asp:ControlParameter ControlID="hdnSourceUserID" Name="AssetUser" PropertyName="Value" Type="Int32" />
                    </SelectParameters>

                </asp:SqlDataSource>
            </div>
        </div>
        <div class="span2" style="margin-top: 76px;">
            <button runat="server" id="btnUserTransfer" validationgroup="form" class="btn btn-primary">Transfer&nbsp;<i class="icon-fast-forward"></i></button>
        </div>

        <div class="span5" style="min-height: 400px">
            <div class="span12 form-top-strip" style="margin-left: 0px;">
                <p>Asset Destination</p>
            </div>
            <div class="span12">

                <div class="well">
                    <div class="left_div">
                        <label>Department</label>
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlDestDept" runat="server" CssClass="input-large" AutoPostBack="true"
                            DataSourceID="sdsDestdept" DataTextField="Department" DataValueField="DeptID">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsDestdept" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [DeptID], [Department]  FROM [Department] WHERE ([Status] = @Status)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                    <div class="left_div">
                        <label>Destination</label>
                    </div>
                    <div class="right_div">

                        <asp:DropDownList ID="ddlUserDest" runat="server" CssClass="input-large" DataSourceID="sdsUserDest" AutoPostBack="true"
                            DataTextField="EmployeeName" DataValueField="UID">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsUserDest" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [UID] , [EmployeeName] FROM [Employees] WHERE (([Status] = @Status) AND ([DepartmentID]= @DeptID))">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                <asp:ControlParameter ControlID="hdnDeptDest" Name="DeptID" PropertyName="Value" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>


                    <table class="table">
                        <thead>
                            <tr>
                                <th>Asset
                                </th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <asp:ListView ID="grdDestUserList" runat="server" DataKeyNames="ItemID" DataSourceID="sdsDestusers">
                                <EmptyDataTemplate>
                                    <table runat="server" style="">
                                        <tr>
                                            <td>No data was returned.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <tr style="">
                                        <%--<td>
                                            <asp:Label ID="ID" runat="server" Text='<%# Eval("ItemName") %>' />
                                        </td>--%>
                                        <td>
                                            <asp:Label ID="Parent" runat="server" Text='<%# Eval("ItemCode") %>' />
                                        </td>

                                    </tr>
                                </ItemTemplate>
                                <LayoutTemplate>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>

                                </LayoutTemplate>
                            </asp:ListView>
                        </tbody>
                    </table>
                </div>
                <asp:SqlDataSource ID="sdsDestusers" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [ItemID], [ItemName], [ItemCode], [Location], [Supplier], [Department], [AssetUser] FROM [Assets] WHERE ([AssetUser] = @AssetUser and Status='Active')">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="hdnDestUserID" Name="AssetUser" PropertyName="Value" Type="Int32" />
                    </SelectParameters>

                </asp:SqlDataSource>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdndate" runat="server" />
    <asp:SqlDataSource ID="sdstransferTracking" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [TransferTracking] ([TransferType], [SourceID], [DestinationID], [TransferedAsset], [TransDate]) VALUES (@TransferType, @SourceID, @DestinationID, @TransferedAsset, @TransDate)">

        <InsertParameters>
            <asp:Parameter DefaultValue="User" Name="TransferType" Type="String" />
            <asp:ControlParameter ControlID="hdnSourceUserID" DefaultValue="" Name="SourceID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnDestUserID" Name="DestinationID" PropertyName="Value" Type="Int32" />
            <asp:Parameter Name="TransferedAsset" Type="String" />
            <asp:ControlParameter ControlID="hdndate" DbType="Date" Name="TransDate" PropertyName="Value" />
        </InsertParameters>

    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
