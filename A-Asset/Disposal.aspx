﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Disposal.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<h1 class="page-title">Asset Dispose</h1>--%>

    <%--<div class="btn-group">
            <script type="text/javascript" language="javascript">
                function OnContactSelected(source, eventArgs) {
                    var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                    document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();

                }
            </script>
            <label>Search by Asset Name or Code</label>
            <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
            <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetFullList"
                ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                OnClientItemSelected="OnContactSelected">
            </cc1:AutoCompleteExtender>
        </div>--%>
    <div class="span5" style="margin-left: 0px; margin-top: 5px;">
        <div class="well-border">
            <div class="tab-content">
                <div class="success-details" visible="false" id="divSuccess" runat="server">
                    <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                    <div class="corners">
                        <span class="success-left-top"></span><span class="success-right-top"></span><span
                            class="success-left-bot"></span><span class="success-right-bot"></span>
                    </div>
                </div>
                <div class="error-details" id="divError" visible="false" runat="server">
                    <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                    <div class="corners">
                        <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
                    </div>
                </div>
                <div class="span12">
                    <asp:DropDownList ID="ddlBranch" Width="100%" runat="server" DataSourceID="sdsLoc" CssClass="input-large"
                        DataTextField="LocationName" DataValueField="LocID" AppendDataBoundItems="true" TabIndex="0">
                        <asp:ListItem Value="">Search in Branch</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsLoc" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [LocID], [LocationName] FROM [Location] WHERE ([Status] = @Status)">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span12">
                    <asp:DropDownList ID="ddlCategory" runat="server" Width="100%" DataSourceID="sdsParent" CssClass="input-large" AutoPostBack="true"
                        DataTextField="CategoryName" DataValueField="CatID" TabIndex="0" AppendDataBoundItems="true">
                        <asp:ListItem Value="">Select Category</asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnSearchCat" runat="server" />
                    <asp:SqlDataSource ID="sdsParent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and CatID>1 and ParentID<>1">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span12">
                    <asp:DropDownList ID="ddlAssets" runat="server" Width="100%" DataSourceID="sdsSearchAsset" CssClass="input-large" AutoPostBack="true"
                        DataTextField="ItemCode" DataValueField="ItemID" TabIndex="0" AppendDataBoundItems="false">
                        <asp:ListItem Value="">Select Asset</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsSearchAsset" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT Distinct Assets.[ItemCode], Assets.[ItemID] FROM YearlyJournal inner join Assets on Assets.ItemID=YearlyJournal.ItemID where  (([Status] = @Status) AND (Assets.[Location] = @Location) AND (Assets.[Category] = @Category))">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="Active" Name="Status" Type="String" />
                            <asp:ControlParameter ControlID="ddlBranch" Name="Location" PropertyName="SelectedValue" Type="Int32" />
                            <asp:ControlParameter ControlID="ddlCategory" Name="Category" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
        </div>
        <div class="span12 form-top-strip">
            <p>Disposal Form</p>
        </div>
        <div class="well">
            <div class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Asset Name :</label>
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:HiddenField ID="hdnCatID" runat="server" Value="" />
                        <asp:HiddenField ID="hdnIsDisposed" runat="server" Value="" />
                        <asp:HiddenField ID="hdnDate" runat="server" Value="" />
                        <asp:HiddenField ID="hdnCurrentClosingDepre" runat="server" Value="" />
                        <asp:HiddenField ID="hdnCurentClosing" runat="server" Value="" />
                        <asp:HiddenField ID="hdnDisposedvalue" runat="server" Value="" />
                        <asp:HiddenField ID="hdnResellValue" runat="server" Value="" />
                        <asp:HiddenField ID="hdnAssetValue" runat="server" Value="" />
                        <asp:HiddenField ID="hdnNewOpeningCost" runat="server" Value="" />
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" ReadOnly="true" MaxLength="400" Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Code :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCode" runat="server" ReadOnly="true" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>

                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Closing Cost :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCClosingCost" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Depreciation :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCDepreciation" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12" runat="server" id="pnlRevSurplus" visible="false">
                    <div class="left_div">
                        <label>
                            Revaluation Surplus:</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtRevSurplus" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>
                        <asp:HiddenField ID="hdnCurrentRevSurplus" runat="server" />
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current Dep Rate :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCRate" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Current WDV :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCWDF" ReadOnly="true" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>

                    </div>
                </div>
                <div class="span12">

                    <div class="left_div">
                        <label>
                            Resell Value :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtResellvalue" runat="server" CssClass="input-large" Width="100%" TabIndex="1"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="form1" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Number Only" ControlToValidate="txtResellvalue" ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form1"
                            ControlToValidate="txtResellvalue" runat="server" ErrorMessage=" *" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                <div class="span4" style="width:33%">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary" tabindex="5"><i class="icon-save"></i>&nbsp;Dispose</button>
                </div>
                <div class="span4" style="width:33%">
                    <button runat="server" id="btnResell" validationgroup="form1" class="btn btn-primary" tabindex="5"><i class="icon-save"></i>&nbsp;Resell</button>
                </div>
                <div class="span4" style="width:33%">
                    <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh" tabindex="7"></i>&nbsp;Cancel</button>
                </div>
                    </div>
            </div>
        </div>
    </div>


    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [CatID], [ItemName], [ClosingDepreciation], [ClosingCost], [CalculatedDate], [IsDisposed]) VALUES (@ItemID, @CatID, @ItemName, @ClosingDepreciation, @ClosingCost, @CalculatedDate, @IsDisposed)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnCatID" Name="CatID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnCurrentClosingDepre" Name="ClosingDepreciation" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnNewOpeningCost" Name="ClosingCost" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="CalculatedDate" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnIsDisposed" Name="IsDisposed" PropertyName="Value" DefaultValue="True" Type="Boolean" />
        </InsertParameters>

    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sdsAssets" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        UpdateCommand="UPDATE [Assets] SET [Status] = @Status WHERE [ItemID] = @ItemID">

        <UpdateParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:Parameter DefaultValue="Disposed" Name="Status" />
        </UpdateParameters>

    </asp:SqlDataSource>

    <asp:SqlDataSource ID="sdsDispose" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [DisposalTracking] ([ItemID], [DisposedValue], [DisposedDate]) VALUES (@ItemID, @DisposedValue, @DisposedDate)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnDisposedValue" Name="DisposedValue" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="DisposedDate" PropertyName="Value" />
        </InsertParameters>

    </asp:SqlDataSource>


    <asp:SqlDataSource ID="sdsObjectresell" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [CatID], [ItemName], [ClosingDepreciation], [ClosingCost], [CalculatedDate], [IsResold]) VALUES (@ItemID, @CatID, @ItemName, @ClosingDepreciation, @ClosingCost, @CalculatedDate, @IsResold)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnCatID" Name="CatID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnCurrentClosingDepre" Name="ClosingDepreciation" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnNewOpeningCost" Name="ClosingCost" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="CalculatedDate" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnIsDisposed" Name="IsResold" PropertyName="Value" DefaultValue="True" Type="Boolean" />
        </InsertParameters>

    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsResell" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [ResellTracking] ([ItemID], [SellValue], [SellingDate]) VALUES (@ItemID, @SellValue, @SellingDate)">

        <InsertParameters>
            <asp:ControlParameter ControlID="hdnID" Name="ItemID" PropertyName="Value" Type="Int32" />
            <asp:ControlParameter ControlID="hdnResellValue" Name="SellValue" PropertyName="Value" Type="Single" />
            <asp:ControlParameter ControlID="hdnDate" DbType="Date" Name="SellingDate" PropertyName="Value" />
        </InsertParameters>

    </asp:SqlDataSource>
</asp:Content>
