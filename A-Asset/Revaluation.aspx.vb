﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page
    Public permissions As List(Of Integer) = New List(Of Integer)
    Public model As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (hdnID.Value <> "") Then
                LoadContent(ddlAssets.SelectedValue)
            End If
            'ddlAssets.AppendDataBoundItems = False
            'ddlAssets.Items.Clear()
        End If
        getPermission()
    End Sub
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 3 ' 3 is the Section ID for Revaluation
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 1 YearlyJournal.*, Assets.ItemCode, Assets.DepreciationRate, Assets.TotalValue, Assets.OpeningDepreciation, Assets.PurchaseDate, Assets.DepreciationModel FROM YearlyJournal inner join Assets on Assets.ItemID=YearlyJournal.ItemID where YearlyJournal.ItemID=@ItemID Order by YearlyJournal.CalculatedDate Desc, YearlyJournal.ID Desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
        cmd.Parameters("ItemID").Value = id
        Dim closingvalue As Double = 0
        Dim closingdepre As Double = 0
        Dim rate As Double = 0

        Dim lastcalculatedate As Date
        Dim isRevaluated As Boolean = False
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("ItemID").ToString()
            hdnCatID.Value = reader("CatID").ToString()
            txtTitle.Text = reader("ItemName").ToString()
            txtCode.Text = reader("ItemCode").ToString()
            closingvalue = reader("ClosingCost").ToString()
            If (closingvalue <> 0) Then
                closingdepre = reader("ClosingDepreciation").ToString()
                rate = reader("DepreciationRate").ToString()
                lastcalculatedate = Date.Parse(reader("CalculatedDate").ToString())
            Else
                closingvalue = reader("TotalValue").ToString()
                closingdepre = reader("OpeningDepreciation").ToString()
                rate = reader("DepreciationRate").ToString()
                lastcalculatedate = Date.Parse(reader("PurchaseDate").ToString())
            End If
            model = reader("DepreciationModel").ToString()
            isRevaluated = reader("IsRevaluated").ToString()
            ' schkStatus.Checked = reader("Status").ToString()
        Else
            reader.Close()
            selectString = "SELECT *  FROM  Assets where ItemID=@ItemID"
            cmd = New Data.SqlClient.SqlCommand(selectString, conn)
            cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
            cmd.Parameters("ItemID").Value = id
            reader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()
                hdnID.Value = reader("ItemID").ToString()
                hdnCatID.Value = reader("Category").ToString()
                txtTitle.Text = reader("ItemName").ToString()
                txtCode.Text = reader("ItemCode").ToString()
                closingvalue = reader("TotalValue").ToString()
                closingdepre = reader("OpeningDepreciation").ToString()
                rate = reader("DepreciationRate").ToString()
                lastcalculatedate = Date.Parse(reader("PurchaseDate").ToString())
                model = reader("DepreciationModel").ToString().Trim()
                'schkStatus.Checked = reader("Status").ToString()
            Else
                conn.Close()
            End If
            conn.Close()

        End If
        conn.Close()
        If (model = "Declining Balance") Then
            pnlRevRate.Visible = False
            pnlRevValue.Visible = True

        ElseIf (model = "Straight Line") Then
            pnlRevValue.Visible = False
            pnlRevRate.Visible = True
        Else
            pnlRevValue.Visible = False
            pnlRevRate.Visible = False
            lblErrMessage.Text = "This Asset can not be Revaluated"
            divError.Visible = True
        End If
        Dim currentvalue = closingvalue - closingdepre
        txtCRate.Text = rate
        txtCClosingCost.Text = closingvalue.ToString("#.###")
        hdnCurentClosing.Value = currentvalue
        hdnCurrentClosingDepre.Value = (currentvalue * ((rate * Date.Today.Subtract(lastcalculatedate).Days) / (365 * 100)))
        Dim totaldep As Double = closingdepre + (currentvalue * ((rate * Date.Today.Subtract(lastcalculatedate).Days) / (365 * 100)))
        txtCDepreciation.Text = If(totaldep <> 0, (totaldep).ToString("#.###"), 0)
        If (isRevaluated) Then
            pnlRevSurplus.Visible = True
            Dim surplus As Double = getRevaluationSurplus(id)
            hdnCurrentRevSurplus.Value = surplus
            txtRevSurplus.Text = surplus.ToString("#.###")
            txtCWDF.Text = (currentvalue - hdnCurrentClosingDepre.Value + surplus).ToString("#.###")
        Else
            pnlRevSurplus.Visible = False
            txtCWDF.Text = (currentvalue - hdnCurrentClosingDepre.Value).ToString("#.###")
        End If

        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub

    Private Function getRevaluationSurplus(ByVal itemid As Integer) As Double
        Dim retval As Double = 0
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT top 1 RevaluationSarplus from RevaluationTracking where ItemID=@ItemID order by RavaluationDate Desc"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
        cmd.Parameters("ItemID").Value = itemid
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retval = reader("RevaluationSarplus").ToString()
        End If
        conn.Close()
        Return retval
    End Function

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnDate.Value = Date.Today
        If (hdnID.Value <> "") Then
            'Last Calculation before revaluation
            If (permissions.Contains(1)) Then
                If (sdsObject.Insert() > 0) Then
                    If (model = "Declining Balance") Then
                        hdnNewWDV.Value = Double.Parse(txtRevValue.Text)
                    Else
                        hdnNewWDV.Value = Double.Parse(txtCWDF.Text)
                    End If

                    hdnRevalSurplus.Value = Double.Parse(hdnNewWDV.Value.ToString()) - Double.Parse(txtCWDF.Text)
                    hdnNewOpening.Value = hdnNewWDV.Value + Double.Parse(hdnCurrentClosingDepre.Value.ToString()) - hdnRevalSurplus.Value

                    If (sdsObjectRevaluation.Insert() > 0) Then

                        If (model = "Straight Line") Then
                            sdsAssets.Update()
                        End If
                        If (sdsRevaluation.Insert()) Then
                            divSuccess.Visible = True
                            ClearField()
                        Else
                            divError.Visible = True
                        End If
                        divSuccess.Visible = True
                    Else
                        divError.Visible = True
                    End If
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Delete"
                divError.Visible = True
            End If
        Else
            lblErrMessage.Text = "Please select a item to Delete"
            divError.Visible = True
        End If

    End Sub


    Private Function ClearField()
        txtTitle.Text = ""
        txtCWDF.Text = ""
        txtCode.Text = ""
        txtCRate.Text = ""
        hdnID.Value = ""
        txtCClosingCost.Text = ""
        txtCDepreciation.Text = ""
        txtNDepreRate.Text = ""
        txtNewOpeningDepre.Text = ""
        txtRevOpening.Text = ""
        txtRevValue.Text = ""
        txtRevSurplus.Text = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub

    'Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
    '    If (txtAutoSearch.Text.Contains(":")) Then
    '        Dim split = txtAutoSearch.Text.Split(":")
    '        Dim name1 As String = split(0)
    '        Dim id As Integer = split(1)
    '        hdnID.Value = id
    '        LoadContent(hdnID.Value)
    '    Else
    '        lblErrMessage.Text = "Please select a Valid item from Suggested List"
    '        divError.Visible = True
    '    End If

    'End Sub
    'Protected Sub txtRevOpening_TextChanged(sender As Object, e As EventArgs) Handles txtRevOpening.TextChanged
    '    Dim openingNewdepri As Double = 0
    '    If (txtNewOpeningDepre.Text <> "") Then
    '        openingNewdepri = Double.Parse(txtNewOpeningDepre.Text)
    '    End If
    '    hdnNewWDV.Value = Double.Parse(txtRevOpening.Text)
    '    txtRevValue.Text = hdnNewWDV.Value
    '    txtNewOpeningDepre.Focus()
    'End Sub
    'Protected Sub txtNewOpeningDepre_TextChanged(sender As Object, e As EventArgs) Handles txtNewOpeningDepre.TextChanged
    '    Dim openingNewdepri As Double = 0.0
    '    If (txtNewOpeningDepre.Text <> "") Then
    '        openingNewdepri = Double.Parse(txtNewOpeningDepre.Text)
    '    End If
    '    hdnNewWDV.Value = Double.Parse(txtRevOpening.Text) - openingNewdepri
    '    txtRevValue.Text = hdnNewWDV.Value
    '    txtNDepreRate.Focus()
    'End Sub
    Protected Sub ddlAssets_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssets.SelectedIndexChanged
        If (ddlAssets.SelectedIndex <> 0) Then
            hdnID.Value = ddlAssets.SelectedValue
            'LoadContent(hdnID.Value)
        End If
    End Sub
    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged
        If (ddlCategory.SelectedIndex <> 0) Then
            ' hdnSearchCat.Value = ddlCategory.SelectedValue

            ddlAssets.DataBind()
            If ddlAssets.Items.Count > 0 Then
                ddlAssets.SelectedIndex = 0
                hdnID.Value = ddlAssets.SelectedValue
                LoadContent(hdnID.Value)
            Else
                ClearField()
            End If

        End If
    End Sub
End Class
