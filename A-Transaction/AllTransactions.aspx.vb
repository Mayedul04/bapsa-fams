﻿
Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page


    Protected Sub btnAddNew_ServerClick(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("Asset.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If
    End Sub
End Class
