﻿
Partial Class Admin_A_HTML_HTML
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If

        'Utility.GetDimentionSetting("HTML", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)
        'HTMLEdit.aspx?hid=1&Title=1&SmallImage=1&BigImage=1&ImageAltText=1&SmallDetails=1&BigDetails=1&SmallImageWidth=100&SmallImageHeight=100&BigImageWidth=200&BigImageHeight=200
        If Not IsPostBack Then

            If Not String.IsNullOrEmpty(Request.QueryString("aid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Asset"
                LoadContent(Request.QueryString("aid"))
            End If
        Else
            Dim nos As Integer = Integer.Parse(txtqty.Text)
            Dim base As Double = Double.Parse(txtBaseValue.Text)
            Dim tax As Decimal = Decimal.Parse(txtTax.Text) / 100
            Dim vat As Decimal = Decimal.Parse(txtVat.Text) / 100

            txtTotal.Text = (base + (base * tax) + (base * vat)) * nos
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If String.IsNullOrEmpty(Request.QueryString("aid")) Then

            If sdsObject.Insert() > 0 Then

                Response.Redirect("AllAsset.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsObject.Update() > 0 Then
                divSuccess.Visible = True
                Response.Redirect("Asset.aspx?aid=" & Request.QueryString("aid"))
            Else
                divError.Visible = True
            End If
        End If

    End Sub





    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        ItemID, [ItemName], [ItemCode], [Category], [Details], [Quantity], [BaseValue], [ItemTax], [ItemVAT], [TotalValue], [PurchaseDate], [ServiceCategory], [ServicePeriod], [DepreciationRate], [Status]  FROM   Assets where ItemID=@ItemID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ItemID", Data.SqlDbType.Int)
        cmd.Parameters("ItemID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            txtTitle.Text = reader("ItemName").ToString()
            txtCode.Text = reader("ItemCode").ToString()
            txtqty.Text = reader("Quantity").ToString()
            txtDetails.Text = reader("Details").ToString()
            txtBaseValue.Text = reader("BaseValue").ToString()
            txtTax.Text = reader("ItemTax").ToString()
            txtVat.Text = reader("ItemVat").ToString()
            txtTotal.Text = reader("TotalValue").ToString()
            txtPurchaseDate.Text = reader("PurchaseDate").ToString()
            ddlCat.SelectedValue = reader("Category").ToString()
            txtServicePeriod.Text = reader("ServicePeriod").ToString()
            txtDepreRate.Text = reader("DepreciationRate").ToString()
            ddlServiceType.SelectedValue = reader("ServiceCategory").ToString()
            ddlStatus.SelectedValue = reader("Status").ToString()
        Else
            conn.Close()
            Response.Redirect("AllAsset.aspx")
        End If
        conn.Close()
    End Sub


    Protected Sub txtVat_TextChanged(sender As Object, e As EventArgs) Handles txtVat.TextChanged
        'Dim nos As Integer = Integer.Parse(txtqty.Text)
        'Dim base As Double = Double.Parse(txtBaseValue.Text)
        'Dim tax As Decimal = Decimal.Parse(txtTax.Text) / 100
        'Dim vat As Decimal = Decimal.Parse(txtVat.Text) / 100

        'txtTotal.Text = (base + (base * tax) + (base * vat)) * nos
    End Sub
End Class
