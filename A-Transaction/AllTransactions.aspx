﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllTransactions.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Transactions</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i>Add New</button>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Transactions"></asp:Label></h2>
    <div>

        <div class="well">
            <table class="table">
                <thead>
                    <tr>
                        
                        <th>Asset
                        </th>
                        <th>Category
                        </th>
                        <th>Code
                        </th>
                        <th>Total Value
                        </th>
                        <th>Purchase Date
                        </th>
                        <th>
                            Depreciation
                        </th>

                        <th style="width: 60px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="ItemID" DataSourceID="sdsAssets">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="ID" runat="server" Text='<%# Eval("ItemName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("CategoryName") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Parent" runat="server" Text='<%# Eval("ItemCode") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Code" runat="server" Text='<%# Eval("TotalValue") %>' />
                                </td>

                                <td>
                                    <asp:Label ID="Desc" runat="server" Text='<%# DateTime.Parse(Eval("PurchaseDate")).ToString("dd/MM/yyyy") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Depr" runat="server" Text='<%# Eval("DepreciationRate") & " %" %>' />
                                </td>
                                <td>
                                    <a href='<%# "Asset.aspx?aid=" & Eval("ItemID") %>' title="Edit"><i class="icon-pencil"></i></a>&nbsp
                                     <a href='<%# "#" & Eval("ItemID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                                    <div class="modal small hide fade" id='<%# Eval("ItemID") %>' tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×</button>
                                            <h3 id="myModalLabel">Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p class="error-text">
                                                <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">
                                                Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                                Text="Delete" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <asp:SqlDataSource ID="sdsAssets" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT Category.CategoryName, Assets.ItemID, Assets.ItemName, Assets.ItemCode, Assets.TotalValue, Assets.PurchaseDate, Assets.DepreciationRate FROM Assets INNER JOIN Category ON Assets.Category = Category.CatID" 
            DeleteCommand="DELETE FROM [Assets] WHERE [ItemID] = @ItemID">
            <DeleteParameters>
                <asp:Parameter Name="ItemID" />
            </DeleteParameters>
            
        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>
