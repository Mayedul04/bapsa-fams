﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Transaction.aspx.vb" Inherits="Admin_A_HTML_HTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Assets</h1>
    <div class="btn-toolbar">

        <a href="../A-Asset/AllAsset.aspx" data-toggle="modal" class="btn btn-primary"><i class="icon-back"></i>Back</a>
        <div class="btn-group">
        </div>
    </div>

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Asset"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>


    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <asp:Panel ID="pnlTitle" runat="server">
                <div class="left_div">
                    <label>
                        Asset Name :</label>
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>


            </asp:Panel>

            <asp:Panel ID="pnlSmallImage" runat="server">
                <div class="left_div">
                    <label>
                        Category :</label>
                </div>
                <div class="right_div">

                    <asp:DropDownList ID="ddlCat" runat="server" DataSourceID="sdsParent" CssClass="input-xlarge"
                        DataTextField="CategoryName" DataValueField="CatID">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsParent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and CatID>1">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlBigImage" runat="server">
                <div class="left_div">
                    <label>
                        Code :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtCode" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtCode" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>



            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div>
                        <div class="left_div">
                            <label>
                                Quantity(Pcs) :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtqty" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtqty" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div>
                        <div class="left_div">
                            <label>
                                Basic Value :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtBaseValue" runat="server" CssClass="input-xlarge"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtBaseValue" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div>
                        <div class="left_div">
                            <label>
                                Tax(in %) :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtTax" runat="server" CssClass="input-xlarge"></asp:TextBox>

                        </div>
                    </div>
                    <div>
                        <div class="left_div">
                            <label>
                                VAT(in %) :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtVat" AutoPostBack="true" runat="server" CssClass="input-xlarge"></asp:TextBox>

                        </div>
                    </div>
                    <div>
                        <div class="left_div">
                            <label>
                                Total Value :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtTotal" runat="server" ReadOnly="true" CssClass="input-xlarge"></asp:TextBox>

                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div>
                <div class="left_div">
                    <label>
                        Purchase Date :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtPurchaseDate" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtPurchaseDate" runat="server">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtPurchaseDate" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <div class="left_div">
                    <label>
                        Service  Type :</label>
                </div>
                <div class="right_div">

                    <asp:DropDownList ID="ddlServiceType" runat="server" CssClass="input-xlarge">
                        <asp:ListItem Value="">Select Service Type</asp:ListItem>
                        <asp:ListItem Value="Warrenty">Warrenty</asp:ListItem>
                        <asp:ListItem Value="Garrenty">Garrenty</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="ddlServiceType" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <div class="left_div">
                    <label>
                        Service Period(Months) :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtServicePeriod" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtServicePeriod" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <div class="left_div">
                    <label>
                        Depreciation Rate(%) :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtDepreRate" runat="server" CssClass="input-xlarge"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtDepreRate" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div>
                <div class="left_div">
                    <label>
                        Status :</label>
                </div>
                <div class="right_div">
                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="input-xlarge">
                        <asp:ListItem Value="Active">Active</asp:ListItem>
                        <asp:ListItem Value="Disposed">Disposed</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div>
                <div class="left_div">
                    <label>
                        Descriptions :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                        runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                        ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                </div>
            </div>

        </div>

        <div class="btn-toolbar">
            <div class="right_div">
                <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            </div>


            <div class="btn-group">
            </div>
        </div>




    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Assets] WHERE [ItemID] = @ItemID"
        InsertCommand="INSERT INTO [Assets] ([ItemName], [ItemCode], [Category], [Details], [Quantity], [BaseValue], [ItemTax], [ItemVAT], [TotalValue], [PurchaseDate], [ServiceCategory], [ServicePeriod], [DepreciationRate], [Status]) VALUES (@ItemName, @ItemCode, @Category, @Details, @Quantity, @BaseValue, @ItemTax, @ItemVAT, @TotalValue, @PurchaseDate, @ServiceCategory, @ServicePeriod, @DepreciationRate, @Status)"
        SelectCommand="SELECT * FROM [Assets]"
        UpdateCommand="UPDATE [Assets] SET [ItemName] = @ItemName, [ItemCode] = @ItemCode, [Category] = @Category, [Details] = @Details, [Quantity] = @Quantity, [BaseValue] = @BaseValue, [ItemTax] = @ItemTax, [ItemVAT] = @ItemVAT, [TotalValue] = @TotalValue, [PurchaseDate] = @PurchaseDate, [ServiceCategory] = @ServiceCategory, [ServicePeriod] = @ServicePeriod, [DepreciationRate] = @DepreciationRate, [Status] = @Status WHERE [ItemID] = @ItemID">
        <DeleteParameters>
            <asp:Parameter Name="ItemID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtCode" Name="ItemCode" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="ddlCat" Name="Category" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="txtDetails" Name="Details" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtqty" Name="Quantity" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="txtBaseValue" Name="BaseValue" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="txtTax" Name="ItemTax" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="txtVat" Name="ItemVAT" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="txtTotal" Name="TotalValue" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="txtPurchaseDate" DbType="Date" Name="PurchaseDate" PropertyName="Text" />
            <asp:ControlParameter ControlID="ddlServiceType" Name="ServiceCategory" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="txtServicePeriod" Name="ServicePeriod" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="txtDepreRate" Name="DepreciationRate" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="ddlStatus" Name="Status" PropertyName="SelectedValue" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="ItemName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtCode" Name="ItemCode" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="ddlCat" Name="Category" PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="txtDetails" Name="Details" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtqty" Name="Quantity" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="txtBaseValue" Name="BaseValue" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="txtTax" Name="ItemTax" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="txtVat" Name="ItemVAT" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="txtTotal" Name="TotalValue" PropertyName="Text" Type="Single" />
            <asp:ControlParameter ControlID="txtPurchaseDate" DbType="Date" Name="PurchaseDate" PropertyName="Text" />
            <asp:ControlParameter ControlID="ddlServiceType" Name="ServiceCategory" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="txtServicePeriod" Name="ServicePeriod" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="txtDepreRate" Name="DepreciationRate" PropertyName="Text" Type="Double" />
            <asp:ControlParameter ControlID="ddlStatus" Name="Status" PropertyName="SelectedValue" Type="String" />
            <asp:QueryStringParameter Name="ItemID" QueryStringField="aid" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

