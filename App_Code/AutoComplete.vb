﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Common
Imports System.Collections

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class AutoComplete
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function
    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetFullList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 ItemName as Name, ItemID from Assets where ItemName Like '" & prefixText.Trim & "%' and Status='Active' "

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("ItemID").ToString())

            End While
            reader.Close()
        Else
            reader.Close()
            Dim selectString1 As String = "SELECT top 10 ItemCode, ItemID from Assets where ItemCode Like '" & prefixText.Trim & "%' and Status='Active'"
            Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
            Dim reader1 As SqlDataReader = cmd1.ExecuteReader()
            If reader1.HasRows Then
                While reader1.Read()
                    items.Add(reader1("ItemCode").ToString().Trim() & ":" & reader1("ItemID").ToString())

                End While
            End If
            reader1.Close()
        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetCompletionList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 ItemName as Name, ItemID from Assets where ItemName Like '" & prefixText.Trim & "%'  and OpeningDepreciation=0 and Status='Active'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("ItemID").ToString())

            End While
            reader.Close()
        Else
            reader.Close()
            Dim selectString1 As String = "SELECT top 10 ItemCode, ItemID from Assets where ItemCode Like '" & prefixText.Trim & "%'  and OpeningDepreciation=0 and Status='Active'"
            Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
            Dim reader1 As SqlDataReader = cmd1.ExecuteReader()
            If reader1.HasRows Then
                While reader1.Read()
                    items.Add(reader1("ItemCode").ToString().Trim() & ":" & reader1("ItemID").ToString())

                End While
            End If
            reader1.Close()
        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetStartList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 ItemName as Name, ItemID from Assets where ItemName Like '" & prefixText.Trim & "%'  and OpeningDepreciation<>0 and Status='Active'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("ItemID").ToString())

            End While
            reader.Close()
        Else
            reader.Close()
            Dim selectString1 As String = "SELECT top 10 ItemCode, ItemID from Assets where ItemCode Like '" & prefixText.Trim & "%'  and OpeningDepreciation<>0 and Status='Active'"
            Dim cmd1 As SqlCommand = New SqlCommand(selectString1, cn)
            Dim reader1 As SqlDataReader = cmd1.ExecuteReader()
            If reader1.HasRows Then
                While reader1.Read()
                    items.Add(reader1("ItemCode").ToString().Trim() & ":" & reader1("ItemID").ToString())

                End While
            End If
            reader1.Close()
        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetCategoryList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 CategoryName as Name, CatID from Category where CategoryName Like '" & prefixText.Trim & "%'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(String.Format("{0}-{1}", reader("Name"), reader("CatID")))
                ' items.Add(reader("Name").ToString().Trim() & ":" & reader("CatID").ToString())

            End While
            reader.Close()

        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetDepartmentList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 Department as Name, DeptID from Department where Department Like '" & prefixText.Trim & "%'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("DeptID").ToString())

            End While
            reader.Close()

        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetDesignationList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 Designation as Name, DesigID from Designations where Designation Like '" & prefixText.Trim & "%'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("DesigID").ToString())

            End While
            reader.Close()

        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetSupplierList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 Supplier as Name, SupID from Supplier where Supplier Like '" & prefixText.Trim & "%'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("SupID").ToString())

            End While
            reader.Close()

        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetEmployeeList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 EmployeeName as Name, UID from Employees where EmployeeName Like '" & prefixText.Trim & "%'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("UID").ToString())

            End While
            reader.Close()

        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetLocationList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 LocationName as Name, LocID from Location where LocationName Like '" & prefixText.Trim & "%'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("LocID").ToString())

            End While
            reader.Close()

        End If


        cn.Close()


        Return items.ToArray()
    End Function

    <WebMethod()>
    <System.Web.Script.Services.ScriptMethod()>
    Public Function GetProjectList(ByVal prefixText As String) As String()
        Dim count As Integer = 15
        Dim rnd As New Random()
        Dim items As New List(Of String)
        Dim sConn As String
        sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
        'sConn = connections("ConnectionString").ConnectionString
        Dim cn As SqlConnection = New SqlConnection(sConn)
        cn.Open()

        'Use a variable to hold the SQL statement.
        'colums: ID, Name, SmallDetails,tableName
        Dim selectString As String = "SELECT top 10 ProjectName as Name, ProjID from Projects where ProjectName Like '" & prefixText.Trim & "%'"

        Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                items.Add(reader("Name").ToString().Trim() & ":" & reader("ProjID").ToString())

            End While
            reader.Close()

        End If


        cn.Close()


        Return items.ToArray()
    End Function
End Class