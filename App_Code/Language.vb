﻿Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Collections.Generic

Public Class Language

    Public Shared Lang As Dictionary(Of String, String)

    Public Shared Function Read(ByVal word As String, ByVal ln As String) As String
        Dim retVal As String = word
        If ln <> "en" Then
            If Lang Is Nothing Then
                LoadLangDictionary(ln)
            End If
            Try
                retVal = Lang(word.ToLower())

            Catch ex As Exception

                retVal = word '& ex.Message
            Finally
                If Utility.isLoggedIn(Request:=HttpContext.Current.Request) Then
                    retVal = "<div class='cms-editview' style='display:inline'>" & _                            "    <div class='cms-text cms-text-style'>" & retVal & "</div>" & _                            "    <div class='cms-buttons cms-buttons-style'>" & _                            "        <a href='javascript:;' name='btnCMSInlineEdit' id='btnCMSInlineEdit'><img src='/Admin/assets/images/icons/switch3.png' /></a>" & _                            "    </div>" & _                            "</div>" & _                            "<div class='cms-updateview cms-updateview-style cms-hide'>" & _                            "    <input id='lang' name='lang' type='hidden' value='" & ln & "' />" & _                            "    <input id='enVertionHTML' name='enVertionHTML' type='hidden' value='" & word & "' />" & _                            "    <input name='txtCMSInlineText' type='text' id='txtCMSInlineText' value='" & retVal & "' />" & _                            "    <a href='javascript:;' name='btnCMSInlineUpdate' id='btnCMSInlineUpdate'><img src='/Admin/assets/images/icons/edit.jpg' /></a>" & _                            "    <a href='javascript:;' name='btnCMSInlineCancel' id='btnCMSInlineCancel'><img src='/Admin/assets/images/icons/delete.png' /></a>" & _                            "    <div style='display:none;' name='CMSloading'><img src='/Admin/assets/images/loading-x.gif' width='60' /></div>" & _                            "</div>"
                Else

                End If
            End Try
        End If
        


        Return retVal
    End Function

    Public Shared Sub LoadLangDictionary(ByVal ln As String)
        Lang = New Dictionary(Of String, String)

        Using sr As New StreamReader(AppDomain.CurrentDomain.BaseDirectory & "\lang\" & ln & ".txt")
            Dim line As String
            line = sr.ReadToEnd
            Dim lines As String() = line.Split(New String() {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
            For Each l As String In lines
                Try
                    Dim words As String() = l.Split(New String() {":="}, StringSplitOptions.RemoveEmptyEntries)
                    Lang.Add(words(0).ToLower(), words(1))
                Catch ex As Exception

                End Try

            Next

        End Using
    End Sub

End Class
