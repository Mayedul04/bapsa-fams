﻿Imports Microsoft.VisualBasic

Public Class GlobalVariables
    Private _financialYearStart As Date
    Public Property FinancialYearStart() As Date
        Get
            Return _financialYearStart
        End Get
        Set(ByVal value As Date)
            _financialYearStart = value
        End Set
    End Property
    Private _userID As Integer
    Public Property UserID() As Integer
        Get
            Return _userID
        End Get
        Set(ByVal value As Integer)
            _userID = value
        End Set
    End Property
    Private _currentfinancestart As Date
    Public Property CurrentFiananceYearStart() As Date
        Get
            Return _currentfinancestart
        End Get
        Set(ByVal value As Date)
            _currentfinancestart = value
        End Set
    End Property
End Class
