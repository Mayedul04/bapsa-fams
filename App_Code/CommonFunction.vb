﻿Imports Microsoft.VisualBasic

Public Class CommonFunction

    Public Function CallAFunction(ByVal functionName As String, ByVal ParamArray paramValues() As String) As String
        If functionName = "GetHotelGallery" Then
            Return GetHotelGallery(paramValues(0))
        End If
        Return ""
    End Function

    Public Function GetHotelGallery(ByVal HotelMasterID As String) As String
        Dim retVal = New StringBuilder()

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "SELECT        GalleryItemID, Title, SmallDetails, SmallImage, BigImage, ImageAltText, SortIndex, Status, LastUpdated, Category " & _
        " FROM GalleryItem " & _
        " WHERE        (GalleryID = @HotelMasterID)"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("HotelMasterID", Data.SqlDbType.Int).Value = HotelMasterID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim category As String = ""
        While reader.Read
            If category <> reader("Category") Then
                category = reader("Category").ToString()
                retVal.Append(If(category <> "", "</div>", "") & "<h3>" & category & "</h3><div class=""clearfix"">")

            End If
            retVal.Append("<div class=""galleryimg"">" & _
                          "    <a href=""/admin/" & reader("BigImage") & """ class=""galleryThumb"" title=""" & reader("ImageAltText") & """>" & _
                          "    <img src=""/admin/" & reader("BigImage") & """ alt=""" & reader("ImageAltText") & """></a>" & _
                          "</div>")
        End While
        If retVal.Length > 0 Then
            retVal.Append("</div>")
        End If

        conn.Close()

        Return retVal.ToString()
    End Function

End Class
