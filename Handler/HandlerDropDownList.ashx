﻿<%@ WebHandler Language="VB" Class="HandlerCruisingSailing" %>

Imports System
Imports System.Web

Public Class HandlerCruisingSailing : Implements IHttpHandler

    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim filter = context.Request("FilterID")
        Dim retVal = New StringBuilder()
        Dim outputString As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim locationsql = "SELECT [LocID] as ID, [LocationName] as Name FROM [Location] WHERE ([Status] = 'True')"
        Dim projectsql = "SELECT [ProjID] as ID, [ProjectName] as Name FROM [Projects] WHERE ([Status] = 'True')"
        Dim departmentsql = "SELECT [DeptID] as ID, [Department] as Name FROM [Department] WHERE ([Status] = 'True' and DeptID>1)"
        Dim suppliersql = "SELECT [SupID] as ID, [Supplier] as Name FROM [Supplier] WHERE ([Status] = 'True')"
        Dim subgroupsql = "SELECT [CatID] as ID, [CategoryName] as Name FROM [Category] WHERE ([Status] = 'True' and ParentID>1)"

        Dim selectString = ""
        If (filter = 2) Then
            selectString = locationsql
        ElseIf (filter = 3) Then
            selectString = projectsql
        ElseIf (filter = 4) Then
            selectString = departmentsql
        ElseIf (filter = 5) Then
            selectString = suppliersql
        ElseIf (filter = 6) Then
            selectString = suppliersql
        ElseIf (filter = 1) Then
            selectString = "SELECT [CatID] as ID, [CategoryName] as Name FROM [Category] WHERE ([Status] = 'True' and ParentID=1)"
        End If

        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim i As Int16 = 0
        ' retVal.Append("<option Value="""" Text=""Please Select""></option>")

        While reader.Read
            outputString += "{" & String.Format("""Value"":""{0}"",""Text"":""{1}""", reader("ID").ToString(), reader("Name")) & "},"
            retVal.Append("<option Value=""" & reader("ID") & """ Text=""" & reader("Name") & """></option>")
            i += 1
        End While

        conn.Close()

        'context.Response.Write(retVal.ToString())
        context.Response.Write("[" & outputString.TrimEnd(New Char() {","}) & "]")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class