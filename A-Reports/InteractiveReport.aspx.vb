﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports System.Web.Services
Imports Microsoft.Reporting.WebForms

Partial Class A_Reports_MasterReport
    Inherits System.Web.UI.Page
    Public locationsql As String = "SELECT [LocID] as ID, [LocationName] as Name FROM [Location] WHERE ([Status] = 'True')"
    Public projectsql As String = "SELECT [ProjID] as ID, [ProjectName] as Name FROM [Projects] WHERE ([Status] = 'True')"
    Public departmentsql As String = "SELECT [DeptID] as ID, [Department] as Name FROM [Department] WHERE ([Status] = 'True' and DeptID>1)"
    Public suppliersql As String = "SELECT [SupID] as ID, [Supplier] as Name FROM [Supplier] WHERE ([Status] = 'True')"
    Public subgroupsql As String = "SELECT [CatID] as ID, [CategoryName] as Name FROM [Category] WHERE ([Status] = 'True' and ParentID>1)"
    Public maingroupsql As String = "SELECT [CatID] as ID, [CategoryName] as Name FROM [Category] WHERE ([Status] = 'True' and ParentID=1)"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If (IsPostBack = False) Then
            'Me.ReportViewer1.DataBind()
            'Me.ReportViewer1.LocalReport.Refresh()
        Else
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
        End If
        ReportViewer1.AsyncRendering = True
    End Sub

    '    Protected Sub btnAllAssets_Click(sender As Object, e As System.EventArgs) Handles btnAllAssets.ServerClick
    '        Dim seleccommand = "SELECT        dbo.Assets.ItemName, dbo.Assets.ItemCode, Category_1.CategoryName, dbo.Location.LocationName, dbo.Projects.ProjectName, dbo.Supplier.Supplier, " &
    '                        " dbo.Department.Department, dbo.Assets.Details, dbo.Assets.BaseValue, dbo.Assets.ItemTax, dbo.Assets.ItemVAT, dbo.Assets.TotalValue, dbo.Assets.PurchaseDate, " &
    '                      "   dbo.Assets.OpeningDepreciation, dbo.Assets.DepreciationRate, dbo.Assets.DepreciationModel " &
    ' " FROM            dbo.Category AS Category_1 INNER JOIN " &
    '                       "  dbo.Assets ON Category_1.CatID = dbo.Assets.Category INNER JOIN " &
    '                        " dbo.Location ON dbo.Assets.Location = dbo.Location.LocID INNER JOIN " &
    '                         " dbo.Projects On dbo.Assets.Project = dbo.Projects.ProjID INNER JOIN " &
    '                         " dbo.Supplier ON dbo.Assets.Supplier = dbo.Supplier.SupID INNER JOIN" &
    '                         " dbo.Department ON dbo.Assets.Department = dbo.Department.DeptID"
    '        Utility.DownloadCSV(Utility.EncodeTitle("All Assets " & DateTime.Now, "-") & ".csv", seleccommand, Response)
    '    End Sub
    '    Protected Sub btnDoCalculation_Click(sender As Object, e As System.EventArgs) Handles btnDoCalculation.ServerClick
    '        Dim depremode As String
    '        Dim closingvalue As Double = 0
    '        Dim closingdetre As Double = 0
    '        Dim newclosingvalue As Double = 0
    '        Dim newclosingdetre As Double = 0
    '        Dim rate As Double
    '        Dim itemid As Integer
    '        Dim daydifference As Integer = 0
    '        Dim lastcalculated As Date
    '        Dim remainingyear As Integer = 0

    '        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)

    '        conn.Open()
    '        Dim cmd As SqlCommand
    '        Dim AssetList As List(Of Assets) = New List(Of Assets)
    '        Dim seleccommand = "SELECT * from  dbo.Assets where Status='Active'"
    '        cmd = New SqlCommand(seleccommand, conn)
    '        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
    '        If reader.HasRows Then
    '            While reader.Read
    '                Dim asset As Assets = New Assets()
    '                asset.ItemID = Integer.Parse(reader("ItemID").ToString())
    '                asset.Model = reader("DepreciationModel").ToString()
    '                asset.DepreciationRate = Double.Parse(reader("DepreciationRate").ToString())
    '                asset.ItemName = reader("ItemName").ToString()
    '                asset.CatID = Integer.Parse(reader("Category").ToString())
    '                asset.Sumofyears = Integer.Parse(reader("SumofYear").ToString())
    '                AssetList.Add(asset)
    '            End While
    '        End If
    '        conn.Close()

    '        For Each item In AssetList
    '            conn.Open()
    '            Dim seleccommand1 = "SELECT top 1.* from  dbo.YearlyJournal where ItemID=@ItemID order by ID Desc"
    '            cmd = New SqlCommand(seleccommand1, conn)
    '            cmd.Parameters.Add("ItemID", SqlDbType.Int, 32).Value = item.ItemID
    '            reader = cmd.ExecuteReader()
    '            If reader.HasRows Then
    '                reader.Read()
    '                closingvalue = Double.Parse(reader("ClosingCost").ToString())
    '                closingdetre = Double.Parse(reader("ClosingDepreciation").ToString())
    '                lastcalculated = Date.Parse(reader("CalculatedDate").ToString())
    '                remainingyear = Integer.Parse(reader("RemainingYear").ToString())

    '            End If
    '            reader.Close()
    '            conn.Close()
    '            If (item.Model = "Declining Balance") Then
    '                daydifference = Date.Today.Subtract(lastcalculated).Days
    '                Dim currentdepre As Double = (closingvalue * (item.DepreciationRate / 100) * (daydifference / 365))
    '                newclosingdetre = closingdetre + currentdepre
    '                newclosingvalue = closingvalue - newclosingdetre
    '                sdsYearJournal.InsertParameters.Item(0).DefaultValue = item.ItemID
    '                sdsYearJournal.InsertParameters.Item(1).DefaultValue = item.ItemName
    '                sdsYearJournal.InsertParameters.Item(2).DefaultValue = item.CatID
    '                sdsYearJournal.InsertParameters.Item(3).DefaultValue = 0
    '                sdsYearJournal.InsertParameters.Item(4).DefaultValue = newclosingvalue
    '                sdsYearJournal.InsertParameters.Item(5).DefaultValue = Date.Today
    '                sdsYearJournal.InsertParameters.Item(6).DefaultValue = 0
    '                If (sdsYearJournal.Insert > 0) Then
    '                    divSuccess.Visible = True
    '                    lblSuccMessage.Text = "Calculation Done Successfully."
    '                Else
    '                    divError.Visible = True
    '                End If
    '            ElseIf (item.Model = "Straight Line") Then
    '                daydifference = Date.Today.Subtract(lastcalculated).Days
    '                Dim currentdepre As Double = (closingvalue * (item.DepreciationRate / 100) * (daydifference / 365))
    '                newclosingdetre = closingdetre + currentdepre


    '                sdsYearJournal.InsertParameters.Item(0).DefaultValue = item.ItemID
    '                sdsYearJournal.InsertParameters.Item(1).DefaultValue = item.ItemName
    '                sdsYearJournal.InsertParameters.Item(2).DefaultValue = item.CatID
    '                sdsYearJournal.InsertParameters.Item(3).DefaultValue = newclosingdetre
    '                sdsYearJournal.InsertParameters.Item(4).DefaultValue = closingvalue
    '                sdsYearJournal.InsertParameters.Item(5).DefaultValue = Date.Today
    '                sdsYearJournal.InsertParameters.Item(6).DefaultValue = 0
    '                If (sdsYearJournal.Insert > 0) Then
    '                    divSuccess.Visible = True
    '                    lblSuccMessage.Text = "Calculation Done Successfully."
    '                Else
    '                    divError.Visible = True
    '                End If
    '            ElseIf (item.Model = "Sum of Years") Then
    '                Dim currentdepre As Double = (closingvalue * (remainingyear / item.Sumofyears))
    '                newclosingdetre = closingdetre + currentdepre

    '                sdsYearJournal.InsertParameters.Item(0).DefaultValue = item.ItemID
    '                sdsYearJournal.InsertParameters.Item(1).DefaultValue = item.ItemName
    '                sdsYearJournal.InsertParameters.Item(2).DefaultValue = item.CatID
    '                sdsYearJournal.InsertParameters.Item(3).DefaultValue = newclosingdetre
    '                sdsYearJournal.InsertParameters.Item(4).DefaultValue = closingvalue
    '                sdsYearJournal.InsertParameters.Item(5).DefaultValue = Date.Today
    '                sdsYearJournal.InsertParameters.Item(6).DefaultValue = remainingyear - 1
    '                If (sdsYearJournal.Insert > 0) Then
    '                    divSuccess.Visible = True
    '                    lblSuccMessage.Text = "Calculation Done Successfully."
    '                Else
    '                    divError.Visible = True
    '                End If

    '            End If

    '        Next
    '    End Sub
    '    Protected Sub btnReval_Click(sender As Object, e As System.EventArgs) Handles btnReval.ServerClick
    '        Dim seleccommand = "SELECT        dbo.Assets.ItemName, dbo.Assets.ItemCode, dbo.RevaluationTracking.RevaluationSarplus, dbo.RevaluationTracking.RavaluationDate, " &
    '                        " dbo.YearlyJournal.ClosingCost, dbo.YearlyJournal.ClosingDepreciation " &
    '" FROM            dbo.YearlyJournal INNER JOIN " &
    '                       "  dbo.Assets ON dbo.YearlyJournal.ItemID = dbo.Assets.ItemID INNER JOIN " &
    '                       "  dbo.RevaluationTracking ON dbo.Assets.ItemID = dbo.RevaluationTracking.ItemID " &
    '" WHERE        (dbo.YearlyJournal.IsRevaluated = 1)"
    '        Utility.DownloadCSV(Utility.EncodeTitle("Revaluations " & DateTime.Now, "-") & ".csv", seleccommand, Response)
    '    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick


        Dim sqlstring As String = ""
        If (ddlSecondary.SelectedIndex = 2 And ddlTertiary.SelectedIndex = 1) Then
            divError.Visible = True
            lblErrMessage.Text = "Main Category can not go under Sub Category"
            Exit Sub
        End If
        Dim startdate As Date
        Dim enddate As Date

        If (txtStartDate.Text <> "") Then
            startdate = Date.Parse(txtStartDate.Text)
        Else
            startdate = Date.Parse(Context.Items("FinancialYearStart"))
        End If
        If (txtEndDate.Text <> "") Then
            enddate = Date.Parse(txtEndDate.Text)
        Else
            enddate = Date.Today
        End If

        If (ddlPrimary.SelectedValue <> "" And ddlSecondary.SelectedValue <> "" And ddlTertiary.SelectedValue <> "") Then


            sqlstring = "SELECT    " & ddlPrimary.SelectedValue & " as SupplierName,    " & ddlTertiary.SelectedValue & " as CategoryName, ReportforAsset_1.ItemID, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
                       "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost , ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
                        " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.PurchaseDate, ReportforAsset_1.RevaluationSurplus, ReportforAsset_1.DisposedValue, ReportforAsset_1.ResellValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
                        " ReportforAsset_1.Location, ReportforAsset_1.Category, " & ddlSecondary.SelectedValue & " as Parent" &
                        " FROM   dbo.Category INNER JOIN "
            Dim parameters As New List(Of ReportParameter)
            Dim counter As Integer = 0
            For i = 0 To chkParameters.Items.Count - 1
                parameters.Add(New ReportParameter(chkParameters.Items(i).Value, If(chkParameters.Items(i).Selected = True, False, True)))
                If (chkParameters.Items(i).Selected = True) Then
                    counter += 1
                End If

            Next
            If (counter < 7) Then
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("/A-Reports/ReportInteractive2L_Potrait.rdlc")
            Else
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("/A-Reports/ReportInteractive.rdlc")
            End If

            parameters.Add(New ReportParameter("Expand", If(chkExpand.Checked = True, True, False)))
            ' parameters.Add(New ReportParameter("Details", If(chkExpand.Checked = True, True, False)))
            parameters.Add(New ReportParameter("StartDate", startdate))
            parameters.Add(New ReportParameter("EndDate", enddate))
            ReportViewer1.LocalReport.SetParameters(parameters)

        ElseIf (ddlPrimary.SelectedValue <> "" And ddlSecondary.SelectedValue = "" And ddlTertiary.SelectedValue = "") Then



            sqlstring = "SELECT    " & ddlPrimary.SelectedValue & " as SupplierName,    'Tertiary' as CategoryName, ReportforAsset_1.ItemID, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
                       "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost , ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
                        " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.PurchaseDate, ReportforAsset_1.RevaluationSurplus, ReportforAsset_1.DisposedValue, ReportforAsset_1.ResellValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
                        " ReportforAsset_1.Location, ReportforAsset_1.Category, 'Secondary' as Parent" &
                        " FROM   dbo.Category INNER JOIN "
            Dim parameters As New List(Of ReportParameter)
            Dim counter As Integer = 0
            For i = 0 To chkParameters.Items.Count - 1
                parameters.Add(New ReportParameter(chkParameters.Items(i).Value, If(chkParameters.Items(i).Selected = True, False, True)))
                If (chkParameters.Items(i).Selected = True) Then
                    counter += 1
                End If

            Next
            If (counter < 7) Then
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("/A-Reports/GL_protrait.rdlc")
            Else
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("/A-Reports/ReportInteractiveGL.rdlc")
            End If

            parameters.Add(New ReportParameter("Expand", If(chkExpand.Checked = True, False, True)))
            parameters.Add(New ReportParameter("StartDate", startdate))
            parameters.Add(New ReportParameter("EndDate", enddate))
            ReportViewer1.LocalReport.SetParameters(parameters)

        ElseIf (ddlPrimary.SelectedValue <> "" And ddlSecondary.SelectedValue <> "" And ddlTertiary.SelectedValue = "") Then

            sqlstring = "SELECT    " & ddlPrimary.SelectedValue & " as SupplierName,    'Blank' as CategoryName, ReportforAsset_1.ItemID, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
                       "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost , ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
                        " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.PurchaseDate, ReportforAsset_1.RevaluationSurplus, ReportforAsset_1.DisposedValue, ReportforAsset_1.ResellValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
                        " ReportforAsset_1.Location, ReportforAsset_1.Category, " & ddlSecondary.SelectedValue & " as Parent" &
                        " FROM   dbo.Category INNER JOIN "
            Dim parameters As New List(Of ReportParameter)
            Dim counter As Integer = 0
            For i = 0 To chkParameters.Items.Count - 1
                parameters.Add(New ReportParameter(chkParameters.Items(i).Value, If(chkParameters.Items(i).Selected = True, False, True)))
                If (chkParameters.Items(i).Selected = True) Then
                    counter += 1
                End If

            Next
            If (counter < 7) Then
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("/A-Reports/ReportInteractive2L_Potrait.rdlc")
            Else
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("/A-Reports/ReportInteractive2L.rdlc")
            End If

            parameters.Add(New ReportParameter("Expand", If(chkExpand.Checked = True, False, True)))
            parameters.Add(New ReportParameter("StartDate", startdate))
            parameters.Add(New ReportParameter("EndDate", enddate))
            ReportViewer1.LocalReport.SetParameters(parameters)
        End If


        sqlstring += "  dbo.ReportforAsset('" & startdate.Year & "/" & startdate.Month & "/" & startdate.Day & "', '" & enddate.Year & "/" & enddate.Month & "/" & enddate.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN "


        sqlstring += " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID "
        If (ddlPrimary.SelectedIndex = 2 Or ddlSecondary.SelectedIndex = 4 Or ddlTertiary.SelectedIndex = 4) Then
            sqlstring += "  INNER JOIN Location ON ReportforAsset_1.Location = Location.LocID "
        End If

        If (ddlPrimary.SelectedIndex = 4 Or ddlSecondary.SelectedIndex = 5 Or ddlTertiary.SelectedIndex = 5) Then
            sqlstring += "  INNER JOIN Department ON ReportforAsset_1.Department = Department.DeptID "
        End If

        If (ddlPrimary.SelectedIndex = 5 Or ddlSecondary.SelectedIndex = 6 Or ddlTertiary.SelectedIndex = 6) Then
            sqlstring += "  INNER JOIN dbo.Supplier ON ReportforAsset_1.Supplier = dbo.Supplier.SupID "
        End If
        If (ddlPrimary.SelectedIndex = 3 Or ddlSecondary.SelectedIndex = 3 Or ddlTertiary.SelectedIndex = 2) Then
            sqlstring += "  INNER JOIN dbo.Projects ON ReportforAsset_1.Project = dbo.Projects.ProjID "
        End If

        If (ddlPrimary.SelectedIndex > 0) Then
            If (ddlPrimaryFilter.SelectedValue > 0) Then

                If (ddlPrimary.SelectedIndex = 1) Then
                    sqlstring += " where Category_1.CatID=" & ddlPrimaryFilter.SelectedValue
                ElseIf (ddlPrimary.SelectedIndex = 2) Then
                    sqlstring += " where ReportforAsset_1.Location=" & ddlPrimaryFilter.SelectedValue
                ElseIf (ddlPrimary.SelectedIndex = 3) Then
                    sqlstring += " where ReportforAsset_1.Project=" & ddlPrimaryFilter.SelectedValue
                ElseIf (ddlPrimary.SelectedIndex = 4) Then
                    sqlstring += " where ReportforAsset_1.Department=" & ddlPrimaryFilter.SelectedValue
                ElseIf (ddlPrimary.SelectedIndex = 5) Then
                    sqlstring += " where ReportforAsset_1.Supplier=" & ddlPrimaryFilter.SelectedValue
                Else
                    sqlstring += ""
                End If
            End If
        Else
            sqlstring += ""
        End If


        If (ddlSecondary.SelectedIndex > 0) Then
            If (ddlSecondaryFilter.SelectedValue > 0) Then

                If (ddlSecondary.SelectedIndex = 1) Then
                    sqlstring += " and Category_1.CatID=" & ddlSecondaryFilter.SelectedValue
                ElseIf (ddlSecondary.SelectedIndex = 2) Then
                    sqlstring += " and ReportforAsset_1.Category=" & ddlSecondaryFilter.SelectedValue
                ElseIf (ddlSecondary.SelectedIndex = 3) Then
                    sqlstring += " and ReportforAsset_1.Project=" & ddlSecondaryFilter.SelectedValue
                ElseIf (ddlSecondary.SelectedIndex = 4) Then
                    sqlstring += " and ReportforAsset_1.Location=" & ddlSecondaryFilter.SelectedValue
                ElseIf (ddlSecondary.SelectedIndex = 5) Then
                    sqlstring += " and ReportforAsset_1.Department=" & ddlSecondaryFilter.SelectedValue
                ElseIf (ddlSecondary.SelectedIndex = 6) Then
                    sqlstring += " and ReportforAsset_1.Supplier=" & ddlSecondaryFilter.SelectedValue
                Else
                    sqlstring += ""
                End If
            End If
        Else
            sqlstring += ""
        End If

        If (ddlTertiaryFilter.SelectedIndex > 0) Then
            If (ddlTertiaryFilter.SelectedValue > 0) Then
                If (ddlTertiary.SelectedIndex = 1) Then
                    sqlstring += " and Category_1.CatID=" & ddlTertiaryFilter.SelectedValue
                ElseIf (ddlTertiary.SelectedIndex = 2) Then
                    sqlstring += " and ReportforAsset_1.Category=" & ddlTertiaryFilter.SelectedValue
                ElseIf (ddlTertiary.SelectedIndex = 3) Then
                    sqlstring += " and ReportforAsset_1.Project=" & ddlTertiaryFilter.SelectedValue
                ElseIf (ddlTertiary.SelectedIndex = 4) Then
                    sqlstring += " and ReportforAsset_1.Location=" & ddlTertiaryFilter.SelectedValue
                ElseIf (ddlTertiary.SelectedIndex = 5) Then
                    sqlstring += " and ReportforAsset_1.Department=" & ddlTertiaryFilter.SelectedValue
                ElseIf (ddlTertiary.SelectedIndex = 6) Then
                    sqlstring += " and ReportforAsset_1.Supplier=" & ddlTertiaryFilter.SelectedValue
                Else
                    sqlstring += ""
                End If
            End If
        Else
            sqlstring += ""
        End If

        ReportViewer1.Visible = True
        If (ddlPrimary.SelectedValue = "" And ddlSecondary.SelectedValue = "" And ddlTertiary.SelectedValue = "") Then
            sqlstring = ""
        End If




        If (sqlstring <> "") Then
            SqlDataSource1.SelectCommand = sqlstring
            SqlDataSource1.DataBind()
            SqlDataSource2.DataBind()
            If (ReportViewer1.LocalReport.DataSources.Count > 0) Then
                ReportViewer1.LocalReport.DataSources.RemoveAt(0)
            End If
            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource(“DataSet1”, SqlDataSource1))
            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource(“DataSet2”, SqlDataSource2))

        Else
            lblErrMessage.Text = "Please select atleast one group"
            divError.Visible = True
            ReportViewer1.Visible = False
        End If

        Me.ReportViewer1.DataBind()
        Me.ReportViewer1.LocalReport.Refresh()






    End Sub


    Protected Sub ddlPrimaryFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrimaryFilter.SelectedIndexChanged
        hdnPrimaryfilterID.Value = ddlPrimaryFilter.SelectedValue
    End Sub
    Protected Sub ddlPrimary_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPrimary.SelectedIndexChanged
        If (ddlPrimary.SelectedIndex = 2) Then
            sdsPrimaryFilter.SelectCommand = locationsql
        ElseIf (ddlPrimary.SelectedIndex = 3) Then
            sdsPrimaryFilter.SelectCommand = projectsql
        ElseIf (ddlPrimary.SelectedIndex = 4) Then
            sdsPrimaryFilter.SelectCommand = departmentsql
        ElseIf (ddlPrimary.SelectedIndex = 5) Then
            sdsPrimaryFilter.SelectCommand = suppliersql
        ElseIf (ddlPrimary.SelectedIndex = 1) Then
            sdsPrimaryFilter.SelectCommand = maingroupsql
        Else
            sdsPrimaryFilter.SelectCommand = ""
        End If
        ddlPrimaryFilter.DataBind()
        Dim listitem As ListItem = New ListItem("All Items", 0)

        If (ddlPrimaryFilter.Items.Count > 0) Then
            ddlPrimaryFilter.Items.Clear()
            ddlPrimaryFilter.AppendDataBoundItems = True
            ddlPrimaryFilter.Items.Add(listitem)
        End If
    End Sub
    Protected Sub ddlSecondary_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSecondary.SelectedIndexChanged
        If (ddlSecondary.SelectedIndex = 2) Then
            sdsSecondFilter.SelectCommand = subgroupsql
        ElseIf (ddlSecondary.SelectedIndex = 3) Then
            sdsSecondFilter.SelectCommand = projectsql
        ElseIf (ddlSecondary.SelectedIndex = 4) Then
            sdsSecondFilter.SelectCommand = locationsql
        ElseIf (ddlSecondary.SelectedIndex = 5) Then
            sdsSecondFilter.SelectCommand = departmentsql
        ElseIf (ddlSecondary.SelectedIndex = 6) Then
            sdsSecondFilter.SelectCommand = suppliersql
        ElseIf (ddlSecondary.SelectedIndex = 1) Then
            sdsSecondFilter.SelectCommand = maingroupsql
        End If
        ddlSecondaryFilter.DataBind()
        Dim listitem As ListItem = New ListItem("All Items", 0)
        If (ddlSecondaryFilter.Items.Count > 0) Then
            ddlSecondaryFilter.Items.Clear()
            ddlSecondaryFilter.AppendDataBoundItems = True
            ddlSecondaryFilter.Items.Add(ListItem)
        End If

    End Sub
    Protected Sub ddlTertiary_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTertiary.SelectedIndexChanged


        If (ddlTertiary.SelectedIndex = 2) Then
            sdsTertairyFilter.SelectCommand = projectsql
        ElseIf (ddlTertiary.SelectedIndex = 3) Then
            sdsTertairyFilter.SelectCommand = subgroupsql
        ElseIf (ddlTertiary.SelectedIndex = 4) Then
            sdsTertairyFilter.SelectCommand = locationsql
        ElseIf (ddlTertiary.SelectedIndex = 5) Then
            sdsTertairyFilter.SelectCommand = departmentsql
        ElseIf (ddlTertiary.SelectedIndex = 6) Then
            sdsTertairyFilter.SelectCommand = suppliersql
        ElseIf (ddlTertiary.SelectedIndex = 1) Then
            sdsTertairyFilter.SelectCommand = maingroupsql
        End If
        ddlTertiaryFilter.DataBind()
        Dim listitem As ListItem = New ListItem("All Items", 0)
        If (ddlTertiaryFilter.Items.Count > 0) Then
            ddlTertiaryFilter.Items.Clear()
            ddlTertiaryFilter.AppendDataBoundItems = True
            ddlTertiaryFilter.Items.Add(ListItem)
        End If
    End Sub
End Class
Public Class Assets
    Private _itemid As Integer
    Public Property ItemID() As Integer
        Get
            Return _itemid
        End Get
        Set(ByVal value As Integer)
            _itemid = value
        End Set
    End Property
    Private _deprirate As Double
    Public Property DepreciationRate() As Double
        Get
            Return _deprirate
        End Get
        Set(ByVal value As Double)
            _deprirate = value
        End Set
    End Property
    Private _deprimodel As String
    Public Property Model() As String
        Get
            Return _deprimodel
        End Get
        Set(ByVal value As String)
            _deprimodel = value
        End Set
    End Property
    Private _itemname As String
    Public Property ItemName() As String
        Get
            Return _itemname
        End Get
        Set(ByVal value As String)
            _itemname = value
        End Set
    End Property
    Private _catid As Integer
    Public Property CatID() As Integer
        Get
            Return _catid
        End Get
        Set(ByVal value As Integer)
            _catid = value
        End Set
    End Property
    Private _sumofyears As Integer
    Public Property Sumofyears() As Integer
        Get
            Return _sumofyears
        End Get
        Set(ByVal value As Integer)
            _sumofyears = value
        End Set
    End Property
End Class
