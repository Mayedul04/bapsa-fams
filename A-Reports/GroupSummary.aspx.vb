﻿
Imports System.Collections.Generic
Imports System.Data
Imports System.IO

Imports System.Data.SqlClient

Partial Class Admin_A_Contact_AllContact
    Inherits System.Web.UI.Page
    Public GropupLevel1 As List(Of Integer) = New List(Of Integer)()
    Public domainName As String
    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not Me.IsPostBack Then
            If (RouteData.Values("gid") Is Nothing) Then
                Me.BindListView(1, "2017/02/21", "" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "")
            Else
                Dim constr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim connection As SqlConnection = New SqlConnection(constr)
                Dim Adapter As SqlDataAdapter = New SqlDataAdapter()
                Dim Sql As String = ""
                Sql = "SELECT * FROM dbo.[GroupSummary](1,'2017/02/01','2017/02/21', '" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "') where Parent=" & RouteData.Values("gid").ToString()
                connection.Open()
                Adapter.SelectCommand = New SqlCommand(Sql, connection)
                Adapter.SelectCommand.Parameters.Add("Parent", SqlDbType.Int, 32).Value = 1
                Dim reader As SqlDataReader = Adapter.SelectCommand.ExecuteReader()
                Dim assets As Integer
                If (reader.HasRows) Then
                    connection.Close()
                    Me.BindListView(Integer.Parse(RouteData.Values("gid").ToString()), "2017/02/21", "" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "")
                Else
                    connection.Close()
                    Response.Redirect(domainName & "AssetReport/" & RouteData.Values("gid").ToString())
                End If

            End If

        End If
    End Sub
    Private Function LoadReport(groupid As Integer, startdate As Date, enddate As Date) As String
        Dim str As String = ""
        str += "<table  cellspacing=""0"" cellpadding=""0"" border=""0"" style=""width:100%;border-collapse:collapse;border: 1px solid #d7d7d7;border-color:d7d7d7"">"
        str += "<tbody><tr class=""tr-title-bg"">"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Code</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Category</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Opening Cost</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Added Value</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Closing Cost</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Rate</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Opening Depreciation</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Adjusted Depreciation</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Closing Depreciation</th>"
        str += "<th style=""border-bottom: 1px solid #d7d7d7;background: #f2f2f2;height: 21px;padding: 4px 4px 4px 10px;line-height:21px;color:#343434;font-size:14px;font-family:Arial, Helvetica, sans-serif;"" scope=""col"">"
        str += "Written Value</th>"
        str += "</tr>"

        Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim sqlConn As SqlConnection = New SqlConnection(strConn)
        sqlConn.Open()
        Dim sqlcomm As SqlCommand = New SqlCommand()

        If (groupid <> 1) Then
            sqlcomm.CommandText = "Select * FROM dbo.[GroupSummary](1,'2017/02/01','" & startdate.ToString("yyyy/MM/dd") & "','" & enddate.ToString("yyyy/MM/dd") & "') where Parent=@CatID"
            sqlcomm.Parameters.Add("CatID", Data.SqlDbType.Int, 32, "CatID").Value = groupid
        Else
            sqlcomm.CommandText = "SELECT   GroupSummary_1.Parent AS Category, SUM(GroupSummary_1.WrittenValue) AS WrittenValue, SUM(GroupSummary_1.ClosingDepreciation) AS ClosingDepreciation, " &
                        " SUM(GroupSummary_1.ClosingCost) AS ClosingCost, SUM(GroupSummary_1.Openingdepreciation) AS Openingdepreciation, SUM(GroupSummary_1.OpeningCost) AS OpeningCost, " &
                        " SUM(GroupSummary_1.AdditionalValue) AS AdditionalValue, AVG(GroupSummary_1.DepreciationRate) AS DepreciationRate, SUM(GroupSummary_1.AdjustedDepreciation) AS AdjustedDepreciation,  " &
                        " dbo.Category.CategoryName, dbo.Category.Code " &
                        " FROM  dbo.GroupSummary(" & 1 & ",'2017/02/01', '" & startdate.ToString("yyyy/MM/dd") & "', '" & enddate.ToString("yyyy/MM/dd") & "') AS GroupSummary_1 INNER JOIN " &
                        "  dbo.Category On GroupSummary_1.Parent = dbo.Category.CatID " &
                        " GROUP BY GroupSummary_1.Parent, dbo.Category.CategoryName, dbo.Category.Code"

        End If


        sqlcomm.Connection = sqlConn
        sqlcomm.CommandType = Data.CommandType.Text

        Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
        If reader.HasRows Then
            While reader.Read()
                str += "<tr style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;text-align:center;font-size:12px;font-family:Arial, Helvetica, sans-serif;"">"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("Code").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("CategoryName").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("OpeningCost").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("AdditionalValue").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("ClosingCost").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("DepreciationRate").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("OpeningDepreciation").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("AdjustedDepreciation").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("ClosingDepreciation").ToString() + "</td>"
                str += "<td style=""border-bottom: 1px solid #d7d7d7;padding: 5px 10px 5px 10px;height: 21px;line-height: 21px;border-right: 1px solid #d7d7d7;"">"
                str += reader("WrittenValue").ToString() + "</td>"
                str += "</tr>"
            End While
            str += "</tbody></table>"
        Else
            str = ""
        End If
        sqlConn.Close()
        Return str
    End Function
    Private Sub PrintReport()
        '  Dim Title As String = ""
        'Dim StartDate As String = Date.Parse(txtStart.Text).ToString("dd / MM / yyyy")
        'Dim EndDate As String = Date.Parse(txtEnd.Text).ToString("dd / MM / yyyy")
        'Dim TotalEntry As String = ""

        Dim Response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
        Dim client As New pdfcrowd.Client("nexa", "605cd51a6f77ab7e9b117770cd717d3d")
        Dim Stream As New MemoryStream
        Dim ReportTitle As String = ""
        ReportTitle = "<h2>Fixed Asset Report </h2><h5>Start Date : 21/02/2017 <br/>End Date : 28/02/2017<br/></h5>"
        Dim str As String = ReportTitle + "<h3>Group Summary</h3>"
        If (RouteData.Values("gid") Is Nothing) Then
            str += LoadReport(1, "2017/02/21", "" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "")
        Else
            str += LoadReport(RouteData.Values("gid").ToString(), "2017/02/21", "" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "")
        End If

        client.convertHtml(str, Stream)
        ' set HTTP response headers
        Response.Clear()
        Response.AddHeader("Content-Type", "application/pdf")
        Response.AddHeader("Cache-Control", "no-cache")
        Response.AddHeader("Accept-Ranges", "none")
        Response.AddHeader("Content-Disposition", "attachment; filename=Group Summary on " & Date.Today.Date.ToShortDateString() & ".pdf")

        ' send the generated PDF
        Stream.WriteTo(Response.OutputStream)
        Stream.Close()
        Response.Flush()
        Response.End()

        ' To View the Report
        'Dim pdf() As Byte = Stream.ToArray()
        'Response.Buffer = True
        'Response.Clear()
        'Response.BinaryWrite(pdf)
        'Response.ContentType = "application/PDF"
        'Response.AddHeader("Content-Disposition:", "attachment; filename=Hotel Reports.pdf")
        'Response.Flush()
        'Response.End()
    End Sub
    Protected Sub btnDownload_Click(sender As Object, e As System.EventArgs) Handles btnDownload.ServerClick
        PrintReport()
    End Sub

    Private Sub BindListView(groupid As Integer, startdate As Date, enddate As Date)
        Dim constr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
        Dim dt As New DataTable()
        Using con As New SqlConnection(constr)
            ' For Each asset In GropupLevel1
            Using cmd As New SqlCommand()
                If (groupid <> 1) Then
                    cmd.CommandText = "Select * FROM dbo.[GroupSummary](1,'2017/02/01','" & startdate.ToString("yyyy/MM/dd") & "','" & enddate.ToString("yyyy/MM/dd") & "') where Parent=" & groupid.ToString()
                Else
                    cmd.CommandText = "SELECT   GroupSummary_1.Parent AS Category, SUM(GroupSummary_1.WrittenValue) AS WrittenValue, SUM(GroupSummary_1.ClosingDepreciation) AS ClosingDepreciation, " &
                        " SUM(GroupSummary_1.ClosingCost) AS ClosingCost, SUM(GroupSummary_1.Openingdepreciation) AS Openingdepreciation, SUM(GroupSummary_1.OpeningCost) AS OpeningCost, " &
                        " SUM(GroupSummary_1.AdditionalValue) AS AdditionalValue, AVG(GroupSummary_1.DepreciationRate) AS DepreciationRate, SUM(GroupSummary_1.AdjustedDepreciation) AS AdjustedDepreciation,  " &
                        " dbo.Category.CategoryName, dbo.Category.Code " &
                        " FROM  dbo.GroupSummary(" & 1 & ",'2017/02/01', '" & startdate.ToString("yyyy/MM/dd") & "', '" & enddate.ToString("yyyy/MM/dd") & "') AS GroupSummary_1 INNER JOIN " &
                        "  dbo.Category On GroupSummary_1.Parent = dbo.Category.CatID " &
                        " GROUP BY GroupSummary_1.Parent, dbo.Category.CategoryName, dbo.Category.Code"
                End If


                cmd.Connection = con
                Using sda As New SqlDataAdapter(cmd)

                    sda.Fill(dt)

                End Using
            End Using


            ' Next

        End Using
        GridView1.DataSource = dt
        GridView1.DataBind()
    End Sub

    Protected Sub btnAddNew_ServerClick(sender As Object, e As System.EventArgs) Handles btnReport.ServerClick
        Dim startdate As Date = Date.Parse(txtStart.Text)
        Dim enddate As Date = Date.Parse(txtEnd.Text)
        If (RouteData.Values("gid") Is Nothing) Then
            Me.BindListView(1, "" & startdate.Year & "/" & startdate.Month & "/" & startdate.Day & "", "" & enddate.Year & "/" & enddate.Month & "/" & enddate.Day & "")
        Else
            Me.BindListView(Integer.Parse(RouteData.Values("gid").ToString()), "" & startdate.Year & "/" & startdate.Month & "/" & startdate.Day & "", "" & enddate.Year & "/" & enddate.Month & "/" & enddate.Day & "")
        End If


    End Sub
End Class
