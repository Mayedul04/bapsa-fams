﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports Microsoft.Reporting.WebForms

Partial Class A_Reports_MasterReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


        'Me.ReportViewer1.LocalReport.Refresh()
        'SqlDataSource1.SelectCommand = "SELECT    '1st (Group by)' as SupplierName,   '3rd (Group by)' as CategoryName, ReportforAsset_1.ItemID, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
        '               "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost , ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
        '                " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
        '                " ReportforAsset_1.Location, ReportforAsset_1.Category, '2nd (Group by)' AS Parent" &
        '                " FROM            dbo.Category INNER JOIN " &
        '                "  dbo.ReportforAsset('2017/01/01', '" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN " &
        '                " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID INNER JOIN dbo.Supplier ON ReportforAsset_1.Supplier = dbo.Supplier.SupID INNER JOIN dbo.Assets ON ReportforAsset_1.ItemID = dbo.Assets.ItemID"



    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        Dim sqlstring As String = ""


        sqlstring = "SELECT        Location_1.LocationName AS SourceLocation, dbo.Location.LocationName AS DestLocation, dbo.Assets.ItemName, dbo.Assets.ItemCode, " &
                        " dbo.TransferTracking.TransferType,  dbo.TransferTracking.TransDate" &
                        " FROM            dbo.TransferTracking INNER JOIN " &
                        " dbo.Location AS Location_1 ON dbo.TransferTracking.SourceID = Location_1.LocID INNER JOIN " &
                        " dbo.Location ON dbo.TransferTracking.DestinationID = dbo.Location.LocID INNER JOIN " &
                       "  dbo.Assets ON dbo.TransferTracking.TransferedAsset = dbo.Assets.ItemID Where dbo.TransferTracking.TransferType='Location' "

        ' End If
        Dim startdate As Date
        Dim enddate As Date

        If (txtStartDate.Text <> "") Then
            startdate = Date.Parse(txtStartDate.Text)
        Else
            startdate = Date.Parse(Context.Items("FinancialYearStart"))
        End If
        If (txtEndDate.Text <> "") Then
            enddate = Date.Parse(txtEndDate.Text)
        Else
            enddate = Date.Today
        End If

        sqlstring += " and dbo.TransferTracking.TransDate between '" & startdate.Year & "/" & startdate.Month & "/" & startdate.Day & "' and '" & enddate.Year & "/" & enddate.Month & "/" & enddate.Day & "'"






        SqlDataSource1.SelectCommand = sqlstring
        SqlDataSource1.DataBind()
        'divSuccess.Visible = True
        'lblSuccMessage.Text = sqlstring
        Me.ReportViewer1.DataBind()
        Me.ReportViewer1.LocalReport.Refresh()





    End Sub
End Class
