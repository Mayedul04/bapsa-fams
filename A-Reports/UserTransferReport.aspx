﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="UserTransferReport.aspx.vb" Inherits="A_Reports_MasterReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="sapn5">
        <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="Two Group can not be same"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="well">
            <div class="tab-content">

               <%-- <div class="span3">
                    <div class="left_div">
                        <label>Group by</label>
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="input-large">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="Category_1.CategoryName">Main Category</asp:ListItem>
                            <asp:ListItem Value="dbo.Location.LocationName">Branch</asp:ListItem>
                            <asp:ListItem Value="dbo.Projects.ProjectName">Projects</asp:ListItem>
                            <asp:ListItem Value="dbo.Department.Department">Department</asp:ListItem>
                            <asp:ListItem Value="dbo.Supplier.Supplier">Supplier</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>--%>
                

                <div class="span3">

                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="input-large"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtStartDate" runat="server">
                    </cc1:CalendarExtender>
                    <cc1:TextBoxWatermarkExtender ID="watervatrate" runat="server" TargetControlID="txtStartDate" WatermarkText="Report Start From"></cc1:TextBoxWatermarkExtender>

                </div>
                <div class="span3">

                    <asp:TextBox ID="txtEndDate" runat="server" CssClass="input-large"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="txtEndDate" runat="server">
                    </cc1:CalendarExtender>

                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEndDate" WatermarkText="Report End On"></cc1:TextBoxWatermarkExtender>
                </div>
                <div class="span2">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-search"></i>&nbsp;Get Report</button>
                </div>
                
            </div>
        </div>
        <asp:HiddenField ID="hdnID" Value="" runat="server" />
    </div>
    <div class="span12">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="986px" Height="622px">
            <LocalReport ReportPath="A-Reports\Report2.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </div>


    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        SelectCommand="SELECT        Employees_1.EmployeeName AS SourceEmployee, Department_1.Department AS SourceDept, dbo.Employees.EmployeeName AS DestEmployee, 
                         dbo.Department.Department AS DestDept, dbo.Assets.ItemName, dbo.Assets.ItemCode, dbo.TransferTracking.TransDate, dbo.TransferTracking.TransferType
FROM            dbo.Department INNER JOIN
                         dbo.TransferTracking INNER JOIN
                         dbo.Employees AS Employees_1 ON dbo.TransferTracking.SourceID = Employees_1.UID INNER JOIN
                         dbo.Department AS Department_1 ON Employees_1.DepartmentID = Department_1.DeptID INNER JOIN
                         dbo.Employees ON dbo.TransferTracking.DestinationID = dbo.Employees.UID ON dbo.Department.DeptID = dbo.Employees.DepartmentID INNER JOIN
                         dbo.Assets ON dbo.TransferTracking.TransferedAsset = dbo.Assets.ItemID
Where  dbo.TransferTracking.TransferType='User'"></asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="Server">
</asp:Content>

