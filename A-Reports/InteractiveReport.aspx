﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="InteractiveReport.aspx.vb" Inherits="A_Reports_MasterReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="Two Group can not be same"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="sapn12" style="margin-left: 0">
        <div class="well">
            <div class="tab-content">
                <div class="span8">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div class="span6">
                                <div class="left_div">
                                    <label>First Group by</label>
                                </div>
                                <div class="right_div">
                                    <asp:DropDownList ID="ddlPrimary" runat="server" CssClass="input-large" AutoPostBack="true">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                        <asp:ListItem Value="Category_1.CategoryName">Main Category</asp:ListItem>
                                        <asp:ListItem Value="dbo.Location.LocationName">Branch</asp:ListItem>
                                        <asp:ListItem Value="dbo.Projects.ProjectName">Projects</asp:ListItem>
                                        <asp:ListItem Value="dbo.Department.Department">Department</asp:ListItem>
                                        <asp:ListItem Value="dbo.Supplier.Supplier">Supplier</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="span6" style="margin-left:2%">
                                <div class="left_div">
                                    <label>Filter by</label>
                                </div>
                                <div class="right_div">

                                    <asp:DropDownList ID="ddlPrimaryFilter" DataSourceID="sdsPrimaryFilter"
                                        DataTextField="Name" DataValueField="ID" runat="server" CssClass="input-large">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                    </asp:DropDownList>

                                    <asp:SqlDataSource ID="sdsPrimaryFilter" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"></asp:SqlDataSource>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="span6">
                                <div class="left_div">
                                    <label>2nd Group by</label>
                                </div>
                                <div class="right_div">
                                    <asp:DropDownList ID="ddlSecondary" runat="server" CssClass="input-large" AutoPostBack="true">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                        <asp:ListItem Value="Category_1.CategoryName">Main Category</asp:ListItem>
                                        <asp:ListItem Value="dbo.Category.CategoryName">Sub Category</asp:ListItem>
                                        <asp:ListItem Value="dbo.Projects.ProjectName">Projects</asp:ListItem>
                                        <asp:ListItem Value="dbo.Location.LocationName">Branch</asp:ListItem>
                                        <asp:ListItem Value="dbo.Department.Department">Department</asp:ListItem>
                                        <asp:ListItem Value="dbo.Supplier.Supplier">Supplier</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="span6" style="margin-left:2%">
                                <div class="left_div">
                                    <label>Filter by</label>
                                </div>
                                <div class="right_div">
                                    <asp:DropDownList ID="ddlSecondaryFilter" DataSourceID="sdsSecondFilter"
                                        DataTextField="Name" DataValueField="ID" runat="server" CssClass="input-large">

                                        <asp:ListItem Value="">Select</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sdsSecondFilter" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"></asp:SqlDataSource>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="span6">
                                <div class="left_div">
                                    <label>3rd Group by</label>
                                </div>
                                <div class="right_div">
                                    <asp:DropDownList ID="ddlTertiary" runat="server" CssClass="input-large" AutoPostBack="true">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                        <asp:ListItem Value="Category_1.CategoryName">Main Category</asp:ListItem>
                                        <asp:ListItem Value="dbo.Projects.ProjectName">Projects</asp:ListItem>
                                        <asp:ListItem Value="dbo.Category.CategoryName">Sub Category</asp:ListItem>
                                        <asp:ListItem Value="dbo.Location.LocationName">Branch</asp:ListItem>
                                        <asp:ListItem Value="dbo.Department.Department">Department</asp:ListItem>
                                        <asp:ListItem Value="dbo.Supplier.Supplier">Supplier</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="span6" style="margin-left:2%">
                                <div class="left_div">
                                    <label>Filter by</label>
                                </div>
                                <div class="right_div">
                                    <asp:DropDownList ID="ddlTertiaryFilter" DataSourceID="sdsTertairyFilter"
                                        DataTextField="Name" DataValueField="ID" runat="server" CssClass="input-large">

                                        <asp:ListItem Value="">Select</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="sdsTertairyFilter" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"></asp:SqlDataSource>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:HiddenField ID="hdnPrimaryfilterID" runat="server" />
                    <asp:HiddenField ID="hdnSecondfilterID" runat="server" />
                    <asp:HiddenField ID="hdnThirdfilterID" runat="server" />
                    <div class="span6">
                        <div class="left_div">
                            <label>Start Date</label>
                        </div>
                        <div class="right_div">
                            <asp:TextBox ID="txtStartDate" runat="server" CssClass="input-large"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtStartDate" runat="server">
                            </cc1:CalendarExtender>
                            <cc1:TextBoxWatermarkExtender ID="watervatrate" runat="server" TargetControlID="txtStartDate" WatermarkText="Report Start From"></cc1:TextBoxWatermarkExtender>
                        </div>
                    </div>
                    <div class="span6" style="margin-left:2%">
                        <div class="left_div">
                            <label>End Date</label>
                        </div>
                        <div class="right_div">
                            <asp:TextBox ID="txtEndDate" runat="server" CssClass="input-large"></asp:TextBox>
                            <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="txtEndDate" runat="server">
                            </cc1:CalendarExtender>

                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtEndDate" WatermarkText="Report End On"></cc1:TextBoxWatermarkExtender>
                        </div>
                    </div>

                    <%--<div class="span2">
                    <button runat="server" id="btnDoCalculation" validationgroup="form" visible="false" class="btn btn-primary"><i class="icon-search"></i>&nbsp;Calculate</button>
                </div>
                <div class="span2">
                    <button runat="server" id="btnAllAssets" validationgroup="form" visible="false" class="btn btn-primary"><i class="icon-search"></i>&nbsp;All Assets</button>
                </div>
                <div class="span2">
                    <button runat="server" id="btnReval" validationgroup="form" visible="false" class="btn btn-primary"><i class="icon-search"></i>&nbsp;Revaluations</button>
                </div>--%>
                      <div class="span4">
                            <asp:CheckBox ID="chkExpand" CssClass="input-xlarge checkboxspan" runat="server" Text=" Details Report" />
                        </div>
                        <div class="span3">
                            <button runat="server" id="btnSubmit" class="btn btn-primary" validationgroup="form"><i class="icon-search"></i>&nbsp;Get Report</button>
                        </div>
                    <asp:HiddenField ID="hdnID" Value="" runat="server" />
                </div>
                <div class="span3">
                    
                        <div class="span12">
                            <label style="font-weight:bold;">Select The field to be on Report</label>
                            <asp:CheckBoxList ID="chkParameters" CssClass="input-xlarge checkboxspan" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
                                <asp:ListItem Value="Item">Item Name</asp:ListItem>
                                <%-- <asp:ListItem Value="Code">Code</asp:ListItem>--%>
                                <asp:ListItem Value="OV">Opening</asp:ListItem>
                                <asp:ListItem Value="ADV">Added</asp:ListItem>
                                <asp:ListItem Value="RS">Reval Surplus</asp:ListItem>
                                <asp:ListItem Value="DV">Disposed</asp:ListItem>
                                <asp:ListItem Value="RV">Resell</asp:ListItem>
                                <asp:ListItem Value="CV">Closing</asp:ListItem>
                                <asp:ListItem Value="Rate">Rate</asp:ListItem>
                                <asp:ListItem Value="OD">Opening Depre</asp:ListItem>
                                <asp:ListItem Value="ADD">Adjusted Depre</asp:ListItem>
                                <asp:ListItem Value="CD">Closing Depre</asp:ListItem>
                                <asp:ListItem Value="PD">Purchase Date</asp:ListItem>
                                <asp:ListItem Value="WDV">WDV</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                      

                    </div>
               
            </div>
        </div>
    </div>

    <div class="span12">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Visible="false" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="986px" Height="622px">
            <LocalReport ReportPath="A-Reports\ReportInteractive.rdlc">
                <DataSources>
                    <%--<rsweb:ReportDataSource  />--%>
                    
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
    </div>

    <asp:SqlDataSource ID="sdsYearJournal" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        InsertCommand="INSERT INTO [YearlyJournal] ([ItemID], [ItemName], [CatID], [ClosingDepreciation], [ClosingCost], [CalculatedDate],[RemainingYear]) VALUES (@ItemID, @ItemName,@CatID, @ClosingDepreciation, @ClosingCost, @CalculatedDate,@RemainingYear)">
        <InsertParameters>
            <asp:Parameter Name="ItemID" DefaultValue="" />
            <asp:Parameter Name="ItemName" />
            <asp:Parameter Name="CatID" />
            <asp:Parameter Name="ClosingDepreciation" DefaultValue="0"></asp:Parameter>
            <asp:Parameter DefaultValue="0" Name="ClosingCost" />
            <asp:Parameter Name="CalculatedDate" DefaultValue="" />
            <asp:Parameter DefaultValue="0" Name="RemainingYear" />
        </InsertParameters>

    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        SelectCommand="SELECT CompanyName, CompanyLogo, Address1, Address2, Phone, Email, FiancialYearStart, CompanyBackGroundColor, GETDATE() AS ReportDate, CompanyID FROM dbo.Company"></asp:SqlDataSource>
    <%--SelectCommand="SELECT 'Primary Group' as SupplierName, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, 'Secondary' AS Parent, 'Tertiary' as CategoryName  FROM Category AS Category_1 INNER JOIN Category ON Category_1.CatID = Category.ParentID INNER JOIN dbo.ReportforAsset('2017/01/01',  '2017/03/03') AS ReportforAsset_1 ON Category.CatID = ReportforAsset_1.Category INNER JOIN Supplier ON ReportforAsset_1.Supplier = Supplier.SupID"--%>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="Script" runat="Server">
    <%--<script type="text/javascript">
       
        $('#ctl00_ContentPlaceHolder1_ddlPrimary').change(function () {
            var end = this.value;
            var index = 0;
            if (end == "Category_1.CategoryName") {
                index = 1;
            }
            else if (end == "dbo.Location.LocationName")
            {
                index = 2;
            }
            else if (end == "dbo.Projects.ProjectName") {
                index = 3;
            }
            else if (end == "dbo.Department.Department") {
                index = 4;
            }
            else if (end == "dbo.Supplier.Supplier") {
                index = 5;
            }
            else {
                index = 0;
            }
            if (index > 0)
            {
                $.ajax({
                    type: "POST",
                    data: "FilterID=" + index,
                    url: "/Handler/HandlerDropDownList.ashx",
                    dataType: "json",
                    success: function (data) {
                        var ddlCustomers = $("[id*=ddlPrimaryFilter]");

                        ddlCustomers.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(data, function () {
                            ddlCustomers.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus); alert(errorThrown);
                    }
                });
            }
                

           // }
           
        });
        $('#ctl00_ContentPlaceHolder1_ddlSecondary').change(function () {
            var end = this.value;
            var index = 0;
            if (end == "Category_1.CategoryName") {
                index = 1;
            }
            else if (end == "dbo.Category.CategoryName") {
                index = 2;
            }
            else if (end == "dbo.Location.LocationName") {
                index = 4;
            }
            else if (end == "dbo.Projects.ProjectName") {
                index = 3;
            }
            else if (end == "dbo.Department.Department") {
                index = 5;
            }
            else if (end == "dbo.Supplier.Supplier") {
                index = 6;
            }
            else {
                index = 0;
            }
            if (index > 0) {
                $.ajax({
                    type: "POST",
                    data: "FilterID=" + index,
                    url: "/Handler/HandlerDropDownList.ashx",
                    dataType: "json",
                    success: function (data) {
                        var ddlCustomers = $("[id*=ddlSecondaryFilter]");

                        ddlCustomers.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(data, function () {
                            ddlCustomers.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus); alert(errorThrown);
                    }
                });
            }


            // }

        });
        $('#ctl00_ContentPlaceHolder1_ddlTertiary').change(function () {
            var end = this.value;
            var index = 0;
            if (end == "Category_1.CategoryName") {
                index = 1;
            }
            else if (end == "dbo.Location.LocationName") {
                index = 2;
            }
            else if (end == "dbo.Projects.ProjectName") {
                index = 3;
            }
            else if (end == "dbo.Department.Department") {
                index = 4;
            }
            else if (end == "dbo.Supplier.Supplier") {
                index = 5;
            }
            else {
                index = 0;
            }
            if (index > 0) {
                $.ajax({
                    type: "POST",
                    data: "FilterID=" + index,
                    url: "/Handler/HandlerDropDownList.ashx",
                    dataType: "json",
                    success: function (data) {
                        var ddlCustomers = $("[id*=ddlTertiaryFilter]");

                        ddlCustomers.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(data, function () {
                            ddlCustomers.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus); alert(errorThrown);
                    }
                });
            }


            // }

        });
        $('#ctl00_ContentPlaceHolder1_ddlPrimaryFilter').change(function () {
            var end = this.value;
            $('#<%=hdnPrimaryfilterID.ClientID %>').val(end);
            //alert($('#<%=hdnPrimaryfilterID.ClientID %>').val());
        });
</script>--%>
</asp:Content>

