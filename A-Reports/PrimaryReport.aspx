﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main_Full.master" AutoEventWireup="false" CodeFile="PrimaryReport.aspx.vb" Inherits="Admin_A_Contact_AllContact" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="page-title">Assets report</h1>

   <div class="btn-toolbar">
    
        <div class="btn-group">
            <div class="span12">
                <div class="span2">
                    <label>
                        Start Date  :</label>
                    <label>
                        End Date :</label>
                    <label>
                        Category :</label>
                </div>
                <div class="span4">
                    <asp:TextBox ID="txtStart" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtStart" runat="server">
                    </cc1:CalendarExtender>
                    
                </div>
                <div class="span2">
                    <label>
                        Department  :</label>
                </div>
                <div class="span4">
                     <asp:DropDownList ID="ddlDept" runat="server" DataSourceID="sdsParent" CssClass="input-xlarge" AppendDataBoundItems="true"
                        DataTextField="Department" DataValueField="DeptID">
                         <asp:ListItem Value="">Filter by Department</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsParent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [DeptID], [Department] FROM [Department] WHERE ([Status] = @Status)">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                
                <div class="span4">
                    <asp:TextBox ID="txtEnd" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="txtEnd" runat="server">
                    </cc1:CalendarExtender>
                   
                </div>

                <div class="span2">
                    <label>
                        Location  :</label>
                </div>
                <div class="span4">
                     <asp:DropDownList ID="ddlLocation" runat="server" DataSourceID="sdsLoc" CssClass="input-xlarge" AppendDataBoundItems="true"
                        DataTextField="LocationName" DataValueField="LocID">
                         <asp:ListItem Value="">Filter by Location</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsLoc" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [LocID], [LocationName] FROM [Location] WHERE ([Status] = @Status)">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span4">
                     <asp:DropDownList ID="ddlCat" runat="server" DataSourceID="sdsCat" CssClass="input-xlarge" AppendDataBoundItems="true"
                        DataTextField="CategoryName" DataValueField="CatID">
                         <asp:ListItem Value="">Filter by Category</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsCat" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [CatID], [CategoryName] FROM [Category] WHERE ([Status] = @Status) and CatID>1">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span2">
                    <label>
                        Supplier  :</label>
                </div>
                <div class="span4">
                     <asp:DropDownList ID="ddlSupplier" runat="server" DataSourceID="sdsSup" CssClass="input-xlarge" AppendDataBoundItems="true"
                        DataTextField="Supplier" DataValueField="SupID">
                         <asp:ListItem Value="">Filter by Supplier</asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="sdsSup" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        SelectCommand="SELECT DISTINCT [SupID], [Supplier] FROM [Supplier] WHERE ([Status] = @Status)">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="span4">
                    </div>
                <div class="span4">
                    <div class="btn-toolbar">
                        <button runat="server" id="btnReport" class="btn btn-primary" validationgroup="form">
                            <i class="icon-save"></i>&nbsp;Show Report</button>
                        <button runat="server" id="btnDownload" class="btn btn-primary"><i class="icon-download-alt"></i>&nbsp;Download </button>
                    </div>
                </div>
            </div>
            
        </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Assets"></asp:Label></h2>
        <div>

           <div class="well">
           
          <table class="table" >
                                  
                                        <thead  >
                                           <tr>
                                               <th >
                                                Code</th>
                                             
                                            <th >
                                                Asset</th>
                                             
                                            <th >
                                                Opening Cost</th>
                                            <th >
                                                Added Value</th>
                                               <th >
                                                Closing Cost</th>
                                            <th >
                                                Depreciation </th>
                                             <th >
                                                Opening Depre </th>
                                               <th >
                                                Adjusted Depr </th>
                                               <th >
                                                Closing Depreciation </th>
                                               <th >
                                                Writen Value </th>
                                            
                                        </tr>
                                            
                                        </thead>
                                        <tbody>
                     <asp:ListView ID="GridView1" runat="server">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">
                            <td>
                                <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("ItemCode") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("ItemName") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Openingcost") %>' />

                            </td>
                            <td>
                                <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("AdditionalValue") %>' />
                            </td>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("ClosingCost") %>' />
                            <td>
                                <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("DepreciationRate") & "%" %>' />
                             </td>
                            
                            <td>
                                <asp:Label ID="lblMessage" runat="server" Text='<%# Eval("OpeningDepreciation") %>'  />
                            </td>
                          <td>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("AdjustedDepreciation") %>'  />
                            </td>
                             <td>
                                <asp:Label ID="Label23" runat="server" Text='<%# Eval("ClosingDepreciation") %>'  />
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("WrittenValue") %>'  />
                            </td>
                           
                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                       
                                        <tr ID="itemPlaceholder"  runat="server">
                                       
                                        </tr>

                                       
                               
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody> 
                </table> 
                                
          </div> 
    <!-- Eof content -->
         
            
       </div>
    <!-- Eof content -->
</asp:Content>

