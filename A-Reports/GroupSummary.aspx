﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main_Full.master" AutoEventWireup="false" CodeFile="GroupSummary.aspx.vb" Inherits="Admin_A_Contact_AllContact" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">Assets report</h1>

    <div class="btn-toolbar">
        <div class="btn-group">
            <div class="left_div">
                    <label>
                        Start Date  :</label>
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtStart" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender3" TargetControlID="txtStart" runat="server">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtStart" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
                <div class="left_div">
                    <label>
                        End Date :</label>
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtEnd" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <cc1:CalendarExtender ID="CalendarExtender4" TargetControlID="txtEnd" runat="server">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtEnd" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
                <div class="right_div">
                    <div class="btn-toolbar">
                        <button runat="server" id="btnReport" class="btn btn-primary" ValidationGroup="form">
                            <i class="icon-save"></i>&nbsp;Show Report</button>
                        <button runat="server" id ="btnDownload"  class="btn btn-primary"><i class="icon-download-alt"></i>&nbsp;Download</button>
                    </div>
                    
                </div>
      </div>
        
    </div>

    <!-- content -->

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Group Summary"></asp:Label></h2>
    <div>

        <div class="well">

            <table class="table">

                <thead>
                    <tr>
                        <th>Code</th>

                        <th>Category</th>

                        <th>Opening Cost</th>
                        <th>Added Value</th>
                        <th>Closing Cost</th>
                        <th>Depreciation </th>
                        <th>Opening Depre </th>
                        <th>Adjusted Depr </th>
                        <th>Closing Depreciation </th>
                        <th>Writen Value </th>

                    </tr>

                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>No data was returned.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Code") %>' />
                                </td>
                                <td>
                                    <a href="<%= domainName %>CategorySummary/<%# Eval("Category") %>">
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("CategoryName") %>' /></a>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Openingcost") %>' />

                                </td>
                                <td>
                                    <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("AdditionalValue") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("ClosingCost") %>' />
                                <td>
                                    <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("DepreciationRate") & "%" %>' />
                                </td>

                                <td>
                                    <asp:Label ID="lblMessage" runat="server" Text='<%# Eval("OpeningDepreciation") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("AdjustedDepreciation") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label23" runat="server" Text='<%# Eval("ClosingDepreciation") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("WrittenValue") %>' />
                                </td>

                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>

                            <tr id="itemPlaceholder" runat="server">
                            </tr>



                        </LayoutTemplate>

                    </asp:ListView>

                </tbody>
            </table>

        </div>
        <!-- Eof content -->


    </div>
    <!-- Eof content -->
</asp:Content>

