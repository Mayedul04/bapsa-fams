﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing
Imports Microsoft.Reporting
Imports Microsoft.Reporting.WebForms

Partial Class A_Reports_MasterReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            ' ReportViewer1.LocalReport.EnableExternalImages = True
            SqlDataSource1.SelectCommand = "SELECT        dbo.Category.CategoryName, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
                           "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
                            " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation,ReportforAsset_1.RevaluationSurplus,ReportforAsset_1.DisposedValue, ReportforAsset_1.ResellValue, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
                            " ReportforAsset_1.Location, ReportforAsset_1.Category, Category_1.CategoryName AS Parent" &
                            " FROM            dbo.Category INNER JOIN " &
                            "  dbo.ReportforAsset('2017/01/01', '" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN " &
                            " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID"

        End If



    End Sub

    'Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
    '    Dim split = txtAutoSearch.Text.Split(":")
    '    Dim name1 As String = split(0)
    '    Dim id As Integer = split(1).Trim
    '    hdnID.Value = id
    '    LoadReport(hdnID.Value)
    'End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If (txtAutoSearch.Text <> "") Then
            Dim split = txtAutoSearch.Text.Split(":")
            Dim name1 As String = split(0)
            Dim id As Integer = split(1).Trim
            hdnID.Value = id
        End If
        Dim startdate As Date
        Dim enddate As Date
        Dim sqlstring As String = ""
        sqlstring = "SELECT        dbo.Category.CategoryName, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
                       "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
                        " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation,ReportforAsset_1.RevaluationSurplus, ReportforAsset_1.DisposedValue, ReportforAsset_1.ResellValue, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
                        " ReportforAsset_1.Location, ReportforAsset_1.Category, Category_1.CategoryName AS Parent" &
                        " FROM            dbo.Category INNER JOIN "
        If (txtStartDate.Text <> "") Then
            startdate = Date.Parse(txtStartDate.Text)
        Else
            startdate = Date.Parse("2017/01/01")
        End If
        If (txtEndDate.Text <> "") Then
            enddate = Date.Parse(txtEndDate.Text)
        Else
            enddate = Date.Today
        End If
        sqlstring += "  dbo.ReportforAsset('" & startdate.Year & "/" & startdate.Month & "/" & startdate.Day & "', '" & enddate.Year & "/" & enddate.Month & "/" & enddate.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN "
        'If (txtEndDate.Text <> "") Then
        '    Dim startdate As Date = Date.Parse(txtStartDate.Text)


        'Else
        '    sqlstring += "  dbo.ReportforAsset('2017/01/01', '" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN "
        'End If
        If hdnID.Value <> "" Then
            If (IsMainGroup(hdnID.Value) = True) Then
                sqlstring += " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID where (Category_1.CatID=" & hdnID.Value & ")"
            Else
                sqlstring += " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID where (ReportforAsset_1.Category=" & hdnID.Value & ")"
            End If
        Else
            sqlstring += " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID"
        End If

        SqlDataSource1.SelectCommand = sqlstring
        SqlDataSource1.DataBind()
        Me.ReportViewer1.DataBind()
        Me.ReportViewer1.LocalReport.Refresh()
    End Sub
    'Private Sub LoadReport(id As Integer)
    '    Dim sqlstring As String = ""
    '    sqlstring = "SELECT        dbo.Category.CategoryName, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
    '                   "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
    '                    " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
    '                    " ReportforAsset_1.Location, ReportforAsset_1.Category, Category_1.CategoryName AS Parent" &
    '                    " FROM            dbo.Category INNER JOIN "
    '    If (txtReportDate.Text <> "") Then
    '        Dim reportdate As Date = Date.Parse(txtReportDate.Text)
    '        sqlstring += "  dbo.ReportforAsset('2017/01/01', '" & reportdate.Year & "/" & reportdate.Month & "/" & reportdate.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN "
    '    Else
    '        sqlstring += "  dbo.ReportforAsset('2017/01/01', '" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN "
    '    End If

    '    sqlstring += " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID where (ReportforAsset_1.Category=" & hdnID.Value & ")"
    '    SqlDataSource1.SelectCommand = sqlstring
    '    SqlDataSource1.DataBind()
    '    Me.ReportViewer1.DataBind()
    '    Me.ReportViewer1.LocalReport.Refresh()

    'End Sub

    'Protected Sub txtReportDate_TextChanged(sender As Object, e As EventArgs) Handles txtReportDate.TextChanged
    '    Dim reportdate As Date = Date.Parse(txtReportDate.Text)
    '    SqlDataSource1.SelectCommand = "SELECT        dbo.Category.CategoryName, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
    '                  "  ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
    '                   " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
    '                   " ReportforAsset_1.Location, ReportforAsset_1.Category, Category_1.CategoryName AS Parent" &
    '                   " FROM            dbo.Category INNER JOIN " &
    '                   "  dbo.ReportforAsset('2017/01/01', '" & reportdate.Year & "/" & reportdate.Month & "/" & reportdate.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN " &
    '                   " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID "
    '    If hdnID.Value <> "" Then
    '        SqlDataSource1.SelectCommand += " where (ReportforAsset_1.Category=" & hdnID.Value & ")"
    '    End If
    '    SqlDataSource1.DataBind()
    '    Me.ReportViewer1.DataBind()
    '    Me.ReportViewer1.LocalReport.Refresh()
    'End Sub
    Private Function IsMainGroup(catid As String) As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        Dim retid As Int32 = 0
        Dim retstr As String = ""
        Dim cmd As SqlCommand = New SqlCommand("Select ParentID from Category where CatID=@CatID", conn)
        conn.Open()
        cmd.Parameters.Add("CatID", SqlDbType.Int, 32).Value = catid
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            If (reader("ParentID").ToString() = "1") Then
                conn.Close()
                Return True
            Else
                conn.Close()
                Return False
            End If
        Else
            conn.Close()
            Return False
        End If

    End Function
End Class
