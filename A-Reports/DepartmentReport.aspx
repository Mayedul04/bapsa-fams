﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="DepartmentReport.aspx.vb" Inherits="A_Reports_MasterReport" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="sapn12">
        <script type="text/javascript" language="javascript">
                                    function OnContactSelected(source, eventArgs) {
                                        var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                                        document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();
                                        
                                    } 
                                </script>
        <div class="span4">
            <div class="left_div">
               <label>Department</label>
            </div>
            <div class="right_div">
        <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                                    onblur="if(this.value=='')this.value='';" AutoCompleteType="Search"></asp:TextBox>
        <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                                    MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetDepartmentList"
                                    ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                                    CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                                    OnClientItemSelected="OnContactSelected">
                                </cc1:AutoCompleteExtender>
                </div>
        </div>
        <div class="span3">
            
                <asp:TextBox ID="txtStartDate" runat="server" CssClass="input-large"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtStartDate" runat="server">
                </cc1:CalendarExtender>
                <cc1:TextBoxWatermarkExtender ID="watervatrate"  runat="server" TargetControlID="txtStartDate" WatermarkText="Report Start From"></cc1:TextBoxWatermarkExtender>
            
        </div>
        <div class="span3">
            
                <asp:TextBox ID="txtEndDate" runat="server" CssClass="input-large"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="txtEndDate" runat="server">
                </cc1:CalendarExtender>
               <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1"  runat="server" TargetControlID="txtEndDate" WatermarkText="Report End On"></cc1:TextBoxWatermarkExtender>
            
        </div>
        <div class="span2">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-search"></i>&nbsp;Get Report</button>
        </div>
        
                
            
        <asp:HiddenField ID="hdnID" Value="" runat="server" />
    </div>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="988px" Height="591px">
        <LocalReport ReportPath="A-Reports\ReportDeptFinal.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSetDept" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Category.CategoryName, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, ReportforAsset_1.AdjustedDepreciation, ReportforAsset_1.OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, Category_1.CategoryName AS Parent, ReportforAsset_1.Category, Department.Department FROM Category AS Category_1 INNER JOIN Category ON Category_1.CatID = Category.ParentID INNER JOIN dbo.ReportforAsset('2017/01/01',  '2017/03/03') AS ReportforAsset_1 ON Category.CatID = ReportforAsset_1.Category INNER JOIN Department ON ReportforAsset_1.Department = Department.DeptID"></asp:SqlDataSource>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" Runat="Server">
</asp:Content>

