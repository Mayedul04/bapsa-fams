﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Printing

Partial Class A_Reports_MasterReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        SqlDataSource1.SelectCommand = "SELECT    dbo.Department.Department,    dbo.Category.CategoryName, ReportforAsset_1.ItemID, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
                       "  ReportforAsset_1.AdjustedDepreciation, dbo.Assets.TotalValue as OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
                        " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
                        " ReportforAsset_1.Location, ReportforAsset_1.Category, Category_1.CategoryName AS Parent" &
                        " FROM            dbo.Category INNER JOIN " &
                        "  dbo.ReportforAsset('2017/01/01', '" & Date.Today.Year & "/" & Date.Today.Month & "/" & Date.Today.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN " &
                        " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID INNER JOIN Department ON ReportforAsset_1.Department = Department.DeptID INNER JOIN dbo.Assets ON ReportforAsset_1.ItemID = dbo.Assets.ItemID"



    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick

        If (txtAutoSearch.Text <> "") Then
            Dim split = txtAutoSearch.Text.Split(":")
            Dim name1 As String = split(0)
            Dim id As Integer = split(1).Trim
            hdnID.Value = id
        Else
            hdnID.Value = ""
        End If
        Dim startdate As Date
        Dim enddate As Date
        Dim sqlstring As String = ""
        sqlstring = "SELECT    dbo.Department.Department,    dbo.Category.CategoryName, ReportforAsset_1.ItemID, ReportforAsset_1.ItemName, ReportforAsset_1.AdditionalValue, ReportforAsset_1.DepreciationRate, " &
                       "  ReportforAsset_1.AdjustedDepreciation, dbo.Assets.TotalValue as OpeningCost, ReportforAsset_1.Openingdepreciation, ReportforAsset_1.ClosingCost, " &
                        " ReportforAsset_1.ItemCode, ReportforAsset_1.ClosingDepreciation, ReportforAsset_1.WrittenValue, ReportforAsset_1.Supplier, ReportforAsset_1.Department, " &
                        " ReportforAsset_1.Location, ReportforAsset_1.Category, Category_1.CategoryName AS Parent" &
                        " FROM            dbo.Category INNER JOIN "

        If (txtStartDate.Text <> "") Then
            startdate = Date.Parse(txtStartDate.Text)
        Else
            startdate = Date.Parse(Context.Items("FinancialYearStart"))
        End If
        If (txtEndDate.Text <> "") Then
            enddate = Date.Parse(txtEndDate.Text)
        Else
            enddate = Date.Today
        End If

        sqlstring += "  dbo.ReportforAsset('" & startdate.Year & "/" & startdate.Month & "/" & startdate.Day & "', '" & enddate.Year & "/" & enddate.Month & "/" & enddate.Day & "') AS ReportforAsset_1 ON dbo.Category.CatID = ReportforAsset_1.Category INNER JOIN "

        If hdnID.Value <> "" Then
            sqlstring += " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID INNER JOIN Department ON ReportforAsset_1.Department = Department.DeptID INNER JOIN dbo.Assets ON ReportforAsset_1.ItemID = dbo.Assets.ItemID where (ReportforAsset_1.Department=" & hdnID.Value & ")"
        Else
            sqlstring += " dbo.Category AS Category_1 ON dbo.Category.ParentID = Category_1.CatID INNER JOIN Department ON ReportforAsset_1.Department = Department.DeptID INNER JOIN dbo.Assets ON ReportforAsset_1.ItemID = dbo.Assets.ItemID"
        End If

        SqlDataSource1.SelectCommand = sqlstring
        SqlDataSource1.DataBind()
        Me.ReportViewer1.DataBind()
        Me.ReportViewer1.LocalReport.Refresh()

    End Sub
End Class
