﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="DBBackup.aspx.vb" Inherits="Admin_DBBackup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Database Backup</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Get Backup</button>
        <div class="btn-group">
        </div>
    </div>


    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>


    <div class="well">
        <div class="tab-content">
            <div class="span8">
                <div class="left_div">
                    <label>DataBase Name:</label>
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtDBName" runat="server" Text="FixedAssets" ReadOnly="true" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                </div>
            </div>
            <div class="span8">
                
                <div class="left_div">
                    <label>Save at:</label>
                </div>
                <div class="right_div">
                    <asp:FileUpload ID="FileUpload2" runat="server"  />
                    <asp:TextBox ID="txtFilepath" runat="server"  CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="well">

        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AutoGenerateColumns="true"
                CellPadding="4" EnableModelValidation="True" GridLines="None" CellSpacing="1">
                <HeaderStyle CssClass="heading" />
                <RowStyle CssClass="first" />
                <AlternatingRowStyle CssClass="alt" />
            </asp:GridView>
        </asp:Panel>



    </div>
    <!-- Eof content -->
</asp:Content>
