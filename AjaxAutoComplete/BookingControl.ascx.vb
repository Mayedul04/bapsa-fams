﻿
Partial Class CustomControl_BookingControl
    Inherits System.Web.UI.UserControl

    Private productionLink As String = ""
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        productionLink = "https://wl6-int.sabre.com/SSW2010/U4U4/webqtrip.html?previewModeConfigVersion=38&userName=u4100004&lang=" & Session("lang") & "&"
    End Sub
    Protected Sub btnBookOneWay_Click(sender As Object, e As EventArgs) Handles btnBookOneWay.Click
        Dim redirectURL As String = productionLink & "journeySpan=OW&origin=" + GetShortCode(txtBookOneWaylocation.Text) + "&destination=" + GetShortCode(txtBookOneWaydestination.Text) + "&lang=en_GB&departureDate=" + txtBookOneWaydepartDate.Text + "&numAdults=1&numChildren=0&numInfants=0&promoCode=&alternativeLandingPage=true"
        Response.Redirect(redirectURL)
    End Sub

    Protected Sub btnBookRoundTrip_Click(sender As Object, e As EventArgs) Handles btnBookRoundTrip.Click
        Dim redirectURL As String = productionLink & "journeySpan=RT&origin=" + GetShortCode(txtRTlocation.Text) + "&destination=" + GetShortCode(txtRTdestination.Text) + "&lang=en_GB&departureDate=" + txtRTdepartureTime.Text + "&returnDate=" + txtRTarrivalTime.Text + "&numAdults=1&numChildren=0&numInfants=0&promoCode=&alternativeLandingPage=true"
        Response.Redirect(redirectURL)
    End Sub



    Protected Sub btnMCSubmit_Click(sender As Object, e As EventArgs) Handles btnMCSubmit.Click
        Dim redirectURL As String = productionLink & "alternativeLandingPage=alternativeLandingPage&searchType=NORMAL&alternativeLandingPage=true&isAward=FALSE&journeySpan=MC&cabinClass=Economy&numAdults=1&numChildren=0&numInfants=0&referrerCode=&promoCode=&searchCars=false&searchHotels=false&checkInDate=&checkOutDate=&origin=" + GetShortCode(txtMCFrom1.Text) + "&destination=" + GetShortCode(txtMCTo1.Text) + "&departureDate=" + txtMCDateDeparture1.Text + "&origin2=" + GetShortCode(txtMCFrom2.Text) + "&destination2=" + GetShortCode(txtMCTo2.Text) + "&departureDate2=" + txtMCDateDeparture2.Text + "" '&origin3=JED&destination3=DMM&departureDate3=2015-05-16
        For i As Int16 = 3 To (hdnRows.Value - 1)
            redirectURL &= "&origin" & i & "=" & GetShortCode(Request.Form("txtMCFrom" & i)) & "&destination" & i & "=" & GetShortCode(Request.Form("txtMCTo" & i)) & "&departureDate" & i & "=" & GetShortCode(Request.Form("txtMCDateDeparture" & i))
        Next

        Response.Redirect(redirectURL)
    End Sub

    Protected Sub btnSubmitMMB_Click(sender As Object, e As EventArgs) Handles btnSubmitMMB.Click
        Dim redirectURL As String = productionLink & "reservationCode=" & txtReservationCodeMMB.Text & "&lastName=" & txtLastNameMMB.Text & "&email=" & txtEmailMMB.Text
        Response.Redirect(redirectURL)
    End Sub

    Private Function GetShortCode(ByVal destination As String) As String
        Return destination.Remove(0, destination.IndexOf(" : ") + 3)

    End Function


    
    Protected Sub btnOnFlightWithNumberCheckNow_Click(sender As Object, e As EventArgs) Handles btnOnFlightWithNumberCheckNow.Click

        lblMessage.Text = ""
        Dim FlightID As String = "", TitleEn As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select MasterID, Title as TitleEn from Flights where Flights.Lang='en' and Flights.Status=1 and FlightNo=@FlightNo  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("FlightNo", Data.SqlDbType.NVarChar, 50).Value = txtOnFlightFlightNumber.Text
        'cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            TitleEn = reader("TitleEn").ToString()
            FlightID = reader("MasterID").ToString()
        End If

        reader.Close()
        conn.Close()

        Dim url = Session("domainName") & Session("lang") & "/fleets/services/" & FlightID & "/" & Utility.EncodeTitle(TitleEn, "-")
        If FlightID <> "" Then
            Response.Redirect(url)
        Else
            lblMessage.Text = "This flight is not available."
            lblMessage.ForeColor = Drawing.Color.Red
        End If


    End Sub

    Protected Sub btnOnFlightWithRouteCheckNow_Click(sender As Object, e As EventArgs) Handles btnOnFlightWithRouteCheckNow.Click
        Dim from_ As String = txtOnFlightRouteFrom.Text.Trim() ' If(txtOnFlightRouteFrom.Text.IndexOf(" : ") > 0, txtOnFlightRouteFrom.Text.Substring(txtOnFlightRouteFrom.Text.IndexOf(" : ") + 3), txtOnFlightRouteFrom.Text)
        Dim to_ As String = txtOnFlightRouteTo.Text.Trim() 'If(txtOnFlightRouteTo.Text.IndexOf(" : ") > 0, txtOnFlightRouteTo.Text.Substring(txtOnFlightRouteTo.Text.IndexOf(" : ") + 3), txtOnFlightRouteTo.Text)
        Dim url = Session("domainName") & Session("lang") & "/fleets/services?from=" & from_ & "&to=" & to_

        Response.Redirect(url)

    End Sub
End Class
