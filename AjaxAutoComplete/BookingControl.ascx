﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingControl.ascx.vb" Inherits="CustomControl_BookingControl" %>

<div class="row">
    <div class="col-md-2">
        <ul class="sideNav">
            <li class="active planBookTrigger"><a href="javascript:;"><%= Language.Read("Plan & Book", Session("lang"))%> <i class="arrow"></i></a></li>
            <li><a href="javascript:;"><%= Language.Read("Manage My Booking", Session("lang"))%></a><i class="arrow"></i></li>
            <li><a href="javascript:;"><%= Language.Read("SaudiGulf Club Login", Session("lang"))%></a><i class="arrow"></i></li>
            <li><a href="javascript:;"><%= Language.Read("Check-In Online", Session("lang"))%></a><i class="arrow"></i></li>
            <li><a href="javascript:;"><%= Language.Read("Check Flight Status", Session("lang"))%></a><i class="arrow"></i></li>
            <li><a href="javascript:;"><%= Language.Read("What's on your Flight", Session("lang"))%></a><i class="arrow"></i></li>
        </ul>
    </div>
    <!-- /col-md-2 --> 
    <div class="col-md-8">   
        <div class="tabContentContainer">

            <!-- Book a Flight Container -->
            <div class="content-1 bannerPopupContainer wow fadeInLeft">

                <a class="bookincloseButton" href="javascript:;"><%= Language.Read("close", Session("lang"))%> <span>X</span></a>
                 
                <div class="tabPan" id="planBook">
                    <h2><%= Language.Read("Book My Flight", Session("lang"))%></h2>

                    <ul class="tripOptionsNav">
                        <li class="active">
                            <a href="javascript:;"><%= Language.Read("One Way", Session("lang"))%></a>
                        </li>
                        <li>
                            <a href="javascript:;"><%= Language.Read("Round Trip", Session("lang"))%></a>
                        </li>
                        <li>
                            <a href="javascript:;"><%= Language.Read("Multi City", Session("lang"))%></a>
                        </li>
                    </ul>
                    <!-- /row -->

                    <div class="tripsContentContainerSection">

                        <!-- Content 1 -->
                        <div class="content-1">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("FROM", Session("lang"))%></div>
                                            <%--<input type="text" class="form-control" name="BookOneWaylocation" id="BookOneWaylocation" placeholder="enter your location" >--%>
                                            <asp:TextBox ID="txtBookOneWaylocation" runat="server" ClientIDMode="Static" CssClass="form-control from" placeholder="enter your location" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="* Required" ControlToValidate="txtBookOneWaylocation" ValidationGroup="BookOneWay" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("TO", Session("lang"))%></div>
                                            <%--<input type="text" class="form-control" name="BookOneWaydestination" id="BookOneWaydestination" placeholder="enter a destination" >--%>
                                            <asp:TextBox ID="txtBookOneWaydestination" runat="server" ClientIDMode="Static" CssClass="form-control to" placeholder="enter a destination" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Required" ControlToValidate="txtBookOneWaydestination" ValidationGroup="BookOneWay" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="spriteImg calendarIcon"></i></div>
                                            <%--<input type="text" class="form-control datepicker" name="BookOneWaydepartDate" id="BookOneWaydepartDate" placeholder="Departing on" >--%>
                                            <asp:TextBox ID="txtBookOneWaydepartDate" runat="server" ClientIDMode="Static" CssClass="form-control datepicker" placeholder="Departing on" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Required" ControlToValidate="txtBookOneWaydepartDate" ValidationGroup="BookOneWay" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <%--<button type="submit" class="btn btn-primary " name="BookOneWay" id="BookOneWay">Book my Flight</button>--%>
                                        <asp:Button ID="btnBookOneWay" ValidationGroup="BookOneWay" Text="Book my Flight" class="btn btn-primary " runat="server" ViewStateMode="Disabled" ClientIDMode="Static" OnClientClick="aspnetForm.target ='_blank';" />
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                            </div>

                            <!-- /row -->


                        </div>
                        <!-- Content 1 -->

                        <!-- Content 2 -->
                        <div class="content-2">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("FROM", Session("lang"))%></div>
                                            <%--<input type="text" class="form-control" id="RTlocation" name="RTlocation" placeholder="enter your location">--%>
                                            <asp:TextBox ID="txtRTlocation" runat="server" ClientIDMode="Static" CssClass="form-control from" placeholder="enter your location" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="* Required" ControlToValidate="txtRTlocation" ValidationGroup="RT" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("TO", Session("lang"))%></div>
                                            <%--<input type="text" class="form-control" id="RTdestination" name="RTdestination" placeholder="enter a destination">--%>
                                            <asp:TextBox ID="txtRTdestination" runat="server" ClientIDMode="Static" CssClass="form-control to" placeholder="enter a destination" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="* Required" ControlToValidate="txtRTdestination" ValidationGroup="RT" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="spriteImg calendarIcon"></i></div>
                                            <%--<input type="text" class="form-control  fromDatepicker" id="RTdepartureTime" name="RTdepartureTime"  placeholder="Departing on">--%>
                                            <asp:TextBox ID="txtRTdepartureTime" runat="server" ClientIDMode="Static" CssClass="form-control fromDatepicker" placeholder="Departing on" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="* Required" ControlToValidate="txtRTdepartureTime" ValidationGroup="RT" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="spriteImg calendarIcon"></i></div>
                                            <%--<input type="text" class="form-control toDatepicker" id="RTarrivalTime" name="RTarrivalTime" placeholder="Arriving on">--%>
                                            <asp:TextBox ID="txtRTarrivalTime" runat="server" ClientIDMode="Static" CssClass="form-control toDatepicker" placeholder="Arriving on" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="* Required" ControlToValidate="txtRTarrivalTime" ValidationGroup="RT" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <%--<button type="submit" class="btn btn-primary BookRoundTrip" id="BookRoundTrip" name="BookRoundTrip">Book my Flight</button>--%>
                                        <asp:Button ID="btnBookRoundTrip" runat="server" Text="Book my Flight" class="btn btn-primary " ValidationGroup="RT" ViewStateMode="Disabled" ClientIDMode="Static" OnClientClick="aspnetForm.target ='_blank';" />
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                            </div>
                            <!-- /row -->

                        </div>
                        <!-- Content 2 -->

                        <!-- Content 3 -->
                        <div class="content-3">


                            <!-- /row -->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("FROM", Session("lang"))%> 1 </div>
                                            <%--<input type="text" class="form-control from" id="location" placeholder="enter your location">--%>
                                            <asp:TextBox ID="txtMCFrom1" runat="server" ClientIDMode="Static" CssClass="form-control from" placeholder="Enter your location" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="* Required" ControlToValidate="txtMCFrom1" ValidationGroup="MC" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("TO", Session("lang"))%> 1</div>
                                            <%--<input type="text" class="form-control to" id="destination" placeholder="enter a destination">--%>
                                            <asp:TextBox ID="txtMCTo1" runat="server" ClientIDMode="Static" CssClass="form-control to" placeholder="Enter a destination" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="* Required" ControlToValidate="txtMCTo1" ValidationGroup="MC" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="spriteImg calendarIcon"></i></div>
                                            <%--<input type="text" class="form-control" id="depart" placeholder="Departing on">--%>
                                            <asp:TextBox ID="txtMCDateDeparture1" runat="server" ClientIDMode="Static" CssClass="form-control datepicker" placeholder="Departing on" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="* Required" ControlToValidate="txtMCDateDeparture1" ValidationGroup="MC" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                            </div>


                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("FROM", Session("lang"))%> 2 </div>
                                            <%--<input type="text" class="form-control from" id="location" placeholder="enter your location">--%>
                                            <asp:TextBox ID="txtMCFrom2" runat="server" ClientIDMode="Static" CssClass="form-control from" placeholder="Enter your location" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="* Required" ControlToValidate="txtMCFrom2" ValidationGroup="MC" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("TO", Session("lang"))%> 2</div>
                                            <%--<input type="text" class="form-control to" id="destination" placeholder="enter a destination">--%>
                                            <asp:TextBox ID="txtMCTo2" runat="server" ClientIDMode="Static" CssClass="form-control to" placeholder="Enter a destination" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="* Required" ControlToValidate="txtMCTo2" ValidationGroup="MC" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="spriteImg calendarIcon"></i></div>
                                            <%--<input type="text" class="form-control" id="depart" placeholder="Departing on">--%>
                                            <asp:TextBox ID="txtMCDateDeparture2" runat="server" ClientIDMode="Static" CssClass="form-control datepicker" placeholder="Departing on" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="* Required" ControlToValidate="txtMCDateDeparture2" ValidationGroup="MC" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                            </div>
                            <div id="newRowMC"></div>

                            <!-- /row -->

                            <div class="row">
                                <div class="col-md-4">
                                    <a class="addCityButton" href="javascript:;" id="addCityButton"><%= Language.Read("Add City", Session("lang"))%></a>
                                </div>
                                <!-- /col-md-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <%--<button type="submit" class="btn btn-primary" >Book my Flight</button>--%>
                                        <asp:HiddenField ID="hdnRows" runat="server" ClientIDMode="Static" Value="3" />
                                        <asp:Button ID="btnMCSubmit" runat="server" Text="Book my Flight" class="btn btn-primary " ValidationGroup="MC" ViewStateMode="Disabled" ClientIDMode="Static" OnClientClick="aspnetForm.target ='_blank';" />
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                            </div>
                        </div>
                        <!-- Content 3 -->

                    </div>

                </div>

            </div>
            <!-- Book a Flight Container -->

            <!-- Book a Flight Container -->
            <div class="content-2 bannerPopupContainer wow fadeInLeft">

                <a class="bookincloseButton" href="javascript:;"><%= Language.Read("close", Session("lang"))%> <span>X</span></a>

                <div class="tabPan" id="planBook">
                    <h2><%= Language.Read("Manage My Booking", Session("lang"))%></h2>

                    <p><%= Language.Read("To access an existing booking, please enter your last name and booking reference below. You can review or change your flight details, add or cancel a hotel or car booking, upgrade with Skywards Miles or cash, make or change requests and add specific services.", Session("lang"))%></p>


                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><%= Language.Read("Code", Session("lang"))%></div>
                                    <asp:TextBox ID="txtReservationCodeMMB" runat="server" ClientIDMode="Static" CssClass="form-control " placeholder="Reservation Code" ViewStateMode="Disabled"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="* Required" ControlToValidate="txtReservationCodeMMB" ValidationGroup="MMB" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>

                            </div>
                        </div>
                        <!-- /col-md-4 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><%= Language.Read("Last Name", Session("lang"))%></div>
                                    <asp:TextBox ID="txtLastNameMMB" runat="server" ClientIDMode="Static" CssClass="form-control " placeholder="Last Name" ViewStateMode="Disabled"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="* Required" ControlToValidate="txtLastNameMMB" ValidationGroup="MMB" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>

                            </div>
                        </div>
                        <!-- /col-md-4 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><%= Language.Read("Email", Session("lang"))%></div>
                                    <asp:TextBox ID="txtEmailMMB" runat="server" ClientIDMode="Static" CssClass="form-control " placeholder="Email" ViewStateMode="Disabled"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="* Required" ControlToValidate="txtEmailMMB" ValidationGroup="MMB" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="* Invalid" ControlToValidate="txtEmailMMB" ValidationGroup="MMB" Display="Dynamic" SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </div>

                            </div>
                        </div>
                        <!-- /col-md-4 -->
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <%--<button type="submit" class="btn btn-primary">Retrieve Booking</button>--%>
                                <asp:Button ID="btnSubmitMMB" runat="server" Text="Retrieve Booking" class="btn btn-primary " ValidationGroup="MMB" ViewStateMode="Disabled" ClientIDMode="Static" OnClientClick="aspnetForm.target ='_blank';" />
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                </div>

            </div>
            <!-- Book a Flight Container -->

            <!-- Book a Flight Container -->
            <div class="content-3 bannerPopupContainer wow fadeInLeft">

                <a class="bookincloseButton" href="javascript:;">close <span>X</span></a>

                <div class="tabPan" id="planBook">
                    <h2>Login</h2>


                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" id="depart" placeholder="Email">
                            </div>
                        </div>
                    </div>
                    <!-- /row -->

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" class="form-control" id="depart" placeholder="Password">
                            </div>
                        </div>
                        <!-- /col-md-4 -->
                    </div>

                    <div class="row">
                        <!-- /col-md-4 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Sign-In</button>
                            </div>
                        </div>
                        <!-- /col-md-4 -->
                    </div>
                    <!-- /row -->

                </div>

            </div>
            <!-- Book a Flight Container -->

            <!-- Book a Flight Container -->
            <div class="content-4 bannerPopupContainer wow fadeInLeft">

                <a class="bookincloseButton" href="javascript:;">close <span>X</span></a>

                <div class="tabPan" id="planBook">
                    <h2>Check In Online</h2>

                    <p>Check in online between 24 hours and 90 minutes before your flight's departure time. If you've already checked in, you can print your boarding pass by entering your details here. </p>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" id="depart" placeholder="Last Name">
                            </div>
                        </div>
                        <!-- /col-md-4 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" id="depart" placeholder="Booking Ref No">
                            </div>
                        </div>
                        <!-- /col-md-4 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Check-In</button>
                            </div>
                        </div>
                        <!-- /col-md-4 -->
                    </div>
                    <!-- /row -->

                </div>

            </div>
            <!-- Book a Flight Container -->

            <!-- Book a Flight Container -->
            <div class="content-5 bannerPopupContainer wow fadeInLeft">

                <a class="bookincloseButton" href="javascript:;">close <span>X</span></a>

                <div class="tabPan" id="planBook">
                    <h2>Check Flight Status</h2>


                    <div class="tripsContentContainerSection">

                        <!-- Content 1 -->
                        <div class="content-1">

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Flight Number</label>
                                        <input type="text" class="form-control" id="depart" placeholder="Flight Number">
                                    </div>
                                </div>
                                <!-- /col-md-4 -->

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Route</label>
                                        <input type="text" class="form-control" id="depart" placeholder="Route">
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                            </div>
                            <!-- /row -->

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Check Status</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <!-- Content 1 -->

                    </div>


                </div>

            </div>
            <!-- Book a Flight Container -->

            <!-- Book a Flight Container -->
            <div class="content-6 bannerPopupContainer wow fadeInLeft">

                <a class="bookincloseButton" href="javascript:;"><%= Language.Read("close", Session("lang"))%> <span>X</span></a>
                <div class="tabPan" id="planBook">
                    <h2><%= Language.Read("What's on Your Flight", Session("lang"))%></h2>
                    <ul class="tripOptionsNav">
                            <li class="active">
                                <a href="javascript:;"><%= Language.Read("Flight Number", Session("lang"))%></a>
                            </li>
                            <li>
                                <a href="javascript:;"><%= Language.Read("Route", Session("lang"))%></a>
                            </li>
                        </ul>
                    <!-- /row -->
                
                    <div class="tripsContentContainerSection">
                        <!-- Content 1 -->
                        <div class="content-1">
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" >
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("Flight Number", Session("lang"))%></div>
                                            <asp:TextBox ID="txtOnFlightFlightNumber" runat="server" ClientIDMode="Static" CssClass="form-control" placeholder="" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17OnFlight" runat="server" ErrorMessage="* Required" ControlToValidate="txtOnFlightFlightNumber" ValidationGroup="OnFlightWithNumber" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>    
                                        </div>                                        
                                    </div>
                                </div>
                                <!-- /col-md-4 -->

                                <%--<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Route</label>
                                        <input type="text" class="form-control" id="depart" placeholder="Route">
                                    </div>
                                </div>--%>
                                <!-- /col-md-4 -->
                            </div>
                            <!-- /row -->

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">                                        
                                        <asp:Button ID="btnOnFlightWithNumberCheckNow" ValidationGroup="OnFlightWithNumber" Text="Check Now" class="btn btn-primary " runat="server" ViewStateMode="Disabled" ClientIDMode="Static"  /><%--OnClientClick="aspnetForm.target ='_blank';"--%>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                        <!-- Content 2 -->
                        <div class="content-2">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" >
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("From", Session("lang"))%></div>
                                            <asp:TextBox ID="txtOnFlightRouteFrom" runat="server" ClientIDMode="Static" CssClass="form-control from" placeholder="" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="* Required" ControlToValidate="txtOnFlightRouteFrom" ValidationGroup="OnFlightWithRouteCheckNow" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->

                                <div class="col-md-6">
                                    <div class="form-group" >
                                        <div class="input-group">
                                            <div class="input-group-addon"><%= Language.Read("To", Session("lang"))%></div>
                                            <asp:TextBox ID="txtOnFlightRouteTo" runat="server" ClientIDMode="Static" CssClass="form-control to" placeholder="" ViewStateMode="Disabled"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="* Required" ControlToValidate="txtOnFlightRouteTo" ValidationGroup="OnFlightWithRouteCheckNow" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <!-- /col-md-4 -->
                            </div>
                            <!-- /row -->

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <asp:Button ID="btnOnFlightWithRouteCheckNow" ValidationGroup="OnFlightWithRouteCheckNow" Text="Check Now" class="btn btn-primary " runat="server" ViewStateMode="Disabled" ClientIDMode="Static"  /><%--OnClientClick="aspnetForm.target ='_blank';"--%>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                

            </div>
            <!-- Book a Flight Container -->

        </div>
        <!-- /tabContent -->
    </div>
    <!-- /col-md-6 -->
</div>