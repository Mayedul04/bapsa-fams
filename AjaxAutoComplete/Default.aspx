﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="<%= Session("domainName") %>Admin/lib/nailthumb/jquery.nailthumb.1.1.min.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_Banner" Runat="Server">

    <div class="bootslider" id="bootslider">
                <!-- Bootslider Loader -->
                <div class="bs-loader">
                    <img src="/img/loader.gif" width="31" height="31" alt="Loading.." id="loader"/>
                </div>
                <!-- /Bootslider Loader -->

                <!-- Bootslider Container -->
                <div class="bs-container">
                    <asp:Repeater ID="rptrBanner" runat="server" DataSourceID="sdsBanner">
                        <ItemTemplate>
                            <!-- Bootslider Slide -->
                            <div class="bs-slide <%# If(Container.ItemIndex=0,"active","") %>" data-animate-in='<%# animateIn(Container.ItemIndex Mod 5)%>' data-animate-out='<%# animateOut(Container.ItemIndex Mod 5)%>'>
                                <%# GetBanner(Container.ItemIndex, Eval("BannerID"), Eval("BigImage").ToString, Eval("SmallImage").ToString, Eval("ImageAltText").ToString)%>
                            </div>
                            <!-- /Bootslider Slide -->
                        </ItemTemplate>
                    </asp:Repeater>
                    


                </div>
                <!-- /Bootslider Container -->

                
                <!-- Bootslider Thumbnails -->
                <div class="bs-thumbnails text-center text-alizarin">
                    <ul class=""></ul>
                </div>
                <!-- /Bootslider Thumbnails -->

                <!-- Bootslider Pagination -->
                <div class="bs-pagination text-center text-alizarin">
                    <ul class="pagination"></ul>
                </div>
                <!-- /Bootslider Pagination -->

                <!-- Bootslider Controls -->
                <div class="bs-controls text-center">
                    <div class="btn-group">
                        <a href="javascript:void(0);" class="btn btn-primary bs-prev">&lt;</a>
                        <a href="javascript:void(0);" class="btn btn-primary bs-next">&gt;</a>
                    </div>
                </div>
                <!-- /Bootslider Controls -->

            </div>

    

   
    <asp:SqlDataSource ID="sdsBanner" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        SelectCommand="SELECT BannerID, [Title], [BigImage],SmallImage, [ImageAltText] FROM [Banner] WHERE (([Lang] = @Lang) AND ([Status] = @Status) AND ([SectionName] = @SectionName)) ORDER BY [SortIndex]">
        <SelectParameters>
            <asp:RouteParameter Name="Lang" RouteKey="lang" Type="String" />
            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
            <asp:ControlParameter ControlID="hdnSectionName" DefaultValue="Home" 
                Name="SectionName" PropertyName="Value" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
<asp:HiddenField ID="hdnSectionName" runat="server" />
    <%--<div class="item active">
        <video autoplay loop mute poster="/ui/media/dist/images/slider/home-banner-1.jpg" id="saudiGulfVideo">            
            <asp:Literal ID="ltrVideo" runat="server" ></asp:Literal>               
            Your browser does not support the video tag.
        </video>
        
    </div> --%>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder_mainBody" Runat="Server">
    <%--<asp:Literal ID="ltrVideoEdit" runat="server" EnableViewState="false"></asp:Literal>--%>
    <div class="innerLavel">
               
        <div class="container">
                  
                  
            <div class="row featuredSection">
            <%= GetFeaturedOffer() %>
            <!-- /col-md-7 -->
                    
            <!-- Flight News & Newsletter Signup -->
            <div class="col-md-3">
                      
                <div class="flightNewsSection">
                        
                    <h2 class="subTitle">Flight News<%= Utility.showAddButton(Request, "Admin/A-FlightNews/NewsEdit.aspx?cat=Flight")%></h2>

                    <div class="flightNewsSlider">
                         <%= GetFlightNews() %>
                    </div>

                </div>

            </div>
            <!-- Flight News & Newsletter Signup -->

            </div>
            <!-- /row featured section -->

            <div class="row marTop marBtm">
                <div class="col-md-5">
                <div class="tagLine">
                    <h1>Destination &amp;<br><span>Experiences</span></h1>
                </div>
                <!-- /tagLine -->
                </div>
                <div class="col-md-7">
                <div class="featureCat tabContent">

                    <div class="tabPane active" id="flightExp">
                        <%= GetSaudiGulfExperience() %>
                        
                    </div>
                    <!-- /catPan flightExp -->
                           
                    <div class="tabPane" id="lounges">
                        <%= GetDining() %>
                    </div>
                    <!-- /catPan Lounges -->


                    <div class="tabPane" id="ourPlanes">
                        <%= GetFleet() %>
                    </div>
                    <!-- /catPan ourPlanes -->
                           

                    <div class="tabPane" id="destinations">
                        <%= GetDestinations() %>
                    </div>
                    <!-- /catPan destinations -->


                    <div class="row">
                        <div class="col-md-12">
                            <ul class="catListTab tabbing">
                            <li class="active"><a href="#flightExp"><%= Language.Read("The SaudiGulf Experience", Session("lang"))%></a></li>
                            <li><a href="#lounges"><%= Language.Read("Dining", Session("lang"))%></a></li>
                            <li><a href="#ourPlanes"><%= Language.Read("Our Fleet", Session("lang"))%></a></li>
                            <li><a href="#destinations"><%= Language.Read("Destinations", Session("lang"))%></a></li>
                            </ul>
                        </div>
                    </div>

                </div>
                <!-- /flightExp -->
                </div>
            </div>

        </div>
               
        <!-- News Section Container -->
        <section class="newsSectionContainer">
                    
            <div class="container">
                      
                <h2><span><%= Language.Read("General Announcements", Session("lang"))%></span> <i class="spriteImg roundDownArrow small"></i></h2>
                      
                <div class="newSliderSection">
                    <asp:Literal ID="ltrNews" runat="server" EnableViewState="false" ></asp:Literal>          
                    

                </div>

            </div>

        </section>
        <!-- News Section Container -->
          

    </div>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder_script" Runat="Server">
    <script src="<%= Session("domainName") %>Admin/lib/nailthumb/jquery.nailthumb.1.1.min.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('.nailthumb-container').nailthumb({ width: 363, height: 190 });
            $(".mainBannerSection").removeClass("innerPage");
            $(".mainBannerSection").addClass("homeImageBanner");
        });
    </script>


    <!--[if lt IE 9]>
      <script>
         document.createElement('video');
      </script>
      <![endif]-->
</asp:Content>
