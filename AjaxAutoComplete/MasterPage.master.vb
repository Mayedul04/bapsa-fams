﻿
Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Public currentLocation As String
    


    'Protected Function IsHomePage() As Boolean
    '    Dim retVal As Boolean = False

    '    If currentLocation = Session("domainname") & "en/home" Or currentLocation = Session("domainname") & "ar/home" Or currentLocation = Session("domainname") & "default.aspx" Or currentLocation = Session("domainname").ToString().TrimEnd(New Char() {"/"}) Then
    '        retVal = True
    '    End If
    '    Return retVal
    'End Function

    Protected Function IsHomePage() As Boolean
        currentLocation = Request.Url.ToString.ToLower()

        Return Regex.IsMatch(currentLocation, ".+/[h][o][m][e].*") Or Regex.IsMatch(currentLocation, ".*default\.aspx.*")
    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        

        If Not IsPostBack Then
            Session.Add("domainName", ConfigurationManager.AppSettings("RedirectUrl").ToString)

            
            If Page.RouteData.Values("lang") Is Nothing Then
                Response.Redirect(Session("domainName") & "en/home")
            End If

            Language.Lang = Nothing
            Dim lang As String = Page.RouteData.Values("lang")
            If lang = "" Or lang = Nothing Then
                lang = "en"
            End If
            Session.Add("lang", lang)
            currentLocation = Request.Url.ToString().ToLower()
        End If
    End Sub

    Protected Function GetLanguage() As String
        Dim retVal As String = ""
        Dim currentLocation = Request.Url.ToString()
        If IsHomePage() Then
            currentLocation = Session("domainName") & Session("lang") & "/home"
        End If
        If Session("lang") = "en" Then
            retVal = "<a href=""javascript:;"" class=""langOption"">EN" & _
                 "    <span class=""dropDonwLang"">AR</span>" & _
                 "    </a>"
        Else
            retVal = "<a href=""javascript:;"" class=""langOption"">AR" & _
                 "    <span class=""dropDonwLang"">EN</span>" & _
                 "    </a>"
        End If
        


        Return retVal
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If IsHomePage() Then

        End If

    End Sub

    Public Function FormateTitle(ByVal title2 As String) As String
        Dim retstr As String = ""
        Dim titlearray As String()
        titlearray = title2.Split(" ")
        Dim middle As Integer = Math.Ceiling(titlearray.Length / 2)
        Dim firstpart As String = ""
        For i As Integer = 0 To middle - 1
            firstpart += titlearray(i) & " "
        Next
        Dim secondpart As String = ""
        For i As Integer = middle To titlearray.Length - 1
            secondpart += titlearray(i) & " "
        Next
        retstr = firstpart & "<span>" & secondpart & "</span>"
        Return retstr
    End Function



    Protected Function GetSaudiGulfExperianceMenu(ByVal CategoryID As String, Optional ByVal title As String = "your-stay", Optional ByVal ddlClass As String = "dropDownList") As String
        Dim retVal = New StringBuilder()

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  list.* ,ListEn.TitleEn    FROM [dbo].[List]  inner join (select MasterID, Title as TitleEn,Category from List where List.Lang='en' and List.Status=1 ) as ListEn on List.MasterID= ListEn.MasterID  and List.Category=ListEn.Category where [Status]=1  and List.Lang=@Lang  and List.Category=@Category   order by List.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")
        cmd.Parameters.Add("Category", Data.SqlDbType.Int).Value = CategoryID

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim url As String = ""
        While reader.Read
            If title = "your-stay" Then
                url = Session("domainName") & Session("lang") & "/your-stay/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")
            Else
                If reader("MasterID").ToString() = "13" Then
                    'our fleet
                    url = Session("domainName") & Session("lang") & "/fleets"
                Else
                    url = Session("domainName") & Session("lang") & "/saudigulf-experience/" & CategoryID & "/" & title & "/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")
                End If

            End If

            retVal.Append("<li><a href=""" & url & """ title=""" & reader("Title") & """><span></span>" & reader("Title") & "</a></li>")

        End While
        conn.Close()

        Return "<ul class=""" & ddlClass & """>" & retVal.ToString & "</ul>"
    End Function

    Protected Function GetDiscoverMenu() As String
        Dim retVal = New StringBuilder()

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  Discover.* ,DiscoverEn.TitleEn    FROM [dbo].[Discover]  inner join (select MasterID, Title as TitleEn,Category from Discover where Discover.Lang='en' and Discover.Status=1 ) as DiscoverEn on Discover.MasterID= DiscoverEn.MasterID  and Discover.Category=DiscoverEn.Category where [Status]=1  and Discover.Lang=@Lang  and Discover.Category=@Category   order by Discover.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")
        cmd.Parameters.Add("Category", Data.SqlDbType.Int).Value = 1

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim url As String = ""
        While reader.Read

            url = Session("domainName") & Session("lang") & "/discover-the-world/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")

            retVal.Append("<li><a href=""" & url & """ title=""" & reader("Title") & """><span></span>" & reader("Title") & "</a></li>")

        End While
        conn.Close()

        Return "<ul class=""megaDropDownList"">" & retVal.ToString & "</ul>"
    End Function


    Protected Function GetDestinationsMenu() As String
        Dim retVal = New StringBuilder()

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  List1.* ,List1En.TitleEn,CountryList.CountryName    FROM [dbo].[List1]  inner join (select MasterID, Title as TitleEn,  Category from List1 where List1.Lang='en' and List1.Status=1 ) as List1En on List1.MasterID= List1En.MasterID  and List1.Category=List1En.Category inner join [CountryList] on List1.Category=[CountryList].CountryCode where  List1.[Status]=1  and List1.Lang=@Lang    order by [CountryName], List1.category, List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")
        'cmd.Parameters.Add("Category", Data.SqlDbType.Int).Value = 1

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim url As String = ""
        Dim CountryName As String = ""
        While reader.Read
            If CountryName = "" Then
                retVal.Append("<li><a href='javascript:;'><span></span>" & Language.Read(reader("CountryName").ToString(), Session("lang")) & "</a><ul class='thirdLevel'>")
            ElseIf CountryName <> reader("CountryName") Then
                retVal.Append("</ul></li><li><a href='javascript:;'><span></span>" & Language.Read(reader("CountryName").ToString(), Session("lang")) & "</a><ul class='thirdLevel'>")
            End If
            CountryName = reader("CountryName")
            url = Session("domainName") & Session("lang") & "/our-destinations/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")

            retVal.Append("<li><a href=""" & url & """ title=""" & reader("Title") & """><span></span>" & reader("AreaName") & "</a></li>")

        End While

        conn.Close()
        retVal.Append("</ul></li>")
        Return "<ul class=""megaDropDownList"">" & retVal.ToString & "</ul>"
    End Function



    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Response.Redirect("/" & Session("lang") & "/search/?q=" & Server.HtmlEncode(txtSearch.Text.Replace(".", " ").Replace("@", "").Replace("*", "").Replace("<", "").Replace(">", "").Replace("?", "").Trim), True)
    End Sub



    
End Class

