﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected animateIn As New List(Of String)
    Protected animateOut As New List(Of String)

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            If Session("lang") Is Nothing Then
                Session.Add("lang", "en")
            End If
            'ltrVideo.Text = "<source src=""" & Session("domainname") & "Admin/" & GetVideoLink() & """ type=""video/mp4"">   "
            'ltrVideoEdit.Text = Utility.showEditButton(Request, "Admin/A-HTML/HTMLEdit.aspx?hid=85&Title=0&SmallImage=0&BigImage=0&ImageAltText=0&SmallDetails=0&BigDetails=0&SmallImageWidth=214&SmallImageHeight=143&BigImageWidth=360&BigImageHeight=242&Link=1")
        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        animateIn.Add("flipInX")
        animateIn.Add("openDownLeftReturn")
        animateIn.Add("bounceInUp")
        animateIn.Add("bounceInDown")
        animateIn.Add("rollIn")
        animateOut.Add("holeOut")
        animateOut.Add("slideUp")
        animateOut.Add("hinge")
        animateOut.Add("flipOutX")
        animateOut.Add("flipOutX")

        ltrNews.Text = GetGeneralAnnouncement()
    End Sub

    Protected Function GetBanner(ItemIndex As Integer, BannerID As Integer, BigImage As String, SmallImage As String, ImageAltText As String) As String
        Dim retVal = New StringBuilder()
        If SmallImage <> "" Then
            'video 
            retVal.Append("<div class=""bs-video-fullscreen"">" & _
                         "          <video autoplay=""autoplay"" muted=""muted"" loop poster=""" & Session("domainName") & "Admin/" & BigImage & """ id=""saudiGulfVideo"">" & _
                         "            <source src=""" & Session("domainName") & "Admin/" & SmallImage & """ type=""video/" & SmallImage.Substring(SmallImage.LastIndexOf(".") + 1) & """>      " & _
                         "            Your browser does not support the video tag." & _
                         "          </video>" & Utility.showEditButton(Request, "admin/A-Banner/TopBannerEdit.aspx?bannerId=" & BannerID) & _
                         "      </div>")
        Else
            'image
            retVal.Append("<div class=""bs-background""> " & _
                          "          <img src='" & Session("domainName") & "Admin/" & BigImage & "' alt='" & ImageAltText & "' />" & _
                                    Utility.showEditButton(Request, "admin/A-Banner/TopBannerEdit.aspx?bannerId=" & BannerID) & _
                          "      </div>")
        End If
        Return retVal.ToString()
    End Function

    Function GetGeneralAnnouncement() As String
        Dim retVal = New StringBuilder()
        Dim urlTitle As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  [NewsID]      ,[Title]      ,[SmallDetails]      ,[SmallImage]      ,[ImageAltText]       ,[MasterID]      ,[Lang]  ,SortIndex      FROM [dbo].[List_News]  where [Status]=1 and [Category]='News'  order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim url As String = ""
        Dim MasterID As String = ""
        Dim counter As Integer = 0
        While reader.Read

            If MasterID <> "" And MasterID <> reader("MasterID").ToString() Then
                MasterID = reader("MasterID").ToString()
                retVal = New StringBuilder(String.Format(retVal.ToString, url & urlTitle))
            ElseIf MasterID = "" Then
                'first time of iteration
                MasterID = reader("MasterID").ToString()
            End If


            If Session("Lang") = reader("lang").ToString() Then
                url = Session("domainName") & Session("lang") & "/news/" & reader("MasterID") & "/"
                retVal.Append("<li>   " & _
                        "    <div class=""contentContainer""> " & _
                        "        <div class=""imgHold  nailthumb-container"">" & _
                        "            <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>" & _
                        "            <span class=""shape""><img src=""" & Session("domainName") & "ui/media/dist/bg/boxInfoBg-golden.png"" alt=""""></span>" & _
                        "        </div>" & _
                        "        <h2><span><img src=""" & Session("domainName") & "ui/media/dist/bg/headingBg.png"" alt=""""></span>" & reader("Title") & "</h2>" & _
                        "        <p>" & reader("SmallDetails") & Utility.showEditButton(Request, "Admin/A-News/NewsEdit.aspx?cat=News&nid=" & reader("NewsID")) & "</p>" & _
                        "        <a class=""viewMore brown"" href=""{0}"" title='" & reader("Title") & "'>View More<i class=""spriteImg viewMoreIcon""></i></a>" & _
                        "    </div>" & _
                        "</li>")
                        
                counter += 1
            End If
            If reader("lang") = "en" Then
                urlTitle = Utility.EncodeTitle(reader("title").ToString(), "-")
            End If



        End While
        retVal = New StringBuilder("<ul class='sliderListings slides'>" & String.Format(retVal.ToString(), url & urlTitle) & "</ul>")

        conn.Close()

        Return retVal.ToString()
    End Function

    Function GetFlightNews() As String
        Dim retVal = New StringBuilder()
        Dim urlTitle As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  [NewsID]      ,[Title]      ,[SmallDetails]      ,[SmallImage]      ,[ImageAltText]       ,[MasterID]      ,[Lang]  ,SortIndex      FROM [dbo].[List_News]  where [Status]=1 and [Category]='Flight'  order by SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim url As String = ""
        Dim MasterID As String = ""
        Dim counter As Integer = 0
        While reader.Read

            If MasterID <> "" And MasterID <> reader("MasterID").ToString() Then
                MasterID = reader("MasterID").ToString()
                retVal = New StringBuilder(String.Format(retVal.ToString, url & urlTitle))
            ElseIf MasterID = "" Then
                'first time of iteration
                MasterID = reader("MasterID").ToString()
            End If


            If Session("Lang") = reader("lang").ToString() Then
                url = Session("domainName") & Session("lang") & "/news/" & reader("MasterID") & "/"
                retVal.Append("<li>" & _
                            "    <h3><a  href=""{0}"" title='" & reader("Title") & "'>" & reader("Title") & "</a></h3>" & _
                            "    <p>" & reader("SmallDetails") & Utility.showEditButton(Request, "Admin/A-FlightNews/NewsEdit.aspx?cat=Flight&nid=" & reader("NewsID")) & "</p>" & _
                            "    <a class=""viewMore brown"" href=""{0}"" title='" & reader("Title") & "'>" & Language.Read("View More", Session("lang")) & "<i class=""spriteImg viewMoreIcon""></i></a>" & _
                            "</li>")

                counter += 1
            End If
            If reader("lang") = "en" Then
                urlTitle = Utility.EncodeTitle(reader("title").ToString(), "-")
            End If



        End While
        retVal = New StringBuilder("<ul class='slides'>" & String.Format(retVal.ToString(), url & urlTitle) & "</ul>")

        conn.Close()

        Return retVal.ToString()
    End Function

    Protected Function GetFeaturedOffer() As String
        Dim retVal = ""

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  [SpecialOfferID]      ,SpecialOffer.[Title]      ,SpecialOffer.[SmallDetails]      ,SpecialOffer.[SmallImage]      ,SpecialOffer.[ImageAltText]       ,SpecialOffer.[MasterID]      ,SpecialOffer.[Lang]  ,SpecialOffer.SortIndex ,SpecialOffer.StartDate,SpecialOffer.EndDate ,SpecialOfferEn.TitleEn    FROM [dbo].[SpecialOffer]  inner join (select MasterID, Title as TitleEn from SpecialOffer where SpecialOffer.Lang='en' and SpecialOffer.Status=1 and (getDate() between StartDate and EndDate ) ) as SpecialOfferEn on SpecialOffer.MasterID= SpecialOfferEn.MasterID   where [Status]=1  and (getDate() between StartDate and EndDate ) and SpecialOffer.Lang=@Lang and SpecialOffer.Featured=1 "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.Read() Then

            Dim url = Session("domainName") & Session("lang") & "/special-offers/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")
            retVal = "<div class='col-md-5'>" & _
                    "    <div class='featuredImgContainer'>" & _
                    "        <div class='imgHolder FeaturedOffer' >" & _
                    "            <img src='" & Session("domainName") & "Admin/" & reader("SmallImage") & "' alt='" & reader("ImageAltText") & "' />" & _
                    "        </div>" & _
                    "        <div class='featuredInfo'>" & _
                    "            <div class='infoText'>" & _
                    "                <h3><a href='{0}' title='" & reader("Title") & "'>" & reader("Title") & "</a></h3>" & _
                    "                <p>Valid From: " & Date.Parse(reader("StartDate").ToString).ToString("dd MMM yyyy") & " To: " & Date.Parse(reader("EndDate").ToString).ToString("dd MMM yyyy") & "</p>" & _
                    "            </div>" & _
                    "        </div>" & _
                    "    </div>" & _
                    "</div>" & _
                    "<!-- /col-md-5 -->" & _
                    "<div class='col-md-4'>" & _
                    "    <div class='featuredBox'>" & _
                    "        <h2><span>" & Language.Read("Offers", Session("lang")) & "</span> <i class='spriteImg roundDownArrow small'></i></h2>" & _
                    "        <h4><a href='{0}' title='" & reader("Title") & "'>" & reader("Title") & "</a>" & Utility.showEditButton(Request, "Admin/A-SpecialOffer/SpecialOfferEdit.aspx?specialOfferId=" & reader("SpecialOfferID")) & "</h4>" & _
                    reader("SmallDetails").ToString.Replace("<ul>", "<ul class='list'>") & _
                    "        <p>" & _
                    "            <a href='{0}' class='viewMore brown' title='" & reader("Title") & "'>" & Language.Read("View More", Session("lang")) & "<i class='spriteImg viewMoreIcon'></i></a>" & _
                    "        </p>" & _
                    "    </div>" & _
                    "</div>"
            retVal = String.Format(retVal, url)
        End If
        conn.Close()



        Return retVal
    End Function

    Protected Function GetSaudiGulfExperience() As String

        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML(42, HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        SmallDetails = SmallDetails & Utility.showEditButton(Request, "Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=0&SmallDetails=1&BigDetails=0&SmallImageWidth=214&SmallImageHeight=143&BigImageWidth=360&BigImageHeight=242&Link=0")
        Dim url = Session("domainName") & Session("lang") & "/saudigulf-experience"

        Dim retVal As String = "<div class=""row"">" & _
                               "     <div class=""col-md-12 textData"">" & _
                               "     <h2><span>" & Title & "</span> <i class=""spriteImg roundDownArrow small""></i></h2>" & _
                               "     <p>" & SmallDetails & "</p>" & _
                               "     <p align=""right"">" & _
                               "         <a href=""" & url & """ class=""viewMore brown"" title='" & Title & "'>" & Language.Read("View More", Session("lang")) & "<i class=""spriteImg viewMoreIcon""></i></a>" & _
                               "     </p>" & _
                               "     </div>" & _
                               "</div>"


        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  List.MasterID,List.SmallImage,List.ImageAltText ,List.Title ,ListEn.TitleEn    FROM [dbo].[List]  inner join (select MasterID, Title as TitleEn,Category from List where List.Lang='en' and List.Status=1 ) as ListEn on List.MasterID= ListEn.MasterID  and List.Category=ListEn.Category where [Status]=1  and List.Lang=@Lang  and List.Category=@Category  order by List.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")
        cmd.Parameters.Add("Category", Data.SqlDbType.Int).Value = 2

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim content As String = ""
        Dim counter As Integer = 0
        While reader.Read
            If counter = 0 Then
                content = "<div class=""item active""><ul class=""serviceList"">"
            ElseIf counter Mod 3 = 0 Then
                content &= "</ul></div><div class=""item""><ul class=""serviceList"">"
            End If
            
            url = Session("domainName") & Session("lang") & "/saudigulf-experience/2/cabin-features/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")

            content &= "<li>" & _
                       "     <div class=""box"">" & _
                       "         <div class=""imgHolder"">" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>" & _
                       "         </div>" & _
                       "         <div class=""boxInfo"">" & _
                       "             <p><a href=""" & url & """><span>" & reader("Title") & "</span> <i class=""spriteImg circleMoreIcon""></i></a></p>" & _
                       "         </div>" & _
                       "     </div>" & _
                       " </li>"
            counter += 1
        End While
        If counter > 0 Then
            content &= "</ul></div>"
        End If
        conn.Close()


        retVal &= "<div class='row'>" & _
                "    <div class='col-md-12'>" & _
                "        <div id='flightExpSlider' class='carousel slide genericSlider' data-ride='carousel'>" & _
                "            <!-- Wrapper for slides -->" & _
                "            <div class='carousel-inner' role='listbox'>" & content & "</div>" & _
                "            <!-- /carousel-inner -->" & _
                "            <!-- Controls -->" & _
                "            <a class='left carousel-control' href='#flightExpSlider' role='button' data-slide='prev'>" & _
                "                <i class='spriteImg sliderLeftArrow'></i>" & _
                "            </a>" & _
                "            <a class='right carousel-control' href='#flightExpSlider' role='button' data-slide='next'>" & _
                "                <i class='spriteImg sliderRightArrow'></i>" & _
                "            </a>" & _
                "        </div>" & _
                "    </div>" & _
                "</div>" 'String.Format(retVal, content)

        Return retVal
    End Function


    Protected Function GetDining() As String

        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML(3, HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        SmallDetails = SmallDetails & Utility.showEditButton(Request, "Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=0&SmallDetails=1&BigDetails=0&SmallImageWidth=214&SmallImageHeight=143&BigImageWidth=360&BigImageHeight=242&Link=0")
        Dim url = Session("domainName") & Session("lang") & "/saudigulf-experience/3/dining"

        Dim retVal As String = "<div class=""row"">" & _
                               "     <div class=""col-md-12 textData"">" & _
                               "     <h2><span>" & Title & "</span> <i class=""spriteImg roundDownArrow small""></i></h2>" & _
                               "     <p>" & SmallDetails & "</p>" & _
                               "     <p align=""right"">" & _
                               "         <a href=""" & url & """ class=""viewMore brown"" title='" & Title & "'>" & Language.Read("View More", Session("lang")) & "<i class=""spriteImg viewMoreIcon""></i></a>" & _
                               "     </p>" & _
                               "     </div>" & _
                               "</div>"


        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  List.MasterID,List.SmallImage,List.ImageAltText ,List.Title,ListEn.TitleEn    FROM [dbo].[List]  inner join (select MasterID, Title as TitleEn,Category from List where List.Lang='en' and List.Status=1 ) as ListEn on List.MasterID= ListEn.MasterID  and List.Category=ListEn.Category where [Status]=1  and List.Lang=@Lang  and List.Category=@Category  order by List.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")
        cmd.Parameters.Add("Category", Data.SqlDbType.Int).Value = 3

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim content As String = ""
        Dim counter As Integer = 0
        While reader.Read
            If counter = 0 Then
                content = "<div class=""item active""><ul class=""serviceList"">"
            ElseIf counter Mod 3 = 0 Then
                content &= "</ul></div><div class=""item""><ul class=""serviceList"">"
            End If
            url = Session("domainName") & Session("lang") & "/saudigulf-experience/3/your-stay/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")
            content &= "<li>" & _
                       "     <div class=""box"">" & _
                       "         <div class=""imgHolder"">" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>" & _
                       "         </div>" & _
                       "         <div class=""boxInfo"">" & _
                       "             <p><a href=""" & url & """><span>" & reader("Title") & "</span> <i class=""spriteImg circleMoreIcon""></i></a></p>" & _
                       "         </div>" & _
                       "     </div>" & _
                       " </li>"
            counter += 1
        End While
        If counter > 0 Then
            content &= "</ul></div>"
        End If
        conn.Close()


        retVal &= "<div class='row'>" & _
                "    <div class='col-md-12'>" & _
                "        <div id='DiningSlider' class='carousel slide genericSlider' data-ride='carousel'>" & _
                "            <!-- Wrapper for slides -->" & _
                "            <div class='carousel-inner' role='listbox'>" & content & "</div>" & _
                "            <!-- /carousel-inner -->" & _
                "            <!-- Controls -->" & _
                "            <a class='left carousel-control' href='#DiningSlider' role='button' data-slide='prev'>" & _
                "                <i class='spriteImg sliderLeftArrow'></i>" & _
                "            </a>" & _
                "            <a class='right carousel-control' href='#DiningSlider' role='button' data-slide='next'>" & _
                "                <i class='spriteImg sliderRightArrow'></i>" & _
                "            </a>" & _
                "        </div>" & _
                "    </div>" & _
                "</div>" 'String.Format(retVal, content)

        Return retVal
    End Function

    Protected Function GetDestinations() As String

        Dim HTMLID As String = "", Title As String = "", SmallDetails As String = "", BigDetails As String = "", SmallImage As String = "", BigImage As String = "", ImageAltText As String = ""
        HTML(41, HTMLID, Title, SmallDetails, BigDetails, SmallImage, BigImage, ImageAltText)
        SmallDetails = SmallDetails & Utility.showEditButton(Request, "Admin/A-HTML/HTMLEdit.aspx?hid=" & HTMLID & "&Title=1&SmallImage=0&BigImage=0&ImageAltText=0&SmallDetails=1&BigDetails=0&SmallImageWidth=214&SmallImageHeight=143&BigImageWidth=360&BigImageHeight=242&Link=0")
        Dim url = Session("domainName") & Session("lang") & "/our-destinations"

        Dim retVal As String = "<div class=""row"">" & _
                               "     <div class=""col-md-12 textData"">" & _
                               "     <h2><span>" & Title & "</span> <i class=""spriteImg roundDownArrow small""></i></h2>" & _
                               "     <p>" & SmallDetails & "</p>" & _
                               "     <p align=""right"">" & _
                               "         <a href=""" & url & """ class=""viewMore brown"" title='" & Title & "'>" & Language.Read("View More", Session("lang")) & "<i class=""spriteImg viewMoreIcon""></i></a>" & _
                               "     </p>" & _
                               "     </div>" & _
                               "</div>"


        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  List1.MasterID,List1.SmallImage,List1.ImageAltText,List1.AreaName ,List1En.TitleEn    FROM [dbo].[List1]  inner join (select MasterID, Title as TitleEn,  Category from List1 where List1.Lang='en' and List1.Status=1 ) as List1En on List1.MasterID= List1En.MasterID  and List1.Category=List1En.Category where [Status]=1  and List1.Lang=@Lang    order by List1.SortIndex"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")
        'cmd.Parameters.Add("Category", Data.SqlDbType.Int).Value = 1

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim content As String = ""
        Dim counter As Integer = 0
        While reader.Read
            If counter = 0 Then
                content = "<div class=""item active""><ul class=""serviceList"">"
            ElseIf counter Mod 3 = 0 Then
                content &= "</ul></div><div class=""item""><ul class=""serviceList"">"
            End If
            url = Session("domainName") & Session("lang") & "/our-destinations/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")
            content &= "<li>" & _
                       "     <div class=""box"">" & _
                       "         <div class=""imgHolder"">" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>" & _
                       "         </div>" & _
                       "         <div class=""boxInfo"">" & _
                       "             <p><a href=""" & url & """><span>" & reader("AreaName") & "</span> <i class=""spriteImg circleMoreIcon""></i></a></p>" & _
                       "         </div>" & _
                       "     </div>" & _
                       " </li>"
            counter += 1
        End While
        If counter > 0 Then
            content &= "</ul></div>"
        End If
        conn.Close()


        retVal &= "<div class='row'>" & _
                "    <div class='col-md-12'>" & _
                "        <div id='DestinationsSlider' class='carousel slide genericSlider' data-ride='carousel'>" & _
                "            <!-- Wrapper for slides -->" & _
                "            <div class='carousel-inner' role='listbox'>" & content & "</div>" & _
                "            <!-- /carousel-inner -->" & _
                "            <!-- Controls -->" & _
                "            <a class='left carousel-control' href='#DestinationsSlider' role='button' data-slide='prev'>" & _
                "                <i class='spriteImg sliderLeftArrow'></i>" & _
                "            </a>" & _
                "            <a class='right carousel-control' href='#DestinationsSlider' role='button' data-slide='next'>" & _
                "                <i class='spriteImg sliderRightArrow'></i>" & _
                "            </a>" & _
                "        </div>" & _
                "    </div>" & _
                "</div>" 'String.Format(retVal, content)

        Return retVal
    End Function

    Protected Function GetFleet() As String
        Dim retVal As String = ""

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim cmd As Data.SqlClient.SqlCommand
        Dim reader As Data.SqlClient.SqlDataReader
        Dim selectString = "select  list.* ,ListEn.TitleEn    FROM [dbo].[List]  inner join (select MasterID, Title as TitleEn,Category from List where List.Lang='en' and List.Status=1 ) as ListEn on List.MasterID= ListEn.MasterID  and List.Category=ListEn.Category where [Status]=1  and List.Lang=@Lang  and List.MasterID=13  order by SortIndex"
        cmd = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")
        reader = cmd.ExecuteReader()
        Dim url = Session("domainName") & Session("lang") & "/fleets"
        Dim tableID As String = ""
        If reader.Read Then
            tableID = reader("ListID").ToString()
            retVal = "<div class=""row"">" & _
                                    "     <div class=""col-md-12 textData"">" & _
                                    "     <h2><span>" & reader("Title") & "</span> <i class=""spriteImg roundDownArrow small""></i></h2>" & _
                                    "     <p>" & reader("SmallDetails") & Utility.showEditButton(Request, "Admin/A-List/ListEdit.aspx?lid=" & reader("ListID")) & "</p>" & _
                                    "     <p align=""right"">" & _
                                    "         <a href=""" & url & """ class=""viewMore brown"" title='" & reader("Title") & "'>" & Language.Read("View More", Session("lang")) & "<i class=""spriteImg viewMoreIcon""></i></a>" & _
                                    "     </p>" & _
                                    "     </div>" & _
                                    "</div>"
        End If
        reader.Close()


        selectString = "select  Flights.FlightNo,Flights.Destinations, Flights.MasterID, Flights.[FlightID]      ,Flights.[Title]      ,Flights.[SmallDetails]     ,Flights.[SmallImage]      ,Flights.[ImageAltText] , Flights.SortIndex ,TitleEn  FROM [dbo].[Flights]  inner join (select MasterID, Title as TitleEn from Flights where Flights.Lang='en' and Flights.Status=1  ) as ContentsEn on Flights.MasterID= ContentsEn.MasterID    where [Flights].Status=1  and  Flights.Lang=@Lang     order by SortIndex"
        cmd = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")
        

        reader = cmd.ExecuteReader()
        Dim content As String = ""
        Dim counter As Integer = 0
        While reader.Read
            If counter = 0 Then
                content = "<div class=""item active""><ul class=""serviceList"">"
            ElseIf counter Mod 3 = 0 Then
                content &= "</ul></div><div class=""item""><ul class=""serviceList"">"
            End If
            url = Session("domainName") & Session("lang") & "/fleets/details/" & reader("MasterID") & "/" & Utility.EncodeTitle(reader("titleEn").ToString(), "-")
            content &= "<li>" & _
                       "     <div class=""box"">" & _
                       "         <div class=""imgHolder"">" & _
                       "             <img src=""" & Session("domainName") & "Admin/" & reader("SmallImage") & """ alt=""" & reader("ImageAltText") & """>" & _
                       "         </div>" & _
                       "         <div class=""boxInfo"">" & _
                       "             <p><a href=""" & url & """><span>" & reader("FlightNo") & "</span> <i class=""spriteImg circleMoreIcon""></i></a></p>" & _
                       "         </div>" & _
                       "     </div>" & _
                       " </li>"
            counter += 1
        End While
        If counter > 0 Then
            content &= "</ul></div>"
        End If
        conn.Close()


        retVal &= "<div class='row'>" & _
                "    <div class='col-md-12'>" & _
                "        <div id='FleetSlider' class='carousel slide genericSlider' data-ride='carousel'>" & _
                "            <!-- Wrapper for slides -->" & _
                "            <div class='carousel-inner' role='listbox'>" & content & "</div>" & _
                "            <!-- /carousel-inner -->" & _
                "            <!-- Controls -->" & _
                "            <a class='left carousel-control' href='#FleetSlider' role='button' data-slide='prev'>" & _
                "                <i class='spriteImg sliderLeftArrow'></i>" & _
                "            </a>" & _
                "            <a class='right carousel-control' href='#FleetSlider' role='button' data-slide='next'>" & _
                "                <i class='spriteImg sliderRightArrow'></i>" & _
                "            </a>" & _
                "        </div>" & _
                "    </div>" & _
                "</div>" 'String.Format(retVal, content)

        Return retVal
    End Function

    Private Sub HTML(ByVal MasterID As String, ByRef HTMLID As String, ByRef Title As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where MasterID=@MasterID and Lang=@Lang  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = MasterID
        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.Read Then
            HTMLID = reader("HtmlID").ToString()
            Title = reader("Title") & ""
            SmallDetails = reader("SmallDetails") & ""

            BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

            SmallImage = reader("SmallImage") & ""
            BigImage = reader("BigImage") & ""
            ImageAltText = reader("ImageAltText") & ""
        End If

        conn.Close()
    End Sub

    Private Function GetVideoLink() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,MasterID  FROM            HTML where HtmlID=@HtmlID " 'and Lang=@Lang  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("HtmlID", Data.SqlDbType.Int).Value = 85
        'cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.Read Then
            retVal = reader("Link").ToString()
        End If

        conn.Close()

        Return retVal
    End Function

    'Private Sub HTML(ByVal MasterID As String, ByRef HTMLID As String, ByRef Title As String, ByRef TitleEn As String, ByRef SmallDetails As String, ByRef BigDetails As String, ByRef SmallImage As String, ByRef BigImage As String, ByRef ImageAltText As String)
    '    Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
    '    conn.Open()
    '    Dim selectString = "SELECT        HtmlID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, LastUpdated,  LinkTitle, VideoLink, VideoCode, SmallImageWidth, SmallImageHeight, BigImageWidth, BigImageHeight, VideoWidth, VideoHeight,ImageAltText,Lang,HTML.MasterID,HTMLEN.TitleEn  FROM    HTML  inner join (select Title as TitleEn, MasterID from HTML where lang='en')as HTMLEN on HTML.MasterID=HTMLEN.MasterID  where HTML.MasterID=@MasterID and Lang=@Lang  "
    '    Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
    '    cmd.Parameters.Add("MasterID", Data.SqlDbType.Int).Value = MasterID
    '    cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = Session("Lang")

    '    Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

    '    If reader.Read Then
    '        HTMLID = reader("HtmlID").ToString()
    '        Title = reader("Title") & ""
    '        TitleEn = reader("TitleEn").ToString()
    '        SmallDetails = reader("SmallDetails") & ""

    '        BigDetails = reader("BigDetails") & "" 'hdnDetails.Value

    '        SmallImage = reader("SmallImage") & ""
    '        BigImage = reader("BigImage") & ""
    '        ImageAltText = reader("ImageAltText") & ""
    '    End If

    '    conn.Close()
    'End Sub
End Class
