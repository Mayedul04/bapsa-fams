﻿<%@ WebHandler Language="VB" Class="DestinationFinder" %>

Imports System
Imports System.Web

Public Class DestinationFinder : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim searchVal = context.Request.QueryString("searchVal")
        Dim CounterPartVal As String=""
        Try
            CounterPartVal = context.Request.QueryString("CounterPartVal").Remove(context.Request.QueryString("CounterPartVal").IndexOf(" : "))
        Catch ex As Exception

        End Try
        Dim outputString As String = ""
        
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()

        Dim selectString = "select  List1.MasterID,List1.AreaName,List1.AreaShortName ,List1En.TitleEn    FROM [dbo].[List1]  inner join (select MasterID, Title as TitleEn,  Category from List1 where List1.Lang='en' and List1.Status=1 ) as List1En on List1.MasterID= List1En.MasterID  and List1.Category=List1En.Category where [Status]=1  and List1.Lang=@Lang  and   List1.AreaName like '%'+ @AreaName + '%' order by List1.AreaName "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("AreaName", Data.SqlDbType.VarChar, 200).Value = searchVal
        cmd.Parameters.Add("Lang", Data.SqlDbType.VarChar, 10).Value = "en"
        'cmd.Parameters.Add("Category", Data.SqlDbType.Int).Value = 1

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        Dim content As String = ""
        Dim counter As Integer = 0
        While reader.Read
            If reader("AreaName").ToString().ToLower <> CounterPartVal.ToLower() Then
                outputString += "{" & String.Format("""id"":""{0}"",""label"":""{1}"",""value"":""{2}""", reader("AreaShortName").ToString(), reader("AreaName").ToString()& " : " & reader("AreaShortName"), reader("AreaName").ToString()& " : " & reader("AreaShortName")) & "},"
            End If
        End While
        If counter > 0 Then
            content &= "</ul></div>"
        End If
        conn.Close()
        'If CounterPartVal.ToLower() <> "dubai" Then
        '    outputString = "{" & String.Format("""id"":""{0}"",""label"":""{1}"",""value"":""{2}""","DXB", "Dubai : DXB", "Dubai : DXB") & "}," +outputString 
        'End If
        context.Response.Write("["& outputString.TrimEnd(New Char(){","}) &"]")
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class
