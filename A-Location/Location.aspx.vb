﻿
Partial Class Admin_A_HTML_HTML
    Inherits System.Web.UI.Page
    Public domainName As String
    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If

        'Utility.GetDimentionSetting("HTML", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)
        'HTMLEdit.aspx?hid=1&Title=1&SmallImage=1&BigImage=1&ImageAltText=1&SmallDetails=1&BigDetails=1&SmallImageWidth=100&SmallImageHeight=100&BigImageWidth=200&BigImageHeight=200
        If Not IsPostBack Then

            If Not String.IsNullOrEmpty(RouteData.Values("lid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Location"
                LoadContent(RouteData.Values("lid"))
            End If

        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If String.IsNullOrEmpty(RouteData.Values("lid")) Then

            If sdsObject.Insert() > 0 Then

                Response.Redirect(domainName & "Locations")
            Else
                divError.Visible = True
            End If
        Else
            If sdsObject.Update() > 0 Then
                divSuccess.Visible = True
                Response.Redirect(domainName & "Location/" & Request.QueryString("lid"))
            Else
                divError.Visible = True
            End If
        End If

    End Sub





    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        LocID, LocationName, Code, Address, ContactNo, Status  FROM   Location where LocID=@LocID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("LocID", Data.SqlDbType.Int)
        cmd.Parameters("LocID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            txtTitle.Text = reader("LocationName").ToString()
            txtCode.Text = reader("Code").ToString()

            txtDetails.Text = reader("Address").ToString()

            txtPhone.Text = reader("ContactNo").ToString()

            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()
            Response.Redirect(domainName & "Locations")
        End If
        conn.Close()
    End Sub


End Class
