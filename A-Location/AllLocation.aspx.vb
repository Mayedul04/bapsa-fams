﻿
Imports System.Data.SqlClient
Imports System.Windows.Forms

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page
    Public permissions As List(Of Integer) = New List(Of Integer)
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (divDelete.Visible = True) Then
                divDelete.Visible = False
            End If
        End If
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If
        getPermission()
    End Sub
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 5 ' 6 is the Section ID for Config
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Private Sub DeleteRecordByID(id As Integer)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim categorycount As Integer = 0
        Dim assetcount As Integer = 0

        Dim selectString1 = "SELECT  Count(ItemID)  FROM   Assets where Location=@Location "
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmd1.Parameters.Add("Location", Data.SqlDbType.Int)
        cmd1.Parameters("Location").Value = id
        assetcount = cmd1.ExecuteScalar()

        If (categorycount > 0 Or assetcount > 0) Then
            divDelete.Visible = True
        Else
            If sdsObject.Delete() > 0 Then
                divSuccess.Visible = True
                hdnID.Value = ""
                ' GridView1.DataBind()
            Else
                divError.Visible = True
            End If

        End If
        conn.Close()
    End Sub
    Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
        If (e.CommandName = "Edit") Then
            Dim LocID As Integer = Integer.Parse(e.CommandArgument)
            LoadContent(LocID)
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If (hdnID.Value <> "") Then
            If (permissions.Contains(2)) Then

                If sdsObject.Update() > 0 Then
                    divSuccess.Visible = True
                    ' GridView1.DataBind()
                    ClearField()
                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Alter"
                divError.Visible = True
            End If
        Else
            If (permissions.Contains(1)) Then

                If sdsObject.Insert() > 0 Then
                    divSuccess.Visible = True
                    ' GridView1.DataBind()
                    ClearField()
                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Add Item"
                divError.Visible = True
        End If
        End If

    End Sub

    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (permissions.Contains(3)) Then

            If (hdnID.Value <> 0) Then
                DeleteRecordByID(hdnID.Value)
                ClearField()
            Else
                lblErrMessage.Text = "Please select a item to Delete"
                divError.Visible = True
            End If
        Else
            lblErrMessage.Text = "You do not have Permission to Delete"
            divError.Visible = True
        End If
    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        txtCode.Text = ""
        txtDetails.Text = ""
        txtPhone.Text = ""
        chkStatus.Checked = True
        hdnID.Value = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        LocID, LocationName, Code, Address, ContactNo, Status  FROM   Location where LocID=@LocID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("LocID", Data.SqlDbType.Int)
        cmd.Parameters("LocID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("LocID").ToString()
            txtTitle.Text = reader("LocationName").ToString()
            txtCode.Text = reader("Code").ToString()

            txtDetails.Text = reader("Address").ToString()

            txtPhone.Text = reader("ContactNo").ToString().Trim()

            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub

    'Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
    '    If (txtAutoSearch.Text.Contains(":")) Then
    '        Dim split = txtAutoSearch.Text.Split(":")
    '        Dim name1 As String = split(0)
    '        Dim id As Integer = split(1)
    '        hdnID.Value = id
    '        LoadContent(hdnID.Value)
    '    Else
    '        lblErrMessage.Text = "Please select a Valid item from Suggested List"
    '        divError.Visible = True
    '    End If

    'End Sub
    Protected Sub ddlLocations_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocations.SelectedIndexChanged
        If (ddlLocations.SelectedIndex <> 0) Then
            hdnID.Value = ddlLocations.SelectedValue
            LoadContent(hdnID.Value)
        Else
            ClearField()
        End If

    End Sub
End Class
