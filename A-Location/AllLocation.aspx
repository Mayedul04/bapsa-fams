﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllLocation.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="span5" style="margin-left:0px; margin-top:5px;">
        <div class="btn-group">
            <%--<script type="text/javascript" language="javascript">
                function OnContactSelected(source, eventArgs) {
                    var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                                        document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();

                                    }
            </script>
            <label>Search by Branch Name</label>
            <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
            <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetLocationList"
                ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                OnClientItemSelected="OnContactSelected">
            </cc1:AutoCompleteExtender>--%>
            
        </div>
        <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divDelete" visible="false" runat="server">
            <asp:Label ID="Label1" runat="server" Text="This Category may have Assets. Please Delete the Depedencies first"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="span12 form-top-strip">
           <p>Branch/Location Form</p> 
        </div>
        <div class="well">
            
            <div id="myTabContent" class="tab-content">
                
                <div class="span12">
                    <div class="left_div">
                        Search Branch
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlLocations" runat="server" CssClass="input-large" Width="100%" AutoPostBack="true" 
                            AppendDataBoundItems="true" DataSourceID="sdsLocations" DataTextField="LocationName" DataValueField="LocID">
                <asp:ListItem Value="">Select Branch</asp:ListItem>

            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsLocations" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT [LocID], [LocationName] FROM [Location]"></asp:SqlDataSource>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Location Name :</label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">

                    <div class="left_div">
                        <label>
                            Location Code :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtCode" runat="server" CssClass="input-xlarge" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Contact Number:
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationGroup="form" SetFocusOnError="True"
                        runat="server" Display="Dynamic" ErrorMessage="Phone" ControlToValidate="txtPhone" ValidationExpression="^(?:\+?(?:88|01)?)(?:\d{11}|\d{13})$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtPhone" runat="server" CssClass="input-xlarge" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtPhone" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>

                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Address :</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine"  CssClass="input-xlarge" Width="100%"
                            Rows="4"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                            runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                            ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                    </div>
                    <div class="right_div">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />
                        <label class="red">
                        </label>
                    </div>
                </div>
                <div class="span12">
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                    </div>
                </div>

            </div>


            <asp:SqlDataSource ID="sdsObject" runat="server"
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [Location] WHERE [LocID] = @LocID"
                InsertCommand="INSERT INTO [Location] ([LocationName], [Code], [Address], [ContactNo], [Status]) VALUES (@LocationName, @Code, @Address, @ContactNo, @Status)"
                SelectCommand="SELECT * FROM [Location]"
                UpdateCommand="UPDATE [Location] SET [LocationName] = @LocationName, [Code] = @Code, [Address] = @Address, [ContactNo] = @ContactNo, [Status] = @Status WHERE [LocID] = @LocID">
                <DeleteParameters>
                    <asp:ControlParameter ControlID="hdnID" Name="LocID" PropertyName="Value" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="LocationName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="LocationName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnID" Name="LocID" PropertyName="Value" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>



        </div>

        
    </div>
    <div class="span7">
        <div class="span12 form-top-strip" style="margin-top:3px; width:100%">
           <p>Location List</p> 
        </div>
        <div class="well" style="height:253px;margin-top:27px; display:block;">

            <table class="table">
                <thead>
                    <tr>
                        
                        <th>Location
                        </th>
                        
                      <%--  <th>Code</th>--%>
                        <th>Contact Number
                        </th>


                        <th style="width: 40px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" style="height:160px; overflow:auto;" DataKeyNames="LocID" DataSourceID="sdsLoc">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <%--<td>
                                    <asp:Label ID="ID" runat="server" Text='<%# Eval("LocID") %>' />
                                </td>--%>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("LocationName") %>' />
                                </td>
                                <%--<td>
                                    <asp:Label ID="Address" runat="server" Text='<%# Eval("Address") %>' />
                                </td>--%>
                              <%--  <td>
                                    <asp:Label ID="Code" runat="server" Text='<%# Eval("Code") %>' />
                                </td>--%>

                                <td>
                                    <asp:Label ID="Contact" runat="server" Text='<%# Eval("ContactNo") %>' />
                                </td>
                                <td>
                                    <asp:LinkButton ID="cmdEdit" CssClass="btn btn-success" CommandName="Edit" runat="server"
                                        CommandArgument='<%# Eval("LocID") %>' Text="Edit" />

                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <asp:SqlDataSource ID="sdsLoc" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT [LocID], [LocationName], [Code], [Address], [Status], [ContactNo] FROM [Location]"
            DeleteCommand="If ((Select COUNT(ItemID) from Assets where Location=@LocID)=0) DELETE FROM [Location] WHERE [LocID] = @LocID">
            <DeleteParameters>
                <asp:Parameter Name="LocID" Type="Int32" />
            </DeleteParameters>

        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>
