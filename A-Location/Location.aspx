﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Location.aspx.vb" Inherits="Admin_A_HTML_HTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Assets Location</h1>
    <div class="btn-toolbar">

        <a href="<%= domainName %>Locations" data-toggle="modal" class="btn btn-primary"><i class="icon-back"></i>Back</a>
        <div class="btn-group">
        </div>
    </div>

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Fixed Text"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>


    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <asp:Panel ID="pnlTitle" runat="server">
                <div class="left_div">
                    <label>
                        Location Name :</label>
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>


            </asp:Panel>

            <asp:Panel ID="pnlSmallImage" runat="server">
                <div class="left_div">
                    <label>
                        Location Code :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtCode" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtCode" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>


            <div class="left_div">
                <label>
                    Contact Number :</label>
            </div>
            <div class="right_div">
                <asp:TextBox ID="txtPhone" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                    ControlToValidate="txtPhone" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>

            </div>
            <div>
                <div class="left_div">
                    <label>
                        Address :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                        runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                        ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div>
                <div class="left_div">
                </div>
                <div class="right_div">
                    <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />
                    <label class="red">
                    </label>
                </div>
            </div>


        </div>

        <div class="btn-toolbar">
            <div class="right_div">
                <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            </div>


            <div class="btn-group">
            </div>
        </div>




    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Location] WHERE [LocID] = @LocID"
        InsertCommand="INSERT INTO [Location] ([LocationName], [Code], [Address], [ContactNo], [Status]) VALUES (@LocationName, @Code, @Address, @ContactNo, @Status)"
        SelectCommand="SELECT * FROM [Location]"
        UpdateCommand="UPDATE [Location] SET [LocationName] = @LocationName, [Code] = @Code, [Address] = @Address, [ContactNo] = @ContactNo, [Status] = @Status WHERE [LocID] = @LocID">
        <DeleteParameters>
            <asp:Parameter Name="LocID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="LocationName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="LocationName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
            <asp:QueryStringParameter Name="LocID" QueryStringField="lid" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

