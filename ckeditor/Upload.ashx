﻿<%@ WebHandler Language="VB" Class="Upload" %>

Imports System
Imports System.Web

Public Class Upload : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim uploads As HttpPostedFile = context.Request.Files("upload")
        Dim CKEditorFuncNum As String = context.Request("CKEditorFuncNum")
        Dim file As String = System.IO.Path.GetFileName(uploads.FileName)
        uploads.SaveAs(context.Server.MapPath("~") & "\Admin\Content\UserFiles\" & file)
        'provide direct URL here
        '"http://"&  context.Request.Url.Host & If(context.Request.Url.Host="localhost",":" & context.Request.Url.Port,"") &"/" & context.Request.a 
        Dim url As String = "http://" & context.Request.Url.Host & If(context.Request.Url.Host = "localhost", ":" & context.Request.Url.Port, "")  & context.Request.Url.AbsolutePath.Replace("ckeditor/Upload.ashx","") & "Content/UserFiles/" & file
        context.Response.Write("<script>window.parent.CKEDITOR.tools.callFunction(  " & CKEditorFuncNum & ", """ & url & """);</script>")
        'context.Response.[End]()
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    Public Sub VoidProcessRequest(context As HttpContext)
        
    End Sub
 
    Public ReadOnly Property boolIsReusable() As Boolean 
        Get
            Return False
        End Get
    End Property
    

End Class