﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="HandlerTest.aspx.vb" Inherits="HandlerTest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="span6">
        <div class="left_div">
            <label>First Group by</label>
        </div>
        <div class="right_div">
            <asp:DropDownList ID="ddlPrimary" runat="server" CssClass="input-large">
                <asp:ListItem Value="">Select</asp:ListItem>
                <asp:ListItem Value="Category_1.CategoryName">Main Category</asp:ListItem>
                <asp:ListItem Value="dbo.Location.LocationName">Branch</asp:ListItem>
                <asp:ListItem Value="dbo.Projects.ProjectName">Projects</asp:ListItem>
                <asp:ListItem Value="dbo.Department.Department">Department</asp:ListItem>
                <asp:ListItem Value="dbo.Supplier.Supplier">Supplier</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="span6">
        <div class="left_div">
            <label>Filter by</label>
        </div>
        <div class="right_div cont">
            <asp:DropDownList ID="ddlPrimaryFilter" runat="server" CssClass="input-large">
            </asp:DropDownList>
            
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Script" runat="Server">
    <script type="text/javascript">
       
        $('#ctl00_ContentPlaceHolder1_ddlPrimary').change(function () {
            var end = this.value;
            var index = 0;
            if (end == "Category_1.CategoryName") {
                index = 1;
            }
            else if (end == "dbo.Location.LocationName")
            {
                index = 2;
            }
            else if (end == "dbo.Projects.ProjectName") {
                index = 3;
            }
            else if (end == "dbo.Department.Department") {
                index = 4;
            }
            else if (end == "dbo.Supplier.Supplier") {
                index = 5;
            }
            else {
                index = 0;
            }
            if (index > 0)
            {
                $.ajax({
                    type: "POST",
                    data: "FilterID=" + index,
                    url: "/Handler/HandlerDropDownList.ashx",
                    dataType: "json",
                    success: function (data) {
                        var ddlCustomers = $("[id*=ddlPrimaryFilter]");

                        ddlCustomers.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(data, function () {
                            ddlCustomers.append($("<option></option>").val(this['Value']).html(this['Text']));
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(textStatus); alert(errorThrown);
                    }
                });
            }
                

           // }
           
        });
        
</script>
</asp:Content>

