﻿
$(function () {

    //*********codes for Open New window**********//
    //    var aspnetForm = $("#aspnetForm");
    //    if (aspnetForm.attr("target") == "_blank") {
    //        aspnetForm.attr("target", "");
    //        aspnetForm.attr("action", window.location.href);
    //    }
    //*********End codes for Open New window**********//

    //*********codes for edit button**********//
    $(".cms").fancybox({
        //                  maxWidth: 800,
        //                  maxHeight: 600,
        autoHeight: true,
        autoWidth: true,
        fitToView: true,
        width: '65%',
        height: '70%',
        //autoSize: false,
        closeClick: false,
        openEffect: 'elastic',
        closeEffect: 'elastic',
        afterClose: function () {
            parent.location.reload(true);
        }
    });

    //*********END of codes for edit button**********//


    //*********codes for static multilingual text edit**********//
    $('a[name="btnCMSInlineEdit"]').each(function (index) {
        var myButton = $(this);
        myButton.on("click", function (event) {
            event.preventDefault();

            var editview = myButton.parent().parent("div");
            var updateview = editview.next("div");

            editview.hide();
            updateview.show();
        });
    });

    $('a[name="btnCMSInlineCancel"]').each(function (index) {
        var myButton = $(this);
        myButton.on("click", function (event) {
            event.preventDefault();
            var updateview = myButton.parent("div");
            var editview = updateview.prev("div");
            var CMSloading = myButton.siblings("div[name='CMSloading']");
            updateview.hide();
            editview.show();
            CMSloading.hide();
        });
    });

    $('a[name="btnCMSInlineUpdate"]').each(function (index) {
        var myButton = $(this);
        myButton.on("click", function (event) {
            event.preventDefault();
            var updateview = myButton.parent("div")
            var editview = updateview.prev('div[class="cms-editview"]')
            var CMSloading = myButton.siblings("div[name='CMSloading']");
            var lang = myButton.siblings("input[name='lang']");
            var enVertionHTML = myButton.siblings("input[name='enVertionHTML']");
            var txtCMSInlineText = myButton.siblings("input[name='txtCMSInlineText']");
            var convertedVersionHTML = editview.find("div[class*='cms-text']");

            CMSloading.attr("style", "display:inline;");
            CMSloading.show();

            PostAjax(lang, enVertionHTML, txtCMSInlineText, convertedVersionHTML, updateview, editview, CMSloading)
        });
    });

    //*********END of codes for static multilingual text edit**********//


    //$.datepicker.setDefaults({
    //    //showAnim: "slideDown",
    //    dateFormat: "yy-mm-dd",
    //    minDate: 1,
    //    maxDate: "+1M +10D"
    //    //showOn: "both",
    //    //buttonImageOnly: true,
    //    //buttonImage: "calendar.gif",
    //    //buttonText: "Calendar"
    //});

    //$(".datepicker").datepicker();

    //$(".fromDatepicker").datepicker({
    //    dateFormat: "yy-mm-dd",
    //    minDate: 1,
    //    defaultDate: "+1D",
    //    changeMonth: true,
    //    numberOfMonths: 2,
    //    onClose: function (selectedDate) {
    //        $(".toDatepicker").datepicker("option", "minDate", selectedDate);
    //    }
    //});
    //$(".toDatepicker").datepicker({
    //    dateFormat: "yy-mm-dd",
    //    defaultDate: "+2D",
    //    changeMonth: true,
    //    numberOfMonths: 2,
    //    onClose: function (selectedDate) {
    //        $(".fromDatepicker").datepicker("option", "maxDate", selectedDate);
    //    }
    //});

});


//************** Ajax posting for static multilingual text ***************//
function PostAjax(lang, enVertionHTML, txtCMSInlineText, convertedVersionHTML, updateview, editview, CMSloading) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        data: "[{ lang: '" + lang.val() + "', enVertionHTML: '" + enVertionHTML.val() + "', newValue :'" + txtCMSInlineText.val() + "',oldValue:'" + convertedVersionHTML.html() + "'}]",
        url: "/Handler/HandlerCMSLanguageEdit.ashx",
        dataType: "json",
        success: function (result) {

            var json_obj = $.parseJSON(result);
            //alert(json_obj['newValue']);

            convertedVersionHTML.html(json_obj['newValue']);
            txtCMSInlineText.html(json_obj['newValue']);
            enVertionHTML.html(json_obj['newValue']);
            updateview.hide();
            editview.show();
            CMSloading.hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert(xhr.responseText);
            updateview.hide();
            editview.show();
            CMSloading.hide();
        }
    });
}
