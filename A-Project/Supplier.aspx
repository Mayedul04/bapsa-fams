﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Supplier.aspx.vb" Inherits="Admin_A_HTML_HTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Assets Supplier</h1>
    <div class="btn-toolbar">

        <a href="<%= domainName %>Suppliers" data-toggle="modal" class="btn btn-primary"><i class="icon-back"></i>Back</a>
        <div class="btn-group">
        </div>
    </div>

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Supplier"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>


    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <asp:Panel ID="pnlTitle" runat="server">
                <div class="left_div">
                    <label>
                        Supplier Name :</label>
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>


            </asp:Panel>

            <asp:Panel ID="pnlSmallImage" runat="server">
                <div class="left_div">
                    <label>
                        Contact Person :</label>
                </div>
                <div class="right_div">

                     <asp:TextBox ID="txtContactPerson" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtContactPerson" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlEmail" runat="server">
                <div class="left_div">
                    <label>
                        Email Address :</label>
                </div>
                <div class="right_div">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtEmail" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" 
                    runat="server" ErrorMessage="RegularExpressionValidator" ValidationGroup="form" 
                    ControlToValidate="txtEmail" SetFocusOnError="True" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">* Invalid Email</asp:RegularExpressionValidator>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage" runat="server">
                <div class="left_div">
                    <label>
                        Contact Number :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtPhone" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtPhone" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>

             <asp:Panel ID="Panel1" runat="server">
                <div class="left_div">
                    <label>
                        Mobile :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtMobile" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                   
                </div>
            </asp:Panel>
            <div>
                <div class="left_div">
                    <label>
                        Address :</label>
                </div>
                <div class="right_div">

                    <asp:TextBox ID="txtDetails"  Height="50" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                        runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                        ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                </div>
            </div>
                
            
            <div>
                <div class="left_div">
                    
                </div>
                <div class="right_div">
                    <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />
                    <label class="red">
                    </label>
                </div>
            </div>


        </div>

        <div class="btn-toolbar">
            <div class="right_div">
                <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
                </div>
            

            <div class="btn-group">
            </div>
        </div>




    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="sdsObject" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Supplier] WHERE [SupID] = @SupID"
        InsertCommand="INSERT INTO [Supplier] ([Supplier], [Address], [ContactName], [ContactNo], [Mobile], [Email],[Status]) VALUES (@Supplier, @Address, @ContactName, @ContactNo, @Mobile, @Email,@Status)"
        SelectCommand="SELECT * FROM [Supplier]"
        UpdateCommand="UPDATE [Supplier] SET [Supplier] = @Supplier, [Address] = @Address, [ContactName] = @ContactName, [ContactNo] = @ContactNo, [Mobile] = @Mobile, [Email] = @Email, [Status]=@Status WHERE [SupID] = @SupID">
        <DeleteParameters>
            <asp:Parameter Name="SupID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Supplier" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtContactPerson" Name="ContactName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Supplier" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtContactPerson" Name="ContactName" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" />
            <asp:RouteParameter Name="SupID" RouteKey="sid" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

