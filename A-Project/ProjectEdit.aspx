﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="ProjectEdit.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Suppliers</h1>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divDelete" visible="false" runat="server">
        <asp:Label ID="Label1" runat="server" Text="This Supplier have Assets. Please Delete the Depedencies first"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div>
        <div class="well">
        <div id="myTabContent" class="tab-content">
                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Project Name :</label>
                        </div>
                        <div class="right_div">
                            <asp:HiddenField ID="hdnID" runat="server" Value="" />
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Short Code :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtCode" runat="server" CssClass="input-large" Width="100%" MaxLength="20"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    
                    
                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Details :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine" CssClass="input-large"
                                Width="100%" Rows="4"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                                runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                                ErrorMessage="*" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="left_div">
                        </div>
                        <div class="right_div">
                            <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                        </div>
                    </div>


                    <div class="span12">
                        <div class="span4">
                            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                        </div>
                        <div class="span4">
                            <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                        </div>
                        <div class="span4">
                            <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                        </div>
                    </div>

                </div>
                <asp:SqlDataSource ID="sdsProject" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    DeleteCommand="DELETE FROM [Projects] WHERE [ProjID] = @ProjID"
                    InsertCommand="INSERT INTO [Projects] ([ProjectName], [ProjectCode], [Description], [Status]) VALUES (@ProjectName, @ProjectCode, @Description, @Status)"
                    SelectCommand="SELECT * FROM [Projects]"
                    UpdateCommand="UPDATE [Projects] SET [ProjectName] = @ProjectName, [ProjectCode] = @ProjectCode, [Description] = @Description, [Status] = @Status WHERE [ProjID] = @ProjID">
                    <DeleteParameters>
                        <asp:Parameter Name="ProjID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="ProjectName" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtCode" Name="ProjectCode" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="Description" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="ProjectName" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtCode" Name="ProjectCode" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="Description" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                        <asp:ControlParameter ControlID="hdnID" Name="ProjID" PropertyName="Value" Type="Int32" />
                    </UpdateParameters>

                </asp:SqlDataSource>


    </div>
        
        
    </div>
    <!-- Eof content -->
</asp:Content>
