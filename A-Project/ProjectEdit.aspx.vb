﻿
Imports System.Data.SqlClient

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page


    Public permissions As List(Of Integer) = New List(Of Integer)
    Protected Sub btnSubmit_ServerClick(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        Try
            If (hdnID.Value <> "") Then
                If (permissions.Contains(2)) Then
                    If sdsProject.Update() > 0 Then
                        divSuccess.Visible = True
                        ' GridView1.DataBind()
                        ClearField()
                        btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                    Else
                        divError.Visible = True
                    End If
                Else
                    lblErrMessage.Text = "You do not have Permission to Alter"
                    divError.Visible = True
                End If


            Else
                If (permissions.Contains(1)) Then
                    If sdsProject.Insert() > 0 Then
                        divSuccess.Visible = True
                        ' GridView1.DataBind()
                        ClearField()
                        btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                    Else
                        divError.Visible = True
                    End If
                Else
                    lblErrMessage.Text = "You do not have Permission to add Item"
                    divError.Visible = True
                End If


            End If
        Catch ex As Exception
            divError.Visible = True
            lblErrMessage.Text = ex.Message

        End Try

    End Sub
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 5 ' 6 is the Section ID for Config
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If

        End If
        getPermission()
    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  ProjID, ProjectName, ProjectCode, Description,Status FROM   Projects where ProjID=@ProjID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ProjID", Data.SqlDbType.Int)
        cmd.Parameters("projID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("ProjID").ToString()
            txtTitle.Text = reader("ProjectName").ToString()
            txtCode.Text = reader("ProjectCode").ToString()
            txtDetails.Text = reader("Description").ToString()
            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub
    Private Sub DeleteRecordByID(id As Integer)
        If sdsProject.Delete() > 0 Then
            divSuccess.Visible = True
            hdnID.Value = ""
            ' GridView1.DataBind()
        Else
            divError.Visible = True
        End If
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (hdnID.Value <> 0) Then
            DeleteRecordByID(hdnID.Value)
            ClearField()
        End If

    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        txtDetails.Text = ""
        txtCode.Text = ""

        chkStatus.Checked = True
        hdnID.Value = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub
End Class
