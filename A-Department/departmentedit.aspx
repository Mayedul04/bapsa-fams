﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="departmentedit.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Department</h1>

    <!-- content -->

    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divDelete" visible="false" runat="server">
        <asp:Label ID="Label1" runat="server" Text="This Department has Assets. Please Delete the Depedencies first"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div>

        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Department  :</label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Code :</label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtCode" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtCode" runat="server" ErrorMessage="* Requred" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                    </div>
                    <div class="right_div">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                    </div>
                </div>
                <div class="span12">
                    <div class="span4">
                        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                    </div>
                    <div class="span4">
                        <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                    </div>
                    <div class="span4">
                        <button runat="server" id="btnClear" validationgroup="form" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                    </div>
                </div>
            </div>
            <asp:SqlDataSource ID="sdsDept" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT * FROM [Department]"
                DeleteCommand="DELETE FROM [Department] WHERE [DeptID] = @DeptID"
                UpdateCommand="UPDATE [Department] SET [Department] = @Department, [Code] = @Code, [Status] = @Status WHERE [DeptID] = @DeptID">
                <DeleteParameters>
                    <asp:ControlParameter ControlID="hdnID" Name="DeptID" PropertyName="Value" Type="Int32" />
                </DeleteParameters>
                <%--<InsertParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="Department" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" DefaultValue="True" Name="Status" PropertyName="Checked" Type="Boolean" />
                </InsertParameters>--%>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="Department" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnID" Name="DeptID" PropertyName="Value" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>


             <asp:SqlDataSource ID="sdsUser" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                InsertCommand="INSERT INTO [Employees] ([EmployeeName], [DepartmentID], [EmployeeCode], [Designation], [Status]) VALUES (@EmployeeName,@DepartmentID, @EmployeeCode, @Designation, @Status)"
                UpdateCommand="UPDATE [Employees] SET [DepartmentID] = @DepartmentID WHERE [DepartmentID] = @DeptID">
                <InsertParameters>
                    <asp:Parameter DefaultValue="Not Applicable" Name="EmployeeName" Type="String" />
                    <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                    <asp:Parameter DefaultValue="" Name="DepartmentID" />
                    <asp:Parameter DefaultValue="NA" Name="EmployeeCode" />
                    <asp:Parameter DefaultValue="0" Name="Designation" />
                </InsertParameters>
                <UpdateParameters>
                    
                    <asp:ControlParameter ControlID="hdnID" Name="DeptID" PropertyName="Value" Type="Int32" />
                    <asp:Parameter DefaultValue="1" Name="DepartmentID" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
        

    </div>
    <!-- Eof content -->
</asp:Content>
