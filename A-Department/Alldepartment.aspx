﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Alldepartment.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<h1 class="page-title">Department</h1>--%>
    <div class="span5" style="margin-left:0px;margin-top:5px;">
    <div class="btn-group">
        <%--<script type="text/javascript" language="javascript">
                                    function OnContactSelected(source, eventArgs) {
                                        var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                                        document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();
                                        
                                    } 
                                </script>
        <label>Search by Department Name</label>
        <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                                    onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
        <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                                    MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetDepartmentList"
                                    ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                                    CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                                    OnClientItemSelected="OnContactSelected">
                                </cc1:AutoCompleteExtender>--%>
        
    </div>

    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divDelete" visible="false" runat="server">
        <asp:Label ID="Label1" runat="server" Text="This Department has Assets. Please Delete the Depedencies first"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    
         <div class="span12 form-top-strip">
           <p>Department Form</p> 
        </div>
        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        Search Department
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlDept" runat="server" AutoPostBack="true" AppendDataBoundItems="true" 
            DataSourceID="sdsDepart" DataTextField="Department" Width="100%" DataValueField="DeptID">
                <asp:ListItem Value="">Select Department</asp:ListItem>

            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsDepart" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                SelectCommand="SELECT [DeptID], [Department] FROM [Department] where DeptID>1 "></asp:SqlDataSource>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Department  :</label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Code :</label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtCode" runat="server" CssClass="input-xlarge" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtCode" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                    </div>
                    <div class="right_div">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                    </div>
                </div>
                <div class="span12">
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                    </div>
                    <div class="span4" style="width:33%" >
                        <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                    </div>
                </div>
            </div>
              <asp:SqlDataSource ID="sdsDept" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT * FROM [Department] where DeptID>1"
                DeleteCommand="DELETE FROM [Department] WHERE [DeptID] = @DeptID"
                UpdateCommand="UPDATE [Department] SET [Department] = @Department, [Code] = @Code, [Status] = @Status WHERE [DeptID] = @DeptID">
                <DeleteParameters>
                    <asp:ControlParameter ControlID="hdnID" Name="DeptID" PropertyName="Value" Type="Int32" />
                </DeleteParameters>
                <%--<InsertParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="Department" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" DefaultValue="True" Name="Status" PropertyName="Checked" Type="Boolean" />
                </InsertParameters>--%>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="Department" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="Code" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnID" Name="DeptID" PropertyName="Value" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

            <asp:SqlDataSource ID="sdsUser" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                InsertCommand="INSERT INTO [Employees] ([EmployeeName], [DepartmentID], [EmployeeCode], [Designation], [Status]) VALUES (@EmployeeName,@DepartmentID, @EmployeeCode, @Designation, @Status)"
                UpdateCommand="UPDATE [Employees] SET [DepartmentID] = @DepartmentID WHERE [DepartmentID] = @DeptID">
                <InsertParameters>
                    <asp:Parameter DefaultValue="Not Applicable" Name="EmployeeName" Type="String" />
                    <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                    <asp:Parameter DefaultValue="" Name="DepartmentID" />
                    <asp:Parameter DefaultValue="NA" Name="EmployeeCode" />
                    <asp:Parameter DefaultValue="0" Name="Designation" />
                </InsertParameters>
                <UpdateParameters>
                    
                    <asp:ControlParameter ControlID="hdnID" Name="DeptID" PropertyName="Value" Type="Int32" />
                    <asp:Parameter DefaultValue="1" Name="DepartmentID" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
        

    
    </div>
    <div class="span5">
        <div class="span12 form-top-strip" style="margin-top:3px;width:100%;">
           <p>Department List</p> 
        </div>
    <div class="well" style="height:180px; overflow-y:scroll; padding-top:0px; display:block;">
        
            <table class="table" style="overflow: auto">
                <thead>
                    <tr>
                        
                        <th>Department
                        </th>
                        <th>Code
                        </th>
                        <th>Status
                        </th>
                        <th style="width: 40px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" style="height:120px; overflow:auto;" DataKeyNames="DeptID" DataSourceID="sdsDept">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <%--<td>
                                    <asp:Label ID="HTMLID" runat="server" Text='<%# Eval("DeptID") %>' />
                                </td>--%>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Department") %>' />
                                </td>

                                <td>
                                    <asp:Label ID="Code1" runat="server" Text='<%# Eval("Code") %>' />
                                </td>

                                <td>
                                    <asp:CheckBox ID="Stat" runat="server" Checked='<%# Eval("Status") %>' />

                                </td>
                                <td>
                                    <asp:LinkButton ID="cmdEdit" CssClass="btn btn-success" CommandName="Edit" runat="server"
                                        CommandArgument='<%# Eval("DeptID") %>' Text="Edit" />
                                </td>
                            </tr>
                        </ItemTemplate>

                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>

          

        </div>
        </div>
    <!-- Eof content -->
</asp:Content>
