﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page
    Public permissions As List(Of Integer) = New List(Of Integer)
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (divDelete.Visible = True) Then
                divDelete.Visible = False
            End If
        End If
        getPermission()
    End Sub
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 5 ' 5 is the Section ID for Config
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Protected Sub btnSubmit_ServerClick(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If (hdnID.Value <> "") Then
            If (permissions.Contains(2)) Then

                If sdsDept.Update() > 0 Then
                    divSuccess.Visible = True
                    '  GridView1.DataBind()
                    ClearField()
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Alter"
                divError.Visible = True
            End If
        Else
            If (permissions.Contains(1)) Then

                Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
                Dim insertstring = "INSERT INTO [Department] ([Department], [Code], [Status]) VALUES (@Department, @Code, @Status); SELECT SCOPE_IDENTITY();"

                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand(insertstring, conn)
                cmd.Parameters.Add("@Department", SqlDbType.NVarChar, 80).Value = txtTitle.Text
                cmd.Parameters.Add("@Code", SqlDbType.NVarChar, 20).Value = txtCode.Text
                cmd.Parameters.Add("@Status", SqlDbType.Bit).Value = chkStatus.Checked

                Dim last = cmd.ExecuteScalar()
                Dim lastentered As Int32 = Int32.Parse(last.ToString())
                sdsUser.InsertParameters.Item(2).DefaultValue = lastentered
                If sdsUser.Insert() > 0 Then
                    divSuccess.Visible = True
                    'GridView1.DataBind()
                    ClearField()
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Add Item"
            divError.Visible = True
        End If
        End If
    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT   DeptID, Department, Code,  Status  FROM   Department where DeptID=@DeptID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("DeptID", Data.SqlDbType.Int)
        cmd.Parameters("DeptID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("DeptID").ToString()
            txtTitle.Text = reader("Department").ToString()
            txtCode.Text = reader("Code").ToString()

            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub

    Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
        If (e.CommandName = "Edit") Then
            Dim categoryID As Integer = Integer.Parse(e.CommandArgument)
            LoadContent(categoryID)

        End If
    End Sub

    Private Sub DeleteRecordByID(id As Integer)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim usercount As Integer = 0
        Dim assetcount As Integer = 0
        Dim selectString = "SELECT  Count(UID)  FROM   Employees where DepartmentID=@DepartmentID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("DepartmentID", Data.SqlDbType.Int)
        cmd.Parameters("DepartmentID").Value = id
        usercount = cmd.ExecuteScalar()

        Dim selectString1 = "SELECT  Count(ItemID)  FROM   Assets where Department=@Department "
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmd1.Parameters.Add("Department", Data.SqlDbType.Int)
        cmd1.Parameters("Department").Value = id
        assetcount = cmd1.ExecuteScalar()

        If (assetcount > 0) Then
            divDelete.Visible = True
        Else
            If (usercount > 0) Then
                sdsUser.Update()
            End If
            If sdsDept.Delete() > 0 Then
                divSuccess.Visible = True
                hdnID.Value = ""
                ' GridView1.DataBind()
            Else
                divError.Visible = True
            End If

        End If
        conn.Close()
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (permissions.Contains(3)) Then

            If (hdnID.Value <> 0) Then
                DeleteRecordByID(hdnID.Value)
                ClearField()
            Else
                lblErrMessage.Text = "Please select a item to Delete"
                divError.Visible = True
            End If
        Else
            lblErrMessage.Text = "You do not have Permission to Delete"
        divError.Visible = True
        End If
    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        chkStatus.Checked = True
        txtCode.Text = ""
        hdnID.Value = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub

    'Protected Sub txtAutoSearch_TextChanged(sender As Object, e As EventArgs) Handles txtAutoSearch.TextChanged
    '    If (txtAutoSearch.Text.Contains(":")) Then
    '        Dim split = txtAutoSearch.Text.Split(":")
    '        Dim name1 As String = split(0)
    '        Dim id As Integer = split(1)
    '        hdnID.Value = id
    '        LoadContent(hdnID.Value)
    '    Else
    '        lblErrMessage.Text = "Please select a Valid item from Suggested List"
    '        divError.Visible = True
    '    End If

    'End Sub
    Protected Sub ddlDept_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDept.SelectedIndexChanged
        If (ddlDept.SelectedIndex <> 0) Then
            hdnID.Value = ddlDept.SelectedValue
            LoadContent(hdnID.Value)
        Else
            ClearField()
        End If

    End Sub
End Class
