﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Designations.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<h1 class="page-title">Designations</h1>--%>
    <div class="span5" style="margin-left:0px; margin-top:5px;">
    <div class="btn-group">
        <%--<script type="text/javascript" language="javascript">
                                    function OnContactSelected(source, eventArgs) {
                                        var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                                        document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();
                                        
                                    } 
                                </script>
        <label>Search by Designation</label>
        <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                                    onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
        <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                                    MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetDesignationList"
                                    ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                                    CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                                    OnClientItemSelected="OnContactSelected">
                                </cc1:AutoCompleteExtender>--%>
        
    </div>
        
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divDelete" visible="false" runat="server">
        <asp:Label ID="Label1" runat="server" Text="This Designation have been assigned to Employees. Please Delete the Depedencies first"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="span12 form-top-strip">
           <p>Designations Form</p> 
        </div>
        <div class="well">
            <div id="myTabContent" class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        Search Designation
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlDesig" runat="server" AutoPostBack="true" AppendDataBoundItems="true" 
            DataSourceID="sdsDesignation" DataTextField="Designation" DataValueField="DesigID" Width="100%">
                <asp:ListItem Value="">Select Designation</asp:ListItem>

            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsDesignation" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                SelectCommand="SELECT [DesigID], [Designation] FROM [Designations]  "></asp:SqlDataSource>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Designation  :</label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large"  Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Department  :</label>
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlDept" runat="server" DataSourceID="sdsDept" CssClass="input-large" Width="100%" AppendDataBoundItems="true"
                            DataTextField="Department" DataValueField="DeptID">
                            <asp:ListItem Value="">Please Select Department</asp:ListItem>
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsDept" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT DISTINCT [DeptID], [Department] FROM [Department] WHERE ([Status] = @Status and DeptID>1) ">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="ddlDept" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                <div class="left_div">
                </div>
                <div class="right_div">
                    <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                </div>
            </div>
                <div class="span12">
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnClear"  class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                    </div>
                </div>

           
            </div>
             <asp:SqlDataSource ID="sdsDesig" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT Designations.DesigID, Designations.Designation, Designations.DeptID, Designations.Status, Department.Department FROM Designations INNER JOIN Department ON Designations.DeptID = Department.DeptID"
            DeleteCommand="DELETE FROM [Designations] WHERE [DesigID] = @DesigID" InsertCommand="INSERT INTO [Designations] ([Designation], [DeptID], [Status]) VALUES (@Designation,@DeptID, @Status)"
            UpdateCommand="UPDATE [Designations] SET [Designation] = @Designation, [DeptID]=@DeptID, [Status] = @Status WHERE [DesigID] = @DesigID">
            <DeleteParameters>
                <asp:ControlParameter ControlID="hdnID" Name="DesigID" PropertyName="Value" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="chkStatus" DefaultValue="True" Name="Status" PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="txtTitle" Name="Designation" PropertyName="Text" DefaultValue="" />
                <asp:ControlParameter ControlID="ddlDept" Name="DeptID" PropertyName="SelectedValue" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter Name="Status" Type="Boolean" ControlID="chkStatus" PropertyName="Checked" />
                <asp:ControlParameter ControlID="hdnID" Name="DesigID" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="txtTitle" Name="Designation" PropertyName="Text" />
                <asp:ControlParameter ControlID="ddlDept" Name="DeptID" PropertyName="SelectedValue" />
            </UpdateParameters>
        </asp:SqlDataSource>
        </div>
        
    </div>
       <!-- Eof content -->
    <div class="span7">
        <div class="span12 form-top-strip" style="margin-top:3px;width:100%;">
           <p>Designations</p> 
        </div>
        <div class="well" style="height:174px; overflow-y:scroll; padding-top:5px; display:block;">
            <table class="table">
                <thead>
                    <tr>
                        <th>Designation
                        </th>
                        <th>Department
                        </th>

                        <th>Status
                        </th>
                        <th style="width: 40px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="DesigID" DataSourceID="sdsDesig">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="Desig" runat="server" Text='<%# Eval("Designation") %>' />
                                </td>
                                <td>

                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Department") %>' />
                                </td>


                                <td>
                                    <asp:CheckBox ID="Stat" runat="server" Checked='<%# Eval("Status") %>' />

                                </td>
                                <td>
                                    <asp:LinkButton ID="cmdEdit" CssClass="btn btn-success" CommandName="Edit" runat="server"
                                        CommandArgument='<%# Eval("DesigID") %>' Text="Edit" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
    </div>
    
</asp:Content>
