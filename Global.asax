﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Globalization" %>
<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        RegisterRoute(Routing.RouteTable.Routes)
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub

    Sub RegisterRoute(ByVal routes As Routing.RouteCollection)
        routes.MapPageRoute("Default", "Default", "~/Default.aspx")
        routes.MapPageRoute("Default1", "Home", "~/A-Home/Home.aspx")
        routes.MapPageRoute("Setup", "Company", "~/A-Home/Company.aspx")

        routes.MapPageRoute("Login", "Login", "~/A-Login/Login.aspx")
        routes.MapPageRoute("Logout", "Signout", "~/A-Login/signout.aspx")
        routes.MapPageRoute("User3", "users", "~/A-Login/AllUser.aspx")
        routes.MapPageRoute("Login1", "ForgetPassword", "~/A-Login/ForgetPassword.aspx")
        routes.MapPageRoute("Login2", "permissions/{uid}", "~/A-Login/Permissions.aspx")

        routes.MapPageRoute("Assets", "AllAssets", "~/A-Asset/AllAsset.aspx")
        routes.MapPageRoute("Assets1", "NewAssets", "~/A-Asset/FixedAssets.aspx")
        routes.MapPageRoute("Assets2", "StartingAsset", "~/A-Asset/StartingItems.aspx")
        routes.MapPageRoute("Assets3", "TransferAsset", "~/A-Asset/Transfer.aspx")
        routes.MapPageRoute("Assets6", "TransferAssetUser", "~/A-Asset/Transfer-User.aspx")
        routes.MapPageRoute("Assets4", "RevaluateAsset", "~/A-Asset/Revaluation.aspx")
        routes.MapPageRoute("Assets5", "DisposeAsset", "~/A-Asset/Disposal.aspx")

        routes.MapPageRoute("Category", "AllCategory", "~/A-Category/AllCategory.aspx")
        routes.MapPageRoute("Category1", "Category/{cid}", "~/A-Category/Category.aspx")
        routes.MapPageRoute("Category2", "CategoryEdit", "~/A-Category/CategoryEdit.aspx")

        routes.MapPageRoute("Department", "Departments", "~/A-Department/Alldepartment.aspx")
        routes.MapPageRoute("Department1", "DepartmentEdit", "~/A-Department/departmentedit.aspx")
        routes.MapPageRoute("Designation", "Designations", "~/A-Designation/Designations.aspx")
        routes.MapPageRoute("Designation1", "DesignationEdit", "~/A-Designation/DesignationEdit.aspx")

        routes.MapPageRoute("Locations", "Locations", "~/A-Location/AllLocation.aspx")
        routes.MapPageRoute("Locations1", "LocationEdit", "~/A-Location/LocationEdit.aspx")
        routes.MapPageRoute("Location", "Location/{lid}", "~/A-Location/Location.aspx")

        routes.MapPageRoute("Suppliers", "Suppliers", "~/A-Supplier/AllSupplier.aspx")
        routes.MapPageRoute("Suppliers1", "SupplierEdit", "~/A-Supplier/SupplierEdit.aspx")
        routes.MapPageRoute("Suppliers2", "Supplier/{sid}", "~/A-Supplier/Supplier.aspx")

        routes.MapPageRoute("Projects", "Projects", "~/A-Project/AllProject.aspx")
        routes.MapPageRoute("Projects1", "ProjectEdit", "~/A-Project/ProjectEdit.aspx")

        routes.MapPageRoute("Reports", "CategorySummary", "~/A-Reports/GroupSummary.aspx")
        routes.MapPageRoute("Reports1", "CategorySummary/{gid}", "~/A-Reports/GroupSummary.aspx")
        routes.MapPageRoute("Report", "AssetReport", "~/A-Reports/PrimaryReport.aspx")
        routes.MapPageRoute("Report3", "InteractiveReport", "~/A-Reports/InteractiveReport.aspx")
        routes.MapPageRoute("Report12", "InteractiveReport/{method}", "~/A-Reports/InteractiveReport.aspx")
        routes.MapPageRoute("Report1", "AssetReport/{gid}", "~/A-Reports/PrimaryReport.aspx")
        routes.MapPageRoute("Report2", "CategoryReport", "~/A-Reports/MasterReport.aspx")
        routes.MapPageRoute("Report4", "DepartmentReport", "~/A-Reports/DepartmentReport.aspx")
        routes.MapPageRoute("Report6", "BranchReport", "~/A-Reports/BranchReport.aspx")
        routes.MapPageRoute("Report8", "SupplierReport", "~/A-Reports/SupplierReport.aspx")
        routes.MapPageRoute("Report9", "ProjectReport", "~/A-Reports/ProjectReport.aspx")
        routes.MapPageRoute("Report10", "LocationTransferReport", "~/A-Reports/LocationTransferReport.aspx")
        routes.MapPageRoute("Report11", "UserTransferReport", "~/A-Reports/UserTransferReport.aspx")

        routes.MapPageRoute("User", "AllEmployee", "~/A-Users/AllEmployee.aspx")
        routes.MapPageRoute("User1", "EmployeeEdit", "~/A-Users/EmployeeEdit.aspx")
        routes.MapPageRoute("User2", "Employee/{uid}", "~/A-Users/Employee.aspx")

        routes.MapPageRoute("DBBackup", "DbBackup", "~/A-DBBackup/DBBackup.aspx")



    End Sub

    Protected Sub Application_BeginRequest(sender As [Object], e As EventArgs)

        Dim domainName As String
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
        Dim newCulture As CultureInfo = DirectCast(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat.ShortDatePattern = "MM/dd/yyyy"
        newCulture.DateTimeFormat.DateSeparator = "/"
        Threading.Thread.CurrentThread.CurrentCulture = newCulture



        Dim url As String = HttpContext.Current.Request.Url.ToString()
        Dim isGet As Boolean = HttpContext.Current.Request.RequestType.ToLowerInvariant().Contains("get")

        ' If we're not a request for the root, and end with a slash, strip it off
        If HttpContext.Current.Request.Url.AbsolutePath <> "/" AndAlso HttpContext.Current.Request.Url.AbsolutePath.EndsWith("/") And HttpContext.Current.Request.Url.AbsolutePath.ToLower().Contains("/admin/") = False Then
            PermanentRedirect(url.Substring(0, url.Length - 1) + HttpContext.Current.Request.Url.Query)

            'If url contains Upper case letters and only get method then convert into lowercase without query string, image and file.             
        ElseIf Regex.IsMatch(url, "[A-Z]") And HttpContext.Current.Request.Url.AbsolutePath.Contains(".") = False And isGet = True Then
            PermanentRedirect(url.ToLower() + HttpContext.Current.Request.Url.Query)
        End If


        'If url = "http://www.hoihgm.com/press-release/4/the-honourable-order-of-international-hotel-general-managers-appoints-ceo" Then
        '    PermanentRedirect("http://hotelgms.com/")
        'End If
    End Sub
    Private Sub PermanentRedirect(ByVal url As String)
        Response.Clear()
        Response.Status = "301 Moved Permanently"
        Response.AddHeader("Location", url)
        Response.End()
    End Sub




</script>