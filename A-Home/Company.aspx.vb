﻿
Imports System.Data.SqlClient

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page

    Public permissions As List(Of Integer) = New List(Of Integer)
    Protected Sub btnSubmit_ServerClick(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtTitle.Text <> "", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If
        If (hdnID.Value <> "") Then
            If (permissions.Contains(2)) Then

                If sdsComp.Update() > 0 Then
                    divSuccess.Visible = True
                    ' GridView1.DataBind()
                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to Alter"
                divError.Visible = True
            End If
        Else
            If (permissions.Contains(1)) Then

                If sdsComp.Insert() > 0 Then
                    divSuccess.Visible = True

                    btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
                Else
                    divError.Visible = True
                End If
            Else
                lblErrMessage.Text = "You do not have Permission to add Item"
                divError.Visible = True
            End If
        End If
    End Sub
    Private Sub getPermission()

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select Addition, Edition, Deletion from UserPermissions where UserID=@userID and SectID=@SectID"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UserID", Data.SqlDbType.Int, 32)
        cmd.Parameters("UserID").Value = 1
        cmd.Parameters.Add("SectID", Data.SqlDbType.Int, 32).Value = 5 ' 6 is the Section ID for Config
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        While reader.Read
            If (reader("Addition").ToString() = True) Then
                permissions.Add(1) '1 for Addition
            End If
            If (reader("Edition").ToString() = True) Then
                permissions.Add(2) '1 for Edition
            End If
            If (reader("Deletion").ToString() = True) Then
                permissions.Add(3) '1 for Deletion
            End If

        End While
        conn.Close()

    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (divDelete.Visible = True) Then
                divDelete.Visible = False
            End If
        Else
            LoadContent()
        End If
        getPermission()

    End Sub
    Private Sub LoadContent()
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  CompanyID, CompanyName, Email, Address1, Address2, Phone, CompanyLogo , FiancialYearStart ,Status FROM   Company where CompanyID=@CompanyID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("CompanyID", Data.SqlDbType.Int)
        cmd.Parameters("CompanyID").Value = 1

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("CompanyID").ToString()
            txtTitle.Text = reader("CompanyName").ToString()
            txtEmail.Text = reader("Email").ToString()
            hdnBigImage.Value = reader("CompanyLogo").ToString()
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If
            txtAddr1.Text = reader("Address1").ToString()
            txtAddr2.Text = reader("Address2").ToString()

            txtPhone.Text = reader("Phone").ToString().Trim()
            txtYearStart.Text = reader("FiancialYearStart").ToString()
            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub



    'Private Function ClearField()
    '    txtTitle.Text = ""
    '    txtEmail.Text = ""
    '    txtPhone.Text = ""
    '    txtAddr1.Text = ""
    '    txtAddr2.Text = ""
    '    hdnBigImage.Value = ""
    '    chkStatus.Checked = True
    '    hdnID.Value = ""
    '    imgBigImage.ImageUrl = ""
    '    imgBigImage.Visible = False
    'End Function

End Class
