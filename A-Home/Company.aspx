﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="Company.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="span6" style="margin-left: 0px; margin-top: 5px;">

        <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divDelete" visible="false" runat="server">
            <asp:Label ID="Label1" runat="server" Text="This Supplier have Assets. Please Delete the Depedencies first"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="span12 form-top-strip">
            <p>Company Setup</p>
        </div>
        <div class="well">
            <div id="myTabContent" class="tab-content">

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Company Name :</label>
                    </div>
                    <div class="right_div">
                        <asp:HiddenField ID="hdnID" runat="server" Value="" />
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" Width="100%" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Logo (250x250):</label>
                    </div>
                    <div class="right_div">
                        <asp:Image ID="imgBigImage" runat="server" Width="100%" Visible="false" />
                        <asp:HiddenField ID="hdnBigImage" runat="server" />
                        

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        
                    </div>
                    <div class="right_div">
                        <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                        </div>
                    </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Email:
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                            runat="server" ErrorMessage="*" ValidationGroup="form"
                            ControlToValidate="txtEmail" SetFocusOnError="True"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtEmail" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Contact Number:
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Phone" ControlToValidate="txtPhone" ValidationExpression="^(?:\+?(?:88|01)?)(?:\d{11}|\d{13})$"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtPhone" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtPhone" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Financial Year Start:</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtYearStart" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                        <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtYearStart" runat="server" Format="">
                                </cc1:CalendarExtender> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                            ControlToValidate="txtYearStart" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="span12">
                    <div class="left_div">
                        <label>
                            Address Line1:</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtAddr1" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                            runat="server" ControlToValidate="txtAddr1" ValidationExpression="^[\s\S\w\W\d\D]{0,100}$"
                            ErrorMessage="*" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Address Line2:</label>
                    </div>
                    <div class="right_div">

                        <asp:TextBox ID="txtAddr2" runat="server" CssClass="input-large" Width="100%"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="Dynamic"
                            runat="server" ControlToValidate="txtAddr2" ValidationExpression="^[\s\S\w\W\d\D]{0,100}$"
                            ErrorMessage="*" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />
                    </div>
                    <div class="right_div">
                        
                        <div class="span6">
                            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                        </div>
                    </div>
                </div>




            </div>
            <asp:SqlDataSource ID="sdsComp" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [Company] WHERE [CompanyID] = @CompanyID"
                InsertCommand="INSERT INTO [Company] ([CompanyName], [CompanyLogo], [CompanyBackGroundColor], [FiancialYearStart], [Address1], [Address2], [Phone], [Email], [Status]) VALUES (@CompanyName, @CompanyLogo, @CompanyBackGroundColor, @FiancialYearStart, @Address1, @Address2, @Phone, @Email, @Status)"
                SelectCommand="SELECT * FROM [Company]"
                UpdateCommand="UPDATE [Company] SET [CompanyName] = @CompanyName, [CompanyLogo] = @CompanyLogo, [CompanyBackGroundColor] = @CompanyBackGroundColor, [FiancialYearStart] = @FiancialYearStart, [Address1] = @Address1, [Address2] = @Address2, [Phone] = @Phone, [Email] = @Email, [Status] = @Status WHERE [CompanyID] = @CompanyID">
                <DeleteParameters>
                    <asp:Parameter Name="CompanyID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="CompanyName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="CompanyLogo" PropertyName="Value" Type="String" />
                    <asp:Parameter Name="CompanyBackGroundColor" Type="String" DefaultValue="" />
                    <asp:ControlParameter ControlID="txtYearStart" DbType="Date" Name="FiancialYearStart" PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtAddr1" Name="Address1" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtAddr2" Name="Address2" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtPhone" Name="Phone" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="CompanyName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="CompanyLogo" PropertyName="Value" Type="String" />
                    <asp:Parameter Name="CompanyBackGroundColor" Type="String" DefaultValue="" />
                    <asp:ControlParameter ControlID="txtYearStart" DbType="Date" Name="FiancialYearStart" PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtAddr1" Name="Address1" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtAddr2" Name="Address2" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtPhone" Name="Phone" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnID" Name="CompanyID" PropertyName="Value" Type="Int32" />
                </UpdateParameters>

            </asp:SqlDataSource>


        </div>

    </div>


</asp:Content>
