﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllCharter.aspx.vb" Inherits="Admin_A_Contact_AllCharter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1 class="page-title">Charters Requests</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnDownload" visible="false"  class="btn btn-primary"><i class="icon-save"></i> Download </button>
        <div class="btn-group">
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Charters"></asp:Label></h2>
        <div>

           <div class="well">
           
          <table class="table" >
                                  
                                        <thead  >
                                           <tr>
                                           <th >
                                                Contact Info</th>
                                            <th >
                                                From</th>
                                                                                       
                                            <th >
                                                To</th>

                                            <th >
                                                No Of Passenger</th>
  
                                            <th >
                                                Date </th>

                                            <th style="width: 20px;">
                                               </th>
                                        </tr>
                                            
                                        </thead>
                                        <tbody>
                     <asp:ListView ID="GridView1" runat="server" DataKeyNames="CharterID" 
                    DataSourceID="sdsContact">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">
                           
                            <td>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Name") %>' /><br />
                                <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone") %>' /><br />
                                <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Email") %>' /><br />
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Country") %>' />

                            </td>
                              <td>
                                <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("CityName") %>' />
                             (<asp:Label ID="Label5" runat="server" Text='<%# Eval("ShortCodeName") %>' />)
                             - <asp:Label ID="Label4" runat="server" Text='<%# Eval("AirPortName") %>' />

                            </td>

                            <td>
                                 <asp:Label ID="Label3" runat="server" Text='<%# Eval("CityNameTo") %>' />
                             (<asp:Label ID="Label6" runat="server" Text='<%# Eval("ShortCodeNameTo") %>' />)
                             - <asp:Label ID="Label7" runat="server" Text='<%# Eval("AirPortNameTo") %>' />
                            </td>
                          
                            <td>
                                 <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoofPassenger") %>' />
                            </td>
                            <td>
                                <asp:Label ID="LastUpdatedLabel" runat="server" 
                                    Text='<%# Convert.ToDateTime(Eval("DateofTravel")).Tostring("MMM dd, yyyy") %>' />
                            </td>
                           

                             <td>

                                <a href='<%# "#" & Eval("CharterID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("CharterID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
                         
                             </td>


                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                       
                                        <tr ID="itemPlaceholder"  runat="server">
                                       
                                        </tr>

                                       <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1"  PageSize="25"  runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False"  ShowPreviousPageButton="True"  />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                                
                                                
                                           
                                            </Fields>
                                        </asp:DataPager>
                                        </div>
                               
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody> 
                </table> 
                                
          </div> 
    <!-- Eof content -->
         
           <asp:SqlDataSource ID="sdscontact" runat="server" 
                ConnectionString="<%$ ConnectionStrings:connectionString %>" 
                SelectCommand="SELECT Charter.*,Route.[CityName],Route.[AirPortName],Route.[ShortCodeName],Route_1.[CityName] as CityNameTo,Route_1.[AirPortName] as AirPortNameTo,Route_1.[ShortCodeName] as ShortCodeNameTo FROM [Charter] inner join Route on Charter.[From]=Route.[RouteID] inner join Route as Route_1 on Charter.[To]=Route_1.[RouteID]"
                
                  DeleteCommand="DELETE FROM Charter WHERE (CharterID = @CharterID)">
                <DeleteParameters>
                    <asp:Parameter Name="CharterID" />
                </DeleteParameters>
                
                </asp:SqlDataSource>
       </div>
    <!-- Eof content -->

</asp:Content>

