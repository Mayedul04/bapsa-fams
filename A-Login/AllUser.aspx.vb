﻿
Imports System.Data.SqlClient

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If

        End If
        sdsHTML.SelectCommand = If(Request.Cookies("userName").Value.ToLower = "admin", "SELECT AdminPanelLogin.* FROM [AdminPanelLogin] order by UN_1", "SELECT AdminPanelLogin.* FROM [AdminPanelLogin] where AdminPanelLogin.UN_1='" & Request.Cookies("userName").Value & "' order by UN_1")
        GridView1.DataBind()
    End Sub
    Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
        If (e.CommandName = "Edit") Then

            Dim categoryID As Integer = Integer.Parse(e.CommandArgument)
            LoadContent(categoryID)

        End If
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnLastUpdated.Value = DateTime.Now

        txtEmail.Text = txtEmail.Text.Trim()
        txtUserID.Text = txtUserID.Text.Trim()
        txtUserName.Text = txtUserName.Text.Trim()




        Try
            If (hdnID.Value <> "") Then
                If (sdsAdminPanelLogin.Update() > 0) Then
                    lblSuccMessage.Text = "User has been Updated"
                    divSuccess.Visible = True
                    GridView1.DataBind()
                Else

                    divError.Visible = True
                End If

            Else
                If IsIDExists(txtUserID.Text) Then
                    divError.Visible = True
                    lblErrMessage.Text = "The ID is already exists. Please try another ID."

                    Exit Sub
                End If
                If IsEmailExists(txtEmail.Text) Then
                    lblErrMessage.Text = "The email is already exists. Please try another email."
                    divError.Visible = True
                    Exit Sub
                End If
                If (Request.Cookies("userName").Value = "admin") Then
                    If (sdsAdminPanelLogin.Insert() > 0) Then
                        Dim lastid As Integer = GetLastEntry()
                        Dim sections As List(Of Integer) = New List(Of Integer)
                        sections.Add(1)
                        sections.Add(2)
                        sections.Add(3)
                        sections.Add(4)
                        sections.Add(5)
                        sections.Add(6)
                        If (lastid > 0) Then
                            For Each item In sections
                                sdsPermissions.InsertParameters.Item(0).DefaultValue = lastid
                                sdsPermissions.InsertParameters.Item(1).DefaultValue = item
                                sdsPermissions.Insert()
                            Next
                        End If
                        lblSuccMessage.Text = "New user has been created"
                        divSuccess.Visible = True
                        GridView1.DataBind()
                    Else

                        divError.Visible = True
                    End If
                Else
                    lblErrMessage.Text = "You Don't have permission to Create User"
                    divError.Visible = True
                End If



            End If


        Catch ex As Data.SqlClient.SqlException
            lblErrMessage.Text = "Error: " & ex.Message
            divError.Visible = True
        End Try


    End Sub

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  [PS_1], [UN_1], UserID, [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]  FROM   AdminPanelLogin where UserID=@UID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("UID", Data.SqlDbType.Int)
        cmd.Parameters("UID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("UserID").ToString()
            txtUserID.Text = reader("UN_1").ToString()
            txtUserName.Text = reader("Title").ToString()
            txtPassword.Text = reader("PS_1").ToString()
            txtRetypePassword.Text = reader("PS_1").ToString()
            txtEmail.Text = reader("Email").ToString()
            hdnRole.Value = reader("Role").ToString()
            hdnLastUpdated.Value = reader("CreationDate").ToString()

        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub
    Private Function GetLastEntry() As Integer
        Dim retval As Integer = 0
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT top 1 UserID from AdminPanelLogin order by UserID Desc"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            retval = reader("UserID").ToString()
        End If
        conn.Close()
        Return retVal
    End Function
    Private Function IsEmailExists(ByVal email As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where email=@email "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("email", Data.SqlDbType.VarChar, 100).Value = email
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function
    Private Function IsIDExists(ByVal UN_1 As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where UN_1=@UN_1 "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UN_1", Data.SqlDbType.VarChar, 100).Value = UN_1
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function
    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (hdnID.Value <> 0) Then
            If sdsAdminPanelLogin.Delete() > 0 Then
                divSuccess.Visible = True
                hdnID.Value = ""
                GridView1.DataBind()
                ClearField()
            Else
                divError.Visible = True
            End If
        End If

    End Sub
    Private Sub ClearField()
        txtEmail.Text = ""
        txtPassword.Text = ""
        txtRetypePassword.Text = ""
        txtUserID.Text = ""
        hdnID.Value = ""
        txtUserName.Text = ""
    End Sub
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub
End Class
