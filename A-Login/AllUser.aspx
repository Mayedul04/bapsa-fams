﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllUser.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <%--<h1 class="page-title">All Users</h1>--%>
    <div class="span5" style="margin-left:0px; margin-top:5px;">
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="span12 form-top-strip">
           <p>User Add/Edit Form</p> 
        </div>
        <div class="well">
            <asp:HiddenField ID="hdnLastUpdated" runat="server" />
            <asp:HiddenField ID="hdnID" runat="server" />
            <div id="myTabContent" class="tab-content">
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Employee Name:
                        </label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtUserName" CssClass="input-xlarge" runat="server" Width="100%" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                            ControlToValidate="txtUserName" Display="Dynamic" runat="server" ErrorMessage="*"
                            ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>Login ID : </label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtUserID" CssClass="input-xlarge" runat="server" Width="100%" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                            ControlToValidate="txtUserID" Display="Dynamic" runat="server" ErrorMessage="*"
                            ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Passowrd :
                        </label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtPassword" CssClass="input-xlarge" runat="server" Width="100%" MaxLength="20" TextMode="Password" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                            ControlToValidate="txtPassword" Display="Dynamic" runat="server" ErrorMessage="*"
                            ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Retype Password :
                        </label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtRetypePassword" CssClass="input-xlarge" Width="100%" runat="server" MaxLength="20" TextMode="Password" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                            ControlToValidate="txtRetypePassword" Display="Dynamic" runat="server" ErrorMessage="*"
                            ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="RequiredFieldValidator5" ControlToValidate="txtRetypePassword" ControlToCompare="txtPassword"  runat="server" ValidationGroup="form" 
                            ErrorMessage="not Matched!" Display="Dynamic" SetFocusOnError="true">

                        </asp:CompareValidator>
                    </div>
                </div>
                <div class="span12">
                    <div class="left_div">
                        <label>
                            Email :
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                            ValidationGroup="form" SetFocusOnError="True"
                            runat="server" ErrorMessage="* Invalid"
                            ControlToValidate="txtEmail"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </label>
                    </div>
                    <div class="right_div">
                        <asp:TextBox ID="txtEmail" CssClass="input-xlarge" Width="100%" runat="server" MaxLength="200"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                            ControlToValidate="txtEmail" Display="Dynamic" runat="server" ErrorMessage="*"
                            ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        
                    </div>
                </div>
                <div class="span12">
                    <asp:HiddenField ID="hdnRole" Value="admin" runat="server" />
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                    </div>
                    <div class="span4" style="width:33%">
                        <button runat="server" id="btnClear"  class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                    </div>
                </div>
            </div>
        </div>
        

        <asp:SqlDataSource ID="sdsHTML" runat="server"
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="">
            
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="sdsAdminPanelLogin" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                    DeleteCommand="DELETE FROM [AdminPanelLogin] WHERE [UserID] = @UserID" 
                    InsertCommand="INSERT INTO [AdminPanelLogin] ([PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (@PS_1, @UN_1, @Title, @Email, @Role, @CreationDate, @CreatedBy, @Status)" 
                    UpdateCommand="UPDATE [AdminPanelLogin] SET [PS_1] = @PS_1, [UN_1] = @UN_1, [Title] = @Title, [Email] = @Email WHERE [UserID] = @UserID">
                    <DeleteParameters>
                        <asp:Parameter Name="UserID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="txtPassword" Name="PS_1" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtUserID" Name="UN_1" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtUserName" Name="Title" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnRole" Name="Role" PropertyName="Value" 
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnLastUpdated" DefaultValue="" 
                            Name="CreationDate" PropertyName="Value" Type="DateTime" />
                        <asp:CookieParameter CookieName="UserName" Name="CreatedBy" Type="String" />
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="txtPassword" Name="PS_1" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtUserID" Name="UN_1" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtUserName" Name="Title" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                            Type="String" />
                       
                        <asp:ControlParameter ControlID="hdnID" Name="UserID" PropertyName="Value" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>

        <asp:SqlDataSource ID="sdsPermissions" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
            InsertCommand="INSERT INTO [UserPermissions] ([UserID], [SectID], [Addition], [Edition], [Deletion], [Explor], [SectPermission]) VALUES (@UserID, @SectID, @Addition, @Edition, @Deletion, @Explor, @SectPermission)">
            
            <InsertParameters>
                <asp:Parameter Name="UserID" Type="Int32" />
                <asp:Parameter Name="SectID" Type="Int32" />
                <asp:Parameter DefaultValue="False" Name="Addition" Type="Boolean" />
                <asp:Parameter DefaultValue="False" Name="Edition" Type="Boolean" />
                <asp:Parameter DefaultValue="False" Name="Deletion" Type="Boolean" />
                <asp:Parameter DefaultValue="True" Name="Explor" Type="Boolean" />
                <asp:Parameter DefaultValue="False" Name="SectPermission" Type="Boolean" />
            </InsertParameters>
            
        </asp:SqlDataSource>

    </div>
    <div class="span7">
        <div class="span12 form-top-strip">
           <p>User List</p> 
        </div>
        <div class="well" style="height:208px;">
            <table class="table">
                <thead>
                    <tr>
                        
                        

                        <th style="width:70px;">Name</th>

                        <th style="width:120px;">Login ID</th>

                        <th style="width:150px;">Manage Permissions</th>

                        <th style="width: 40px;"></th>
                    </tr>
                </thead>
                <tbody>

                    <asp:ListView ID="GridView1" runat="server" style="height:160px; overflow:auto;"  DataKeyNames="UserID"
                        DataSourceID="sdsHTML">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>No data was returned.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">

                               <%-- <td>
                                    <asp:Label ID="UserIDLabel" runat="server" Text='<%# Eval("UserID") %>' />
                                </td>--%>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server"
                                        Text='<%# Eval("Title") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="lblUID" runat="server"
                                        Text='<%# Eval("UN_1") %>' />
                                </td>

                               <%-- <td>
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                                </td>--%>

                                <td>
                                    <a href="permissions/<%# Eval("UserID") %>">Permissions</a>
                                </td>

                                <td>
                                    <asp:LinkButton ID="cmdEdit" CssClass="btn btn-success" CommandName="Edit" runat="server"
                                        CommandArgument='<%# Eval("UserID") %>' Text="Edit" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                        </LayoutTemplate>

                    </asp:ListView>

                </tbody>
            </table>


        </div>
    </div>

</asp:Content>

