﻿
Imports System.Data.SqlClient

Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page

    Public domainName As String
    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If

        End If
        LoadContent(RouteData.Values("uid"))
        If (HasPermission()) = False Then
            Response.Redirect(domainName & "users")
        End If
    End Sub
    Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles ListView1.ItemCommand
        For Each item In ListView1.Items

            Dim chkBox = DirectCast(item.FindControl("select"), CheckBox)
            Dim add = DirectCast(item.FindControl("Add"), CheckBox)
            Dim edit = DirectCast(item.FindControl("Edit"), CheckBox)
            Dim delete = DirectCast(item.FindControl("Delete"), CheckBox)
            If (chkBox.Checked = True) Then
                chkBox.Checked = False
            End If
            If (add.Checked = True) Then
                add.Checked = False
            End If
            If (edit.Checked = True) Then
                edit.Checked = False
            End If
            If (delete.Checked = True) Then
                delete.Checked = False
            End If
        Next

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick

        Try
            Dim sConn As String
            sConn = ConfigurationManager.ConnectionStrings("ConnectionString").ToString
            Dim cn As SqlConnection = New SqlConnection(sConn)
            cn.Open()

            Dim insertCount = 0
            For Each item In ListView1.Items
                Dim selectString As String = "Update  UserPermissions set SectPermission=@SectPermission,Addition=@Addition,Edition=@Edition,Deletion=@Deletion,Explor=@Explor where PerID=@PerID"
                Dim cmd As SqlCommand = New SqlCommand(selectString, cn)
                cmd.Parameters.Add("PerID", Data.SqlDbType.Int, 32)
                cmd.Parameters.Add("SectPermission", Data.SqlDbType.Bit)
                cmd.Parameters.Add("Addition", Data.SqlDbType.Bit)
                cmd.Parameters.Add("Edition", Data.SqlDbType.Bit)
                cmd.Parameters.Add("Deletion", Data.SqlDbType.Bit)
                cmd.Parameters.Add("Explor", Data.SqlDbType.Bit).Value = True

                Dim chkBox = DirectCast(item.FindControl("select"), CheckBox)
                Dim add = DirectCast(item.FindControl("Add"), CheckBox)
                Dim edit = DirectCast(item.FindControl("Edit"), CheckBox)
                Dim delete = DirectCast(item.FindControl("Delete"), CheckBox)

                Dim PerID As String = DirectCast(item.FindControl("hdnPerID"), HiddenField).Value
                cmd.Parameters("Addition").Value = add.Checked
                cmd.Parameters("Edition").Value = edit.Checked
                cmd.Parameters("Deletion").Value = delete.Checked
                cmd.Parameters("SectPermission").Value = chkBox.Checked
                cmd.Parameters("PerID").Value = PerID
                'divSuccess.Visible = True

                If cmd.ExecuteNonQuery() > 0 Then
                    insertCount += 1
                    divSuccess.Visible = True
                    ' ListView1.DataBind()

                End If

            Next
            If (insertCount > 0) Then

                divSuccess.Visible = True
            End If

        Catch ex As Data.SqlClient.SqlException
            lblErrMessage.Text = "Error: " & ex.Message
            divError.Visible = True
        End Try


    End Sub
    Private Function HasPermission() As Boolean
        If (hdnUserName.Value = Request.Cookies("userName").Value Or Request.Cookies("userName").Value = "admin") Then
            Return True
        Else
            Return False
        End If
        'Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        'conn.Open()
        'Dim selectString = "SELECT * FROM  UserPermissions where UserID=@UID"
        'Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        'cmd.Parameters.Add("UID", Data.SqlDbType.Int)
        'cmd.Parameters("UID").Value = userid
        'Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        'If reader.HasRows Then

        'End If
        'conn.Close()
    End Function
    Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.ServerClick
        Response.Redirect(domainName & "users")
    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  [PS_1], [UN_1], UserID, [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]  FROM   AdminPanelLogin where UserID=@UID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("UID", Data.SqlDbType.Int)
        cmd.Parameters("UID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("UserID").ToString()
            hdnUserName.Value = reader("UN_1").ToString()
        Else
            conn.Close()
        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub

End Class
