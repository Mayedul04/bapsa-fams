﻿Imports System.Data.SqlClient
Partial Class Admin_A_Login_NewUser
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnLastUpdated.Value = DateTime.Now
        lblMessage.Text = ""
        txtEmail.Text = txtEmail.Text.Trim()
        txtUserID.Text = txtUserID.Text.Trim()
        txtUserName.Text = txtUserName.Text.Trim()

        If txtEmail.Text = "" Then
            lblMessage.Text = "Email is empty"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        ElseIf Not New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*").IsMatch(txtEmail.Text) Then
            lblMessage.Text = "Email is invalid"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        If txtPassword.Text <> "" Then
            If txtPassword.Text <> txtRetypePassword.Text Then
                lblMessage.Text = "Password not matched"
                lblMessage.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
        Else
            lblMessage.Text = "Please enter password"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If
        If IsIDExists(txtUserID.Text) Then
            lblMessage.Text = "The ID is already exists. Please try another ID."
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If
        If IsEmailExists(txtEmail.Text) Then
            lblMessage.Text = "The email is already exists. Please try another email."
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        Try
            sdsAdminPanelLogin.Insert()
            'lblMessage.Text = "New user is created"
            'lblMessage.ForeColor = Drawing.Color.Green
            Response.Redirect("../A-Login/AllUser.aspx")
        Catch ex As Data.SqlClient.SqlException
            lblMessage.Text = "Error: " & ex.Message
            lblMessage.ForeColor = Drawing.Color.Red
        End Try


    End Sub

    Private Function IsEmailExists(ByVal email As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where email=@email "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("email", Data.SqlDbType.VarChar, 100).Value = email
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function
    Private Function IsIDExists(ByVal UN_1 As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where UN_1=@UN_1 "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("UN_1", Data.SqlDbType.VarChar, 100).Value = UN_1
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function



End Class
