﻿
Partial Class Admin_MasterPage_signout
    Inherits System.Web.UI.Page
    Public domainName As String
    Private Sub MasterPage_Init(sender As Object, e As EventArgs) Handles Me.Init
        domainName = ConfigurationManager.AppSettings("RedirectUrl").ToString

    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Response.Cookies("userName").Value = ""
        Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
        Response.Cookies("userFullName").Value = ""
        Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
        Response.Cookies("userpass").Value = ""
        Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)
        Context.Items.Remove("userpass")
        Context.Items.Remove("UserID")
        Response.Redirect(domainName)
    End Sub
End Class
