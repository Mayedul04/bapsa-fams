﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false" CodeFile="Permissions.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">Manage Permissions</h1>
    <div class="span6" style="margin-left: 0px;">
        <div class="span4" style="width:50%">
            <button runat="server" id="btnBack" class="btn btn-primary"><i class="icon-backward"></i>&nbsp;Back</button>

        </div>
        <div class="span4" style="width:50%">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Set</button>
        </div>
        <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
    

    <asp:HiddenField ID="hdnID" runat="server" />
<asp:HiddenField ID="hdnUserName" runat="server" />
    
        <div class="span12 form-top-strip">
            <p>Manage the permissions</p>
        </div>
        <div class="span12">
        <div class="well" style="margin-top: 0px;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Select</th>
                            <th>Section</th>

                            <th>Add</th>

                            <th>Edit</th>

                            <th>Delete</th>
                            <th>View</th>

                        </tr>
                    </thead>
                    <tbody>

                        <asp:ListView ID="ListView1" runat="server" DataKeyNames="PerID"
                            DataSourceID="sdsExistingPermission">
                            <EmptyDataTemplate>
                                <table id="Table1" runat="server" style="">
                                    <tr>
                                        <td>No data was returned.</td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <ItemTemplate>
                                <tr style="">

                                    <td>
                                        <asp:CheckBox ID="select" runat="server" Checked='<%# Eval("SectPermission") %>' AutoPostBack="False" />
                                        <asp:HiddenField runat="server" ID="hdnSectID" Value='<%# Eval("SectID") %>' />
                                        <asp:HiddenField runat="server" ID="hdnPerID" Value='<%# Eval("PerID") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="TitleLabel" runat="server"
                                            Text='<%# Eval("SectionName") %>' />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="Add" runat="server" Checked='<%# Eval("Addition") %>' />
                                    </td>

                                    <td>
                                        <asp:CheckBox ID="Edit" runat="server" Checked='<%# Eval("Edition") %>' />
                                    </td>

                                    <td>
                                        <asp:CheckBox ID="Delete" runat="server" Checked='<%# Eval("Deletion") %>' />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="View" runat="server" Enabled="false" Checked='<%# Eval("Explor") %>' />
                                    </td>

                                </tr>
                            </ItemTemplate>
                            <LayoutTemplate>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                            </LayoutTemplate>

                        </asp:ListView>

                    </tbody>
                </table>


            </div>
         </div>   
        
    </div>


    <asp:SqlDataSource ID="sdsPermissions" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        SelectCommand="SELECT [SectionName], [SectID] FROM [UserSections] WHERE ([Status] = @Status)">
        <SelectParameters>
            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
        </SelectParameters>

    </asp:SqlDataSource>
    <asp:SqlDataSource ID="sdsExistingPermission" runat="server"
        ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        SelectCommand="SELECT UserPermissions.*, SectionName FROM [UserPermissions] inner join UserSections on UserSections.SectID=UserPermissions.SectID WHERE ([UserID] = @UserID)">
        <SelectParameters>
            <asp:RouteParameter DefaultValue="" Name="UserID" RouteKey="uid" />
        </SelectParameters>

    </asp:SqlDataSource>



</asp:Content>

