﻿
Partial Class Admin_A_HTML_AllHTML
    Inherits System.Web.UI.Page


    Protected Sub btnSubmit_ServerClick(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        If (hdnID.Value <> "") Then
            If sdsSup.Update() > 0 Then
                divSuccess.Visible = True
                'GridView1.DataBind()
                ClearField()
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
            Else
                divError.Visible = True
            End If

        Else
            If sdsSup.Insert() > 0 Then
                divSuccess.Visible = True
                'GridView1.DataBind()
                ClearField()
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Save"
            Else
                divError.Visible = True
            End If

        End If
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = True Then
            If (divSuccess.Visible = True) Then
                divSuccess.Visible = False
            End If
            If (divError.Visible = True) Then
                divError.Visible = False
            End If
            If (divDelete.Visible = True) Then
                divDelete.Visible = False
            End If
        End If

    End Sub
    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  SupID, Supplier, Email, Address, ContactName, ContactNo,Mobile ,Status FROM   Supplier where SupID=@SupID "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("SupID", Data.SqlDbType.Int)
        cmd.Parameters("SupID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            hdnID.Value = reader("SupID").ToString()
            txtTitle.Text = reader("Supplier").ToString()
            txtEmail.Text = reader("Email").ToString()

            txtDetails.Text = reader("Address").ToString()

            txtContactPerson.Text = reader("ContactName").ToString()
            txtPhone.Text = reader("ContactNo").ToString()
            txtMobile.Text = reader("Mobile").ToString()
            chkStatus.Checked = reader("Status").ToString()
        Else
            conn.Close()

        End If
        conn.Close()
        btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
    End Sub
    Private Sub DeleteRecordByID(id As Integer)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim categorycount As Integer = 0
        Dim assetcount As Integer = 0

        Dim selectString1 = "SELECT  Count(ItemID)  FROM   Assets where Supplier=@Supplier "
        Dim cmd1 As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString1, conn)
        cmd1.Parameters.Add("Supplier", Data.SqlDbType.Int)
        cmd1.Parameters("Supplier").Value = id
        assetcount = cmd1.ExecuteScalar()

        If (categorycount > 0 Or assetcount > 0) Then
            divDelete.Visible = True
        Else
            If sdsSup.Delete() > 0 Then
                divSuccess.Visible = True
                hdnID.Value = ""
                'GridView1.DataBind()
            Else
                divError.Visible = True
            End If

        End If
        conn.Close()
    End Sub
    'Protected Sub GridView1_ItemCommand(sender As Object, e As ListViewCommandEventArgs) Handles GridView1.ItemCommand
    '    If (e.CommandName = "Edit") Then


    '        Dim categoryID As Integer = Integer.Parse(e.CommandArgument)
    '        LoadContent(categoryID)

    '    End If
    'End Sub
    Protected Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.ServerClick
        If (hdnID.Value <> 0) Then
            DeleteRecordByID(hdnID.Value)
            ClearField()
        End If

    End Sub
    Private Function ClearField()
        txtTitle.Text = ""
        txtDetails.Text = ""
        txtContactPerson.Text = ""
        txtEmail.Text = ""
        txtMobile.Text = ""
        txtPhone.Text = ""
        chkStatus.Checked = True
        hdnID.Value = ""
    End Function
    Protected Sub btnClear_Click(sender As Object, e As System.EventArgs) Handles btnClear.ServerClick
        ClearField()
    End Sub
End Class
