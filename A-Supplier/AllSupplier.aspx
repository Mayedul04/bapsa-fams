﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="AllSupplier.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<h1 class="page-title">Suppliers</h1>--%>
    <div class="span5" style="margin-left:0px; margin-top:5px;">
        <div class="btn-group">
            <%--<script type="text/javascript" language="javascript">
                function OnContactSelected(source, eventArgs) {
                    var hdnValueID = "<%= txtAutoSearch.ClientID %>";
                                        document.getElementById("<%= txtAutoSearch.ClientID %>").value = eventArgs.get_value();

                                    }
            </script>
            <label>Search by Supplier Name</label>
            <asp:TextBox ID="txtAutoSearch" runat="server" CssClass="input-large" onfocus="if(this.value=='')this.value='';"
                onblur="if(this.value=='')this.value='';" AutoCompleteType="Search" AutoPostBack="True"></asp:TextBox>
            <cc1:AutoCompleteExtender ID="txtAutoSearch_AutoCompleteExtender" runat="server" CompletionInterval="100"
                MinimumPrefixLength="1" CompletionSetCount="10" ServiceMethod="GetSupplierList"
                ServicePath="AutoComplete.asmx" TargetControlID="txtAutoSearch" CompletionListHighlightedItemCssClass="hoverautocompletelistitemsmall"
                CompletionListCssClass="Autocompletelistsmall" CompletionListItemCssClass="autocompletelistitemsmall"
                OnClientItemSelected="OnContactSelected">
            </cc1:AutoCompleteExtender>--%>
            
        </div>
        <div class="success-details" visible="false" id="divSuccess" runat="server">
            <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
            <div class="corners">
                <span class="success-left-top"></span><span class="success-right-top"></span><span
                    class="success-left-bot"></span><span class="success-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divError" visible="false" runat="server">
            <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="error-details" id="divDelete" visible="false" runat="server">
            <asp:Label ID="Label1" runat="server" Text="This Supplier have Assets. Please Delete the Depedencies first"></asp:Label>
            <div class="corners">
                <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
            </div>
        </div>
        <div class="span12 form-top-strip">
           <p>Suppliers</p> 
        </div>
            <div class="well">
                <div id="myTabContent" class="tab-content">
                    <div class="span12">
                    <div class="left_div">
                        Search Supplier
                    </div>
                    <div class="right_div">
                        <asp:DropDownList ID="ddlSupplier" runat="server" Width="100%" AutoPostBack="true" AppendDataBoundItems="true" DataSourceID="sdsSuppl" DataTextField="Supplier" DataValueField="SupID">
                <asp:ListItem Value="">Select Supplier</asp:ListItem>

            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsSuppl" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                SelectCommand="SELECT [SupID], [Supplier] FROM [Supplier] where SupID>1"></asp:SqlDataSource>
                    </div>
                </div>
                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Supplier Name :</label>
                        </div>
                        <div class="right_div">
                            <asp:HiddenField ID="hdnID" runat="server" Value="" />
                            <asp:TextBox ID="txtTitle" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtTitle" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Contact Person :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtContactPerson" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtContactPerson" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Email Address :
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                            runat="server" ErrorMessage="*" ValidationGroup="form"
                            ControlToValidate="txtEmail" SetFocusOnError="True"
                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </label>
                        </div>
                        <div class="right_div">
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtEmail" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>

                        </div>
                    </div>
                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Contact Number:
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="form" SetFocusOnError="True"
                            runat="server" Display="Dynamic" ErrorMessage="Phone" ControlToValidate="txtPhone" ValidationExpression="^(?:\+?(?:88|01)?)(?:\d{11}|\d{13})$"></asp:RegularExpressionValidator>
                            </label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtPhone" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtPhone" runat="server" ErrorMessage="*" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Mobile :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtMobile" runat="server" CssClass="input-large" Width="100%" MaxLength="400"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="form" SetFocusOnError="True"
                                runat="server" Display="Dynamic" ErrorMessage="Phone" ControlToValidate="txtMobile" ValidationExpression="^(?:\+?(?:88|01)?)(?:\d{11}|\d{13})$"></asp:RegularExpressionValidator>
                        </div>
                    </div>

                    <div class="span12">
                        <div class="left_div">
                            <label>
                                Address :</label>
                        </div>
                        <div class="right_div">

                            <asp:TextBox ID="txtDetails" Height="50" runat="server" TextMode="MultiLine" CssClass="input-large"
                                Width="100%" Rows="4"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic"
                                runat="server" ControlToValidate="txtDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                                ErrorMessage="*" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="left_div">
                        </div>
                        <div class="right_div">
                            <asp:CheckBox ID="chkStatus" runat="server" Text="Active" TextAlign="Right" />

                        </div>
                    </div>


                    <div class="span12">
                        <div class="span4" style="width:33%">
                            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>&nbsp;Save</button>
                        </div>
                        <div class="span4" style="width:33%">
                            <button runat="server" id="btnDelete" validationgroup="form" class="btn btn-primary"><i class="icon-trash"></i>&nbsp;Delete</button>
                        </div>
                        <div class="span4" style="width:33%">
                            <button runat="server" id="btnClear" class="btn btn-primary"><i class="icon-refresh"></i>&nbsp;Clear</button>
                        </div>
                    </div>

                </div>
                <asp:SqlDataSource ID="sdsSup" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    DeleteCommand="DELETE FROM [Supplier] WHERE [SupID] = @SupID"
                    InsertCommand="INSERT INTO [Supplier] ([Supplier], [Address], [ContactName], [ContactNo], [Mobile], [Email],[Status]) VALUES (@Supplier, @Address, @ContactName, @ContactNo, @Mobile, @Email,@Status)"
                    SelectCommand="SELECT * FROM [Supplier] where SupID>1"
                    UpdateCommand="UPDATE [Supplier] SET [Supplier] = @Supplier, [Address] = @Address, [ContactName] = @ContactName, [ContactNo] = @ContactNo, [Mobile] = @Mobile, [Email] = @Email, [Status]=@Status WHERE [SupID] = @SupID">
                    <DeleteParameters>
                        <asp:ControlParameter ControlID="hdnID" Name="SupID" PropertyName="Value" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="Supplier" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtContactPerson" Name="ContactName" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="Supplier" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="Address" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtContactPerson" Name="ContactName" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtPhone" Name="ContactNo" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtMobile" Name="Mobile" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" />
                        <asp:ControlParameter ControlID="hdnID" Name="SupID" PropertyName="Value" Type="Int32" />
                    </UpdateParameters>

                </asp:SqlDataSource>

           
        </div>
        
    </div>
    <!-- Eof content -->
    <div class="span7">
        <div class="span12 form-top-strip" style="margin-top:3px;width:100%">
           <p>Suppliers</p> 
        </div>
        <div class="well" style="height:354px; overflow-y:scroll; padding-top:0px; display:block;">
            <table class="table">
                <thead>
                    <tr>
                       
                        <th>Supplier
                        </th>
                        <th>Contact Person
                        </th>
                        <%--<th>Email
                        </th>--%>
                        <th>Contact Number
                        </th>


                        <th style="width: 40px;"></th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="SupID" DataSourceID="sdsSup">
                        <EmptyDataTemplate>
                            <table runat="server" style="">
                                <tr>
                                    <td>No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <%--<td>
                                    <asp:Label ID="ID" runat="server" Text='<%# Eval("SupID") %>' />
                                </td>--%>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Supplier") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="Parent" runat="server" Text='<%# Eval("ContactName") %>' />
                                </td>
                                <%--<td>
                                    <asp:Label ID="Code" runat="server" Text='<%# Eval("Email") %>' />
                                </td>--%>

                                <td>
                                    <asp:Label ID="Desc" runat="server" Text='<%# Eval("ContactNo") %>' />
                                </td>
                                <td>
                                    <asp:LinkButton ID="cmdEdit" CssClass="btn btn-success" CommandName="Edit" runat="server"
                                        CommandArgument='<%# Eval("SupID") %>' Text="Edit" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
